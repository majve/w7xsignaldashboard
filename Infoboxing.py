import logging
import numpy as np
log = logging.getLogger("Infoboxing")

def Divertorgasinfo(signallist, dims, vals, oks, settings={}):
    gastypesig = signallist[0]
    gastypenum = vals[gastypesig][0]

    gaslist = {0: 'valve off',
               1: 'no bottle',
               2: 'undefined',
               3: 'evacuated',
               4: 'res1',
               5: 'res2',
               6: 'B2H6',
               7: 'SiH4',
               8: 'He',
               9: 'D2',
               10: 'H2',
               11: 'N2',
               12: 'Ne',
               13: 'Ar',
               14: 'Kr',
               15: 'CH4',
               16: 'C2H6',
               17: 'CO',
               18: '14N',
               19: '13CH4',
               20: '12CH4',
               21: '13CH4',
               22: '15N',
               23: 'Xe',
               24: 'Mix1 (H/He)',
               25: '3He',
               }
    gas_type = gaslist[gastypenum]

    # Get the pressure at the time that is closest to t1
    boxPsig = signallist[1]
    t1 = getSetting(settings, 'trel')
    index = np.argmin(np.abs(dims[boxPsig] - t1))
    gas_pressure = vals[boxPsig][index]

    module = getSetting(settings, 'module')

    gas_info = f'{module}: {gas_type} at {gas_pressure:.0f} mbar'

    return gas_info

def Maingasinfo(signallist, dims, vals, oks, settings={}):
    gastypesig = signallist[0]
    gastypenum = vals[gastypesig][0]

    gaslist = {0: 'valve off',
               1: 'no bottle',
               2: 'undefined',
               3: 'evacuated',
               4: 'res1',
               5: 'res2',
               6: 'B2H6',
               7: 'SiH4',
               8: 'He',
               9: 'D2',
               10: 'H2',
               11: 'N2',
               12: 'Ne',
               13: 'Ar',
               14: 'Kr',
               15: 'CH4',
               16: 'C2H6',
               17: 'CO',
               18: '14N',
               19: '13CH4',
               20: '12CH4',
               21: '13CH4',
               22: '15N',
               23: 'Xe',
               24: 'Mix1 (H/He)',
               25: '3He',
               }
    gas_type = gaslist[gastypenum]

    gas_info = f'{gas_type}'

    return gas_info


def AllMainGasinfo(signallist, dims, vals, oks, settings={}):
    info = ""

    # Iterate through the first half of the signallist (valve voltages)
    for i in range(11):
        if oks[signallist[i]] and oks[signallist[i+11]]:
            # Get the voltage values for the current valve
            valve_voltages = vals[signallist[i]]
            # Check if any voltage is greater than 10
            if np.any(valve_voltages > 10):
                # Get the valve info for the current valve
                valve_info = Maingasinfo([signallist[i+11]], {signallist[i+11]: dims[signallist[i+11]]}, {signallist[i+11]: vals[signallist[i+11]]}, {signallist[i+11]: oks[signallist[i+11]]})
                if not info:
                    info = f'valve {i}: {valve_info}'
                else:
                    info = info + f'\nvalve {i}: {valve_info}'

    return info

def AllDivertorGasinfo(signallist, dims, vals, oks, settings={}):
    info = ""
    valves = ['H10', 'H11', 'H20', 'H21', 'H30 valve1', 'H30 valve2', 'H30 valve3', 'H30 valve4', 'H30 valve5',
              'H31', 'H40', 'H41', 'H50 valve1', 'H50 valve2', 'H50 valve3', 'H50 valve4', 'H50 valve5',
              'H51 valve1', 'H51 valve2', 'H51 valve3', 'H51 valve4', 'H51 valve5']
    # Iterate through the first half of the signallist (valve voltages)
    for i in range(22):
        if oks[signallist[i]] and oks[signallist[i+22]]:
            # Get the voltage values for the current valve
            valve_voltages = vals[signallist[i]]
            # Check if any voltage is greater than 10
            if np.any(valve_voltages > 10):
                # Get the valve info for the current valve
                # Using main gas info because ignoring the pressure
                valve_info = Maingasinfo([signallist[i+22]], {signallist[i+22]: dims[signallist[i+22]]}, {signallist[i+22]: vals[signallist[i+22]]}, {signallist[i+22]: oks[signallist[i+22]]})
                if not info:
                    info = f'{valves[i]}: {valve_info}'
                else:
                    info = info + f'\n{valves[i]}: {valve_info}'
    return info

def getSetting(settings, key):
    """ Get the value corresponding to key from the settings dictionary. Apply necessary checks

        Parameters
        ----------
        settings : dict
            Names and files of the settings
        key : string
            name of the setting

        Examples
        --------
        >>> getSetting({'ICRHfreq': 375, 'coiltype' : 'CC'}, 'ICRHfreq')
        375
        """
    if key in settings.keys():
        return settings[key]
    else:
        log.warning('{} not provided in settings for calibration. Aborting.'.format(key))
        raise Exception