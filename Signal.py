
class Signal:

    def __init__(self, signalname, dimensions=None, values=[], unit="[]", legend="", ok=False, signal_type=""):
        self.signalname = signalname  # Name as can be found in the signal dicts
        self.dimensions = dimensions  # numpy array of dtype _np.int64
        self.values = values  # numpy array of dtype
        self.unit = unit  # Unit as a string with brackets
        self.legend = legend  # Legend string
        self.ok = ok  # Boolean indicating if signal was read successfully
        self.signal_type = signal_type  # String: timetrace, profile, video, or image

    def __repr__(self):
        return (f"SignalData(signal_name={self.signalname}, "
                f"dimensions={self.dimensions}, signal_values={self.values}, "
                f"unit='{self.unit}', legend='{self.legend}', ok={self.ok}, "
                f"signal_type='{self.signal_type}')")
