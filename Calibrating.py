import numpy as np  # np.loadtxt, np.power, np.sqrt, np.zeros, np.dtype, np.arange, nanmean
import os
import logging
from w7xarchive import parameters  # Calibration functions can read their required parameters that are stored on ArchiveDB as PARLOGs, using 7xarchive.parameters. PARLOG urls can't be added to the signal_dicts since the PARLOGs don't follow a fixed structure

log = logging.getLogger("Calibrating")

calibrationpath = os.path.dirname(__file__)
# TODO: vals is an array so is it necessary to loop over the elements? Or can you just do e.g. 13*val+5. Sometimes arrays have to be multiplied element wise

"""This file contains the calibration functions that have to be called for specific signals, according to the specifications in archive_signal_dict a.o.
The calibration functions follow a strict template, because the Reader will give a generic call to a calibration function. 
The calibration functions need to have as input parameters val_in and settings. The return value must be val.

Parameters
----------
val_in : numpy array of np.dtype 
    values that have to be calibrated
    
settings : dictionary
    Convenient way of providing different kinds of settings for the different calibration functions.
    The settings can be tailor made to the specific calibration function you need
    The helper function getSetting() [see bottom] is used to extra the desired setting from the dict

Return
------
val : numpy array of np.dtype
    values after calibration
    
Note
----
Necessary calibration files are located in w7xsignalviewer/Calibrationfiles/
"""


def voltage_to_amp0_DRC1(val_in, settings={}):
    """ settings = {'ICRHfreq' : int}
    """
    # DRC1 uses a different calibration file as DRC3
    calibrationfile = 'Calibrationfiles/DET1_CAL_AMP_{}_V1.txt'.format(getSetting(settings, 'ICRHfreq'))
    filename = os.path.join(calibrationpath, calibrationfile)
    return voltage_to_amp_from_table0(val_in, filename, settings={})


def voltage_to_amp0_DRC3(val_in, settings={}):
    """ settings = {'ICRHfreq' : int}
    """
    calibrationfile = 'Calibrationfiles/DET2_CAL_AMP_{}_V1.txt'.format(getSetting(settings, 'ICRHfreq'))
    filename = os.path.join(calibrationpath, calibrationfile)
    return voltage_to_amp_from_table0(val_in, filename, settings={})


# Different input parameters since it is only called by other calibration functions
def voltage_to_amp_from_table0(val_in, filename, settings={}):
    cal_vals = np.loadtxt(filename, comments='#', delimiter='\t')
    val = np.zeros(val_in.shape)
    for i in range(len(val_in)):
        P = cal_vals[0, 1] * val_in[i] + cal_vals[0, 2]
        val[i] = dBm_to_V(P)
    return val

#####################################################################"


def voltage_to_amp1_DRC1(val_in, settings={}):
    """ settings = {'ICRHfreq' : int}
    """
    # DRC1 uses a different calibration file as DRC3
    calibrationfile = 'Calibrationfiles/DET1_CAL_AMP_{}_V1.txt'.format(getSetting(settings, 'ICRHfreq'))
    filename = os.path.join(calibrationpath, calibrationfile)

    return voltage_to_amp_from_table1(val_in, filename, settings={})


def voltage_to_amp1_DRC3(val_in, settings={}):
    """ settings = {'ICRHfreq' : int}
    """
    calibrationfile = 'Calibrationfiles/DET2_CAL_AMP_{}_V1.txt'.format(getSetting(settings, 'ICRHfreq'))
    filename = os.path.join(calibrationpath, calibrationfile)

    return voltage_to_amp_from_table1(val_in, filename, settings={})


# Different input parameters since it is only called by other calibration functions
def voltage_to_amp_from_table1(val_in, filename, settings={}):
    cal_vals = np.loadtxt(filename, comments='#', delimiter='\t')
    val = np.zeros(val_in.shape)
    for i in range(len(val_in)):
        P = cal_vals[1, 1] * val_in[i] + cal_vals[1, 2]
        val[i] = dBm_to_V(P)
    return val


def dBm_to_V(P_dBm, Z0L=50.):
    P_W = 0.001 * np.power(10, P_dBm / 10.)
    V = np.sqrt(2. * Z0L * P_W)
    return V


def squared_voltage_to_offset(val_in, settings={}):
    """ settings = {'signal' : string}
        See Calibrationfiles/Gen_CAL_OFFSET for the signals
    """
    signal = getSetting(settings, 'signal')

    calibrationfile = 'Calibrationfiles/GEN_CAL_OFFSET.txt'
    filename = os.path.join(calibrationpath, calibrationfile)

    datatype = np.dtype([('signal', 'U25'), ('Voffset', float), ('K', float)])
    cal_vals = np.loadtxt(filename, comments='#', dtype=datatype, delimiter='\t', usecols=[0, 1, 2])

    val = np.zeros(val_in.shape)
    for i in range(len(cal_vals['signal'])):
        if signal == cal_vals['signal'][i]:
            for j in range(len(val_in)):
                val[j] = cal_vals['K'][i]*(val_in[j] - cal_vals['Voffset'][i])**2  # K(Vin - Voffset)^2
                # print(val[j], "=", cal_vals['K'][i], "(", val_in[j], "-", cal_vals['Voffset'][i], ")^2")
            return val

    log.error('Signal {} not found in calibration file Gen_CAL_OFFSET'.format(signal))
    raise Exception


def coilcurrent(val_in, settings={}):
    """ settings = {'coiltype' : string}
        Coiltype being PC, NPC or CC
    """
    coiltype = getSetting(settings, 'coiltype')
    if coiltype == 'PC':
        windings = 36
    elif coiltype == 'NPC':
        windings = 108
    elif coiltype == 'CC':
        windings = 8

    val = val_in*windings*(-1)  # - because current different in archive and FLT
    return val


def ECE_calibration(val_in, settings={}):
    # TODO: is each channel treated separately or is the calibration intertwined?
    """
            Melina: for all channels, get_signal_box
            raw data_url_ECE    http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/ControlStation.2159/X2_DATASTREAM/8/QME-ch09/_signal.html?from=1667395495977124591&upto=1667395513077114591

        1. Conversion from bit to Volt
            Melina: for all channels (requesting channel 0 but with get_parameters_box)
            bittovolt PARLOG    http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/Minerva/Minerva.ECE.Configurationvalues/values_PARLOG/V24/parms/chanDescs/%5B0%5D/bitToVoltConversionFactor/_signal.html?from=1667347200000000000&upto=1667433599999999999

        2. Baseline correction
            raw data_url_ECE

        3. Resample (optional)

        4. Multiply with calibration factor
            for program
            amplification coeff lfs DATA       http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/ControlStation.2054/PLC.1_DATASTREAM//250/if1Attenuation/unscaled/_signal.html?from=1667395430804000000&upto=1667398629865000000
            amplification coeff hfs DATA       http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/ControlStation.2054/PLC.1_DATASTREAM/251/if2Attenuation/unscaled/_signal.html?from=1667395430804000000&upto=1667395529807000000

            for all channels, with in-line multi thread
            relcal PARLOG       http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/Minerva/Minerva.ECE.Calibrationvalues/values_PARLOG/V102/parms/chanDescs/[15]/calibrationFactor/_signal.html?from=1667347200000000000&upto=1667433599999999999
            gain DATA           http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/ControlStation.2054/PLC.1_DATASTREAM/56/Gain02/unscaled/_signal.html?from=1667395430804000000&upto=1667398629865000000

            calibration_factors_lfs = relcal[:16] * (AmplCoeff_lfs / gain[:16])
            calibration_factors_hfs = relcal[16:32] * (AmplCoeff_hfs / gain[16:32])
            calibration_factors = np.hstack((calibration_factors_lfs, calibration_factors_hfs)).reshape(32, 1, 1)

            val = val * calibration_factors
    """

    # 1. Conversion from bit to Volt
    XPid = getSetting(settings, 'XPid')
    # TODO: what is the unit of mindim and maxdim?
    mindim = getSetting(settings, 'mindim')
    maxdim = getSetting(settings, 'maxdim')
    channel = getSetting(settings, 'channel')

    if mindim is None or maxdim is None:
        # Use XPid
        bittovolt_conversionfactor = parameters.get_parameters_box_for_program('/ArchiveDB/raw/Minerva/Minerva.ECE.Configurationvalues/values_PARLOG/V0/parms/chanDescs/[' + channel + ']/bitToVoltConversionFactor', XPid, use_last_version=True, update_url_version=True)
    else:
        bittovolt_conversionfactor = parameters.get_parameters_box('/ArchiveDB/raw/Minerva/Minerva.ECE.Configurationvalues/values_PARLOG/V0/parms/chanDescs/[' + channel + ']/bitToVoltConversionFactor', mindim, maxdim, use_last_version=True, update_url_version=True)
    bittovolt = bittovolt_conversionfactor['values'][0]

    # 2. Baseline correction
    # TODO: does the baseline change with each shot or can it be calculated once and then set for the whole OP?

    # Option 1)
    # Use constant finder. Find the first plateau and consider this as baseline
    # drawback: dependent of threshold and timewindow
    # dim = np.arange(len(val))  # dim is given in samples
    # start_times, start_values, end_times, end_values, moving_avg = Plotter.constantfinder(dim, val, threshold=0.1, timewindow=10)  # timewindow is 10 samples
    # baseline_average_ECE = np.nanmean(values[(dims >= start_times[0]) & (dims <= end_times[0])])

    # Option 2)
    # Use constant finder. Use the first 100 samples
    # Drawback: then you can as well use mean
    # dim = np.arange(100)  # dim is given in samples
    # start_times, start_values, end_times, end_values, moving_avg = Plotter.constantfinder(dim, val[:100], threshold=1, timewindow=10)
    # baseline_average_ECE = np.nanmean(values[(dims >= start_times[0]) & (dims <= end_times[0])])
    # baseline_average_ECE = np.nanmean(values[(dims >= start_times[0]) & (dims <= end_times[0])])

    # Option 3)
    # Use mean of 100 first samples
    baseline_average_ECE = np.nanmean(val_in[:100])

    # Option 4)
    # Melina
    # time_ECE_baseline_raw, data_ECE_baseline_raw = w7xarchive.get_signal_box(data_url_ECE, mindim, maxdim-50000000, channels=channels, use_cache=True) # get ECE raw data, time as absolute nanosecond timestamp
    # data_ECE_baseline = data_ECE_baseline_raw*bittovolt
    # baseline_average_ECE = np.mean(data_ECE_baseline, axis=1).reshape(32, 1, 1) # calculating of the height of the noise of the ECE data before heating
    # data_ECE_baseline_corrected = data_ECE_volt-baseline_average_ECE

    val = np.zeros(val_in.shape)
    for i in range(len(val_in)):
        val[i] = (val_in[i] - baseline_average_ECE) * bittovolt

    # 3. Resample (optional)
    # skip. Maybe using 'resolution' of GUI is sufficient

    # 4. Multiply with calibration factor
    # TODO: another signal is read. Move this Calibration to Derivating? Where data_ECE_raw passes Calibrating to do (val-baseline)*bittovolt, or where slope and intercept are usd with PARLOG (common structure necessary)
    # get the amplification coefficients for the ECE Radiometer for the low field side and high field side
    # TODO: make if lse for XPid or mindim, maxdim
    if channel < 16:
        amplf_coeff = w7xarchive.get_signal_for_program('/ArchiveDB/codac/W7X/ControlStation.2054/PLC.1_DATASTREAM//250/if1Attenuation/unscaled', XPid, use_cache=False)[1][5] * 5  # low field side, *5 is for converting the [dB] value in a factor
    else:
        ampl_coeff = w7xarchive.get_signal_for_program('/ArchiveDB/codac/W7X/ControlStation.2054/PLC.1_DATASTREAM/251/if2Attenuation/unscaled', XPid, use_cache=False)[1][5] * 2 / 3  # high field side, *(2/3) is for converting the [dB] value in a factor

    gain = w7xarchive.get_signal_for_program(f'/ArchiveDB/codac/W7X/ControlStation.2054/PLC.1_DATASTREAM/{channel+55}/Gain{channel+1:02d}/unscaled', XPid, use_cache=False)[1][5]
    relcal = parameters.get_parameters_box_for_program(f'/ArchiveDB/raw/Minerva/Minerva.ECE.Calibrationvalues/values_PARLOG/V0/parms/chanDescs/[{channel}]/calibrationFactor', XPid, use_last_version=True, update_url_version=True)

    val = val * relcal * (ampl_coeff / gain)

    return val

def faradaycup_current(val_in, settings={}):
    val = 100.0E-12 * 10 ** (np.array(val_in) - 1.0)
    return val


def Stepfunction(val_in, settings={}):
    pass
    # val = abs(np.array(val_in))
    # val[val >= 0.1] = 1.0
    # val[val < 0.1] = 0.0
    # return val


def getSetting(settings, key):
    """ Get the value corresponding to key from the settings dictionary. Apply necessary checks

        Parameters
        ----------
        settings : dict
            Names and files of the settings
        key : string
            name of the setting

        Examples
        --------
        >>> getSetting({'ICRHfreq': 375, 'coiltype' : 'CC'}, 'ICRHfreq')
        375
        """
    if key in settings.keys():
        return settings[key]
    else:
        log.warning('{} not provided in settings for calibration. Aborting.'.format(key))
        raise Exception
