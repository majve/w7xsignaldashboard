import os
import matplotlib.pyplot as plt
import h5py
import logging

log = logging.getLogger("Saver")

def checkpath(path):
    """ Check if the path exists. If not, create the path

        Parameters
        ----------
        path : string

        Returns
        -------
        Boolean
            True if path exists

        Examples
        --------
        >>> checkpath(
        "/home/IPP-HGW/majve/Dokumente/ICRH/icrhmonitor")
        """

    if os.path.exists(path):
        return True
    else:
        try:
            os.mkdir(path)
            log.info('savepath {} did not exist and was created.'.format(path))
            return True
        except Exception:
            log.error("Not able to create path, can't save output at {}".format(path))
            return False


def uniquify(path):
    """ Check if the path for a file is unique or already exists

        Parameters
        ----------
        path : string
            path for file that has to be made unique

        Returns
        -------
        path : string
            unique path for file

        Examples
        --------
        >>> uniquify(
        "../plots/2023-06-07_Generator 1_XP_20230223.76.png")

        "../plots/2023-06-07_Generator 1_XP_20230223.76 (2).png"
        """
    filename, extension = os.path.splitext(path)
    counter = 1

    while os.path.exists(path):
        path = filename + " (" + str(counter) + ")" + extension
        counter += 1

    return path


def saveFigAsPNG(pngname, savepath="./plots/"):
    """ Save the figure as a png with the given filename at the given path

        Parameters
        ----------
        pngname : string
            filename for the figure
        savepath : string
            path to folder where the figure will be stored

        Examples
        --------
        >>> saveFigAsPNG(
        "2023-06-07_Generator 1_XP_20230223.76.png")
        """

    if checkpath(savepath):
        fullpath = os.path.join(savepath, pngname + '.png')
        uniquepath = uniquify(fullpath)
        try:
            plt.savefig(uniquepath, dpi=300)
        except Exception:
            log.error("can't save figure at {}".format(uniquepath))


def saveFigsAsPDF(pdfname, savepath="./pdfs/"):
    """ Save the figure as a pdf with the given filename at the given path

        Parameters
        ----------
        pdfname : string
            filename for the figure
        savepath : string
            path to folder where the figure will be stored

        Examples
        --------
        >>> saveFigAsPNG(
        "2023-06-07_Generator 1_XP_20230223.76.pdf")
        """

    if checkpath(savepath):
        fullpath = os.path.join(savepath, pdfname + '.pdf')
        uniquepath = uniquify(fullpath)
        try:
            f = plt.gcf()  # f = figure(n) if you know the figure number
            f.set_size_inches(8.27, 11.69)
            plt.savefig(uniquepath)

        except Exception:
            log.error("can't save figure at {}.".format(uniquepath))


def saveData(signals, filename, savepath="./data/"):
    """ Save the data as a h5 file with the given filename at the given path

        Parameters
        ----------
        signals : signal_detail_dicts : dicts
            dicts where the key is the signalname and the value is respectively the dimensions, signalvalues, the unit, the legend, ok, signaltype
                    dimensions (usually times) : numpy array of dtype _np.int64
                    signalvalues : numpy array of dtype
                    unit : string that contains unit in [ ]
                    legend : string
                    ok : boolean whether signal was read succesfully
                    signaltype : string. timetrace, profile, video or image
        filename : string
            filename for the data file
        savepath : string
            path to folder where the data file will be stored

        Examples
        --------
        >>> graphs = get_signalsOfList(['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power'], 1680186447514002176, 1680186452909001984)
        saveData(graphs, "datatest")
        """

    if checkpath(savepath):
        fullpath = os.path.join(savepath, filename + '.h5')  # TODO: file type for excel
        uniquepath = uniquify(fullpath)
        with h5py.File(uniquepath, 'w') as f:
            vals, dims, units, legends, oks, types = signals
            for key in vals.keys():
                if oks[key]:
                    name = legends[key] + "[" + units[key] + "]"
                    f.create_dataset('/traces/{}/dims'.format(name), data=dims[key], compression="gzip")
                    f.create_dataset('/traces/{}/vals'.format(name), data=vals[key], compression="gzip")


def saveVideo(ani, filename, savepath="./videotracks/", fps=20, dpi=300):
    """ Save a video as an mp4 file with the given filename at the given path

        Parameters
        ----------
        ani : matplotlib ArtistAnimation
            animation video
        filename : string
            filename for the video
        savepath : string
            path to folder where the video will be stored

        Examples
        --------
        mysignal = Reader.get_signal('W7-X/Video tracks/Edicam/AEQ10', 1680186447514002176, 1680186452909001984)
        _, ani, _, _, _ = Video.make_video(mysignal, self.timeType, trel=self.t1, scale=self.yScale, title="", cmap="Greys_r", axes=False, show=self.showOutput)>>>
        saveVideo(ani, "testvideo")
        """

    if checkpath(savepath):

        fullpath = os.path.join(savepath, filename + '.mp4')
        uniquepath = uniquify(fullpath)
        try:
            ani.save(uniquepath, writer="ffmpeg", fps=fps, dpi=dpi)
        except Exception:
            log.error("can't save video at {}.".format(uniquepath))

