import Reader as Reader
import Writer as Writer
from ICRHsettings import ICRHsettings

from archive_signals.ICRH_signals import ICRH_signal_dict
from derived_signal_dict import derived_signal_dict

# Create separate mainfunction that can be run from terminal
# Then it is also possible to run it in the background
def processSignals(signalList, time_from, time_upto, settings):
    """ (1) Read all the signals from the signalList. For signals from archive_signal_dict, the urlRaw will be used and the calibration applied.
            For signals from derived_signals_dict, the input_signals will be read from urlRaw and calibrated, after which the derived signal is determined.
        (2) Write the processed signals to the designated stream in the archive DB.

        Parameters
        ----------
        signalList : list of signal names,
            where the names correspond to any name in archive_signal_dict, derived_signal_dict, w7x_profile_dict or w7xdia_signal dict
            or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'
        time_from : 64bit integer or string(?)
            The time from which valid time intervals is to be searched.
            Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
        time_upto : 64bit integer or string(?)
            The time up to which valid time intervals is to be searched.
            Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
        settings : dict
            Settings that have to be passed to the reaader (i.e. ICRHfrequency and magnetic settings)
        Examples
        --------
        >>> processSignals(['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power'], 1680186447514002176, 1680186452909001984, {'ICRHfreq':375})
        """

    # (1) Read all the signals from the signalList, using the urlRaw and applying the calibration.
    dims, vals, units, legends, oks, _ = Reader.get_signalsOfList(signalList, time_from, time_upto, resolution=0, urlRaw=True, Calibrate=True, settings=settings)

    print(oks)
    # Write the processed signals to the archive DB at the signal url
    if all(oks):
        Writer.write(signalList, dims, vals, units, oks)


def ProcessAllICRHsignals(time_from, time_upto, settings):  #, forceProcessing=False):
    """ Collect all the signals in arhive_signal_dict that have a urlRaw, meaning they can be calibrated,
        and collect also the signals in derived_signal_dict and send them to the processer,
        which will calibrate the signal, and  write the result to the designated stream in the archiveDB.

        Parameters
        ----------
        time_from : 64bit integer or string(?)
            The time from which valid time intervals is to be searched.
            Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
        time_upto : 64bit integer or string(?)
            The time up to which valid time intervals is to be searched.
            Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
        settings : dict
            Settings that have to be passed to the reaader (i.e. ICRHfrequency and magnetic settings)
        # forceProcessing : boolean
        #     Obsolete at the moment. Intendded to choose wheather all time intervals have to processed
        #     or only the ones that have not been processed yet.
        Examples
        --------
        >>> ProcessAllICRHsignals(1680186447514002176, 1680186452909001984, {'ICRHfreq':375})
        """

    my_ICRHsettings = ICRHsettings()
    OperationGen1, ICRHfreq1, OperationGen2, ICRHfreq2 = my_ICRHsettings.getICRHparameters(filter_stop=time_from)

    # For the signals from the ICRH archive
    signalList = []
    for signal, signalDict in ICRH_signal_dict.items():
        # that have to be calibrated, i.e. that have a raw url
        if "urlRaw" in signalDict.keys():
            if "Generator 1" in signal:
                if OperationGen1:
                    signalList.append(signal)
            elif "Generator 2" in signal:
                if OperationGen2:
                    signalList.append(signal)
            else:
                signalList.append(signal)

    # For the signals from the derived_signal_dict
    for signal, signalDict in derived_signal_dict.items():
        if "Calorimetry" in signal or "Antenna" in signal:
            signalList.append(signal)

    # Remove suffixes denoting different OPs
    signalList = [name.split('_OP')[0] for name in signalList]
    # Remove duplicates by converting the list to a set and back to a list
    signalList = list(set(signalList))

    # Sort to maintain consistent order (optional)
    signalList.sort()
    print(signalList)
    # send them to the processer
    processSignals(signalList, time_from, time_upto, settings)


