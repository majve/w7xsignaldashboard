import Reader as Reader
import LogbookAPI as LogbookAPI
import Calibrating as Calibrating
import w7xarchive
from numpy import loadtxt, dtype, arange, mean
import logging
import os

log = logging.getLogger("MagneticConfig")


def getMagneticConfig(XPid):
    # See coil definitions at http://webservices.ipp-hgw.mpg.de/docs/usecases.html
    # We are using ideal coil geometry (ID 160 - 230), therefore we do not use the trim coils because they are designed to counteract the small
    # deviations in the real coils from the ideal coils because of manufacturing and installation.

    # Get the log for the program from the logbook
    XPlog = LogbookAPI.read_program_log(XPid.split("_")[1])

    # Get from the XPlog the tags, to determine the configuration of the main field (e.g. EIM012+1750) and whether the control coils were used.
    # Note: the trim coils give minor corrections to the magnetic fields, that we don't have to consider (?)
    config = None
    CC = False  # TC
    for tag in XPlog['tags']:
        # Get the configuration of the mainfield
        if 'Main field' in tag.keys():
            config = tag['Main field']
        # Get whether the control coils (CC) were used
        if 'CC' in tag.keys():
            CC = True
        # Get whether the trim coils (TC) were used
        # if 'TC' in tag.keys():
        #     TC = True

    # Initialize variables
    configID = False
    coils = None
    currents = None

    # The configuration is either a number between 1 and 37: http://webservices.ipp-hgw.mpg.de/docs/fieldlinetracer.html#MagneticConfig
    if config in range(37):
        configID = config
    # or it corresponds to a configuration that is specified in a table: https://w7x-magneticfields.ipp-hgw.mpg.de/apex/r/w7x/magnetic-field-configurations/magnetic-field-configurations?session=29944347741429
    else:
        calibrationpath = os.path.dirname(__file__)
        calibrationfile = 'Calibrationfiles/MAGNETIC_CONFIGURATIONS.csv'
        filename = os.path.join(calibrationpath, calibrationfile)

        # Specify the names and datatypes of the columns that we will use from the csv file
        datatype = dtype([('ID', 'U25'), ('I1', float), ('I2', float), ('I3', float), ('I4', float), ('I5', float), ('IA', float), ('IB', float)])
        # Load the requested columns from the csv file. columns 1, 2 become list elements with index 0, 1 etc.
        config_table = loadtxt(filename, comments='#', dtype=datatype, delimiter='|', usecols=[1, 2, 3, 4, 5, 6, 7, 8])
        coils = []
        currents = []
        # Loop over the configuration IDs in the csv file
        for i in range(len(config_table['ID'])):
            # If our config corresponds to the configuration ID fom the file
            if config == config_table['ID'][i]:
                # then set the coil IDs and get the coilcurrents
                coils = arange(160, 230)  # NPC + PC
                # Read the currents of the 50 non planar coils (NPC) from that same row of the file
                currents = [config_table['I1'][i], config_table['I2'][i], config_table['I3'][i], config_table['I4'][i], config_table['I5'][i]] * 10
                # Calibrate the NPC current
                # For calibration, it may be better to convert to ndarray and back to list
                for i in range(len(currents)):
                    currents[i] = Calibrating.coilcurrent(currents[i], settings={'coiltype': 'NPC'})
                # Add the calibrated currents of the 20 planar coils (PC)
                currents.extend([Calibrating.coilcurrent(config_table['IA'][i], settings={'coiltype': 'PC'}), Calibrating.coilcurrent(config_table['IB'][i], settings={'coiltype': 'PC'})] * 10)

        # If the control coils (CC) were used
        if CC:
            # Set the coil IDs
            coils = arange(160, 240)  # CC
            # Read the currents of the control coils from the archive from trigger 1, until 2 s later. The current should remain constant
            time_from = w7xarchive.get_program_triggerts(XPid.split("_")[1], 1)  # in ns since epoch
            time_upto = int(time_from + 2e9)
            # List the signal names of the control coils. The CC IDs are 11 19 21 29 31 39 41 49 51 59
            signalList = []
            for i in range(1, 6):
                signalList.extend(['Magnetic Configuration/Correction coils/Control Coils/ACM {}1'.format(i), 'Magnetic Configuration/Correction coils/Control Coils/ACM {}9'.format(i)])
            # Read the signals
            dims, vals, _, _, oks, _ = Reader.get_signalsOfList(signalList, time_from, time_upto, resolution=0)
            # Take the mean value of each signal as the coil current.
            for signal in signalList:
                if oks[signal]:
                    currents.append(mean(vals[signal]))  # TODO: is it always valid to take the mean (or min) value from t1, to t1+2s?
                    # log.info('for {}, difference mean and min is {}'.format(signal, mean(vals[signal])-min(vals[signal])))
                else:
                    currents.append(None)
                    log.info('for {}, no current found, therefore set to None'.format(signal))

    # Return a dict with the details of the magnetic configuration
    settings = {}
    settings['configID'] = configID  # if configuration is false, specified coils and currents are used
    settings['coils'] = coils
    settings['currents'] = currents
    settings['archiveConfig'] = config
    settings['CC'] = CC
    log.info("Magnetic configuration {} is described by configID {}, coils {}, currents {}".format(config, configID, coils, currents))

    return settings


