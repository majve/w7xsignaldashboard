import w7xarchive
import flt_functions as flt
import numpy as np
import logging
from osa import Client
import MagneticConfig as MagneticConfig

log = logging.getLogger("Profiles")


def get_poincare_VMEC(settings={}):  # VMEC

    coils = None
    currents = None

    XPid = getSetting(settings, 'XPid')
    magneticSettings = MagneticConfig.getMagneticConfig(XPid)

    configID = getSetting(magneticSettings, 'configID')
    if configID is False:
        coils = getSetting(magneticSettings, 'coils')
        currents = getSetting(magneticSettings, 'currents')
    if configID is False and coils is None:
        configID = 0
        log.warning('configID not provided in settings, nor coils and currents. Defaulting to configID {}'.format(configID))

    phi0 = getSetting(settings, 'phi0')

    # url = 'http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/geiger/w7x/1000_1000_1000_1000_+0390_+0390/01/020s/'
    # fs0 = requests.get(url + 'fluxsurfaces.json?phi=0').json()
    # plt.plot(fs0['surfaces'][0]['x1'], fs0['surfaces'][0]['x3'], 'r.')

    client = Client("http://esb:8280/services/w7xfp?wsdl")

    pressure = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    toroidalCurrent = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    aeff = 0.52
    currents.extend([0., 0.])
    coilCurrents = currents  # [10000., 10000., 10000., 10000., 10000., 0., 0.]
    toroidalAngle = [phi0/360*np.pi]  # radian
    # toroidalAngle = [phi0, phi0 + 72, phi0 + 144, phi0 + 216, phi0 + 288]  # to get all 5 islands in Poincare plot,  we evaluate Poincare plot at all 5 stellarator symmetric cross sections
    reff = [0, 0.05,  0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1]
    numPointsPerSurface = 80
    # out = Points3D[ ] An array of Points3D objects, each containing the (R,phi,Z) coordinates for one 2D flux surface cross section.
    # For toroidal angle index n, reff index k, the returned flux surface is stored in out[n*numPointsPerSurface+k].
    out = client.service.getFluxSurfaces(pressure, toroidalCurrent, aeff, coilCurrents, toroidalAngle, reff, numPointsPerSurface)

    dim = []
    val = []
    for i in range(0, len(out)):
        dim.append(np.asarray(out[i].x1))  # R
        val.append(np.asarray(out[i].x3))  # z

    return dim, val, True


def get_poincare(settings={}, use_cache=True):

    coils = None
    currents = None

    XPid = getSetting(settings, 'XPid')
    magneticSettings = MagneticConfig.getMagneticConfig(XPid)

    configID = getSetting(magneticSettings, 'configID')
    if configID is False:
        coils = getSetting(magneticSettings, 'coils')
        currents = getSetting(magneticSettings, 'currents')
    if configID is False and coils is None:
        configID = 0
        log.warning('configID not provided in settings, nor coils and currents. Defaulting to configID {}'.format(configID))

    phi0 = getSetting(settings, 'phi0')

    dim_from = 5  # TODO: avoid hardcoded values
    dim_to = 7
    dims = np.array([dim_from, dim_to], dtype=np.int64)

    archiveConfig = getSetting(magneticSettings, 'archiveConfig')
    CC = getSetting(magneticSettings, 'CC')
    if CC:
        CC = currents[-1]

    normed_signal = "Services/Poincare/W7X/config_{}/CC_{}_DATASTREAM/0/phi0_{}".format(archiveConfig, CC, phi0)

    N = 5000  # if you change this number, then clear the cache
    # retrieve cache data if possible
    if use_cache:
        dim, val = w7xarchive.get_signal_from_cachefile(normed_signal, *dims)
        dim = [np.array(dim[i:i+N]) for i in range(0, len(dim)-(N-1), N)]
        val = [np.array(val[i:i+N]) for i in range(0, len(val)-(N-1), N)]
    else:
        dim, val = np.array([]), np.array([])

    # download data if necessary
    if (len(dim) == 0) or (len(val) == 0):

        # Start Points for Poincare plot
        x0 = np.concatenate([np.linspace(5.97, 6.19, 8), np.linspace(6.207, 6.28, 4)])
        y0 = np.zeros(x0.size)
        z0 = np.zeros(x0.size)
        Start_Pos = np.vstack([x0, y0, z0]).T

        phi_list = [phi0, phi0 + 72, phi0 + 144, phi0 + 216, phi0 + 288]  # to get all 5 islands in Poincare plot,  we evaluate Poincare plot at all 5 stellarator symmetric cross sections

        # Create Poincare Plot for start points
        res_pc = flt.get_pc(Start_Pos, coils=coils, currents=currents, configID=configID, phi=phi_list, N=N, stepsize=5e-3)

        dim = []
        val = []
        for i in range(0, len(res_pc.surfs)):
            t1 = np.asarray(res_pc.surfs[i].points.x1)
            t2 = np.asarray(res_pc.surfs[i].points.x2)
            if t1.size > 1:
                dim.append(np.sqrt(t1 ** 2 + t2 ** 2))  # R

            val.append(np.asarray(res_pc.surfs[i].points.x3))

        if 'use_cache':
            # concatenate because form of mesh is [array([0, 1]), array([0, 1]), ...] but has to be array for caching
            w7xarchive.write_signal_to_cache(normed_signal, np.concatenate(dim), np.concatenate(val), dim_from, dim_to)

    return dim, val, True


def get_lcfs(settings={}, use_cache=True):

    coils = None
    currents = None

    XPid = getSetting(settings, 'XPid')
    magneticSettings = MagneticConfig.getMagneticConfig(XPid)

    configID = getSetting(magneticSettings, 'configID')
    if configID is False:
        coils = getSetting(magneticSettings, 'coils')
        currents = getSetting(magneticSettings, 'currents')
    if configID is False and coils is None:
        configID = 0
        log.warning('configID not provided in settings, nor coils and currents. Defaulting to configID {}'.format(configID))

    phi0 = getSetting(settings, 'phi0')

    dim_from = 5  # TODO: avoid hardcoded values
    dim_to = 7
    dims = np.array([dim_from, dim_to], dtype=np.int64)

    archiveConfig = getSetting(magneticSettings, 'archiveConfig')
    CC = getSetting(magneticSettings, 'CC')
    if CC:
        CC = currents[-1]

    normed_signal = "Services/LCFS/W7X/config_{}/CC_{}_DATASTREAM/0/phi0_{}".format(archiveConfig, CC, phi0)

    # retrieve cache data if possible
    N = 20000  # if you change this number, then clear the cache
    # retrieve cache data if possible
    if use_cache:
        dim, val = w7xarchive.get_signal_from_cachefile(normed_signal, *dims)
        dim = [np.array(dim[i:i+N]) for i in range(0, len(dim)-(N-1), N)]
        val = [np.array(val[i:i+N]) for i in range(0, len(val)-(N-1), N)]
    else:
        dim, val = np.array([]), np.array([])

    # download data if necessary
    if (len(dim) == 0) or (len(val) == 0):

        # Determine x0 coordinate of LCFS on bean midplane
        x0_lcfs = flt.get_lcfs(coils=coils, currents=currents, configID=configID, plotflag=False, side='LFS')

        # get LCFS flux surface at x0
        lcfs = flt.get_pc(np.expand_dims(np.array([x0_lcfs - 5e-4, 0, 0]), 0), coils=coils, currents=currents, configID=configID, phi=phi0, N=N, stepsize=5e-3)

        dim = []
        val = []
        for i in range(0, len(lcfs.surfs)):
            t1 = np.asarray(lcfs.surfs[i].points.x1)
            t2 = np.asarray(lcfs.surfs[i].points.x2)
            if t1.size > 1:
                dim.append(np.sqrt(t1 ** 2 + t2 ** 2))  # R

            val.append(np.asarray(lcfs.surfs[i].points.x3))

        if 'use_cache':
            # concatenate because form of mesh is [array([0, 1]), array([0, 1]), ...] but has to be array for caching
            w7xarchive.write_signal_to_cache(normed_signal, np.concatenate(dim), np.concatenate(val), dim_from, dim_to)

    return dim, val, True


def get_AntennaLimiter(settings={}, signallist=None, inputsignals=None, use_cache=True):

    phi0 = 0
    if 'phi0' in settings.keys():
        phi0 = settings['phi0']
    else:
        log.warning('phi0 not provided in settings, defaulting to {}'.format(phi0))

    # Get the maximal extension of the antenna during the time period
    maxPos = (np.nanmax(inputsignals[signallist[0]].values) - 93) / 1000  # converted mm to m

    compID_ant = 565  # ICRH limiter Auskleidungs 102 mm

    dim_from = 5  # TODO: avoid hardcoded values
    dim_to = 7
    dims = np.array([dim_from, dim_to], dtype=np.int64)
    normed_signal = "Services/MeshSrv/W7X/componentID_{}/phi_{}_DATASTREAM/0/position_{}".format(compID_ant, phi0, maxPos)

    # retrieve cache data if possible
    if use_cache:
        dim, val = w7xarchive.get_signal_from_cachefile(normed_signal, *dims)
        dim = [np.array([dim[i], dim[i+1]]) for i in range(0, len(dim)-1, 2)]
        val = [np.array([val[i], val[i+1]]) for i in range(0, len(val)-1, 2)]
    else:
        dim, val = np.array([]), np.array([])

    # download data if necessary
    if (len(dim) == 0) or (len(val) == 0):

        mesh_ant = flt.get_mesh_at_phi(compID_ant, phi0, assembly=False)

        dim = []
        val = []
        for r in mesh_ant:
            x = np.array(r.vertices.x1)
            y = np.array(r.vertices.x2)
            if x.size > 1:
                dim.append(np.sqrt(x ** 2 + y ** 2) - maxPos)  # R
            val.append(np.array(r.vertices.x3))  # z

        # To add other components
        # compID_vessel = [8, 332, 419, 424, 439]  # 8 is vessel hm 31, 332 is  shield in m 3, 419 and 424 are top and bottom divertor plate, 439 steel panel
        # meshes_vessel = [0]*len(compID_vessel)
        # for i in range(len(compID_vessel)):
        #     meshes_vessel[i] = flt.get_mesh_at_phi(compID_vessel[i], phi0, assembly=False)
        # for mesh_vessel in meshes_vessel:
        #     if mesh_vessel:  # not None
        #         for r in mesh_vessel:
        #             x = np.array(r.vertices.x1)
        #             y = np.array(r.vertices.x2)
        #             if x.size > 1:
        #                 dim.append(np.sqrt(x ** 2 + y ** 2) + maxPos)
        #             val.append(np.array(r.vertices.x3))

        # write the data to cache (if requested)
        if 'use_cache':
            # concatenate because form of mesh is [array([0, 1]), array([0, 1]), ...] but has to be array for caching
            w7xarchive.write_signal_to_cache(normed_signal, np.concatenate(dim), np.concatenate(val), dim_from, dim_to)

    return dim, val, True


def getSetting(settings, key):

    if key in settings.keys():
        return settings[key]
    else:
        log.warning('{} not provided in settings for calibration. Aborting.'.format(key))
        raise Exception