from archive_signals.ICRH_signals import ICRH_signal_dict
from archive_signals.ECRH_signals import ECRH_signal_dict
from archive_signals.NBI_signals import NBI_signal_dict
from archive_signals.video_signals import video_signal_dict
from archive_signals.physics_signals import physics_signal_dict
from archive_signals.magnetics_signals import magnetics_signal_dict
from archive_signals.diagnostics_signals import diagnostics_signal_dict
from archive_signals.gas_signals import gas_signal_dict

archive_signal_dict = {
    # TEMPLATE
    # 'PUT HERE the signal name': {  # corresponding to the structure on the GUI, group/system/subsystem/signal
    #     'url': 'PUT HERE the url to read from ArchiveDB (after http://archive-webapi.ipp-hgw.mpg.de/) Note url does not start with /',
    #     'type': 'PUT HERE the type, being timetrace or video',
    #     'description': "PUT HERE a description of the signal",
    #     'slope': 1,  # PUT HERE a value for the conversion slope*signalvalue + intercept,
    #     'intercept': 0,  # PUT HERE a value for the conversion slope*signalvalue + intercept,
    #     'valconv': 1/ 1000,  # PUT HERE a value for conversion valconv*value (e.g. 1/1000 from W to MW),
    #     'unit': "PUT HERE the unit that must contain [ and ] (e.g. P [MW])",
    #     'legend': "PUT HERE the legend"
    # }
}

archive_signal_dict.update(ICRH_signal_dict)
archive_signal_dict.update(ECRH_signal_dict)
archive_signal_dict.update(NBI_signal_dict)
archive_signal_dict.update(video_signal_dict)
archive_signal_dict.update(physics_signal_dict)
archive_signal_dict.update(magnetics_signal_dict)
archive_signal_dict.update(diagnostics_signal_dict)
archive_signal_dict.update(gas_signal_dict)



def getTimetraces():
    """ Get the signals from the dicts that are of the type timetrace
    """
    # I do it this way since I also want the system name in the correlations drop down menu
    dicts ={'ICRH': ICRH_signal_dict, 'ECRH': ECRH_signal_dict, 'NBI': NBI_signal_dict, 'video': video_signal_dict, 'physics': physics_signal_dict, 'magnetics': magnetics_signal_dict, 'diagnostics': diagnostics_signal_dict, 'gas': gas_signal_dict}
    # Create a tuple that hold all signals o the type timetrace
    timetracetuple = ()
    for system, sysdict in dicts.items():
        for signal, specs in sysdict.items():
            # print('test', specs['type'])
            if specs['type'] == 'timetrace':
                timetracetuple = timetracetuple + (system + '/' + signal,)

    return timetracetuple