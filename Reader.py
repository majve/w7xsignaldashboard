from archive_signal_dict import archive_signal_dict
from derived_signal_dict import derived_signal_dict
from w7xdia_signal_dict import w7xdia_signal_dict
from w7x_profile_dict import w7x_profile_dict
from info_dict import info_dict
from params_dict import params_dict

from Signal import Signal
import numpy as np
import w7xarchive
import math
import re
import logging

import requests
import time

log = logging.getLogger("Reader")

def get_signal(signalname, time_from, time_upto, resolution=100, urlRaw=False, Calibrate=False, signal_intervals=None, settings={}):
    """ Get the signal from a source, such as (1a) from archiveDB (timetrace or video), (2) from fieldlinetracer or componentDB (profile), (3) from w7xdia (timetrace).
       Or get a (1b) derived signal, which is constructed from other signals (timetrace).
       (4) Try to find the signal in any source if the OP is added to the signal name
       Otherwise (5) the signal is expected to be the adress in ArchiveDB as URL (timetrace).
       Independent of the source, the code will determine dim, val, unit, legend, ok, sigtype

    Parameters
    ----------
    signalname : signal name,
        where the name corresponds to a name in archive_signal_dict, derived_signal_dict, w7x_profile_dict or w7xdia_signal dict
        or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'
    other : see get_signal_from_archive(...)

    Returns
    -------
    dimensions, signalvalues, unit, legend, boolean and signaltype
        dimensions (usually times) : numpy array of dtype _np.int64
        signalvalues : numpy array of dtype
        unit : string that contains unit in [ ]
        legend : string
        ok : boolean whether signal was read succesfully
        signaltype : string. timetrace, profile, video or image

    See Also
    --------
    get_video_from_archive(...)
    get_signal_from_archive(...)
    derivation functions in Derivating.py
    profile functions in Profile.py

    Examples
    --------
    >>> get_signal(
    'Generator/Generator 1/Endstage power/Forward power', 1680186447514002176, 1680186452909001984, 100)

    (array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984]),
     array([3.31494596e-01, 3.21567363e-01, 3.26694578e-01, ..., 5.88288144e-08, 4.69324613e-08, 3.60661929e-08]),
     'P [MW]', '$P_{f,\\ end}$ (MW)', True, 'timetrace')

    """
    log.debug('Trying to get signal {}'.format(signalname))

    my_signal = Signal(signalname)


    # (1a) The signal is in archive_signal_dict (timetrace or video) and will be read from ArchiveDB
    if signalname in archive_signal_dict.keys():

        my_signal.signal_type = archive_signal_dict[signalname]['type']
        if my_signal.signal_type == 'video':
            my_signal.dimensions, my_signal.values, my_signal.ok = get_video_from_archive(signalname, time_from, time_upto, signal_intervals)
        elif my_signal.signal_type == 'timetrace':
            my_signal.dimensions, my_signal.values, my_signal.ok = get_signal_from_archive(signalname, time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)
            my_signal.unit = archive_signal_dict[signalname]['unit']
            my_signal.legend = archive_signal_dict[signalname]['legend']
        else:
            log.error('type {} not implemented for archive_signal_dict'.format(my_signal.signal_type))

    # (1b) The signal is constructed from other signals (must be timetraces). First read the input signals and then determine the signal with the derivation function
    elif signalname in derived_signal_dict.keys():

        my_signal.signal_type  = derived_signal_dict[signalname]['type']
        if my_signal.signal_type  in ['timetrace', 'event']:
            # First, read the input signals
            if not Calibrate:
                #  Then either, read the already derived signal from the ArchiveDB
                my_signal.dimensions, my_signal.values, my_signal.ok = get_signal_from_archive(derived_signal_dict[signalname]['url'], time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)
            # if dim is None:  # True if Calibrate or if get_signal_from_archive could not read signal from url
            if not my_signal.dimensions:  # Using this syntax in case dim = []. Happened when testing ProcessPnetDRC3
                #  or, read the input signals (that are already calibrated or will be)
                signallist = derived_signal_dict[signalname]['inputsigs']
                # For the inputsignals, the regular url can be used. If the url is not available, the urlRaw will be calibrated. But calibration is not mandatory
                # dims, vals, units, legends, oks, _ = get_signalsOfList(signallist, time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)
                inputsignals = get_signalsOfList(signallist, time_from, time_upto, resolution, signal_intervals=signal_intervals, settings=settings)

                # and determine the signal by applying the derivation function to the inputsignals
                if 'derivationsetting' in derived_signal_dict[signalname].keys():
                    settings.update(derived_signal_dict[signalname]['derivationsetting'])
                my_signal.dimensions, my_signal.values, my_signal.ok = derived_signal_dict[signalname]['derivationfunc'](signallist, inputsignals, settings)
                my_signal.unit = derived_signal_dict[signalname]['unit']
                my_signal.legend = derived_signal_dict[signalname]['legend']
        else:
            log.error('type {} not implemented for derived_signal_dict'.format(my_signal.signal_type ))

    # (2) The signal is in w7x_profile_dict (profile)
    elif signalname in w7x_profile_dict:

        my_signal.signal_type  = w7x_profile_dict[signalname]['type']
        if my_signal.signal_type  == 'profile':
            # If necessary, read and pass input signals
            if 'inputsigs' in w7x_profile_dict[signalname].keys():
                signallist = w7x_profile_dict[signalname]['inputsigs']
                inputsignals = get_signalsOfList(signallist, time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)
                my_signal.ok = all(inputsignal.ok for inputsignal in inputsignals.values())
                # Retrieve the profile from any of the profile functions, that are implemented in Profiles.py
                my_signal.dimensions, my_signal.values, my_signal.ok = w7x_profile_dict[signalname]['profilefunc'](settings, signallist, inputsignals)
            else:
                my_signal.dimensions, my_signal.values, my_signal.ok = w7x_profile_dict[signalname]['profilefunc'](settings)
            my_signal.unit = w7x_profile_dict[signalname]['unit']
            my_signal.legend = w7x_profile_dict[signalname]['legend']

    # (3) The signal is in w7xdia_signal_dict (timetrace), and is coming from w7xdia, which is the diagnostic software package (ask Sergey Bozhenkov for details)
    elif signalname in w7xdia_signal_dict:

        my_signal.signal_type = w7xdia_signal_dict[signalname]['type']
        if my_signal.signal_type == 'timetrace':
            # To get the correct format as expected from w7xdia
            shot = w7xarchive.get_program_list(time_from, time_upto)[0]['id']
            try:
                dim, val = w7xdia_signal_dict[signalname]['w7xdiafunction'](shot, timeout=10)
                # TODO: w7xdia already provides the time relative to t1 in s... But this is done in plotter, so the conversion is undone here... Change this approach if possible
                my_signal.dimensions = (dim + settings['trel'] / 1e9) * 1e9
                #  Appply calibration and unit conversion
                if 'calibrationfunc' in w7xdia_signal_dict[signalname].keys():
                    val = w7xdia_signal_dict[signalname]['calibrationfunc'](val)
                valConversion = w7xdia_signal_dict[signalname]['valconv']
                my_signal.values = val * valConversion
                my_signal.ok = True if len(val) > 0 else False
            except Exception as e:
                if 'NoneType' in str(e):
                    log.error('Error: Monitor is not connected to w7xdia. See w7xdia_signal_dict.py. Exception getting signal: {}'.format(e, signalname))
                else:
                    log.error('{}: exception getting signal: {}'.format(e, signalname))
                my_signal.ok = False
            my_signal.unit = w7xdia_signal_dict[signalname]['unit']
            my_signal.legend = w7xdia_signal_dict[signalname]['legend']
        else:
            log.error('type {} not implemented for w7xdia_signal_dict'.format(my_signal.signal_type))
    elif signalname in params_dict:
        try:
            url = params_dict[signalname]['url']
            my_signal.dimensions, val, my_signal.ok = params_dict[signalname]['paramfunc'](url, time_from, time_upto)
            valConversion = params_dict[signalname]['valconv']
            my_signal.values = val * valConversion
        except Exception as e:
            log.error('{}: exception getting parameter: {}'.format(e, signalname))
            my_signal.ok = False
        my_signal.unit = params_dict[signalname]['unit']
        my_signal.legend = params_dict[signalname]['legend']
        my_signal.signal_type = params_dict[signalname]['type']


    # (4) Try if the signal is found in any dict when the OP is added
    # Check if "OP" is NOT already part of the last part of the signal name, to halt the recursion
    elif "OP" not in signalname.split('/')[-1]:
        # Add the OP to the signal name
        OP = get_op(time_from)
        OPnumber = OP.split('.')[0]
        log.debug('Signal not found. Try to get signal specifically for {}'.format(OPnumber))
        signal_temp = signalname + "_" + OPnumber
        # Call get_signal recursively for OP1 or OP2
        my_signal = get_signal(signal_temp, time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)
        if not my_signal.ok:
            log.debug('Signal not found. Try to get signal specifically for {}'.format(OP))
            signal_temp = signalname + "_" + OP
            # Call get_signal recursively for OP1.2a or OP1.2b or OP2.1 or OP2.2
            my_signal = get_signal(signal_temp, time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)

    # (5) If still the signal could not be read, it is expected to be the adress in ArchiveDB as URL
    # Currently, this is never the case
    # if not my_signal.ok:
    #     log.debug('Signal not ok, trying if the signal name is the url')
    #     my_signal.signal_type = 'timetrace'
    #     my_signal.unit = "[a.u.]"
    #     my_signal.legend = signalname.split("/")[-1]
    #     my_signal.dimensions, my_signal.values, my_signal.ok = get_signal_from_archive(signalname, time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)

    if my_signal.ok: log.debug('got signal {}'.format(signalname))
    else: log.warning('Something is not ok with signal {}'.format(signalname))

    return my_signal


def get_video_from_archive(signalname, time_from, time_upto, signal_intervals=None):
    """ (1) Get the relevant information from the archive_signal_dict, (2) Read the video images from the archiveDB, (3) Process the data such that the video can be constructed later on

        Parameters
        ----------
        signalname : signal name,
            where the name corresponds to a name in archive_signal_dict of the type 'video'
            or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'
        time_from : 64bit integer or string(?)
            The time from which valid time intervals is to be searched.
            Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
        time_upto : 64bit integer or string(?)
            The time up to which valid time intervals is to be searched.
            Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
            If None is supplied, current time is used.
        signal_intervals : nd.array or None
            2d array of all time intervals where the data has to be read.
            Each element of the list contains a 2-integer element list
            indicating the upper and lower end of the interval.
            None -> signal interval between time_form and time_none will be used with the specifie resolution

        Returns
        -------
        dimensions, values, and boolean
            dimensions (times) : numpy array of dtype _np.int64
            values : numpy array containing the images
            ok : boolean whether the video images was read succesfully

        See Also
        --------
        get_signal(...) : Read and process video signal

        Examples
        --------
        >>> get_video_from_archive('W7X/Video tracks/Edicam/AEQ40', 1680186447514002176, 1680186452909001984)
        """

    # (1) Get the relevant information from the archive_signal_dict
    url = archive_signal_dict[signalname]['url']

    # (2) Read the video from the archiveDB
    try:
        # Construct the URL
        base_url = 'http://archive-webapi.ipp-hgw.mpg.de/'
        signal_url = base_url + url

        # If no time intervals were specified, retrieve all interval in the range [time_from, time_upto]
        if signal_intervals is None:
            signal_intervals = w7xarchive.get_time_intervals(signal_url, time_from, time_upto)[::-1]  # Sort in ascending order

        # Get all the images and their timestamps
        log.debug("Reading signal at {} from {} upto {}".format(signal_url, time_from, time_upto))

        dim_list = []
        val_list = []
        #  Time intervals have to be read and appended manually
        for interval in signal_intervals:
            # Read the video from ArchiveDB, using the w7xarchive functionality. The options have to be set like this
            # Little endian specification not necessary since w7xarchive 2023.04.20
            time, image = w7xarchive.get_image_raw(signal_url, *interval, archive_types=True)  # , little_endian=True
            if image.ndim == 2:
                image = np.expand_dims(image, axis=0)
            dim_list.append(time)
            val_list.append(image)

        dim = np.concatenate(dim_list)
        val = np.concatenate(val_list, axis=0)
        # Set the boolean that will be returned, indicating whether retrieving the video was succesful
        ok = True if len(val) > 0 else False

    except Exception as e:
        log.debug('{}: exception getting signal: {}'.format(e, signalname))
        dim = None
        val = []
        ok = False

    return dim, val, ok


def get_signal_from_archive(signalname, time_from, time_upto, resolution=100, urlRaw=False, Calibrate=False, signal_intervals=None, settings={}):
    """ (1) Get the relevant information from the archive_signal_dict, (2) Read the signal from the archiveDB, (3) Apply the calibration to the signal and (4) Perform the conversion of the data.

    Parameters
    ----------
    signalname : signal name,
        where the name corresponds to a name in archive_signal_dict
        or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'
    time_from : 64bit integer or string(?)
        The time from which valid time intervals is to be searched.
        Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
    time_upto : 64bit integer or string(?)
        The time up to which valid time intervals is to be searched.
        Of the form 'YYYY-MM-DD HH:MM:SS.%f', or nanosecond time stamp.
        If None is supplied, current time is used.
    resolution : integer
        The number of time-intervals that will be read from the time-range
    urlRaw : Boolean
        Whether the raw url from the dict, containing the uncalibrated signal,
        will be used or the url, containing the calibrated signal
    Calibrate : Boolean
        Whether the signal has to be calibrated or not. It is expected to use
        Calibrate when urlRaw is read. If Calibrate is used on url an warning is given.
    signal_intervals : nd.array or None
        2d array of all time intervals where the data has to be read.
        Each element of the list contains a 2-integer element list
        indicating the upper and lower end of the interval.
        None -> signal interval between time_form and time_none will be used with the specifie resolution
    settings : dict
        Dictionary to pass settings to calibration functions. The functions will look at 'key' for
        the necessary settings

    Returns
    -------
    dimensions, signalvalues, and boolean
        dimensions (usually times) : numpy array of dtype _np.int64
        signalvalues : numpy array of dtype given by the archive
        ok : boolean whether signal was read succesfully

    See Also
    --------
        get_signal(...)
        w7xarchive.get_time_intervals(...)
        w7xarchive.get_signal_multiinterval(...)

    Examples
    --------
    >>> get_signal_from_archive(
    'Generator/Generator 1/Endstage power/Forward power', 1680186447514002176, 1680186452909001984, 100)

    (array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984]),
     array([3.31494596e-01, 3.21567363e-01, 3.26694578e-01, ..., 5.88288144e-08, 4.69324613e-08, 3.60661929e-08]),
     True)
    """

    # (1) Get the relevant information from the archive_signal_dict
    if signalname in archive_signal_dict.keys():
        if not urlRaw:
            url = archive_signal_dict[signalname]['url']
        else:
            url = archive_signal_dict[signalname]['urlRaw']
        slope = archive_signal_dict[signalname]['slope']
        intercept = archive_signal_dict[signalname]['intercept']
        valConversion = archive_signal_dict[signalname]['valconv']

    #  Otherwise, the signalname is supposed to be a signal address in ArchiveDB as URL.
    else:
        url = signalname
        slope = 1
        intercept = 0
        valConversion = 1

    # (2) Read the signal from the archiveDB
    dim = None
    val = []
    called_url = ""  # TODO: pass this to the dashboard
    try:
        # Construct the URL
        base_url = 'http://archive-webapi.ipp-hgw.mpg.de/'
        signal_url = base_url + url
        log.debug("Trying to read signal at {} from {} upto {}".format(signal_url, time_from, time_upto))

        # If no time intervals were specified, retrieve all interval in the range [time_from, time_upto]
        if signal_intervals is None:
            signal_intervals = w7xarchive.get_time_intervals(signal_url, time_from, time_upto)
        log.debug("There are {} time intervals".format(len(signal_intervals)))
        # To obtain the requested resolution, only every n-th interval will be read
        if int(resolution) > 0:
            nResolution = math.ceil(len(signal_intervals) / int(resolution))
            log.debug('To plot maximally {} intervals, reading 1/{} intervals'.format(resolution, nResolution))

        # Read the latest version from ArchiveDB, by trying versions from 20 downwards, until reading is succesful
        v = 20  # TODO: watch out when version becomes larger than 20 !
        while v > 0:

            try:
                # Substitute the version for the version-to-be-tested in the signal url.
                try_url = re.sub("/V[0-19]", "/V{}".format(v), signal_url)
                # Read the signal from ArchiveDB, using the w7xarchive functionality. Note that the signals are stored in the cache.
                # The built-in versio-functionality of w7xarchive was not working (fast enough) so I opted for the while loop, ass suggested by Adrian Stechow (IIRC)
                # TODO: Parallelization is not used at the moment. Look further into this.
                if int(resolution) > 0:
                    dim, val = w7xarchive.get_signal_multiinterval(try_url, signal_intervals[::nResolution], use_cache=True)  #, use_last_version=True, update_url_version=True)  # , parallelize=True)
                else:
                    dim, val = w7xarchive.get_signal_multiinterval(try_url, signal_intervals, use_cache=True)  #, use_last_version=True, update_url_version=True)  # , parallelize=True)
                log.debug("Reading versioned signal at {} from {} upto {}".format(try_url, time_from, time_upto))
                # If reading was succesful for this version, break from the while-loop
                called_url = f'{try_url}_signal.html?from={time_from}&upto={time_upto}'
                break
            # If the signal could not be read for the current version, decrease the version number and perform another iteration of the while loop
            except w7xarchive.HTTPError:
                v -= 1
        # (3) Apply the calibration to the signal
        # For every signal value x, perform the operation a*x + b (convenient for simple calibration settings)
        if not (slope == 1 and intercept == 0):
            val = val * slope + intercept
        # Perform calibration if requested
        if Calibrate:
            if not urlRaw:
                log.error('unexpected to apply calibration to signal from a URL! A signal from a raw url is expected!')
            #  Add the calibration settings that are specified in the archive_signal_dict
            if 'calibrationsetting' in archive_signal_dict[signalname].keys():
                settings.update(archive_signal_dict[signalname]['calibrationsetting'])
            # Call the calibration function that is specified in archive_signal_dict and implemented in Calibrating.py
            if archive_signal_dict[signalname]['calibrationfunc'] is None:
                log.debug('Signal read from urlRaw but calibration function is None. The signal is uncalibrated! '
                         'Probably because calibration is done by Minerva.')
                # TODO: it would be ideal if we could add 'uncalibrated' to the legend
            else:
                val = archive_signal_dict[signalname]['calibrationfunc'](val, settings)

        # Set the boolean that will be returned, indicating whether retrieving the signal was succesful
        ok = True if len(val) > 0 else False

        # (4) Perform the conversion of the data
        # Convert the signal values, as specified in the archive_signal_dict. This is intended for unit conversion, e.g. *1000 to go from MW to kW
        val = val * valConversion

    except Exception as e:
        # If the signal could not be read, this might be because we tried to read the calibrated signal from url, which is not available (yet).
        #  Instead, try to read the raw signal from rawUrl and apply calibration
        log.debug('{}: exception getting signal: {}'.format(e, signalname))
        dim = None
        val = []
        ok = False
        if signalname in archive_signal_dict.keys():
            # Make sure there is a urlRaw and we're not already using it
            if 'urlRaw' in archive_signal_dict[signalname] and not urlRaw:
                log.debug('Trying to read and calibrate the raw signal')
                dim, val, ok = get_signal_from_archive(signalname, time_from, time_upto, resolution, urlRaw=True, Calibrate=True, signal_intervals=signal_intervals, settings=settings)

    return dim, val, ok


def get_signalsOfList(signalList, time_from, time_upto, resolution=100, urlRaw=False, Calibrate=False, signal_intervals=None, settings={}):
    """Extension to get_signal where now a list of signals are read

    Parameters
    ----------
    signalList : list of signal names,
        where the names correspond to any name in archive_signal_dict, derived_signal_dict, w7x_profile_dict or w7xdia_signal dict
        or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'
    other : see get_signal_from_archive(...)

    Returns
    -------
    signal_detail_dicts : dicts
        dicts where the key is the signalname and the value is respectively the dimensions, signalvalues, the unit, the legend, ok, signaltype
                    dimensions (usually times) : numpy array of dtype _np.int64
                    signalvalues : numpy array of dtype
                    unit : string that contains unit in [ ]
                    legend : string
                    ok : boolean whether signal was read succesfully
                    signaltype : string. timetrace, profile, video or image

    See Also
    --------
    get_signal : Return signal_detail_dicts for one signal, i.e. where the dict contains one key

    Examples
    --------
    >>> get_signalsOfList(
    ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power'], 1680186447514002176, 1680186452909001984, 100)

    ({'Generator/Generator 1/Endstage power/Forward power': array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984]),
      'Generator/Generator 1/Endstage power/Reflected power': array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984])},
    {'Generator/Generator 1/Endstage power/Forward power': array([3.31494596e-01, 3.21567363e-01, 3.26694578e-01, ..., 5.88288144e-08, 4.69324613e-08, 3.60661929e-08]),
     'Generator/Generator 1/Endstage power/Reflected power': array([5.74903380e-01, 6.26232477e-01, 6.62313770e-01, ..., 2.48760446e-05, 2.46257701e-05, 2.94657759e-05])},
    {'Generator/Generator 1/Endstage power/Forward power': 'P [MW]',
     'Generator/Generator 1/Endstage power/Reflected power': 'P [kW]'},
    {'Generator/Generator 1/Endstage power/Forward power': '$P_{f,\\ end}$ (MW)',
     'Generator/Generator 1/Endstage power/Reflected power': '$P_{r,\\ end}$ (kW)'},
    {'Generator/Generator 1/Endstage power/Forward power': True,
     'Generator/Generator 1/Endstage power/Reflected power': True},
    {'Generator/Generator 1/Endstage power/Forward power': 'timetrace',
     'Generator/Generator 1/Endstage power/Reflected power': 'timetrace'}
    )
    """
    # TODO: use multi threading!
    # For all signalList that will go in plot

    my_signals = {}
    for signalname in signalList:
        my_signals[signalname] = get_signal(signalname, time_from, time_upto, resolution, urlRaw, Calibrate, signal_intervals, settings)

    return my_signals


def get_signal_type(signalname, *dicts):
    for d in dicts:
        for key in d.keys():
            if key.startswith(signalname):
                return d[key]['type']
    return None


def getsignalTypesOfList(signalList):
    """Extension to get_signal where now a list of signals are read

    Parameters
    ----------
    signalList : list of signal names,
        where the names correspond to any name in archive_signal_dict, derived_signal_dict, w7x_profile_dict or w7xdia_signal dict
        or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'

    Returns
    -------
    signal_type_dict : dict
        dicts where the key is the signalname and the value is signal type: timetrace, image, video, profile

    Examples
    --------
    >>> getsignalTypesOfList(
    ['Generator/Generator 1/Endstage power/Forward power', ''W7X/Video tracks/Edicam/AEQ10'])

    {'Generator/Generator 1/Endstage power/Forward power': 'timetrace',
     'W7X/Video tracks/Edicam/AEQ10': 'video'}
    """
    types = {}
    for signalname in signalList:
        signal_type = get_signal_type(signalname, archive_signal_dict, derived_signal_dict, w7x_profile_dict, info_dict, params_dict)
        if signal_type is not None:
            types[signalname] = signal_type
        elif 'image' in signalname:
            types[signalname] = 'image'  # TODO: make a image_dict.py?
        else:
            types[signalname] = 'timetrace'
    return types

def get_text(info, time_from, time_upto, settings):
    if info in info_dict.keys():
        try:
            #  read the input signals (that are already calibrated or will be)
            signallist = info_dict[info]['inputsigs']
            inputsignals = get_signalsOfList(signallist, time_from, time_upto, settings=settings)
            # and create the info with information from the inputsignals
            if 'infosetting' in info_dict[info].keys():
                settings.update(info_dict[info]['infosetting'])
            text = info_dict[info]['boxfunc'](signallist, inputsignals, settings)
            xloc = info_dict[info]['xpos']
            yloc = info_dict[info]['ypos']
            ha = info_dict[info]['ha']
            ok = True
        except Exception as e:
            log.error('{}: exception getting infobox: {}'.format(e, info))
            text = None
            xloc = 0
            yloc = 0
            ha = 'left'
            ok = False
    # Try if the signalname is found in any dict when the OP is added
    # Check if "OP" is NOT already part of the last part of the signalname, to halt the recursion
    elif "OP" not in info.split('/')[-1]:
        # Add the OP to the signalname
        OP = get_op(time_from)
        OPnumber = OP.split('.')[0]
        log.debug('Info not found. Try to get info specifically for {}'.format(OPnumber))
        info_temp = info + "_" + OPnumber
        # Call get_text recursively for OP1 or OP2
        text, xloc, yloc, ha, ok = get_text(info_temp, time_from, time_upto, settings)
        if not ok:
            log.debug('Info not found. Try to get info specifically for {}'.format(OP))
            info_temp = info + "_" + OP
            # Call get_text recursively for OP1.2a or OP1.2b or OP2.1 or OP2.2
            text, xloc, yloc, ha, ok = get_text(info_temp, time_from, time_upto, settings)

    else:
        log.warning('Something is not ok with infobox {}'.format(info))
        text = None
        xloc = 0
        yloc = 0
        ha = 'left'
        ok = False

    if ok: log.debug('Got infobox {}'.format(info))
    return text, xloc, yloc, ha, ok

def get_texts(infos, time_from, time_upto, settings):

    texts, xlocs, ylocs, has, oks = ({}, {}, {}, {}, {})
    for info in infos:
        text, xloc, yloc, ha, ok = get_text(info, time_from, time_upto, settings)
        texts[info] = text
        xlocs[info] = xloc
        ylocs[info] = yloc
        has[info] = ha
        oks[info] = ok

    return texts, xlocs, ylocs, has, oks


# Based on code from Adrian Stechow, adapted by Maja Verstraeten
def get_op(time):
    shotranges = {
        # Adrian used the shot IDs (see end of each line)
        # but here we use the corresponding timestamps
        'OP1.2a': [1503566052367903561, 1512666494727887240],  # [170824000, 171207054],
        'OP1.2b': [1531902347236677601, 1539876926169392500],  # [180718000, 181018041],
        'OP2.1': [1664349047151000000, 1680189210186999999],  # [220928000, 230401000]
        'OP2.2': [1680189210186999999, 1740870000000000000]  # [230401000, 250230000]
    }

    # The code below checks if a timestamp is during a program
    for OP, range in shotranges.items():
        if shotranges[OP][0] < time < shotranges[OP][1]:
            return OP

def getLastTimeInterval(signalname, filter_stop=None):

    # Timestamp of Jan 1, 2000, 00:00:00 UTC
    filter_start = '946688461000000000'
    # Create the current UTC time in nanoseconds since the epoch
    if not filter_stop:
        filter_stop = int(time.time() * 1e9)

    if signalname in archive_signal_dict.keys():
        if 'url' in archive_signal_dict[signalname].keys():
            url = archive_signal_dict[signalname]['url']
        else:
            url = archive_signal_dict[signalname]['urlRaw']
    elif signalname in derived_signal_dict:
        url = derived_signal_dict[signalname]['url']
    elif signalname in params_dict:
        url = params_dict[signalname]['url']
    else:
        log.error("Signal not found in dicts that are checked.")

    # Construct the URL
    base_url = 'http://archive-webapi.ipp-hgw.mpg.de/'
    address = base_url + url
    log.debug("Trying to get last interval from {}".format(address))
    query = f'?filterstart={filter_start}&filterstop={filter_stop}&pageSize=2'
    req_url = f'{address}/_signal.json{query}'
    log.debug(f'Searching last time interval in {req_url}')

    # Make the request
    response = requests.get(req_url)
    if response.status_code == 200:
        data = response.json()
    else:
        print(f'Failed to retrieve data: {response.status_code}')

    # Extract the last time interval
    last_interval_from = data['time_intervals'][0]['from']
    last_interval_upto = data['time_intervals'][0]['upto']

    log.debug(f'last_interval_from {last_interval_from}, last_interval_upto {last_interval_upto}')

    return last_interval_from, last_interval_upto