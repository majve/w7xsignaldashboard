import tkinter as tk
import tkinter.simpledialog
from tkinter.messagebox import showinfo
from tkcalendar import Calendar

from datetime import date, datetime
import w7xarchive
import pprint
import logging
import re
import LogbookAPI as LogbookAPI

pp = pprint.PrettyPrinter(indent=2)  # just for looks

log = logging.getLogger("selection")


class Selection:
    def __init__(self):

        self.GUI = None
        self.currentProgram = "None"
        self.currentProgramType = None

        self.from_date = None
        self.from_time = "00:00:00"
        self.upto_date = None
        self.upto_time = "23:59:59"

        self.filtername = ""
        self.filterdescr = ""

        # TODO: turn this into a list
        # Use a list instead
        self.filtertagname1 = ""
        self.filtertagname2 = ""
        self.filtertagname3 = ""
        self.filtertagval1 = ""
        self.filtertagval2 = ""
        self.filtertagval3 = ""


    def serialize(self):
        """Convert the Selection object into a JSON-serializable dictionary."""
        return {
            'currentProgram': self.currentProgram,
            'currentProgramType': self.currentProgramType,
            'from_date': self.from_date,
            'from_time': self.from_time,
            'upto_date': self.upto_date,
            'upto_time': self.upto_time,
            'filtername': self.filtername,
            'filterdescr': self.filterdescr,
            'filtertagname1': self.filtertagname1,
            'filtertagname2': self.filtertagname2,
            'filtertagname3': self.filtertagname3,
            'filtertagval1': self.filtertagval1,
            'filtertagval2': self.filtertagval2,
            'filtertagval3': self.filtertagval3
        }

    @staticmethod
    def deserialize(data):
        """Convert a dictionary into a Selection object."""
        selection = Selection()
        selection.currentProgram = data.get('currentProgram')
        selection.currentProgramType = data.get('currentProgramType')
        selection.from_date = data.get('from_date')
        selection.from_time = data.get('from_time')
        selection.upto_date = data.get('upto_date')
        selection.upto_time = data.get('upto_time')
        selection.filtername = data.get('filtername')
        selection.filterdescr = data.get('filterdescr')
        selection.filtertagname1 = data.get('filtertagname1')
        selection.filtertagname2 = data.get('filtertagname2')
        selection.filtertagname3 = data.get('filtertagname3')
        selection.filtertagval1 = data.get('filtertagval1')
        selection.filtertagval2 = data.get('filtertagval2')
        selection.filtertagval3 = data.get('filtertagval3')
        return selection

    def updateCurrentProgram(self, type, newProgram):
        # Program should be of the form XP_20230330.64 or None

        pattern = r'^None$|^XP_\d{8}\.\d{1,2}$'
        if re.match(pattern, newProgram):
            self.currentProgram = newProgram
            self.currentProgramType = type
            log.info('Current program set to: {}'.format(self.currentProgram))
        else:
            log.error('Not possible to set program to: {}. Abort'.format(newProgram))
            raise Exception("Invalid program number")

    def getCurrentProgramID(self):
        """Get the ID of the first program that is selected in the list on the GUI"""
        return self.currentProgram

    def getTimeRange(self):
        """ Returns the time_from and the time_to that were set by the user."""

        selection_from = self.from_date + " " + self.from_time
        selection_upto = self.upto_date + " " + self.upto_time

        log.debug('selection range: {} to {} '.format(selection_from, selection_upto))

        return selection_from, selection_upto

    def getPrograms(self):
        """ Get the programs in the time selection. The program ID -and description is set in the list on the GUI."""

        if datetime.strptime(f"{self.upto_date} {self.upto_time}", '%Y-%m-%d %H:%M:%S') <= datetime.strptime(f"{self.from_date} {self.from_time}", '%Y-%m-%d %H:%M:%S'):
            return ["None"]
        time_from, time_upto = self.getTimeRange()
        try:
            # w7xarchive alternative: self.GUI.XPs = w7xarchive.get_program_list(time_from, time_upto)
            query = self.getfilterquery()
            json = LogbookAPI.search_programlogs(query, w7xarchive.to_timestamp(time_from), w7xarchive.to_timestamp(time_upto))
            XPs = json['hits']['hits']
        except Exception as e:
            log.error('{}: exception getting program list from {} upto {}'.format(e, time_from, time_upto))
            proglist = ["None"]
        else:
            proglist = ["None"]
            for prog in XPs:
                # w7xarchive alternative: proglist.append(prog["id"] + ": " + prog["description"])
                proglist.append(prog["_source"]["id"])  # + ": " + prog["_source"]["description"])
        return proglist

    # def setXPprogram(self, event):
    #     # If double-clicked in program list, then currentProgram is updated
    #
    #     index = self.GUI.Program_list.curselection()[0]
    #     if index == 0:
    #         self.updateCurrentProgram("None", "None")
    #     else:
    #         # w7xarchive alternative: self.updateCurrentProgram("XP", self.GUI.XPs[index]["id"])
    #         self.updateCurrentProgram("XP", self.GUI.XPs[index-1]["_source"]["id"])  # -1 because None is at the front

    def getProgramScenario(self):
        """ For the program that is selected on the list in the GUI, get the scenario. The result is displayed in a messagebox."""

        t0 = self.getProgramTriggerTs(0)  # ns since epoch
        program = w7xarchive.get_program_list(t0-10, t0+10, self.currentProgramType)
        # I wanted to cross check if we got the correct program but XPid in w7xarchive is 20230215.026 and in logbook is 20230215.26
        # for i in range(len(programs)):
        #     if programs[i]["id"] == self.getCurrentProgramID().split("_")[1]:
        info = program[0]["id"] + "\n" + pprint.pformat(program[0]["scenarios"], indent=4) + "\n"
        tk.messagebox.showinfo("Program scenarios", info)
        # Alternatively, use from w7x_overviewplot code the following:
        # w7x_overviewplot.get_program_info(program_nr) where program_nr is without the XP_

    def getProgramTriggers(self):
        """ For the program that is selected on the list in the GUI, get the trigger. The result is displayed in a messagebox."""

        t0 = self.getProgramTriggerTs(0)  # ns since epoch
        program = w7xarchive.get_program_list(t0 - 10, t0 + 10, self.currentProgramType)
        # I wanted to cross check if we got the correct program but XPid in w7xarchive is 20230215.026 and in logbook is 20230215.26
        # for i in range(len(programs)):
        #     if programs[i]["id"] == self.getCurrentProgramID().split("_")[1]:
        info = program[0]["id"] + "\n" + pprint.pformat(program[0]["trigger"], indent=4) + "\n"
        tk.messagebox.showinfo("Program triggers", info)

    # def getICRHSAPs(self):
    #     """ Get the ICRH stand-alone-programs (SAP) in the time selection. The result is set in the list on the GUI."""
    #
    #     time_from, time_upto = self.getTimeRange()
    #     try:
    #         # self.GUI.SAPs = [{"id": "0", "description": "test0"}, {"id": "1", "description": "test1"}, {"id": "2", "description": "test2"}]
    #         json = LogbookAPI.search_SAPlogs('CCA', w7xarchive.to_timestamp(time_from), w7xarchive.to_timestamp(time_upto))
    #         self.GUI.SAPs = json['hits']['hits']
    #     except Exception as e:
    #         log.error('{}: exception getting program list from {} upto {}'.format(e, time_from, time_upto))
    #         self.GUI.ICRHSAP_var.set(["None"])
    #         self.GUI.update()
    #     else:
    #         SAPlist = []
    #         for SAP in self.GUI.SAPs:
    #             SAPlist.append(SAP["_source"]["id"] + ": " + SAP["_source"]["description"])
    #         if not SAPlist:
    #             self.GUI.ICRHSAP_var.set(["None"])
    #         else:
    #             self.GUI.ICRHSAP_var.set(SAPlist)
    #         self.GUI.update()
    #
    # def setSAPprogram(self, event):
    #     # If double-clicked in SAP list, then currentProgram is updated
    #
    #     index = self.GUI.ICRHSAP_list.curselection()[0]
    #     self.updateCurrentProgram("SAP", self.GUI.SAPs[index]["id"])

    def getICRHevents(self):
        """ Get the ICRH sevents in the time selection. The result is set in the list on the GUI.
        The ICRH system is not integrated in the W7-X program, therefore we temporarily have to use our own events"""

        time_from, time_upto = self.getTimeRange()
        try:
            # self.GUI.SAPs = [{"id": "0", "description": "test0"}, {"id": "1", "description": "test1"}, {"id": "2", "description": "test2"}]
            json = LogbookAPI.search_eventlogs('CCA', w7xarchive.to_timestamp(time_from), w7xarchive.to_timestamp(time_upto))
            self.GUI.events = json['hits']['hits']
        except Exception as e:
            log.error('{}: exception getting program list from {} upto {}'.format(e, time_from, time_upto))
            self.GUI.ICRHevent_var.set(["None"])
            self.GUI.update()
        else:
            eventlist = []
            for event in self.GUI.events:
                eventlist.append(event["_source"]["id"] + ": " + event["_source"]["description"])
            if not eventlist:
                self.GUI.ICRHevent_var.set(["None"])
            else:
                eventlist.append("")  # Otherwise last program is hidden behind scroll foo
                self.GUI.ICRHevent_var.set(eventlist)
            self.GUI.update()

    def setevent(self, event):
        # If double-clicked in event list, then currentProgram is updated

        index = self.GUI.ICRHevent_list.curselection()[0]
        self.updateCurrentProgram("event", self.GUI.events[index]["id"])

    # TODO: is this function useful or can w7xarchive be called directly where this is function is used
    def getProgramTriggerTs(self, trigger=0):  # ns since epoch
        """Get the time of a specific trigger in the program. If an exception occurs, instead the from_time or the upto_time is returned.
        This is relevant for ICRH where there are no programs."""

        try:
            # The logbook handles XPid as XP_20230215.4
            # The w7xarchive software handles XPid as 20230215.4
            # Therefore, split the XPid on _ and pass the second part to w7xarchive.
            t = w7xarchive.get_program_triggerts(self.getCurrentProgramID().split("_")[1], trigger, use_cache=False)
            log.debug('trigger {}, t {}, UTC {}'.format(trigger, t, datetime.utcfromtimestamp(t / 1e9).strftime('%H:%M:%S', )))
        except Exception:
            raise Exception("Program trigger {} not available. Please do not plot while shot is still running. If this was the case, please clear the cache".format(trigger))
        return t

    def add_filters(self):

        self.win = tk.Toplevel()
        self.win.wm_title("Filter")

        frm = tk.Frame(self.win, borderwidth=15, bg="alice blue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        lbl = tk.Label(frm, text="Filters", bg="alice blue", font='helvetica 15 bold')
        frm.grid(column=0, row=0, rowspan=1, sticky="nsew")
        lbl.grid(column=0, row=0, sticky="w", padx=5)

        explanation = "\n use quotes \"...\" to search for exact phrases" \
                      "\n write \\ before a white space and other special symbols" \
                      "\n wildcard searches via ? (single character) or * (multiple character) " \
                      "\n combine criteria with AND, OR, ( ) " \
                      "\n exclude results with !, NOT,  - " \
                      "\n range queries via brackets  " \
                      "\n example. description: sniffer\\ test, name: *ID50 "
        explanation_lbl = tk.Label(frm, text=explanation, bg="alice blue", fg="gray", justify="left")
        explanation_lbl.grid(column=0, row=1, columnspan=3, sticky="w", padx=5)

        name_lbl = tk.Label(frm, text="name: ", bg="alice blue")
        self.name_var = tk.StringVar()
        self.name_var.set(self.filtername)
        name_entr = tk.Entry(frm, textvariable=self.name_var, width=20)

        descr_lbl = tk.Label(frm, text="description: ", bg="alice blue")
        self.descr_var = tk.StringVar()
        self.descr_var.set(self.filterdescr)
        descr_entr = tk.Entry(frm, textvariable=self.descr_var, width=20)

        tag_lbl = tk.Label(frm, text="tags: ", bg="alice blue")

        tagname_lbl = tk.Label(frm, text="name: ", bg="alice blue")
        self.tagname_var1 = tk.StringVar()
        self.tagname_var1.set(self.filtertagname1)
        tagname_entr1 = tk.Entry(frm, textvariable=self.tagname_var1, width=20)
        self.tagname_var2 = tk.StringVar()
        self.tagname_var2.set(self.filtertagname2)
        tagname_entr2 = tk.Entry(frm, textvariable=self.tagname_var2, width=20)
        self.tagname_var3 = tk.StringVar()
        self.tagname_var3.set(self.filtertagname3)
        tagname_entr3 = tk.Entry(frm, textvariable=self.tagname_var3, width=20)

        tagval_lbl = tk.Label(frm, text="value: ", bg="alice blue")
        self.tagval_var1 = tk.StringVar()
        self.tagval_var1.set(self.filtertagval1)
        tagval_entr1 = tk.Entry(frm, textvariable=self.tagval_var1, width=20)
        self.tagval_var2 = tk.StringVar()
        self.tagval_var2.set(self.filtertagval2)
        tagval_entr2 = tk.Entry(frm, textvariable=self.tagval_var2, width=20)
        self.tagval_var3 = tk.StringVar()
        self.tagval_var3.set(self.filtertagval3)
        tagval_entr3 = tk.Entry(frm, textvariable=self.tagval_var3, width=20)

        btn = tk.Button(frm, text="Set", bg="LightSteelBlue", command=self.set_filters)

        name_lbl.grid(row=2, column=0, sticky="w", padx=5)
        name_entr.grid(row=2, column=1, sticky="w", padx=5, pady=8)
        descr_lbl.grid(row=3, column=0, sticky="w", padx=5)
        descr_entr.grid(row=3, column=1, sticky="w", padx=5, pady=8)

        tag_lbl.grid(row=4, column=0, sticky="w", padx=5)
        tagname_lbl.grid(row=4, column=1, sticky="w", padx=5)
        tagval_lbl.grid(row=4, column=2, sticky="w", padx=5)
        tagname_entr1.grid(row=5, column=1, sticky="w", padx=5, pady=8)
        tagval_entr1.grid(row=5, column=2, sticky="w", padx=5, pady=8)
        tagname_entr2.grid(row=6, column=1, sticky="w", padx=5, pady=8)
        tagval_entr2.grid(row=6, column=2, sticky="w", padx=5, pady=8)
        tagname_entr3.grid(row=7, column=1, sticky="w", padx=5, pady=8)
        tagval_entr3.grid(row=7, column=2, sticky="w", padx=5, pady=8)
        btn.grid(row=8, column=2, sticky="e", padx=5, pady=8)

    def set_filters(self):
        self.filtername = self.name_var.get()
        self.filterdescr = self.descr_var.get()
        self.filtertagname1 = self.tagname_var1.get()
        self.filtertagname2 = self.tagname_var2.get()
        self.filtertagname3 = self.tagname_var3.get()
        self.filtertagval1 = self.tagval_var1.get()
        self.filtertagval2 = self.tagval_var2.get()
        self.filtertagval3 = self.tagval_var3.get()

        self.win.destroy()

    def getfilterquery(self):
        query = ""
        if self.filtername != "":
            query = "name:{}".format(self.filtername)
        if self.filterdescr != "":
            if query != "": query += " AND "
            query += "description:{}".format(self.filterdescr)
        if self.filtertagname1 != "":
            if query != "": query += " AND "
            query += "{}:{}".format(self.filtertagname1, self.filtertagval1)
        if self.filtertagname2 != "":
            if query != "": query += " AND "
            query += "{}:{}".format(self.filtertagname2, self.filtertagval2)
        if self.filtertagname3 != "":
            if query != "": query += " AND "
            query += "{}:{}".format(self.filtertagname3, self.filtertagval3)

        return query


class CalendarDialog(tk.simpledialog.Dialog):

    def __init__(self, parent, title, inDate):
        """Initialize the calendar with the date that is passed (i.e. that is currently set in the entry on the GUI)"""
        self.date = inDate
        super().__init__(parent, title)

    def body(self, master):
        """Crates a dialog box that displays a calendar. The highlighted date is the input date.
        In case that date was the defautl YYYY-MM-DD, then today is highlighted."""

        myDate = self.date.split("-")
        if myDate[0] == "YYYY":
            myDate = [date.today().year, date.today().month, date.today().day]

        self.calendar = Calendar(master, year=int(myDate[0]), month=int(myDate[1]), day=int(myDate[2]))
        self.calendar.pack()

    def apply(self):
        """ and returns the selected date"""

        self.result = self.calendar.selection_get()


# TODO: implement exception handling for wrong dates
def check_date(date):
    """Check whether the date contains only acceptable characters. Check whether the length of the date is correct and try to correct if possible"""

    for i in date:
        if not (i in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-"]):
            log.warning('The date contains unaccepted characters, namely {}'.format(i))

    YYYY, MM, DD = date.split("-")
    if len(YYYY) != 4:
        if len(YYYY) == 2:
            YYYY = "20" + YYYY
        else:
            log.warning('The year is not in the correct format')
    if len(MM) != 2:
        if len(MM) == 1:
            MM = "0" + MM
        else:
            log.warning('The month is not in the correct format')
    if len(DD) != 2:
        if len(DD) == 1:
            DD = "0" + DD
        else:
            log.warning('The day is not in the correct format')

    return YYYY + "-" + MM + "-" + DD


# TODO: implement exception handling for wrong times
def check_time(time):
    """Check whether the time contains only acceptable characters. Check whether the length of the date is correct and try to correct if possible"""

    for i in time:
        if not (i in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", "."]):
            log.warning('The date contains unaccepted characters, namely {}'.format(i))

    hh, mm, ss = time.split(":")
    if len(hh) != 2:
        if len(hh) == 1:
            hh = "0" + hh
        else:
            log.warning('The hour is not in the correct format')
    if len(mm) != 2:
        if len(mm) == 1:
            hh = "0" + mm
        else:
            log.warning('The minutes are not in the correct format')

    return hh + ":" + mm + ":" + ss

def getXPs(XPstring):
    # XP_20230330.61-XP_20230330.64,XP_20230228.10-XP_20230228.20
    XPpattern = r'^XP_\d{8}\.\d{1,2}$'

    XPs = []
    for piece in XPstring.split(','):
        piece = piece.strip()  # strip spaces at beginning and end
        if re.match(XPpattern, piece):
            XPs.append(piece)

        elif "-" in piece:
            range = piece.split('-')
            if len(range) == 2:
                startXP = range[0].strip()
                stopXP = range[1].strip()
                if re.match(XPpattern, startXP) and re.match(XPpattern, stopXP):
                    # The logbook handles XPid as XP_20230215.4
                    # The w7xarchive software handles XPid as 20230215.4
                    # Therefore, split the XPid on _ and pass the second part to w7xarchive.
                    tstart = w7xarchive.get_program_triggerts(startXP.split("_")[1], 0)
                    tstop = w7xarchive.get_program_triggerts(stopXP.split("_")[1], 0)
                    XPrange = w7xarchive.get_program_list(tstart-10, tstop+10, "XP")
                    for XP in XPrange:
                        # w7xarchive return the XPid with an additional 0 behind the ., e.g. 20230215.04
                        if XP['id'].split('.')[1].startswith('0'):
                            XPs.append("XP_" + XP['id'].split('.')[0] + "." + XP['id'].split('.')[1][1:])
                        else:
                            XPs.append("XP_" + XP['id'])
                else:
                    log.debug(f"cannot interpret value {startXP} or {stopXP}.")
            else:
                log.debug(f"The range in {piece} cannot be interpreted")
        else:
            log.debug(f"cannot interpret value {piece}. Moving on to next value")

    return XPs
