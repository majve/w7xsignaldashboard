import tkinter as tk
import math
import Output as Output


class Tab:
    def __init__(self, tabframe):
        self.tabframe = tabframe
        self.groups = {}

    def addGroup(self, GUI, name):
        self.groups[name] = Group(GUI, self.tabframe, self, name, len(self.groups) + 1)

    def addSystemToGroup(self, groupName, systemName):
        if groupName in self.groups:
            self.groups[groupName].addSystem(systemName)

    def setSubsystemsToSystem(self, groupName, systemName, subsystems):
        if groupName in self.groups:
            self.groups[groupName].systems_dict[systemName].setSubsystems(subsystems)


class Group:
    """ The group contains a frame, a (de)collapse-button, a plot-button
        A dictionary to which systems can be added."""

    def __init__(self, GUI, tabframe, groups, name, irow):

        self.GUI = GUI
        self.tabframe = tabframe
        self.groups = groups
        self.groupName = name

        self.frm = tk.Frame(tabframe, borderwidth=5, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=2)
        self.grid_btn = tk.Button(self.frm, text=name + " >", command=self.switchGridGroup, bg="LightsteelBlue", font='helvetica 15 bold', borderwidth=0, width=12)
        plot_btn = tk.Button(self.frm, text="Plot selected", command=self.outputGroupPlots, bg="DarkSeaGreen4")

        # GRID Group
        self.frm.grid(column=0, row=irow, sticky="nsew")
        self.grid_btn.grid(column=0, row=0, columnspan=2, sticky="w", padx=(0, 500))
        plot_btn.grid(column=1, row=0, sticky="e")

        self.systems_dict = {}

    def switchGridGroup(self):
        """If the grid is collapsed (>), then grid the group, otherwise, clear the group from the grid."""

        if ">" in self.grid_btn.cget('text'):
            self.gridGroup()
        else:
            self.clearGridGroup()

    def gridGroup(self):
        """Clear all other groups from the grid. Grid all the frames corresponding to the systems in the group."""

        for name, group in self.groups.groups.items():
            group.clearGridGroup()

        icolumn = 0
        irow = 1
        for name, system in self.systems_dict.items():
            system.frame.grid(column=icolumn, row=math.floor(irow), sticky="nsew")
            icolumn = (icolumn + 1) % 2
            irow += 0.5

        self.grid_btn.config(text=self.groupName + " v")
        self.tabframe.update()

    def clearGridGroup(self):
        """Clear the frames corresponding to the systems in the group."""

        for name, system in self.systems_dict.items():
            system.frame.grid_forget()

        self.grid_btn.config(text=self.groupName + " >")
        self.tabframe.update()

    def addSystem(self, systemName):  # Generator 1, Generator 2
        """Add a system to the list of the group."""

        self.systems_dict[systemName] = System(self.frm, systemName)

    def outputGroupPlots(self):  # Generator
        """For each system, collect all the selected signals, get the outputsettings and let the output be produced."""

        # Group (pdf): Generator
        # Sytems (figures): Generator 1, Generator 2
        # Subsystems (plots): [Endstage power, Cooling water, ...]
        # Signals (graphs): Forward power, Reflected power, ...
        for systemName, system in self.systems_dict.items():
            # All subsystem-plots of the system have to be on one figure as subplots
            subsystem_signals = {}
            for subsystem in system.subsystems_lst:  # list of subsystem objects
                # All signal-graphs of the subsystem have to be on the same plot (e.g. {Endstage power: [Forward, Reflected]} >>  the plot Endstage power contains graphs for the signals Forward and Reflected
                for signal, var in subsystem.vars.items():
                    if var.get() == 1:
                        if subsystem.name in subsystem_signals:
                            subsystem_signals[subsystem.name].append(self.groupName + "/" + systemName + "/" + subsystem.name + "/" + signal)
                        else:
                            subsystem_signals[subsystem.name] = [self.groupName + "/" + systemName + "/" + subsystem.name + "/" + signal]

            if not len(subsystem_signals) == 0:
                set = self.GUI.OutputSettings.getOutputSettings()
                specSet = self.GUI.ICRHsettings.getICRHsettings()
                output = Output.Output(systemName, subsystem_signals, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], mindim=set["mindim"], maxdim=set["maxdim"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                       showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
                output.produceOutput()
                # Update cache information on GUI
                self.GUI.update_cache_info()


class System:
    """ The system contains a frame, a select-all button, a deselect-all button
        A list is made to hold the sybsystems to this system."""

    def __init__(self, groupFrame, name):

        self.name = name
        self.subsystems_lst = []

        self.frame = tk.Frame(groupFrame, borderwidth=5, bg="LightsteelBlue")
        btn = tk.Button(self.frame, text=name, fg="midnight blue", bg="LightsteelBlue", borderwidth=0, highlightthickness=0, font='helvetica 12 bold', command=lambda bl=1: self.selectSystem(bl))
        xbtn = tk.Button(self.frame, text="x", fg="LightPink3", bg="LightsteelBlue", borderwidth=0, highlightthickness=0, command=lambda bl=0: self.selectSystem(bl))

        # GRID
        btn.grid(column=0, row=0, columnspan=1, sticky="w", pady=(10, 0))
        xbtn.grid(column=1, row=0, columnspan=1, sticky="w", pady=(10, 0))

    def selectSystem(self, bl):
        """Set the checkboxes of the subsystems of the system as specified by the boolean bl"""

        for subsystem in self.subsystems_lst:
            subsystem.selectSubsystem(bl)

        self.frame.update()

    def setSubsystems(self, subsystem_graphs_dict):  # Endstage power, Cooling water
        """Add the subsystems to the system"""

        irow = 2 + len(self.subsystems_lst)  # The first two rows of the system frame are for the label and buttons of the system itself
        for name, signals in subsystem_graphs_dict.items():
            self.subsystems_lst.append(Subsystem(name, signals, self.frame, irow))
            irow += 1


class Subsystem:
    """The subsystem contains all the signals for which the graphs will all be shown on one plot."""

    def __init__(self, name, signals, system_frm, irow):

        self.name = name
        self.subsystem_frm = tk.Frame(system_frm, bg="LightsteelBlue")

        self.vars = {}
        chks = {}

        name_btn = tk.Button(self.subsystem_frm, text=name, fg="midnight blue", bg="LightsteelBlue", borderwidth=0, highlightthickness=0, command=lambda bl=1: self.selectSubsystem(bl))
        x_btn = tk.Button(self.subsystem_frm, text="x", fg="LightPink3", bg="LightsteelBlue", borderwidth=0, highlightthickness=0, command=lambda bl=0: self.selectSubsystem(bl))

        for signal in signals:
            self.vars[signal] = tk.BooleanVar()
            chks[signal] = tk.Checkbutton(self.subsystem_frm, variable=self.vars[signal], onvalue=1, offvalue=0, text=signal, bg="LightsteelBlue")
        # GRID
        self.subsystem_frm.grid(column=0, row=irow, columnspan=4, sticky="nsew")
        name_btn.grid(column=0, row=0, columnspan=1, sticky="w", pady=(5, 0))
        x_btn.grid(column=1, row=0, columnspan=1, sticky="w", pady=(5, 0))
        i = 1
        for signal in signals:
            chks[signal].grid(column=0, row=i, columnspan=4, sticky="w", padx=30)
            i += 1

    def selectSubsystem(self, bl):
        """Set all the signals of the subsystem as specified by the boolean bl."""

        for signal, var in self.vars.items():
            var.set(bl)

        self.subsystem_frm.update()
