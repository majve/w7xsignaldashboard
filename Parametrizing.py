import w7xarchive
import numpy as np

def lbo(url, time_from, time_upto):
    paramdict = w7xarchive.get_parameters_box(url, time_from, time_upto)

    # {'values': [{'laser': {'flash_shots': '2134755', 'pc_shots': '629686', 'temperature': '25.1', 'pc_delay': '225',
    #                        'flow': '8.4', 'energy': '0.0'}}], 'dimensions': [1680174487734064621, 1680174487734064621]}

    dimlist = paramdict['dimensions']
    dim = np.array(dimlist[::2])
    valstr = paramdict['values'][0]['laser']['flow']
    val = np.array([float(valstr)])

    ok = True if len(val) > 0 else False

    return dim, val, ok


def ICRH(url, time_from, time_upto):

    try:
        paramdict = w7xarchive.get_parameters_box(url, time_from, time_upto)
        # {'label': 'parms', 'values': [{'ICRHfreq Gen1 unit': 'MHz', 'ICRHfreq Gen1 value': 37.5, 'ICRHfreq Gen2 unit': 'MHz', 'ICRHfreq Gen2 value': 37.5, 'Operational Gen1': True, 'Operational Gen2': False, 'Transmission line scenario': 5}], 'dimensions': [1659002404238002183, 1659002404238002222]}
    except Exception as e:
        print(f'exception {e}')
        return [], [], False

    dimlist = paramdict['dimensions']
    dim = np.array(dimlist[::2])

    OperationGen1 = paramdict['values'][0]['Operational Gen1']
    ICRHfreq1 = paramdict['values'][0]['ICRHfreq Gen1 value'] * 10  # value has to be 375 instead of 37.5
    OperationGen2 = paramdict['values'][0]['Operational Gen2']
    ICRHfreq2 = paramdict['values'][0]['ICRHfreq Gen2 value'] * 10

    val = np.array([OperationGen1, int(ICRHfreq1), OperationGen2, int(ICRHfreq2)])

    ok = True if len(val) > 0 else False

    return dim, val, ok