import Infoboxing

info_dict = {}

info_dict[f'Midplane/Valves/Module 2/A21 Infobox'] = {
    'type': 'info',
    'description': "",
    'inputsigs': ['Midplane/Valves/Module 2/A21 Gas type', 'Midplane/Valves/Module 2/A21 pressure low'],
    'xpos': 0.5,
    'ypos': 0.8,
    'ha': 'left',
    'boxfunc': Infoboxing.Divertorgasinfo,
    'infosetting': {'module': 'A21'}
}

modules = ['H10', 'H11', 'H20', 'H21', 'H30', 'H31', 'H40', 'H41', 'H50', 'H51']

for module in modules:
    nr = module[1]  # Extract the first digit after 'H' to determine the module number
    ypos_value = 0.8 if module.endswith('0') else 0.65

    # The low pressure gauges (cp12) are more accurate in low pressure ranges (<1 bar)
    # but do not work for most of OP2.1, so the high pressure gauges (cp11) are used by default.
    info_dict[f'Divertor/Valves/Module {nr}/{module} Infobox_OP2.1'] = {
        'type': 'info',
        'description': "",
        'inputsigs': [f'Divertor/Valves/Module {nr}/{module} gas type', f'Divertor/Valves/Module {nr}/{module} box P high'],
        'xpos': 0.5,
        'ypos': ypos_value,
        'ha': 'left',
        'boxfunc': Infoboxing.Divertorgasinfo,
        'infosetting': {'module': module}
    }
    info_dict[f'Divertor/Valves/Module {nr}/{module} Infobox_OP2.2'] = {
        'type': 'info',
        'description': "",
        'inputsigs': [f'Divertor/Valves/Module {nr}/{module} gas type', f'Divertor/Valves/Module {nr}/{module} box P low'],
        'xpos': 0.5,
        'ypos': ypos_value,
        'ha': 'left',
        'boxfunc': Infoboxing.Divertorgasinfo,
        'infosetting': {'module': module}
    }


for i in range(0, 11):
    info_dict[f'Main/Valves/Valve {i}/Gas type Infobox'] = {
        'type': 'info',
        'description': "",
        'inputsigs': [f'Main/Valves/Valve {i}/Gas type'],
        'xpos': 0.5,
        'ypos': 0.8,
        'ha': 'left',
        'boxfunc': Infoboxing.Maingasinfo,
    }

info_dict['Main/Valves/Active valves/Gas type Infobox'] = {
    'type': 'info',
    'description': "",
    'inputsigs': [f'Main/Valves/Valve {i}/Valve voltage' for i in range(0, 11)] +
                 [f'Main/Valves/Valve {i}/Gas type' for i in range(0, 11)],
    'xpos': 0.5,
    'ypos': 0.8,
    'ha': 'left',
    'boxfunc': Infoboxing.AllMainGasinfo
}

# Todo: The same as above. Avoid duplicate
info_dict['W7X/Power/Gas/Main gas type Infobox'] = {
    'type': 'info',
    'description': "",
    'inputsigs': [f'Main/Valves/Valve {i}/Valve voltage' for i in range(0, 11)] +
                 [f'Main/Valves/Valve {i}/Gas type' for i in range(0, 11)],
    'xpos': 0.3,
    'ypos': 0.8,
    'ha': 'left',
    'boxfunc': Infoboxing.AllMainGasinfo
}

# To get the sum of all the divertor voltages
modules = {
    'Module 1': ['H10', 'H11'],
    'Module 2': ['H20', 'H21'],
    'Module 3': ['H30 valve1', 'H30 valve2', 'H30 valve3', 'H30 valve4', 'H30 valve5', 'H31'],
    'Module 4': ['H40', 'H41'],
    'Module 5': ['H50 valve1', 'H50 valve2', 'H50 valve3', 'H50 valve4', 'H50 valve5',
                 'H51 valve1', 'H51 valve2', 'H51 valve3', 'H51 valve4', 'H51 valve5']
}
info_dict[f'Divertor/Valves/All valves/Gas type Infobox'] = {
    'type': 'info',
    'description': "",
    'inputsigs': [f'Divertor/Valves/{module}/{signal} Piezo V'
                  for module, signals in modules.items()
                  for signal in signals] +
                 [f'Divertor/Valves/{module}/{signal.split(" ")[0]} gas type'
                  for module, signals in modules.items()
                  for signal in signals],
    'xpos': 0.5,
    'ypos': 0.8,
    'ha': 'left',
    'boxfunc': Infoboxing.AllDivertorGasinfo
}

# Todo: The same as above. Avoid duplicate
info_dict['W7X/Power/Gas/Divertor gas type Infobox'] = {
    'type': 'info',
    'description': "",
    'inputsigs': [f'Divertor/Valves/{module}/{signal} Piezo V'
                  for module, signals in modules.items()
                  for signal in signals] +
                 [f'Divertor/Valves/{module}/{signal.split(" ")[0]} gas type'
                  for module, signals in modules.items()
                  for signal in signals],
    'xpos': 0.5,
    'ypos': 0.8,
    'ha': 'left',
    'boxfunc': Infoboxing.AllDivertorGasinfo
}