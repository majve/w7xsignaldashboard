import w7xarchive
from params_dict import params_dict
from generator_settings import GenSettings

def writeICRHParlog():
    signal = 'Generator/Generators/RF/ICRH'
    stream = params_dict[signal]['url']

    for key, val in GenSettings.items():
        start_ts, stop_ts = w7xarchive.to_timestamp([val['start'], val['stop']])
        time_interval = [start_ts, stop_ts]

        reason = "original version"
        code_release = "2024.09.19"
        # w7xarchive.create_version(stream, reason, code_release, url_autocomplete=False)
        w7xarchive.write_parameters(stream + "/V1/", val['data_dict'], time_interval, validate=False)


# writeICRHParlog()