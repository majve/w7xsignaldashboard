import Profiles as Profiles

w7x_profile_dict = {
    'Antenna/Positions/Profiles/Poincare plot': {
        'type': 'profile',
        'description': "",
        'unit': ["R [m]", "z [m]"],
        'legend': ["Poincare plot", 1, ",k"],
        'profilefunc': Profiles.get_poincare
    },
    'Antenna/Positions/Profiles/LCFS': {
        'type': 'profile',
        'description': "",
        'unit': ["R [m]", "z [m]"],
        'legend': ["Last closed flux surface", 1, "-r"],
        'profilefunc': Profiles.get_lcfs
    },
    'Antenna/Positions/Profiles/Antenna': {
        'inputsigs': ["Antenna/Positions/Antenna/Antenna head"],
        'type': 'profile',
        'description': "",
        'unit': ["R [m]", "z [m]"],
        'legend': ["ICRH antenna limiter", 0.8, "-m"],
        'profilefunc': Profiles.get_AntennaLimiter
    }


}