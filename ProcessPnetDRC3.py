import Processer as Processer
import w7xarchive
from ICRHsettings import ICRHsettings
import argparse

# python ProcessPnetDRC3.py 20240920.001

parser = argparse.ArgumentParser(
                    prog='MY Analysis',
                    description='Determine net power of ICRH antenna at DRC3',
                    epilog='Text at the bottom of help')

parser.add_argument('prog_id')

args = parser.parse_args()
programnumber = args.prog_id


# programnumber = "20240918.55"
time_from, time_upto = w7xarchive.get_program_from_to(programnumber)

# PROVIDE ICRH frequency
my_ICRHsettings = ICRHsettings()
ICRHSet = my_ICRHsettings.getICRHsettings(time_from)
print('the ICRH freq for {} is {} e-1 MHz'.format(programnumber, ICRHSet['ICRHfreq']))

# FILTER
# sanity check for the frequency that is set. The frequency depends on the plasma composition and on
# the field. So we could check if the field and the frequency we read, agree.
# magneticSettings = MagneticConfig.getMagneticConfig("XP_" + programnumber)
# signal = ['Calorimetry/Directional Couplers/DRC3 Voltages/Phase difference']
signal = ['Calorimetry/Directional Couplers/DRC3 Power/Net power']
# Processer.processSignals(signal, time_from, time_upto, ICRHSet)
