import seaborn as sns
import math
from scipy.stats import spearmanr
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import host_subplot
from mpl_toolkits import axisartist
import numpy as np
import itertools
import logging
import datetime

log = logging.getLogger("Plotter")

class Plotter:
    """ Plotter is a class since the different functions will use and update the same member variables.
        After initializing the class, first call prepfig. Then you can call plotIm, plot2D and/or plotTimeTrace.
        The other functions will be called by the class itself.
    """

    def __init__(self):
        """ Initialize the plotter. Set the seaborn style, regarding the grid, the ticks and the color palette.
            Initialize the dictionaries that will hold the signal details.
        """

        # figure style setup
        sns.set_style('whitegrid', {'axes.linewidth': 1.5,
                                    'axes.edgecolor': '.3',
                                    'xtick.direction': 'in',
                                    'xtick.top': True,
                                    'xtick.bottom': True,
                                    'ytick.direction': 'in',
                                    'xtick.major.size': 4.0,
                                    'xtick.minor.size': 2.0,
                                    'ytick.major.size': 4.0,
                                    'ytick.minor.size': 2.0, })
        sns.set_palette("colorblind")

        self.figure = None
        self.graphs = {}
        self.event_signals = {}

    def prepfig(self, nPlots, title, fig_name=None):
        """ Prepare the figure, regarding number of subplots

            Parameters
            ----------
            nPlots : uint
                the number of subplots that will go on the figure. Count 2 subplots for every image and every profile!
            title : string
                the title that goes on top of the figure
            fig_name: a unique figure name (to avoid interference between different user session of the dashboard

            Examples
            --------
            >>> prepfig(
            4, "Generator1 forward power | XP_20230330.64", "my_figure")
            """

        self.nPlots = nPlots
        log.debug(f'Creating figure for {self.nPlots} subplots')
        matplotlib.rcParams['mathtext.fontset'] = 'custom'
        matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
        matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans'

        # Create a new figure with a specific name if provided
        self.figure = plt.figure(facecolor='w', edgecolor='k', figsize=(9, 2 * self.nPlots), num=fig_name)
        self.figure.tight_layout()
        # Set the margins of the figure. The margins are not set in cm but as a fraction of the figure... Since the figure size depends on the number of plots, I had to use the formula...
        plt.subplots_adjust(top=0.85 + 0.018 * self.nPlots, right=1, bottom=0.2 - (0.03 * (self.nPlots - 1)), left=0.08, hspace=0.18, wspace=0)
        plt.clf()
        self.figure.suptitle(title)


    def plotIm(self, imName, rowI):
        """ Add an image to a subplot of the figure. Currently only supported to add one image

            Parameters
            ----------
            imName : string
                The name of the image, as it can be found in the code directory
            rowI : uint
                The number of the row where the image has to be added. The first row is 1
            Returns
            -------
            plotok : boolean
                Indicates whether the image subplot was added successfully to the figure

            Examples
            --------
            >>> plotIm(
            "Thermocouples.png", 1)
            """

        # Increase the horizontal space between the subplots, such that the x axis label of the subplot above the figure remains visible
        self.figure.subplots_adjust(hspace=0.25)

        # TODO: Currently it is only supported to show one image per subplot. Multiple figures side-by-side could be supported in future.
        # Read the image
        log.info('Reading and adding image {}'.format(imName))
        # Select two subplots on the figure
        try:
            # Select the subplot spanning two rows
            gs = GridSpec(self.nPlots, 1, figure=self.figure)
            self.ax = self.figure.add_subplot(gs[rowI - 1:rowI + 1, 0])
            plotok = True
        except Exception as e:
            log.error('{}: exception occured while adding image to figure'.format(e))
            plotok = False

        # Show the image and make the axes invisible
        img = mpimg.imread(imName)
        self.ax.imshow(img)
        self.ax.axis('off')  # Turn off axes for the image

        return plotok

    def plot2D(self, graphs, rowI=1, show=True):
        """ Add 2D plots to a subplot of the figure.

        Parameters
        ----------
        graphs : dict
            A dictionary of Signal objects, keyed by signal names.
        rowI : int, optional
            The index of the row to plot in, starting from 1.
        show : bool, optional
            Whether to show the plot interactively or prepare it for saving.
        """
        # Increase the horizontal space between the subplots, so that the x-axis label of the subplot above remains visible
        self.figure.subplots_adjust(hspace=0.30)

        # Set whether the plot will be shown
        if show:
            plt.ion()
        else:
            plt.switch_backend('Agg')  # Use 'Agg' backend for non-interactive mode

        # Select two subplots on the figure
        try:
            # Select the subplot spanning two rows
            gs = GridSpec(self.nPlots, 1, figure=self.figure)
            self.ax = self.figure.add_subplot(gs[rowI - 1:rowI + 1, 0])
            plotok = True
        except Exception as e:
            log.error(f'{e}: exception occurred while adding image to figure')
            plotok = False

        # Initialize labels for the plot
        xlabel = None
        ylabel = None

        # Iterate over the Signal objects in the provided graphs
        for key, signal in graphs.items():
            log.info(f'Adding graph {key}')

            # Determine the labels for the x and y axes from the signal's unit
            if not xlabel:
                xlabel = signal.unit  # .split('[')[0].strip()  # Extract the x-axis label from the unit
                ylabel = signal.unit  # .split('[')[1].strip(']')
            # elif xlabel != signal.unit.split('[')[0].strip() or ylabel != signal.unit.split('[')[1].strip(']'):
            elif xlabel != signal.unit or ylabel != signal.unit:
                log.warning('Not possible to add graph with different units to 2D plot. Skipping graph')
                plotok = False
                continue

            # Only plot if the signal is marked as 'ok'
            if signal.ok:
                # Plot each dimension-value pair for the signal
                for i in range(len(signal.dimensions)):
                    # Plot with legend and line attributes
                    if i == 0:
                        self.ax.plot(
                            signal.dimensions[i],
                            signal.values[i],
                            signal.legend[2],  # Line color/style
                            lw=signal.legend[1],  # Line width
                            label=signal.legend[0]  # Legend text
                        )
                    else:
                        self.ax.plot(
                            signal.dimensions[i],
                            signal.values[i],
                            signal.legend[2],  # Line color/style
                            lw=signal.legend[1]
                        )

        # Set axis labels
        self.ax.set_xlabel(xlabel)
        self.ax.set_ylabel(ylabel)

        # Add legend, default to the upper right corner
        self.addLegend()

        # Show the plot if specified
        if show:
            plt.show()

        return plotok

    def plotTimeTrace(self, graphs, mindim, maxdim, yScale, timeType, rowI=1, drawXaxis=True, show=True, trel=1, cstRegions=False):
        """ Add timetrace plots to a subplot of the figure.

        Parameters
        ----------
        graphs : dict
            A dictionary of Signal objects, keyed by signal names.
        mindim : float
            Timestamp of the minimal time that is shown on the plot.
        maxdim : float
            Timestamp of the maximal time that is shown on the plot.
        yScale : string
            Indicates whether the y-axis is "lin" or "log".
        timeType : string
            Indicates whether the time is displayed in "Abs" values or "Rel" to trel.
        rowI : int, optional
            The number of the row where the image has to be added. The first row is 1.
        show : bool, optional
            Whether the output has to be shown on the screen.
        trel : float
            In case timeType is "Rel", trel is the timestamp that all other times will be set relative to.
        cstRegions : tuple of floats, optional
            Depict the tolerance to find regions of constant values.

        Returns
        -------
        plotok : bool
            Indicates whether the timetrace subplot was added successfully to the figure.
        """
        # Store the graphs as member variable
        self.graphs = graphs

        # Set whether the plot will be shown
        if show:
            plt.ion()
        else:
            plt.switch_backend('Agg')  # Use 'Agg' backend for non-interactive mode.

        # Get the axis of the subplot to add multiple y-axes.
        self.ax = host_subplot(self.nPlots * 100 + 10 + rowI, axes_class=axisartist.Axes)

        # Convert the dim (in ns) to s and adjust for timeType
        for key, signal in self.graphs.items():
            if signal.ok:
                if timeType == 'Rel':
                    # Set the dimension relative to trel
                    signal.dimensions = [(t - trel) / 1e9 for t in signal.dimensions]
                else:
                    signal.dimensions = [t / 1e9 for t in signal.dimensions]

        # Separate event-type signals
        keys_to_remove = [key for key, signal in self.graphs.items() if signal.signal_type == 'event']
        # Extract and remove the event signals using pop
        for key in keys_to_remove:
            self.event_signals[key] = self.graphs.pop(key)

        # Create sufficient Y axes for graphs with different units on the same subplot.
        unitAxis_dict = self.createYaxes(self.ax)

        # Work graph per graph for non-event signals.
        palette = itertools.cycle(sns.color_palette())
        for key, signal in self.graphs.items():
            log.info(f'Adding graph {key}')

            if signal.ok:
                dims = signal.dimensions
                values = np.squeeze(signal.values)

                # Add the graph to the current plot, on the axis with the correct unit.
                unitAxis_dict[signal.unit].plot(dims, values, color=next(palette), label=signal.legend)

                # Find regions with rather constant values and annotate the value.
                if cstRegions[0] > 0:
                    start_times, start_values, end_times, end_values, moving_avg = self.constantfinder(
                        dims, values, threshold=cstRegions[0], timewindow=cstRegions[1]
                    )

                    # Annotate regions of constant value on the plot.
                    for start, end in zip(start_times, end_times):
                        mid_time = (start + end) / 2
                        mid_value = np.nanmean(values[(np.array(dims) >= start) & (np.array(dims) <= end)])
                        max_value = np.nanmax(values)

                        # Ignore regions that are likely noise.
                        if mid_value < 0.01 * max_value:
                            continue

                        # Plot the moving average of the region.
                        unitAxis_dict[signal.unit].plot(
                            np.array(dims)[(np.array(dims) >= start) & (np.array(dims) <= end)],
                            np.array(moving_avg)[(np.array(dims) >= start) & (np.array(dims) <= end)],
                            color="gray", linestyle='dashed'
                        )

                        # Annotate the value of the constant region in scientific notation.
                        text = f"{mid_value:.2f}"
                        exponent = np.floor(np.log10(abs(mid_value))).astype(int)
                        if not -3 < exponent < 3:
                            coeff = mid_value / (np.double(pow(10.0, exponent)))
                            text = f"{coeff:.2f}" + r"$\cdot$10$^{" + f"{exponent}" + "}$"

                        # Align the text based on its position in the plot.
                        vertalignment = 'center'
                        ymin, ymax = unitAxis_dict[signal.unit].get_ylim()
                        if mid_value - ymin < (ymax - ymin) * 0.05:
                            vertalignment = 'bottom'
                        if mid_value - ymin > (ymax - ymin) * 0.95:
                            vertalignment = 'top'

                        unitAxis_dict[signal.unit].text(
                            mid_time, mid_value, text, horizontalalignment='center',
                            verticalalignment=vertalignment, fontsize=10, fontweight='bold',
                            bbox=dict(facecolor='white', alpha=0.5)
                        )

        # Format the Y axes.
        self.formatYaxes(unitAxis_dict, yScale)

        # Format the X axis.
        self.formatXaxis(timeType, trel, mindim, maxdim, bottom=drawXaxis)

        # Mark event times with vertical bars.
        self.addEventMarks(trel)

        # Add legend, default to the upper right corner.
        self.addLegend()

        # Show the plot if specified.
        if show:
            plt.show()

        plotok = True  # TODO: Update if additional error checking is needed.
        return plotok

    def addtextboxes(self, textboxes=None):
        if textboxes:
            texts, xlocs, ylocs, has, oks = textboxes
            for key, text in texts.items():
                self._textbox(xlocs[key], ylocs[key], text, self.ax.transAxes, ha=has[key])

    # I left the rowI parameter in case multiple correlations will be supported in the future (unlikely)
    def plotCorrelation(self, x_graphs, y_graphs, x_label=None, y_label=None, rowI=1, scale="lin", data="all", coeff=[], fit=None, show=True):
        """ Add 2D plots to a subplot of the figure.

            Parameters
            ----------
            x_graphs, y_graphs : dict
                Dictionaries containing Signal objects, keyed by signal names.
            x_label, y_label : str, optional
                Labels for the x and y axes.
            rowI : int, optional
                The row index to plot in, starting from 1.
            scale : str, optional
                "lin" for linear scale or "log" for logarithmic scale.
            data : str, optional
                Specifies which data to correlate (e.g., "all", "xmaximum", "ymaximum", "xymaxima").
            coeff : list, optional
                List of correlation coefficients to compute.
            fit : str, optional
                Type of fit to apply (e.g., "linear").
            show : bool, optional
                Whether to display the plot.

            Returns
            -------
            plotok : bool
                Indicates whether the 2D subplot was added successfully.

            Examples
            --------
            >>> graphs = Reader.get_signalsOfList(['Antenna/Positions/Profiles/Poincare plot'], 1680186447514002176, 1680186452909001984)
            plotCorrelation(graphs)
        """
        # Adjust the space between the subplots for better visibility
        self.figure.subplots_adjust(hspace=0.30)

        # Set whether the plot will be shown
        if show:
            plt.ion()
        else:
            plt.switch_backend('Agg')  # Use 'Agg' for non-interactive mode.

        # Select three subplots on the figure
        try:
            # Use GridSpec for flexible subplot layout
            gs = GridSpec(self.nPlots, 1, figure=self.figure)
            self.ax = self.figure.add_subplot(gs[rowI - 1:rowI + 2, 0])  # Spans three rows
            plotok = True
        except Exception as e:
            log.error(f'{e}: exception occurred while adding image to figure')
            plotok = False

        if all(signal.ok for signal in x_graphs.values()) and all(signal.ok for signal in y_graphs.values()):
            if fit or coeff:
                # Initialize empty arrays to hold all the x and y values
                allval_x = np.array([], dtype=np.int64)
                allval_y = np.array([], dtype=np.int64)

            # Iterate over each signal in x_graphs
            for key, x_signal in x_graphs.items():
                # Retrieve the corresponding y_signal
                y_signal = y_graphs.get(key)
                if y_signal is None:
                    log.error(f"For correlation, each key should be in both x and y graphs. Missing key: {key}")
                    return False

                if data == "all":
                    size = 1
                    # If the signals have different timestamps, interpolate the signal
                    if set(x_signal.dimensions) != set(y_signal.dimensions):
                        log.warning(f"Correlation signals have different lengths for {key}. Interpolating signals.")
                        # Create a common time base
                        dim = np.unique(np.concatenate([x_signal.dimensions, y_signal.dimensions]))
                        # Interpolate the signals
                        val_x = np.interp(dim, x_signal.dimensions, x_signal.values, left=0, right=0)
                        val_y = np.interp(dim, y_signal.dimensions, y_signal.values, left=0, right=0)
                    else:
                        val_x = x_signal.values
                        val_y = y_signal.values

                elif data == "xmaximum":
                    size = 40
                    index_xmax = np.argmax(x_signal.values)
                    x_dim = x_signal.dimensions[index_xmax]
                    closest_index = np.argmin(np.abs(y_signal.dimensions - x_dim))
                    val_x = x_signal.values[index_xmax]
                    val_y = y_signal.values[closest_index]

                elif data == "ymaximum":
                    size = 40
                    index_ymax = np.argmax(y_signal.values)
                    y_dim = y_signal.dimensions[index_ymax]
                    closest_index = np.argmin(np.abs(x_signal.dimensions - y_dim))
                    val_x = x_signal.values[closest_index]
                    val_y = y_signal.values[index_ymax]

                elif data == "xymaxima":
                    size = 40
                    val_x = np.max(x_signal.values)
                    val_y = np.max(y_signal.values)

                else:
                    log.error("Data setting indicating which samples to correlate is not set properly.")
                    return False

                # Scatter plot for the current pair of x and y values.
                self.ax.scatter(val_x, val_y, s=size, label=x_signal.legend)

                # Store values for fitting or correlation analysis
                if fit or coeff:
                    allval_x = np.append(allval_x, val_x)
                    allval_y = np.append(allval_y, val_y)

            # Apply fit and/or quantify correlation if specified
            if fit:
                self.fitplot(allval_x, allval_y, fit)
            if coeff:
                self.quantifycorrelation(allval_x, allval_y, coeff)

            # Set axis labels based on the provided parameters and units
            if x_label:
                x_label = x_label.split("(")[0] + " [" + list(x_graphs.values())[0].unit.split("[")[1]
            if y_label:
                y_label = y_label.split("(")[0] + " [" + list(y_graphs.values())[0].unit.split("[")[1]

            self.ax.set_xlabel(x_label or "X-axis")
            self.ax.set_ylabel(y_label or "Y-axis")

            # Add legend, default to the upper right corner
            self.addLegend()

            # Apply scale if specified
            if scale == "log":
                self.ax.xscale('log')
                self.ax.yscale('log')

            plotok = True
        else:
            plotok = False

        # Show the plot if specified.
        if show:
            plt.show()

        return plotok

    def createYaxes(self, ax1):
        """ Create as many y-axes as necessary, corresponding to the different units on the subplot.

        Parameters
        ----------
        ax1 : axis
            The axis of the subplot that has to be manipulated.

        Returns
        -------
        unitAxis_dict : dict
            Dictionary where the key is the unit and the value is the corresponding axis object.

        Examples
        --------
        >>> import mpl_toolkits.axisartist as AA
        fig = plt.figure(1)
        axis = AA.Axes(fig, 111)
        createYaxes(axis)
        """

        unitAxis_dict = {}
        # Iterate over the units from each signal in self.graphs
        for signal in self.graphs.values():
            unit = signal.unit
            # Check if the unit already has an associated axis
            if unit not in unitAxis_dict:
                if not unitAxis_dict:
                    # Assign the first unit to the primary axis
                    log.info(f'First unit is: {unit}')
                    ax1.set_ylabel(unit)
                    unitAxis_dict[unit] = ax1
                else:
                    # Create a new Y-axis for different units
                    log.info(f'Graph with different unit added to plot! Adding: {unit}')
                    ax2 = ax1.twinx()
                    ax2.set_ylabel(unit)

                    # Adjust the position of the new axis
                    ax2.axis["right"] = ax2.new_fixed_axis(loc="right", offset=(50 * (len(unitAxis_dict) - 1), 0))
                    # plt.subplots_adjust(right=0.95 - 0.08 * len(unitAxis_dict))
                    unitAxis_dict[unit] = ax2

        # Adjust the figure to account for the additional axes
        self.figure.subplots_adjust(right=0.85 - 0.05 * (len(unitAxis_dict) - 1))
        return unitAxis_dict

    def formatYaxes(self, unitAxis_dict, yScale):
        """ (1) Determine and set the minimal and maximal unit for each axis, dependent on the min max values of the plots corresponding to the axis.
            (2) Set the number of ticks on the y axis, such that for all y axes, the ticks align with the gridlines.
            (3) Use scientific notation and move the exponent to the unit in the y axis label.

            Parameters
            ----------
            unitAxis_dict : dict
                Dictionary where the key is the unit and the value is the corresponding axis object.
            yScale : string
                Indicates whether the scale is "lin" or "log".
        """

        for unit, axis in unitAxis_dict.items():
            # (1) Set the minimal and maximal unit for each axis.
            minUnit = None
            maxUnit = None

            # If the unit is in %, the unit is limited by 0 and 100.
            if "%" in unit and yScale == "lin":
                minUnit = 0
                maxUnit = 110
                axis.set_ylim(minUnit, maxUnit)
            else:
                # For each graph in self.graphs with the current unit.
                for key, signal in self.graphs.items():
                    if signal.ok and signal.unit == unit:  # Updated line: Access `signal.ok` and `signal.unit`.
                        # Get the minimal and maximal values.
                        mingraph = np.nanmin(signal.values)  # Updated line: Use `signal.values` instead of `self.vals`.
                        maxgraph = np.nanmax(signal.values)  # Updated line: Use `signal.values` instead of `self.vals`.

                        # Update minUnit and maxUnit based on `mingraph` and `maxgraph`.
                        if minUnit is None or mingraph < minUnit:
                            if yScale == "log":
                                # For logarithmic scale, the minUnit must be greater than 0.
                                if mingraph > 0:
                                    minUnit = mingraph
                            else:
                                minUnit = mingraph
                        if maxUnit is None or maxgraph > maxUnit:
                            if yScale == "log":
                                if maxgraph > 0:
                                    maxUnit = maxgraph
                            else:
                                maxUnit = maxgraph

                # Handle some edge cases.
                if minUnit is None:
                    minUnit = 0.1
                if maxUnit is None:
                    maxUnit = 1
                if maxUnit == minUnit:
                    minUnit = minUnit - 0.5
                    maxUnit = maxUnit + 0.5

                # Extend limits to the nearest multiple of 10 in appropriate power of 10.
                powerTen = 10 ** (np.floor(np.log10(maxUnit - minUnit)))
                if yScale != "log":
                    minUnit = minUnit - minUnit % powerTen
                    maxUnit = maxUnit - maxUnit % powerTen + powerTen
                else:
                    # For logarithmic plots, give the plot some space.
                    if minUnit < powerTen:
                        minUnit *= 0.99
                    else:
                        minUnit -= powerTen
                    maxUnit += powerTen

                axis.set_ylim(minUnit, maxUnit)

                # (2) Set the number of ticks on the y-axis.
                axis.minorticks_on()
                if yScale != "log":
                    if len(unitAxis_dict) > 1:
                        step = (maxUnit - minUnit) / 5
                    else:
                        step = powerTen
                        nSteps = (maxUnit - minUnit) / step
                        while nSteps < 3:
                            step /= 2
                            nSteps = (maxUnit - minUnit) / step
                        while nSteps > 7:
                            step *= 2
                            nSteps = (maxUnit - minUnit) / step
                    axis.set_yticks(np.arange(minUnit, maxUnit + step, step))

                    # (3) Use scientific notation and move the exponent to the unit in the y-axis label.
                    axis.ticklabel_format(style='sci', axis='y', scilimits=(-3, 3), useMathText=True)
                    axis.axis["left"].offsetText.set_visible(False)
                    axis.axis["right"].offsetText.set_visible(False)

                    ax_max = max(axis.get_yticks())
                    if not ax_max == 0:
                        exponent_axis = np.floor(np.log10(abs(ax_max))).astype(int)
                        if not -3 < exponent_axis < 3:
                            exponent = ('10$^{%i}$' % (exponent_axis))
                            labelparts = axis.get_ylabel().split("[")
                            newlabel = labelparts[0] + "[" + exponent + " " + labelparts[1]
                            label = axis.set_ylabel(newlabel)

            if yScale == "log":
                axis.set_yscale('log')

    def formatXaxis(self, timeType, trel, mindim, maxdim, bottom):
        """ If requested, set the values of the x-axis relative to trel.
            Set the minimal and maximal unit for the axis to the given values
            If this axis is from the bottom subplot on the figure (or if the next subplot contains an image or 2D plot), then show the x axis label

            Parameters
            ----------
            ax: axis
                The axis object to be formatted
            timeType : string
                indicates whether the time is displayed in "Abs" values or "Rel" to trel
            trel : float
                In case timeType is "Rel", trel is the timestamp that all other times will be set relative to. trel will therefore correspond to 0 on the plot.
            mindim : float
                timestamp of the minimal time that is shown on the plot
            maxdim : float
                timestamp of the maximal time that is shown on the plot
            bottom : boolean

            Examples
            --------
            >>> formatXaxes(
            "Rel", 1680186451000000000, 1680186450000000000, 1680186453000000000, True)
            """

        # Set the limit dimensions relative to trel, in seconds
        if timeType == 'Rel':
            mindim -= trel / 1e9
            maxdim -= trel / 1e9
            # TODO the time is not always relative to t1! Sometimes tmaxval, tstart, or tmin
            xlabel = 't - $t_{rel}$ [s]'
        else:
            xlabel = 'time'

        # set limits
        self.ax.set_xlim(mindim, maxdim)

        # If this axis is from the bottom subplot on the figure (or if the next subplot contains an image or 2D plot), then show the x axis label
        if bottom:
            if timeType == 'Abs':
                # If the time is given in absolute values, show 4 ticks with labels
                deltax = (maxdim - mindim) / 4
                xticks = np.arange(mindim, maxdim + deltax, deltax).tolist()
                # If the time range exceeds 24 hours, set the unit of the x axis to day
                if maxdim - mindim > 86400:  # 86400 s = 24 hour
                    xticklabels = [datetime.datetime.utcfromtimestamp(xtick).strftime('%y/%m/%d %H:%M') for xtick in xticks]
                    xlabel = 'date time'
                elif maxdim - mindim < 10:  # s
                    xticklabels = [datetime.datetime.utcfromtimestamp(xtick).strftime('%H:%M:%S.%f')[:-4] for xtick in xticks]
                else:
                    xticklabels = [datetime.datetime.utcfromtimestamp(xtick).strftime('%H:%M:%S') for xtick in xticks]

                self.ax.set_xticks(xticks)
                self.ax.set_xticklabels(xticklabels, rotation=45)  # TODO: rotation not working

            self.ax.set_xlabel(xlabel)
        else:
            # remove x tick labels
            self.ax.set_xticklabels([])

        # Set x ticks both at top and bottom
        self.ax.tick_params(axis='x', which='both', top=True, bottom=True)

    def addEventMarks(self, trel):
        if self.event_signals:
            pos = 0
            pos_incr = 1 / (len(self.event_signals) + 1)
            for key, signal in self.event_signals.items():
                pos += pos_incr
                if signal.ok:
                    text = signal.legend
                    for t in signal.dimensions:
                        self.axvline_with_text(t, text, pos, ls=':', color='k')

    def axvline_with_text(self, t, text, pos, ls=':', color='k'):
        # Draw the vertical line
        line = self.ax.axvline(x=t, ls=ls, color=color)
        # Get the y-axis limits
        ymin, ymax = self.ax.get_ylim()
        # Add text
        self.ax.text(t,ymin + pos*(ymax - ymin), text, rotation=90, verticalalignment='center', horizontalalignment='right', color=color)
        return line

    def addLegend(self, legloc='upper right'):
        """ Add the legend to the subplot

            Parameters
            ----------
            legloc : string
                indicating the location of the legend

            Examples
            --------
            >>> addlegend(
            )
            """

        legend = self.ax.legend(loc=legloc, fancybox=True, frameon=True, framealpha=0.7, labelspacing=0.1)
        # bring legend to front
        legend.set_zorder(100)

    def addLegendTitle(self, title):
        """ Add a title to the top of the legend box

            Parameters
            ----------

            title : string

            Examples
            --------
            >>> addMarks(
            "different XPs")
            """

        self.ax.legend(title=title+":")

    def constantfinder(self, dims, values, threshold=1, timewindow=1):
        # TODO: edge case when the signal is to be considered constant over the whole range...
        """ Find the regions of the graph, where the values remain rather constant, i.e. within (threshold) % of the value range, during (timewindow) seconds

            Parameters
            ----------
            dims : numpy array of dtype _np.int64
                timestamps of the signal
            values : numpy array of dtype
                values of the signal
            threshold : float
                percentage depicting how constant the value has to remain relative to the whole value range
            timewindow : float
                time during which the value has to remain constant

            Examples
            --------
            >>> constantfinder(
            array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984]),
            array([3.31494596e-01, 3.21567363e-01, 3.26694578e-01, ..., 5.88288144e-08, 4.69324613e-08, 3.60661929e-08]),
            5, 2)
            """

        tolerance = (np.nanmax(values) - np.nanmin(values)) * threshold

        # Determine the moving average of the signal (to avoid outliers)
        avg_window = min(30, math.ceil(len(values)/20))
        moving_avg = np.convolve(values, np.ones(avg_window) / avg_window, mode='same')
        incr = int(np.floor(len(moving_avg)/10000)) if len(moving_avg) > 10000 else 1

        # Initialize the lists that will be returned
        start_times = []
        start_values = []
        end_times = []
        end_values = []

        # Loop over the timestamps
        i = 0
        while i < len(moving_avg)-1:

            # Determine the index j of the timestamp that closes that timewindow that starts at the timestamp with index i
            windowj = None
            for j, dim in enumerate(dims[i:]):
                if dim >= dims[i] + timewindow:
                    windowj = j
                    break
            # If all timestamps remained within the time window, end the loops and return the lists
            if not windowj:
                return start_times, start_values, end_times, end_values, moving_avg

            # Check if the value range of the moving average, during the timewindow stays within the tolerance
            # print("i", i)
            # print(np.nanmax(moving_avg[i:i+windowj]), " - ", np.nanmin(moving_avg[i:i+windowj]), " = ", np.nanmax(moving_avg[i:i+windowj]) - np.nanmin(moving_avg[i:i+windowj]), "  <= ", tolerance, " is ",  np.nanmax(moving_avg[i:i+windowj]) - np.nanmin(moving_avg[i:i+windowj]) <= tolerance)
            if np.nanmax(moving_avg[i:i+windowj]) - np.nanmin(moving_avg[i:i+windowj]) <= tolerance:
                # If so, find the first index j after the window, for which the value is outside of the tolerance, or is the last value
                for j in range(i+windowj, len(moving_avg)):
                    # print("j", j)
                    # print(np.nanmax(moving_avg[i:i + windowj]), " - ", np.nanmin(moving_avg[i:j]), " = ", np.nanmax(moving_avg[i:j]) - np.nanmin(moving_avg[i:j]), "  <= ", tolerance, " is ", np.nanmax(moving_avg[i:j]) - np.nanmin(moving_avg[i:j]) <= tolerance)
                    if j == len(moving_avg)-1 or np.nanmax(moving_avg[i:j]) - np.nanmin(moving_avg[i:j]) >= tolerance:
                        # for this index j, store the start and end times and values
                        start_times.append(dims[i])
                        start_values.append(moving_avg[i])
                        end_times.append(dims[j])
                        end_values.append(moving_avg[j])
                        # continue the while loop at the index after the region that was just determined
                        i = j
                        break
            # If not, continue the while loop with the next i
            else:
                i += incr

        return start_times, start_values, end_times, end_values, moving_avg

    def fitplot(self, x, y, fitsetting):
        """ Fit the x and y values according to the fit settings

            Parameters
            ----------
            x : numpy array of dtype _np.int64
            y : numpy array of dtype
            fitsetting : list
                first element is fit type. Other elements are optional and depend on the fit type

            Examples
            --------
            >>> fitplot(
            array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984]),
            array([3.31494596e-01, 3.21567363e-01, 3.26694578e-01, ..., 5.88288144e-08, 4.69324613e-08, 3.60661929e-08]),
            ["poly", 2])
            """
        # Check if fit_type is valid
        if fitsetting[0] not in ["lin", "log", "exp", "poly"]:
            raise ValueError("Invalid fit_type. Please use 'lin', 'log', 'exp', or 'poly'.")

        # Create a range of x values for the fitted curve
        x_fit = np.linspace(min(x), max(x), 1000)

        # Fit the data based on the specified type
        if fitsetting[0] == "lin":
            params = np.polyfit(x, y, 1)  # Linear fit
            y_fit = np.polyval(params, x_fit)
            fit_eq = f"Linear Fit: $y = {params[0]:.2f}x + {params[1]:.2f}$"
        elif fitsetting[0] == "log":
            params = np.polyfit(np.log(x), y, 1)  # Logarithmic fit
            y_fit = np.exp(np.polyval(params, np.log(x_fit)))
            fit_eq = f"Logarithmic Fit: $y = {np.exp(params[1]):.2f} \\cdot e^{{{params[0]:.2f}x}}$"
        elif fitsetting[0] == "exp":
            params = np.polyfit(x, np.log(y), 1)  # Exponential fit
            y_fit = np.exp(np.polyval(params, x_fit))
            fit_eq = f"Exponential Fit: $y = {np.exp(params[1]):.2f} \\cdot e^{{ {params[0]:.2f}x }}$"
        elif fitsetting[0] == "poly":
            degree = fitsetting[1]
            params = np.polyfit(x, y, degree)  # Polynomial fit
            y_fit = np.polyval(params, x_fit)
            fit_eq = "Polynomial Fit: $y = "
            for i, coeff in enumerate(params):
                if coeff != 0:
                    if i == degree:
                        fit_eq += f"{coeff:.2f}"
                    else:
                        fit_eq += f"{coeff:.2f}x^{degree - i} \\cdot "
            # fit_eq = fit_eq.replace(" \\cdot ", " \cdot ")

        # Calculate R-squared value
        residuals = y - np.polyval(params, x)
        ss_res = np.sum(residuals ** 2)
        ss_tot = np.sum((y - np.mean(y)) ** 2)
        r_squared = 1 - (ss_res / ss_tot)

        # Plot the fitted curve
        self.ax.plot(x_fit, y_fit, label=f"{fitsetting[0].capitalize()} Fit", color='red')

        # Display the fit result and R-squared value in LaTeX math mode
        fit_result = f"{fit_eq}\n$R^2 = {r_squared:.4f}$"
        self.ax.annotate(fit_result, xy=(0.1, 0.83), xycoords='axes fraction', fontsize=12, textcoords='offset points', xytext=(0, 10), ha='left')

    def quantifycorrelation(self, allval_x, allval_y, coeff):

        result = ""
        if "pearson" in coeff:
            pearsoncoeff = np.corrcoef(allval_x, allval_y)[0, 1]
            result += f"Pearson correlation = {pearsoncoeff:.4f}\n"
        if "spearman" in coeff:
            # The Spearman rank correlation coefficient, which quantifies the strength and direction of the monotonic relationship between x and y.
            # It ranges from -1 (perfectly decreasing monotonic relationship) to 1 (perfectly increasing monotonic relationship), with 0 indicating no monotonic relationship.
            # The P-value is the two-sided p-value for a hypothesis test whose null hypothesis is that the correlation between x and y is zero (no correlation).
            # If pval is very small (typically less than 0.05), it suggests that the correlation is likely not due to random chance.
            spearmancoeff, pval = spearmanr(allval_x, allval_y)
            result += f"Spearman correlation = {spearmancoeff:.4f}, p-value = {pval:.2f}\n"
        if "covariance" in coeff:
            # The covariance between x and y is a single numerical value that quantifies the strength and direction of the linear relationship between the two variables.
            # If positive, it indicates a positive linear relationship. if negative, a negative linear relationship, and if close to zero, it suggests little to no linear relationship.
            cov = np.cov(allval_x, allval_y)[0, 1]
            result += f"Covariance = {cov:.4f}\n"
        # if "chisq" in coeff:
        #     chisq = np.corrcoef(allval_x, allval_y)[0, 1]
        #     result += f"Correlation coefficient = {corrcoeff:.4f}"

        # Show the correlation coefficient on the plot
        self.ax.annotate(result, xy=(0.1, 0.65), xycoords='axes fraction', fontsize=12, textcoords='offset points', xytext=(0, 10), ha='left')

    def _textbox(self, xpos, ypos, text, transform, ha='left', va='top',
                 rotation='horizontal', bbox='default', zorder=40, color='k'):
        if bbox == 'default':
            bbox = dict(boxstyle="round", alpha=0.5,
                        ec=(0, 0, 0), fc=(0.5, 0.5, 0.5)
                        )
        self.ax.text(xpos, ypos, text,
                     transform=transform,
                     fontsize=8,
                     ha=ha, va=va,
                     rotation=rotation,
                     bbox=bbox,
                     zorder=zorder,
                     color=color,
                     )