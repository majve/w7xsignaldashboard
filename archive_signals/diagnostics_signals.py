import Calibrating as Calibrating

diagnostics_signal_dict = {
# QHF Faraday Cup Fast Ion Loss Diagnostic
    'E3/Faraday Cup/Distance/Head': {
        'url': 'Test/raw/W7X/QHF/RANGE0_DATASTREAM/0/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': -0.033,
        'valconv': 100,
        'unit': "Distance [cm]",
        'legend': "$d_{head}$"
    },
    'E3/Faraday Cup/Distance/Plate': {
        'url': 'Test/raw/W7X/QHF/RANGE1_DATASTREAM/0/',
        'type': 'timetrace',
        'description': "",
        'slope': -1,
        'intercept': 0.268,  # 0.235 - (dist-0.033)
        'valconv': 100,
        'unit': "Distance [cm]",
        'legend': "$d_{plate}$"
    },
    # 'QHF_Switch0': {
    #         '/Test/raw/W7X/QHF/SWITCH0_DATASTREAM/0/',
    # 'QHF_Switch1': {
    #         '/Test/raw/W7X/QHF/SWITCH1_DATASTREAM/0/'
    # 'QHF_Switch2': {
    #         '/Test/raw/W7X/QHF/SWITCH2_DATASTREAM/0/'
    'E3/Faraday Cup/Raw voltage/sensor 1': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/0/Channel1/',  # QHF_CH0
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Raw Voltage [V]",
        'legend': "$V_{1}$"
    },
    'E3/Faraday Cup/Raw voltage/sensor 2': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/1/Channel2/',  # QHF_CH1
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Raw Voltage [V]",
        'legend': "$V_{2}$"
    },
    'E3/Faraday Cup/Raw voltage/sensor 3': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/2/Channel3/',  # QHF_CH2
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Raw Voltage [V]",
        'legend': "$V_{3}$"
    },
    'E3/Faraday Cup/Raw voltage/sensor 4': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/3/Channel4/',  # QHF_CH3
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Raw Voltage [V]",
        'legend': "$V_{4}$"
    },
    'E3/Faraday Cup/Raw voltage/sensor 5': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/4/Channel5/',  # QHF_CH4
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Raw Voltage [V]",
        'legend': "$V_{5}$"
    },
    #The signals below could be moved to derived_signal_dict but the processing is really calibrating and not deriving. The signals above are rather redundant
    'E3/Faraday Cup/Source current/sensor 1': {
        'url': None,
        'urlRaw': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/0/Channel1/',  # QHF_CH0
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Source Current [A]",
        'legend': "$I_{1}$",
        'calibrationfunc': Calibrating.faradaycup_current,
    },
    'E3/Faraday Cup/Source current/sensor 2': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/1/Channel2/',  # QHF_CH1
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Source Current [A]",
        'legend': "$I_{2}$",
        'calibrationfunc': Calibrating.faradaycup_current,
    },
    'E3/Faraday Cup/Source current/sensor 3': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/2/Channel3/',  # QHF_CH2
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Source Current [A]",
        'legend': "$I_{3}$",
        'calibrationfunc': Calibrating.faradaycup_current,
    },
    'E3/Faraday Cup/Source current/sensor 4': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/3/Channel4/',  # QHF_CH3
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Source Current [A]",
        'legend': "$I_{4}$",
        'calibrationfunc': Calibrating.faradaycup_current,
    },
    'E3/Faraday Cup/Source current/sensor 5': {
        'url': 'Test/raw/W7X/QHF/SENSOR_DATASTREAM/4/Channel5/',  # QHF_CH4
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "Source Current [A]",
        'legend': "$I_{5}$",
        'calibrationfunc': Calibrating.faradaycup_current,
    },
    'E5/MPM/Position/FZJ-COMB3': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.71001/ACQ1-1_DATASTREAM/1/ch02/scaled',
        'type': 'timetrace',
        'description': "",
        'slope': 50,  # 50 mm per V
        'intercept': 0,
        'valconv': 1,
        'unit': "position [mm]",
        'legend': "MPM position"
    }
}
    # 'QHF_THERMOCOUPLE': {
    #         '/ArchiveDB/codac/W7X/ControlStation.2137/PLC.10_DATASTREAM/479/'