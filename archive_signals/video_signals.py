import Calibrating as Calibrating

video_signal_dict = {

    # Video AEQ10
    'Video/In vessel/Edicam/AEQ10': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2084/AEQ10.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ11
    'Video/In vessel/Edicam/AEQ11': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2085/AEQ11.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ20
    'Video/In vessel/Edicam/AEQ20': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2086/AEQ20.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ30
    'Video/In vessel/Edicam/AEQ30': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2087/AEQ30.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ31
    'Video/In vessel/Edicam/AEQ31': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2088/AEQ31.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ40
    'Video/In vessel/Edicam/AEQ40': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2083/AEQ40.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ41
    'Video/In vessel/Edicam/AEQ41': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2184/AEQ41.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ50
    'Video/In vessel/Edicam/AEQ50': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2089/AEQ50.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    },
    # Video AEQ51
    'Video/In vessel/Edicam/AEQ51': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2090/AEQ51.Camera_DATASTREAM/0/full/',
        'type': 'video',
        'description': ""
    }

}