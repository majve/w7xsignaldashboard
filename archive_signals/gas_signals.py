
gas_signal_dict = {
    'Midplane/Valves/Module 2/A21 Piezo V_OP2.2': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletReferences_DATASTREAM/133/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': "A21",
        'unit': "valve voltage [V]"
    },
    'Midplane/Valves/Module 2/A21 pressure low_OP2.2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2059/PLC.1_DATASTREAM/1236/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': "A21",
        'unit': "pressure [mbar]"
    },
    'Midplane/Valves/Module 2/A21 Gas type_OP2.2': {
        'url': 'ArchiveDB//codac/W7X/ControlStation.2059/PLC.1_DATASTREAM/415/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': "A21",
        'unit': "Gas type [a.u.]"
    },
    'Midplane/GPI/Gas Puff Imaging/Differential pressure': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.70101/FAST_PRESSURE-1_DATASTREAM/1/gauge1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': r'$\Delta$$_P$ GPI gas',
        'unit': "diff. press. [a.u.]"
    }
}

_vorder = [343, 349, 344, 350, 345, 351, 348, 346, 352, 347, 353]
# main gas valves - OP1
for i in range(0, 11):
    # gas type in gas line
    gas_signal_dict[f'Main/Valves/Valve {i}/Gas type_OP1'] = {
        'url': f'ArchiveDB/raw/W7X/CoDaStationDesc.10/DataModuleDesc.11_DATASTREAM/{_vorder[i]}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Gas type valve {i}',
        'unit': "Gas type [a.u.]"
    }
    # valve flow rates
    gas_signal_dict[f'Main/Valves/Valve {i}/Valve flow rate_OP1'] = {
        'url': f'ArchiveDB/raw/W7X/CoDaStationDesc.10/DataModuleDesc.10_DATASTREAM/{i + 22}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Flow rate valve {i}',
        'unit': "Flow rate [a.u.]"
    }
    # valve voltages
    gas_signal_dict[f'Main/Valves/Valve {i}/Valve voltage_OP1'] = {
        'url': f'ArchiveDB/codac/W7X/ControlStation.2059/DataModuleDesc.10_DATASTREAM/{i}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Voltage valve {i}',
        'unit': "Voltage [V]"
    }
    # density controller setpoints
    gas_signal_dict[f'Main/Valves/Valve {i}/Density controller setpoint_OP1'] = {
        'url': f'ArchiveDB/raw/W7X/CoDaStationDesc.10/DataModuleDesc.12_DATASTREAM/{i}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Setpoint valve {i}',
        'unit': "Setpoint [a.u.]"
    }

# main gas valves - OP2.1
for i in range(0, 11):
    # gas type in gas line
    gas_signal_dict[f'Main/Valves/Valve {i}/Gas type_OP2.1'] = {
        'url': f'ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/{_vorder[i]}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Gas type valve {i}',
        'unit': "Gas type [a.u.]"
    }
    # valve flow rates
    gas_signal_dict[f'Main/Valves/Valve {i}/Valve flow rate_OP2.1'] = {
        'url': f'ArchiveDB/raw/W7X/ControlStation.2059/PiezoValveData_DATASTREAM/{i + 11}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Flow rate valve {i}',
        'unit': "Flow rate [a.u.]"
    }
    # valve voltages
    gas_signal_dict[f'Main/Valves/Valve {i}/Valve voltage_OP2.1'] = {
        'url': f'ArchiveDB/raw/W7X/ControlStation.2059/PiezoValveVoltage_DATASTREAM/{i}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Voltage valve {i}',
        'unit': "Voltage [V]"
    }

# main gas valves - OP2.2
for i in range(0, 11):
    # gas type in gas line
    gas_signal_dict[f'Main/Valves/Valve {i}/Gas type_OP2.2'] = {
        'url': f'ArchiveDB/raw/W7X/ControlStation.2059/PLC.1_DATASTREAM/{_vorder[i]}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Gas type valve {i}',
        'unit': "Gas type [a.u.]"
    }
    # valve flow rates
    gas_signal_dict[f'Main/Valves/Valve {i}/Flow rate_OP2.2'] = {
        'url': f'ArchiveDB/raw/W7X/ControlStation.2224/PiezoValveData_DATASTREAM/{i + 11}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Flow rate valve {i}',
        'unit': "Flow rate [a.u.]"
    }
    # valve voltages
    gas_signal_dict[f'Main/Valves/Valve {i}/Valve voltage_OP2.2'] = {
        'url': f'ArchiveDB/raw/W7X/ControlStation.2224/PiezoValveVoltage_DATASTREAM/{i}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'Voltage valve {i}',
        'unit': "Voltage [V]"
    }

# He beam valves
for i in range(1, 6):
    # voltages in HM30
    gas_signal_dict[f'HeBeam/Voltages/HM30/Valve {i}'] = {
        'url': f'ArchiveDB/raw/W7X/QSQ_Hebeam/Piezo_valve_voltage_AEH30_DATASTREAM/V1/{i-1}/AEH30_{i}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'HM30 Valve {i}',
        'unit': "Voltage [V]"
    }
    # voltages in HM51
    gas_signal_dict[f'HeBeam/Voltages/HM51/Valve {i}'] = {
        'url': f'ArchiveDB/raw/W7X/QSQ_Hebeam/Piezo_valve_voltage_AEH51_DATASTREAM/V1/{i-1}/AEH51_{i}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'HM51 Valve {i}',
        'unit': "Voltage [V]"
    }

# He beam feedback setpoints
for i in range(110, 132):
    gas_signal_dict[f'HeBeam/Feedback/Density setpoint/Setpoint {i-110}'] = {
        'url': f'ArchiveDB/raw/W7X/ControlStation.2059/DivertorGasInletReferences_DATASTREAM{i}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'legend': f'HM51 Valve {i}',
        'unit': "[a.u.]"
    }

# -----------
# Divertor signal dicts constructed with loops
# Define the modules and signal types, accounting for multiple valves in H30, H50, and H51
modules = {
    'Module 1': ['H10', 'H11'],
    'Module 2': ['H20', 'H21'],
    'Module 3': ['H30 valve1', 'H30 valve2', 'H30 valve3', 'H30 valve4', 'H30 valve5', 'H31'],
    'Module 4': ['H40', 'H41'],
    'Module 5': ['H50 valve1', 'H50 valve2', 'H50 valve3', 'H50 valve4', 'H50 valve5',
                 'H51 valve1', 'H51 valve2', 'H51 valve3', 'H51 valve4', 'H51 valve5']
}

# Define URLs corresponding to each signal type
url_map = {
    'Piezo V_OP2.1': {
        'H10': '32', 'H11': '30', 'H20': '36', 'H21': '34', 'H30 valve1': '20', 'H30 valve2': '21',
        'H30 valve3': '22', 'H30 valve4': '23', 'H30 valve5': '24', 'H31': '38', 'H40': '42', 'H41': '40',
        'H50 valve1': '10', 'H50 valve2': '11', 'H50 valve3': '12', 'H50 valve4': '13', 'H50 valve5': '14',
        'H51 valve1': '0', 'H51 valve2': '1', 'H51 valve3': '2', 'H51 valve4': '3', 'H51 valve5': '4'
    },
    'Piezo V_OP2.2': {
        'H10': '38', 'H11': '37', 'H20': '40', 'H21': '39', 'H30 valve1': '32', 'H30 valve2': '33',
        'H30 valve3': '34', 'H30 valve4': '35', 'H30 valve5': '36', 'H31': '41', 'H40': '43', 'H41': '42',
        'H50 valve1': '27', 'H50 valve2': '28', 'H50 valve3': '29', 'H50 valve4': '30', 'H50 valve5': '31',
        'H51 valve1': '22', 'H51 valve2': '23', 'H51 valve3': '24', 'H51 valve4': '25', 'H51 valve5': '26'
    },
    'gas type': {
        'H10': '409', 'H11': '410', 'H20': '411', 'H21': '412', 'H30': '413', 'H31': '414',
        'H40': '416', 'H41': '417', 'H50': '418', 'H51': '419'
    },
    'box P high': {
        'H10': '644', 'H11': '653', 'H20': '782', 'H21': '791', 'H30': '920', 'H31': '929',
        'H40': '1058', 'H41': '1067', 'H50': '1196', 'H51': '1205'
    },
    'box P low_OP2.1': {
        'H10': '647', 'H11': '656', 'H20': '785', 'H21': '794', 'H30': '923', 'H31': '933',
        'H40': '1062', 'H41': '1070', 'H50': '1199', 'H51': '1208'
    },
    'box P low_OP2.2': {
        'H10': '654', 'H11': '656', 'H20': '785', 'H21': '794', 'H30': '923', 'H31': '933',
        'H40': '1062', 'H41': '1070', 'H50': '1199', 'H51': '1208'
    },
    'reference': {
        'H10': '126', 'H11': '125', 'H20': '128', 'H21': '127', 'H30 valve1': '120', 'H30 valve2': '121',
        'H30 valve3': '122', 'H30 valve4': '123', 'H30 valve5': '124', 'H31': '129',
        'H40': '131', 'H41': '130', 'H50 valve1': '115', 'H50 valve2': '116', 'H50 valve3': '117', 'H50 valve4': '118', 'H50 valve5': '119',
        'H51 valve1': '110', 'H51 valve2': '111', 'H51 valve3': '112', 'H51 valve4': '113', 'H51 valve5': '114'
    },
    'P-contribution': {
        'H10': '92', 'H11': '89', 'H20': '98', 'H21': '95', 'H30 valve1': '74', 'H30 valve2': '77',
        'H30 valve3': '80', 'H30 valve4': '83', 'H30 valve5': '86', 'H31': '101',
        'H40': '107', 'H41': '104', 'H50 valve1': '59', 'H50 valve2': '62', 'H50 valve3': '65', 'H50 valve4': '68', 'H50 valve5': '71',
        'H51 valve1': '44', 'H51 valve2': '47', 'H51 valve3': '50', 'H51 valve4': '53', 'H51 valve5': '56'
    },
    'I-contribution': {
        'H10': '93', 'H11': '90', 'H20': '99', 'H21': '96', 'H30 valve1': '75', 'H30 valve2': '78',
        'H30 valve3': '81', 'H30 valve4': '84', 'H30 valve5': '87', 'H31': '102',
        'H40': '108', 'H41': '105', 'H50 valve1': '60', 'H50 valve2': '63', 'H50 valve3': '66', 'H50 valve4': '69', 'H50 valve5': '72',
        'H51 valve1': '45', 'H51 valve2': '48', 'H51 valve3': '51', 'H51 valve4': '54', 'H51 valve5': '57'
    },
    'D-contribution': {
        'H10': '94', 'H11': '91', 'H20': '100', 'H21': '97', 'H30 valve1': '76', 'H30 valve2': '79',
        'H30 valve3': '82', 'H30 valve4': '85', 'H30 valve5': '88', 'H31': '103',
        'H40': '109', 'H41': '106', 'H50 valve1': '61', 'H50 valve2': '64', 'H50 valve3': '67', 'H50 valve4': '70', 'H50 valve5': '73',
        'H51 valve1': '46', 'H51 valve2': '49', 'H51 valve3': '52', 'H51 valve4': '55', 'H51 valve5': '58'
    }
}

for module, signals in modules.items():
    for signal in signals:
        base_signal = signal.split(' ')[0]  # Extract base signal for lookups (e.g., 'H30' from 'H30 valve1')

        # For Piezo V_OP2.1 signals
        if signal in url_map['Piezo V_OP2.1']:
            gas_signal_dict[f'Divertor/Valves/{module}/{signal} Piezo V_OP2.1'] = {
                'url': f'ArchiveDB/codac/W7X/ControlStation.2059/DivertorGasInletMeasuredValues_DATASTREAM/{url_map["Piezo V_OP2.1"][signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 26,
                'intercept': 0,
                'valconv': 1,
                'legend': signal,
                'unit': "valve voltage [V]"
            }

        # For Piezo V_OP2.2 signals
        if signal in url_map['Piezo V_OP2.2']:
            gas_signal_dict[f'Divertor/Valves/{module}/{signal} Piezo V_OP2.2'] = {
                'url': f'ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletReferences_DATASTREAM/{url_map["Piezo V_OP2.2"][signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': signal,
                'unit': "valve voltage [V]"
            }
        # For gas type signals
        if base_signal in url_map['gas type']:
            gas_signal_dict[f'Divertor/Valves/{module}/{base_signal} gas type'] = {
                'url': f'ArchiveDB/codac/W7X/ControlStation.2059/PLC.1_DATASTREAM/{url_map["gas type"][base_signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{base_signal} gas type',
                'unit': "gas [type]"
            }

        # For box P high signals
        if base_signal in url_map['box P high']:
            gas_signal_dict[f'Divertor/Valves/{module}/{base_signal} box P high'] = {
                'url': f'ArchiveDB/codac/W7X/ControlStation.2059/PLC.1_DATASTREAM/{url_map["box P high"][base_signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{base_signal}'+ '$P_{box, high\\ P\\ gauges}$',
                'unit': "pressure [mbar]"
            }

        # For box P low signals
        if base_signal in url_map['box P low_OP2.1']:
            gas_signal_dict[f'Divertor/Valves/{module}/{base_signal} box P low'] = {
                'url': f'ArchiveDB/codac/W7X/ControlStation.2059/PLC.1_DATASTREAM/{url_map["box P low_OP2.1"][base_signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{base_signal}'+ '$P_{box, low\\ P\\ gauges}$',
                'unit': "pressure [mbar]"
            }

        # For box P low signals
        if base_signal in url_map['box P low_OP2.2']:
            gas_signal_dict[f'Divertor/Valves/{module}/{base_signal} box P low'] = {
                'url': f'ArchiveDB/codac/W7X/ControlStation.2059/PLC.1_DATASTREAM/{url_map["box P low_OP2.2"][base_signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{base_signal}'+ '$P_{box, low\\ P\\ gauges}$',
                'unit': "pressure [mbar]"
            }
        # For reference signals
        if signal in url_map['reference']:
            gas_signal_dict[f'Divertor/PID feedback/{module}/{signal} reference'] = {
                'url': f'ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletReferences_DATASTREAM/{url_map["reference"][signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{signal} ref.',
                'unit': "voltage [V]"
            }

        # For P-contribution signals
        if signal in url_map['P-contribution']:
            gas_signal_dict[f'Divertor/PID feedback/{module}/{signal} P-contr.'] = {
                'url': f'ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletReferences_DATASTREAM/{url_map["P-contribution"][signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{signal} P-contr.',
                'unit': "voltage [V]"
            }

        # For I-contribution signals
        if signal in url_map['I-contribution']:
            gas_signal_dict[f'Divertor/PID feedback/{module}/{signal} I-contr.'] = {
                'url': f'ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletReferences_DATASTREAM/{url_map["I-contribution"][signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{signal} I-contr.',
                'unit': "voltage [V]"
            }

        # For D-contribution signals
        if signal in url_map['D-contribution']:
            gas_signal_dict[f'Divertor/PID feedback/{module}/{signal} D-contr.'] = {
                'url': f'ArchiveDB/raw/W7X/ControlStation.2224/DivertorGasInletReferences_DATASTREAM/{url_map["D-contribution"][signal]}/',
                'type': 'timetrace',
                'description': "",
                'slope': 1,
                'intercept': 0,
                'valconv': 1,
                'legend': f'{signal} D-contr.',
                'unit': "voltage [V]"
            }
