import w7xsignalviewer.Calibrating as Calibrating

physics_signal_dict = {

    'W7X/Power/Stored energy/Wdia': {
        'url': 'Test/raw/Minerva1/Minerva.Magnetics15.Wdia/Wdia_compensated_QXD31CE001_DATASTREAM/V1/0/Wdia_compensated_for_diamagnetic_loop_QXD31CE001',  # ArchiveDB/codac/W7X/CoDaStationDesc.16339/DataModuleDesc.16341_DATASTREAM/0/Line%20integrated%20density/',
        'url': 'ArchiveDB/views/Alias/Wdia_QXG_auto/diamagneticEnergy/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "W$_{dia}$ [kJ]",
        'legend': "W$_{dia}$"
    },
    # Interferometer electron density (slow signal with considerable time jitter but no phase wrap and low noise)
    'W7X/Environment/Density/Line integrated density': {
        'url': 'ArchiveDB/raw/W7X/ControlStation.2185/Density_DATASTREAM/0/Density',  # ArchiveDB/codac/W7X/CoDaStationDesc.16339/DataModuleDesc.16341_DATASTREAM/0/Line%20integrated%20density/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "$n_e$ [$m^{-2}$]",
        'legend': "$\\int n dl$"
    },
    # PCI Line Integrate Electron Density Fluctuations
    'W7X/Environment/Density/Line integrated density fluctuations (Det 1)': {
        'url': 'ArchiveDB/codac/W7XAnalysis/QOC_PCI/FluctuationAmplitude_Det1_DATASTREAM/0/fluc_amp/unscaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "$n_e$ [$m^{-2}$]",
        'legend': "$\\int \\tilde{n} dl$"
    },
    'W7X/Environment/Density/Line integrated density fluctuations (Detector 2)': {
        'url': 'ArchiveDB/codac/W7XAnalysis/QOC_PCI/FluctuationAmplitude_Det2_DATASTREAM/0/fluc_amp/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "$n_e$ [$m^{-2}$]",
        'legend': "$\\int \\tilde{n} dl$"
    },
    # Thomson electron density
    'W7X/Environment/Density/Thomson': {
        'url': 'ArchiveDB/raw/W7X/QTB_Profile/volume_2_DATASTREAM/V1/1/ne_map/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "$n_e$ [$m^{-2}$]",
        'legend': "$n_e$"
    },
    # Temperature
    'W7X/Environment/Line integrated temperature/Ion temperature': {
        'url': 'ArchiveDB/raw/Minerva/Minerva.IonTemperature.XICS/Ti_lineIntegrated_DATASTREAM/V2/0/signalTi/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "T [keV]",
        'legend': "$\\int T_i dl$",
    },
    'W7X/Environment/Line integrated temperature/Electron temperature': {
        'url': 'ArchiveDB/raw/Minerva/Minerva.ElectronTemperature.XICS/Te_lineIntegrated_DATASTREAM/V2/0/signalTe/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "T [keV]",
        'legend': "$\\int T_e dl$",
    },
    # Plasma vessel pressure
    'W7X/Environment/Pressure/Plasma vessel pressure': {
        'url': 'ArchiveDB/views/KKS/ADB_PlasmaVacuum/pressures/plasma vessel/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "pressure [mbar]",
        'legend': "$p_{vacuum vessel}$"
    },
    # Bolometry
    'W7X/Power/Bolometry/Total radiated power_OP2.1': {
        'url': 'Test/views/KKS/QSB_Bolometry/Prad_HBC_AEU30/',  # 'Test/raw/W7XAnalysis/QSB-Bolometry/Prad_HBC_DATASTREAM/V1/0/Prad_HBC',
        # 'Prad_OP12a': 'Test/raw/W7XAnalysis/BoloTest6/PradVBC_DATASTREAM/', 1/1e6
        # 'Prad': 'Test/raw/W7XAnalysis/BoloData/PradHBC_DATASTREAM/0/Prad_HBC/',
        # OP12b >> 'Prad_HBC': 'Test/raw/W7XAnalysis/BoloTest6/PradHBC_DATASTREAM/0/Prad_HBC/', 1/1e6
        # 'Prad_fast': 'Test/raw/W7X/QSB_Bolometry/Bolo_HBCmPrad_DATASTREAM/',
        # 'Prad_HBC': '/Test/raw/W7XAnalysis/BoloTest6/PradHBC_DATASTREAM/0/Prad_HBC/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1 / 1e3,
        'unit': "P [MW]",
        'legend': "$p_{rad}$"
    },
    'W7X/Power/Bolometry/Total radiated power_OP2.2': {
        'url': 'ArchiveDB/raw/W7XAnalysis/QSB-Bolometry/Prad_HBC_DATASTREAM/V1/0/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1 / 1e3,
        'unit': "P [MW]",
        'legend': "$p_{rad}$"
    },
    'W7X/Power/Bolometry/Inboard_OP2.1': {
        'url': 'Test/views/KKS/QSB_Bolometry/chpw/inboard/',
        # 'bolo_inneredge_OP12a': 'Test/raw/W7X/QSB_Bolometry/BoloSignal_DATASTREAM/56/E_Chan_57/', * -1e4
        # else >> 'bolo_inneredge': 'Test/raw/W7XAnalysis/BoloData/lin_VBCl_DATASTREAM/22/gch23/', *10
        # 'bolo_inneredge_fast': 'Test/raw/W7X/QSB_Bolometry/BoloPowerRaw_DATASTREAM/62/E_Chan_63/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 10,
        'unit': "radiation [a.u.]",
        'legend': "Inboard"
    },
    'W7X/Power/Bolometry/Inboard_OP2.2': {
        'url': 'ArchiveDB/raw/W7XAnalysis/QSB-Bolometry/lin_VBCl_DATASTREAM/V1/22/gch23/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 10,
        'unit': "radiation [a.u.]",
        'legend': "Inboard"
    },
    'W7X/Power/Bolometry/Outboard_OP2.1': {
        'url': 'Test/views/KKS/QSB_Bolometry/chpw/outboard/',
        # 'bolo_outeredge_OP12a': 'Test/raw/W7X/QSB_Bolometry/BoloSignal_DATASTREAM/66/E_Chan_67/', * -1e4
        # else >> 'bolo_outeredge': 'Test/raw/W7XAnalysis/BoloData/lin_VBCr_DATASTREAM/1/gch2/', *10
        # 'bolo_outeredge_fast': 'Test/raw/W7X/QSB_Bolometry/BoloPowerRaw_DATASTREAM/65/E_Chan_66/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 10,
        'unit': "radiation [a.u.]",
        'legend': "Outboard"
    },
    'W7X/Power/Bolometry/Outboard_OP2.2': {
        'url': 'ArchiveDB/raw/W7XAnalysis/QSB-Bolometry/lin_VBCr_DATASTREAM/V1/1/gch2/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 10,
        'unit': "radiation [a.u.]",
        'legend': "Outboard"
    },
    'W7X/Power/Bolometry/Core radiation_OP2.1': {
        'url': 'Test/views/KKS/QSB_Bolometry/chpw/core/',
        # 'bolo_core_OP12a': 'Test/raw/W7X/QSB_Bolometry/BoloSignal_DATASTREAM/48/E_Chan_49/', * -1e4
        # else >> 'bolo_core': 'Test/raw/W7XAnalysis/BoloData/lin_VBCr_DATASTREAM/15/gch16/', *10
        # 'bolo_core_fast': 'Test/raw/W7X/QSB_Bolometry/BoloPowerRaw_DATASTREAM/79/E_Chan_80/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 10,
        'unit': "radiation [a.u.]",
        'legend': "Core radiation"
    },
    'W7X/Power/Bolometry/Core radiation_OP2.2': {
        'url': 'ArchiveDB/raw/W7XAnalysis/QSB-Bolometry/lin_VBCl_DATASTREAM/V1/14/gch15/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 10,
        'unit': "radiation [a.u.]",
        'legend': "Core radiation"
    }
}