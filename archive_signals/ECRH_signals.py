import Calibrating as Calibrating

ECRH_signal_dict = {

# Total ECRH power
    'Gyrotrons/Power/Total/ECRH power_OP1': {
        # FROM ADRIAN. 'ECRH_tot_OP1'. Using ArchiveDB instead of Test. What is this?
        'url': 'ArchiveDB/views/KKS/CBG_ECRH/Total_power/',
        # FROM GAVIN
        # 'url': 'ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_DATASTREAM/V1/0/Ptot_ECRH/',
        # FROM ADRIAN AND GAVIN. 'ECRH_tot_old'
        # 'url': 'raw/W7X/CoDaStationDesc.18774/FeedBackProcessDesc.18770_DATASTREAM/0/ECRH%20Total%20Power/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1 / 1000,
        'unit': "P [MW]",
        'legend': "$P_{ECRH}$"
    },
# Total ECRH power OP2
    'Gyrotrons/Power/Total/ECRH power_OP2': {
        # FROM ADRIAN
        'url': 'Test/views/KKS/CBG_ECRH/Total_power',
        # 'url': 'Test/raw/W7X/CBG_ECRH/TotalPower_DATASTREAM/V1/Ptot_ECRH/',  # temporary stream for start of OP2.1
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1 / 1000,  # ?
        'unit': "P [MW]",
        'legend': "$P_{ECRH}$"
    },
# Maja: not useable as signal that should return dims and values
# string with active ECRH channels
# 'ECRH_active': '/ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_PARLOG/V1/parms/ActiveGyrotrons/',

# Gyrotron power OP 1
# Idem for Gavin and Adrian
    'Gyrotrons/Power/Series 1/Gyrotron A1_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.108/DataModuleDesc.240_DATASTREAM/0/Rf_A1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{A1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron B1_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.108/DataModuleDesc.240_DATASTREAM/8/Rf_B1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{B1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron C1_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.104/DataModuleDesc.236_DATASTREAM/0/Rf_C1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{C1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron D1_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.104/DataModuleDesc.236_DATASTREAM/8/Rf_D1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{D1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron E1_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.94/DataModuleDesc.209_DATASTREAM/0/Rf_E1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{E1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron F1_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.94/DataModuleDesc.209_DATASTREAM/8/Rf_F1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{F1}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron A5_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.106/DataModuleDesc.237_DATASTREAM/0/Rf_A5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{A5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron B5_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.106/DataModuleDesc.237_DATASTREAM/8/Rf_B5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{B5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron C5_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.101/DataModuleDesc.229_DATASTREAM/0/Rf_C5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{C5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron D5_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.101/DataModuleDesc.229_DATASTREAM/8/Rf_D5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{D5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron E5_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.17/DataModuleDesc.24_DATASTREAM/0/Rf_E5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{E5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron F5_OP1': {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.17/DataModuleDesc.179_DATASTREAM/4/Rf_F5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{F5}$"
    },

# Gyrotron power OP 2
# FROM ADRIAN
    'Gyrotrons/Power/Series 1/Gyrotron A1_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2155/A1B1.MedRes_DATASTREAM/0/Rf_A1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{A1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron B1_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2155/A1B1.MedRes_DATASTREAM/8/Rf_B1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{B1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron C1_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2151/C1D1.MedRes_DATASTREAM/0/Rf_C1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{C1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron D1_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2151/C1D1.MedRes_DATASTREAM/8/Rf_D1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{D1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron E1_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2070/E1F1.MedRes_DATASTREAM/0/Rf_E1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{E1}$"
    },
    'Gyrotrons/Power/Series 1/Gyrotron F1_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2070/E1F1.MedRes_DATASTREAM/8/Rf_F1/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{F1}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron A5_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2153/A5B5.MedRes_DATASTREAM/0/Rf_A5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{A5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron B5_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2153/A5B5.MedRes_DATASTREAM/8/Rf_B5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{B5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron C5_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2149/C5D5.MedRes_DATASTREAM/0/Rf_C5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{C5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron D5_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2149/C5D5.MedRes_DATASTREAM/8/Rf_D5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{D5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron E5_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2065/E5F5.MedRes_DATASTREAM/0/Rf_E5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{E5}$"
    },
    'Gyrotrons/Power/Series 5/Gyrotron F5_OP2': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.2065/E5F5.MedRes_DATASTREAM/8/Rf_F5/scaled/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1000,
        'unit': "P [MW]",
        'legend': "$P_{F5}$"
    }
}

######

# gyrotrons
_gyros = ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'A5', 'B5', 'C5', 'D5', 'E5', 'F5']
for idx, g in enumerate(_gyros):
    # z offset of launchers
    key = f'Gyrotrons/z offset/Series {g[1]}/Gyrotron {g}'
    ECRH_signal_dict[key] = {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.69/DataModuleDesc.138_DATASTREAM/{}/Z_off_{}/scaled/'.format(34 + idx * 9, g),
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "offset [m?]",
        'legend': '$z_{off}$' + f'{g}'
    }

    # phi offset of launchers
    key = f'Gyrotrons/phi offset/Series {g[1]}/Gyrotron {g}'
    ECRH_signal_dict[key] = {
        'url': 'ArchiveDB/codac/W7X/CoDaStationDesc.69/DataModuleDesc.138_DATASTREAM/{}/Phi_tor_{}/scaled/'.format(33 + idx * 9, g),
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "offset [°]",
        'legend': '$Phi_{off}$' + f'{g}'
    }

    # ECRH power setpoints
    key = f'Gyrotrons/setpoint/Series {g[1]}/Gyrotron {g}_OP1'  # Maja added _OP1
    ECRH_signal_dict[key] = {
        'url': 'Test/raw/Data4SoftwareTest/CBG_ECRH.Test/{}_MORE_DATASTREAM/V1/5/{}Pset/'.format(g, g),
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1e6,
        'unit': "setpoint [MW]",
        'legend': "$P_{ECRH}$" + f'setpoint {g}'
    }
    # FROM ADRIAN for OP2
    key = f'Gyrotrons/setpoint/Series {g[1]}/Gyrotron {g}_OP2'  # Maja wrote OP2 instead of OP21
    ECRH_signal_dict[key] = {
        'url': f'ArchiveDB/codac/W7X/ControlStation.2067/Power.Gate_DATASTREAM/{idx}/',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1/1e3,
        'unit': "setpoint [MW]",
        'legend': f'setpoint {g}'
    }

# ECE channels
for i in range(1, 33):
    # OP1
    key = f'ECEchannels/ECE/channels/channel {i:02d}'
    if i == 12: key += ' (~core)'
    if i == 13: key += ' (~core)'
    if i == 24: key += ' (~LFS half radius)'
    key += 'OP1'
    ECRH_signal_dict[key] = {
        # Minerva-processed channels (available minutes to hours after shot)
        'url': 'ArchiveDB/raw/Minerva/Minerva.ECE.DownsampledRadiationTemperatureTimetraces/signal_DATASTREAM/V1/{:02d}/QME-ch{:02d}/'.format(i - 1, i),
        # OP1 "fast" channels (available soon after shot)
        'urlRaw': 'ArchiveDB/views/KKS/QME_ECE/standard_reduced/{:02d}/'.format(i - 1),
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "$T_e$ [keV]",
        'legend': "$T_{e,ECE}$" + f"{i:02d}",
        'calibrationfunc': None
    }
    # Added to previous as calibrated signal
    # Minerva-processed channels (available minutes to hours after shot)
    # key = f'ECE_{i:02d}_minerva'
    # ECRH_signal_dict[key] = '/ArchiveDB/raw/Minerva/Minerva.ECE.DownsampledRadiationTemperatureTimetraces/signal_DATASTREAM/V1/{:02d}/QME-ch{:02d}/'.format(i - 1, i)

    # OP2
    key = f'ECEchannels/ECE/channels/channel {i:02d}'
    if i == 12: key += ' (~core)'
    if i == 13: key += ' (~core)'
    if i == 24: key += ' (~LFS half radius)'
    key += '_OP2'
    ECRH_signal_dict[key] = {
        # Calibrated data starting OP2
        'url': 'ArchiveDB/raw/Minerva/Minerva.ECE.DownsampledRadiationTemperatureTimetraces/signal_DATASTREAM/V1/{:02d}/QME-ch{:02d}/'.format(i - 1, i),
        # Melina
        # 'url': 'ArchiveDB/raw/Minerva/Minerva.ECE.RadiationTemperatureTimetraces/signal_DATASTREAM',
        # Raw data starting OP2
        'urlRaw': f'ArchiveDB/views/KKS/QME_ECE/x2/QME-ch{i:02d}/X2_DATASTREAM/{i-1:02d}/QME-ch{i:02d}/unscaled/',
        # Melina
        # 'urlRaw': 'ArchiveDB/raw/W7X/ControlStation.2159/X2_DATASTREAM',
        'type': 'timetrace',
        'description': "",
        'slope': 1,
        'intercept': 0,
        'valconv': 1,
        'unit': "$T_e$ [keV]",
        'legend': "$T_{e,ECE}$" + f"{i:02d}",
        'calibrationfunc': None
         # Melina
         # 'calibrationfunc': Calibrating.ECE_calibration
         # 'calibrationsetting': {'channel': i}
    }
    # Added to previous as calibrated signal
    # Calibrated data starting OP2
    # key = f'ECE_{i:02d}_OP2'
    # ECRH_signal_dict[key] = f'ArchiveDB/raw/Minerva/Minerva.ECE.DownsampledRadiationTemperatureTimetraces/relative_calibrated_signal_DATASTREAM/V1/{i-1:02d}/'
