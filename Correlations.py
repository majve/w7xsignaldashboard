from dash import dcc, html
import dash_bootstrap_components as dbc
from menu_utils import get_menus_from_tabs

# Layout for the Correlations card
def correlation_layout(tabs):
    menu_x = get_menus_from_tabs(tabs, {'type': 'dropdown-menu', 'spec': 'corr_x'})
    menu_y = get_menus_from_tabs(tabs, {'type': 'dropdown-menu', 'spec': 'corr_y'})
    return html.Div([
        html.Div([
            html.Div([
                html.H5("Signals"),
                html.Div([
                    html.Label("x =", style={'margin-right': '10px'}),
                    menu_x
                ], style={'display': 'flex', 'align-items': 'left', 'margin-bottom': '10px'}),
                html.Div([
                    html.Label("y =", style={'margin-right': '10px'}),
                    menu_y
                ], style={'display': 'flex', 'align-items': 'left'}),
            ], style={'margin-bottom': '20px'}),
            html.Div([
                html.H5("Datapoints"),
                dcc.RadioItems(id='data-var', options=[
                    {'label': ' All samples', 'value': 1},
                    {'label': ' Timestamp of maximal x value per shot', 'value': 2},
                    {'label': ' Timestamp of maximal y value per shot', 'value': 3},
                    {'label': ' Maximal x value and maximal y value per shot', 'value': 4},
                ], value=1, style={'margin-top': '10px'}),
            ], style={'margin-bottom': '20px'}),
            html.Div([
                html.H5("Quantify relation"),
                dcc.Checklist(id='coeff-var', options=[
                    {'label': " Pearson's correlation coefficient", 'value': 'pearson'},
                    {'label': " Spearman's correlation coefficient (non linear)", 'value': 'spearman'},
                    {'label': ' Covariance', 'value': 'covariance'}
                ]),
            ], style={'margin-bottom': '20px'}),
            html.Div([
                html.H5("Fit"),
                dcc.RadioItems(id='fit-var', options=[
                    {'label': ' None', 'value': 0},
                    {'label': ' Linear', 'value': 1},
                    {'label': ' Logarithmic', 'value': 2},
                    {'label': ' Exponential', 'value': 3},
                    {'label': ' Polynomial of order', 'value': 4}
                ], value=0),
                dcc.Input(id='degree', type='number', value=2, style={'width': '100px'}, placeholder="Degree"),
            ], style={'margin-bottom': '20px'}),
            html.Div([
                html.Div([
                    html.Button('Plot', id='correlate-button', n_clicks=0, style={'background-color': '#006c66', 'color': 'white'}),
                ]),
            ], style={'margin-bottom': '20px'}),
        ], style={'padding': '20px'})
    ])
