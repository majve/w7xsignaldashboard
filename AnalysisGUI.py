import Output as Output
import archive_signal_dict as archive_signal_dict
import derived_signal_dict as derived_signal_dict
import tkinter as tk
import logging

log = logging.getLogger("Analyzer")


class AnalysisGUI:
    """Class definition of the Analysis GUI, where the user can select signals as variables and can define functions
    that use those variables.
    The Analysis GUI is connected to a GUI, from which it uses the other settings, such as the time window.

    Example
    -------
    Variables:
       a : 'Generator/Generator 1/Endstage power/Forward power'
       b : 'Generator/Generator 1/Endstage power/Reflected power'
    Functions:
       z : a - b
    """

    def __init__(self, GUI, signals):
        """ The initializer generates the GUI and calls its mainloop (see bottom of this function), awaiting user actions.
        """
        self.GUI = GUI

        # list of small letters in the alphabet, to name the variables, starting at a, b, c
        self.smallLetters = list(map(chr, range(97, 123)))
        self.variables = []
        self.signali = 0

        # list of capital letters in the alphabet, to name the functions, starting at Z, Y, X
        self.capitalLetters = list(map(chr, range(65, 91)))
        self.functions = []
        self.functioni = 0

        # Get the signalnames of time traces, to make a drop down menu for each variable to select the corresponding signal
        self.timetraces = archive_signal_dict.getTimetraces() + derived_signal_dict.getTimetraces()

        # Create a tk window for the GUI
        self.win = tk.Toplevel()
        self.win.wm_title("Analyzer")

        # Create a frame for the variables, and grid it in the window
        frm = tk.Frame(self.win, borderwidth=15, bg="alice blue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        frm.grid(column=0, row=0, rowspan=1, sticky="nsew")

        variables_lbl = tk.Label(frm, text="Variables", bg="alice blue", font='helvetica 15 bold')
        self.variables_frm = tk.Frame(frm, borderwidth=15, bg="alice blue")
        variables_lbl.grid(column=0, row=0, sticky="w", padx=5)
        self.variables_frm.grid(column=0, row=1, sticky="nsew")

        # Add the signals that were passed to the Analysis GUI (i.e. that were selected on the GUI) as variables
        for signal in signals:
            self.addvariable(signal)
        if not signals:
            self.addvariable()

        # Create a button such that the user can add more variables with a drop-down-menu for signal selection
        addVar_btn = tk.Button(frm, text="Add signal", width=14, bg="lightsteelblue3", command=self.addvariable)
        addVar_btn.grid(column=0, row=2, sticky="nsw", padx=20)

        # Create a frame for the function, and grid it in the window
        frm2 = tk.Frame(self.win, borderwidth=15, bg="alice blue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        frm2.grid(column=1, row=0, rowspan=1, sticky="nsew")

        function_lbl = tk.Label(frm2, text="Functions", bg="alice blue", font='helvetica 15 bold')
        self.function_frm = tk.Frame(frm2, borderwidth=15, bg="alice blue")
        function_lbl.grid(column=0, row=0, sticky="w", padx=5)
        self.function_frm.grid(column=0, row=1, columnspan=2, sticky="nsew")

        # Add a function
        self.addfunction()

        # Create a button such that the user can add more functions
        addFunc_btn = tk.Button(frm2, text="Add function", width=14, bg="lightsteelblue3", command=self.addfunction)
        addFunc_btn.grid(column=0, row=2, sticky="nsw", padx=20)

        # Create a button such that the required signals are read, the functions calculated, and the results plotted
        plotFunc_btn = tk.Button(frm2, text="Plot functions", width=10, bg="lightsteelblue3", command=self.plotfunction)
        plotFunc_btn.grid(column=1, row=2, sticky="nse", padx=20)

    def addvariable(self, signal=""):
        """ Add a variable to the frame

            Parameters
            ----------
            signal : string
                Signalname that correspond to a timetrace in archive_signal_dict or derived_signal_dict

            Examples
            --------
            >>> addvariable(
            'Generator/Generator 1/Endstage power/Forward power')
            """
        # the variables are given the names a, b, ... z, aa, bb, ...zz, aaa, etc.
        # Determine the repetition of the letter
        a = int(self.signali/len(self.smallLetters)) + 1
        # Determine the index of the letter in the list of small letters
        index = self.signali % len(self.smallLetters)
        sigName = a * self.smallLetters[index]
        # Create an object of the Variable class and add it to the list of variables
        self.variables.append(Variable(sigName, self.variables_frm, self.signali, self.timetraces, signal))
        # Increase the signal counter and update the window
        self.signali += 1
        self.win.update()

    def addfunction(self):
        """ Add a function to the frame
        """
        # Choose a capital letter for the function name, starting at the end of the alphabet
        # It is assumed the user won't make more that 26 functions.
        funcName = self.capitalLetters[25 - self.functioni]
        # Create an object of the function class and add it to the list of functions
        self.functions.append(Function(funcName, self.function_frm, self.functioni))
        # Increase the function counter and update the window
        self.functioni += 1
        self.win.update()

    def plotfunction(self):
        """ Plot the functions that are specified by the user
        """
        # get the signals in variables
        sigNames = []
        signals = []
        # TODO: only read variables that are used in the functions
        for i in range(len(self.variables)):
            # if the checkbox is not selected, skip the variable
            if not self.variables[i].check_var.get():
                continue
            # If the signal is not empty, add its name and signal to the list
            signal = self.variables[i].signal_var.get()
            if signal != "":
                sigNames.append(self.variables[i].sigName_var.get())
                signals.append(self.variables[i].signal_var.get())

        # Create a dict to pass to the Output object
        plot_signals = {"Analysis": signals}
        # Get the settings from the GUI to pass to the Output object
        set = self.GUI.OutputSettings.getOutputSettings()
        specSet = self.GUI.ICRHsettings.getICRHsettings()
        # Create the Output object
        output = Output.Output("Analysis", plot_signals, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], mindim=set["mindim"], maxdim=set["maxdim"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                               showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
        # The output object can create the output, given the variable names for the signals and the funcions
        # Note that the signals were already passed to Output in the initializer. The sigNames must correspond to the signals in plot_signals!
        output.produceAnalysisoutput(sigNames, self.functions)
        # Update cache information on GUI
        self.GUI.update_cache_info()

    # Idea: add a button that stores the functions that you have constructed, to easily recall them in the future


class Variable:
    """ Class definition of a variable, as it is shown on a certain ow of a tk frame,
    with its checkbox, entrybox for its name, drop-down-menu for its signal and label for its unit
    """

    def __init__(self, sigName, frame, irow, timetraces, signal=""):
        # Frame on which to grid the elements of the variable
        self.frame = frame
        # Checkbox to select whether the variable should be used (i.e. read)
        self.check_var = tk.IntVar(None, 1)
        check_chk = tk.Checkbutton(frame, variable=self.check_var, bg="alice blue")
        # Entry box to specify the name of the variable (i.e. a, b, c)
        self.sigName_var = tk.StringVar()
        self.sigName_var.set(sigName)
        sigName_entr = tk.Entry(frame, textvariable=self.sigName_var, width=12)
        # Label for the equality sign
        equality_lbl = tk.Label(frame, text="=", bg="alice blue")
        # drop-down-menu listing all the optional signals
        self.signal_var = tk.StringVar()
        signal_combo = tk.ttk.Combobox(frame, width=50, textvariable=self.signal_var, state='readonly')
        # Make sure that for the selected signal, the unit is shown as a label
        self.signal_unit = tk.Label(frame, text="    ", bg="alice blue")
        signal_combo.bind('<<ComboboxSelected>>', self.showunit)
        # Add the timetrace signals in the drop-down-menu
        signal_combo['values'] = timetraces

        # Shows signal as default value
        if signal != "":
            if signal in timetraces:
                select = timetraces.index(signal)
                signal_combo.current(select)
                self.showunit()
            else:
                log.error("Signal is not found in dictionaries. Ignoring this signal")
                return

        check_chk.grid(column=0, row=irow, sticky="w", padx=5)
        sigName_entr.grid(column=1, row=irow, sticky="w", padx=5)
        equality_lbl.grid(column=2, row=irow, sticky="w", padx=5)
        signal_combo.grid(column=3, row=irow, sticky="w", padx=5)
        self.signal_unit.grid(column=4, row=irow, sticky="w", padx=5)

    def showunit(self):
        """ Helper function that finds for the current signal, the unit in the signal dictionary and shows it on the GUI
        """
        # Get the unit from the dictionary that contains the signal
        signal = self.signal_var.get()
        if signal in archive_signal_dict.archive_signal_dict.keys():
            unit = archive_signal_dict.archive_signal_dict[signal]['unit']
        elif signal in derived_signal_dict.derived_signal_dict.keys():
            unit = derived_signal_dict.derived_signal_dict[signal]['unit']
        else:
            log.error("Signal nog found for analysis. Unit cannot be displayed.")
            return
        # Get the unit from in between the brackets (e.g. P [MW])
        unit = unit.split("[")[1]
        unit = unit.split("]")[0]
        # Display the unit on the frame
        self.signal_unit.config(text=unit)
        self.frame.update()


class Function:
    """ Class definition of a function, as it is shown on a certain row of a tk frame.
    The function has a checkbox, entrybox for its name, entrybox for its function definition and entrybox for its unit
    """

    def __init__(self, funcName, frame, irow):
        # Checkbox to select whether the functions should be used (i.e. plotted)
        self.check_var = tk.IntVar(None, 1)
        check_chk = tk.Checkbutton(frame, variable=self.check_var, bg="alice blue")
        # Entry box to specify the name of the function (i.e. Z, Y, X)
        self.funcName_var = tk.StringVar()
        self.funcName_var.set(funcName)
        funcName_entr = tk.Entry(frame, textvariable=self.funcName_var, width=12)
        # Label for the equality sign
        equality_lbl = tk.Label(frame, text="=", bg="alice blue")
        # Entry box to specify the function where the variables can be used (e.g. 1000*a - b)
        self.function_var = tk.StringVar()
        self.function_var.set("0")
        function_entr = tk.Entry(frame, textvariable=self.function_var, width=50)
        # Entry box to specify the unit of the function result
        self.funcUnit_var = tk.StringVar()
        self.funcUnit_var.set("a.u.")
        funcUnit_entr = tk.Entry(frame, textvariable=self.funcUnit_var, width=5)

        check_chk.grid(column=0, row=irow, sticky="w", padx=5)
        funcName_entr.grid(column=1, row=irow, sticky="w", padx=5)
        equality_lbl.grid(column=2, row=irow, sticky="w", padx=5)
        function_entr.grid(column=3, row=irow, sticky="w", padx=5)
        funcUnit_entr.grid(column=4, row=irow, sticky="w", padx=5)
