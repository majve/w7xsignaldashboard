import dash_bootstrap_components as dbc

def create_nested_menu(structure, menu_id, parent_id=""):
    """Recursively create nested dropdown menus from a given structure."""
    if isinstance(structure, dict):
        return [
            dbc.DropdownMenu(
                label=key,
                children=create_nested_menu(value, menu_id, f"{parent_id}-{key}" if parent_id else key),
                direction="right",
                toggle_style={"background-color": "white", "color": "black", "border": "2px solid white"}
            ) if isinstance(value, dict) else create_dropdown_item(value, menu_id, f"{parent_id}-{key}")
            for key, value in structure.items()
        ]
    else:
        return [create_dropdown_item(structure, menu_id, parent_id)]

def create_dropdown_item(value, menu_id, full_id):
    """Creates a dropdown item."""
    item_id = menu_id.copy()
    item_id['type'] = 'dropdown-item'
    item_id['item'] = full_id
    return dbc.DropdownMenuItem(value, id=item_id, style={"background-color": "white", "color": "black", "border": "2px solid white"})

def get_menus_from_tabs(tabs, menu_id):
    """Creates a nested signal dropdown for a menu based on the tabs structure."""
    menu_structure = {}

    # Build the hierarchical structure from the tabs for signals
    for tab_name, tab in tabs.items():
        menu_structure[tab_name] = {}
        for group in tab.groups:
            group_dict = {}
            for system in group.systems:
                system_dict = {}
                for subsystem in system.subsystems:
                    system_dict[subsystem.name] = {signal: signal for signal in subsystem.signals}
                group_dict[system.name] = system_dict
            menu_structure[tab_name][group.name] = group_dict

    return dbc.DropdownMenu(
        id=menu_id,
        label=" Select Signal",
        children=create_nested_menu(menu_structure, menu_id),
        direction="right",
        toggle_style={
            "background-color": "white",
            "color": "black",
            "border": "1px solid gray",
            "width": "255px",
            "font-size": "12px",
            "text-align": "left",
            "white-space": "nowrap",
            "overflow": "hidden",
            "text-overflow": "ellipsis",
            "padding-right": "10px"
        },
    )
