import Parametrizing as Parametrizing

params_dict = {
    'W7X/Power/Bolometry/LBO events': {
        'url': 'ArchiveDB/codac/W7X/ControlStation.70301/LASER-0_PARLOG/',
        'type': 'event',
        'description': "",
        'valconv': 1,
        'unit': "[a.u.]",
        'legend': "LBO",
        'paramfunc': Parametrizing.lbo
    },
    'Generator/Generators/RF/ICRH': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/ICRH_PARLOG',
        'type': 'parameter',
        'description': "",
        'valconv': 1,
        'unit': "[a.u.]",
        'legend': "$f_{ICRH}$",
        'paramfunc': Parametrizing.ICRH
    }
}