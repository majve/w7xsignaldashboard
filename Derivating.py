import MagneticConfig as MagneticConfig
import vmec as vmec

import os
import numpy as np
from skrf import Network
import math
import logging
import scipy
from scipy.signal import butter, filtfilt, decimate, medfilt


calibrationpath = os.path.dirname(__file__)

log = logging.getLogger("Derivation")

# TODO: Check if dimensions of input signals agree

"""This file contains the derivation functions that have to be called for specific signals, according to the specifications in derived_signal_dict.
The derivation functions follow a strict template, because the Reader will give a generic call to a derivation function. 
The derivation functions need to have as input parameters signallist, dims, vals, oks, and settings. The return value must be dim, val, ok.

Parameters
----------
signallist : list of strings
    The names correspond to the keys in dims and vals
dims : dict{string : numpy array of dtype _np.int64}
    the key is the signalname and the value is dimensions (usually timestamps)
vals : dict{string : numpy array of dtype _np.int64}
    the key is the signalname and the value is signalvalues
settings : dictionary
    Convenient way of providing different kinds of settings for the different derivation functions.
    The settings can be tailor made to the specific derivation function you need
    The helper function getSetting() [see bottom] is used to extra the desired setting from the dict

Return
------
dim : numpy array of dtype
    dimensions of calibrated signal
val : numpy array of dtype
    values after calibration
ok : Boolean
    indicating whether the calibrated signal is ok
"""

def AllMainGas(signallist, inputsignals, settings={}):

    # signallist: list of signalnames
    # dims : dict where the key is the signalname and the value is the dimension(usually times) : numpy array of dtype _np.int64
    # vals : dict where the key is the signalname and the value is the signalvalues: numpy array of dtype

    # Initialize sum of flow rates
    flow_rate_sum = np.zeros_like(inputsignals[signallist[11]].values)

    # Iterate through the first half of the signallist (valve voltages)
    for i in range(11):
        # Get the voltage values for the current valve

        valve_voltages = inputsignals[signallist[i]].values
        # Check if any voltage is greater than 10
        if np.any(valve_voltages > 10):
            # Get the flow rate values for the current valve
            valve_flow_rates = inputsignals[signallist[i+11]].values

            # print(len(valve_flow_rates))
            # Add the flow rates of this valve to the sum
            flow_rate_sum += valve_flow_rates

    # Output the summed flow rates across all valves that had voltage > 10
    # print('Summed flow rates for valves with voltage > 10:', flow_rate_sum)

    return inputsignals[signallist[11]].dimensions, flow_rate_sum, True

def voltage_to_phase_from_table(signallist, inputsignals, settings={}):
    """
    Function that checks in which domain is a given output pair, and returns
    the corresponding phase value. Reads from TXT file.
    """

    ICRHfreq = getSetting(settings, 'ICRHfreq')

    dim = inputsignals[signallist[0]].dimensions
    valphi1 = inputsignals[signallist[0]].values
    valphi2 = inputsignals[signallist[1]].values

    calibrationfile = 'Calibrationfiles/DET2_CAL_PHASE_' + str(ICRHfreq) + '_V1.txt'
    filename = os.path.join(calibrationpath, calibrationfile)

    log.debug('calibration file name {} for {}'.format(filename, ICRHfreq))
    calibration_table = np.loadtxt(filename, comments='#', delimiter='\t', )
    phases = np.zeros(valphi1.shape)
    domain_number = None

    for i in range(len(valphi1)):

        domain_number = None

        kk = 0
        domains = {}
        calib_tabs = {}
        for kk in range(len(calibration_table[:, 0])):
            domains[int(calibration_table[kk, 0])] = [[calibration_table[kk, 1], calibration_table[kk, 2]], [calibration_table[kk, 3], calibration_table[kk, 4]]]
            calib_tabs[int(calibration_table[kk, 0])] = [int(calibration_table[kk, 5]), calibration_table[kk, 6], calibration_table[kk, 7]]

        log.debug('phi1 is {}, phi2 is {}'.format(valphi1[i], valphi2[i]))
        log.debug('domains are {}'.format(domains))

        for ii in range(len(domains)):
            check_phi1 = domains[ii][0][0] <= valphi1[i] <= domains[ii][0][1]
            check_phi2 = domains[ii][1][0] <= valphi2[i] <= domains[ii][1][1]
            if check_phi1 and check_phi2:
                domain_number = ii
                break

        if domain_number is not None:
            cal_vals = calib_tabs[domain_number]
            log.debug('cal_vals {}'.format(cal_vals))
            which_output = cal_vals[0]
            log.debug('which output'.format(which_output))
            slope = cal_vals[1]
            intercept = cal_vals[2]

            if which_output == 1:
                phase = slope * valphi1[i] + intercept
            elif which_output == 2:
                phase = slope * valphi2[i] + intercept

            log.debug("Output is in domain {:d}, value is {:0.1f} deg".format(int(domain_number), float(phase)))

        else:
            phase = 222
            log.debug('domain_number is None. phase is {}'.format(phase))

        phases[i] = phase

    ok = True if len(phases) > 0 else False

    return dim, phases, ok


############################

def power_from_current_and_voltage(signallist, inputsignals, settings={}):
    dim = inputsignals[signallist[0]].dimensions
    V = inputsignals[signallist[0]].values
    I = inputsignals[signallist[1]].values

    val = np.zeros(V.shape)
    for i in range(len(V)):
        val[i] = V[i] * I[i]  # V[i]

    ok = True if len(val) > 0 else False

    return dim, val, ok


############################

def heat_exchange(signallist, inputsignals, settings={}):
    dim = inputsignals[signallist[0]].dimensions
    flow = inputsignals[signallist[0]].values  # l/min or kg/min
    T_in = inputsignals[signallist[1]].values  # °C
    T_out = inputsignals[signallist[2]].values  # °C
    C = 4184  # specific heat capacity of water, J/(kg.K) or J/(kg.°C)

    val = np.zeros(flow.shape)
    for i in range(len(flow)):
        val[i] = C * flow[i] / 60 * (T_out[i] - T_in[i])  # Heattransfer

    ok = True if len(val) > 0 else False

    return dim, val, ok


############################


def calc_Gmsr_single(signallist, inputsignals, settings={}):
    """
    Function that takes the two voltages and the phase between them at the
    input of the Garching phase detector (or splitter, depending on the
    DAQ setup) and computes physically relevant quantities on a transmission
    line, normally at the midplane between two directional couplers.

    Input:
    CH1 =       voltage 1 (FWD mix), in volts,
    CH2 =       voltage 2 (RFL mix), in volts,
    PH =        phase between them, in degrees.
    freq =      string containing the frequency: '25.0 mhz', '37.5 mhz' or
                '38.0 mhz'
    output_att = list containing additional attenuators that are sometimes
                placed on the output of each directional coupler, in dB.
                For the moment this is zero for DRC1, DRC2 and DRC3.
                THIS IS NOT THE ATTENUATION OF THE CABLE.
    corr_att =  list containing manual correction factors for the coupling value
                of each directional couplers (magnitude only), in dB.
                Negative values increase coupling. For testing only.
                Normally set to zero.
    ph_corr =   a phase correction factor used for testing, normally set to
                zero.
    CM_name =   name of the calibration file. It's in Touchstone format but
                is NOT identical to the scattering parameters of the
                corresponding measurement transmission line section.
    rev =       Boolean variable that is set to True if the user needs the
                RF quantities 'in the other direction', i. e. if the
                measurement section is mounted backwards.
    print_out = Boolean variable to have printed output.
    ignore_opposite = Boolean variable. If True, calculation of the physical
                quantities on the line ignores the coupling of the dir. cplr.
                to the wave in the opposite direction. For DRC0 and DRC1 it's
                the right setting because the DRCs were mounted that way.
                For DRC2-3 it should be for testing only.

    Output:
    Gmsr =      Complex voltage reflection cofficient, at midplane between the
                two directional couplers, looking towards the antenna.
    PFWD =      Forward (incident) power, in Watts
    PRFL =      Reflected power, in Watts
    PNET =      Net (forward minus reflected) power, in Watts

    ###########################################################################

    Note 1:     |Gmsr|^2 is the reflected power fraction.

    Note 2:     the calibration file includes the attenuation of the cables
                that connect the directional couplers to the DAQ input, the
                filters mounted on the cplrs.' output (if any) and the
                impedance mismatch at the DAQ input. If cables or filters are
                changed, the calib file must be measured anew.
    """

    # input parameters from Ivan that were not used by functions that called this function.
    # If necessary as input parameter, then pass them with settings. Otherwise define them in the function
    output_att = [0., 0.]
    corr_att = [0., 0.]
    ph_corr = 0.
    rev = False

    ICRHfreq = getSetting(settings, 'ICRHfreq')
    signal = getSetting(settings, 'signal')
    CMfile = getSetting(settings, 'CMfile')
    DRC = getSetting(settings, 'DRC')

    if DRC == 1:
        ignore_opposite = True
    elif DRC == 3:
        ignore_opposite = False

    # print(signallist[0], signallist[1], signallist[2])
    valphi = inputsignals[signallist[0]].values
    valforw = inputsignals[signallist[1]].values
    valrefl = inputsignals[signallist[2]].values

    CM = Network(CMfile)

    # This calibration is valid starting from 01/09/2022 at 14:00
    Z0 = 50.
    cnv = 1000.
    corr_att = np.asarray(corr_att)
    output_att = np.asarray(output_att)
    add_att = -(output_att + corr_att)
    att_fact = np.sqrt(np.power(10., add_att / 10.))
    freq = '{:.1f} MHz'.format(ICRHfreq / 10)

    values = {}
    values['gam_amp'] = np.zeros(valforw.shape)
    values['gam_pha'] = np.zeros(valforw.shape)
    values['pwr_fwd'] = np.zeros(valforw.shape)
    values['pwr_rfl'] = np.zeros(valforw.shape)
    values['pwr_net'] = np.zeros(valforw.shape)

    for i in range(len(valforw)):
        # Determine matrix
        b3 = att_fact[0] * valforw[i] * np.exp(1j * np.deg2rad(valphi[i] + ph_corr)) / np.sqrt(Z0)
        b4 = att_fact[1] * valrefl[i] / np.sqrt(Z0)
        b3b4 = np.array([[b3], [b4]])
        if ignore_opposite:
            Mred = np.squeeze(CM[freq].s)
            Mred[0, 1] = 0.
            Mred[1, 0] = 0.
            fr = np.squeeze(np.matmul(Mred, b3b4))
        else:
            fr = np.squeeze(np.matmul(CM[freq].s, b3b4))

        # Calculate value   s
        if rev:
            Gmsr = fr[0] / fr[1]
            PFWD = 0.5 * np.real(fr[1] * np.conjugate(fr[1]))
            PRFL = 0.5 * np.real(fr[0] * np.conjugate(fr[0]))
            PNET = 0.5 * np.real((fr[1] + fr[0]) * np.conjugate(fr[1] - fr[0]))
        else:
            Gmsr = fr[1] / fr[0]
            PFWD = 0.5 * np.real(fr[0] * np.conjugate(fr[0]))
            PRFL = 0.5 * np.real(fr[1] * np.conjugate(fr[1]))
            PNET = 0.5 * np.real((fr[0] + fr[1]) * np.conjugate(fr[0] - fr[1]))

        # log.debug('G abs = {:0.3f}, G phase = {:0.1f} deg, P.R.F. = {:0.1f}%'.format(float(abs(Gmsr)), float(rad2deg(angle(Gmsr))), float(100. * abs(Gmsr) * abs(Gmsr))))
        # log.debug('Power: FWD = {:0.1f} W, RFL = {:0.1f} W, CPL = {:0.1f} W'.format(float(PFWD), float(PRFL), float(PNET)))

        values['gam_amp'][i] = abs(Gmsr)
        values['gam_pha'][i] = np.rad2deg(np.angle(Gmsr))
        values['pwr_fwd'][i] = PFWD / cnv
        values['pwr_rfl'][i] = PRFL / cnv
        values['pwr_net'][i] = PNET / cnv

    ok = True if len(values[signal]) > 0 else False

    return inputsignals[signallist[0]].dimensions, values[signal], ok


def calc_Rload(signallist, inputsignals, settings={}):
    dim = inputsignals[signallist[0]].dimensions
    GDRC1 = inputsignals[signallist[0]].values  # complex reflection coeffcient at DRC 1
    L_trolley = inputsignals[signallist[1]].values  # antenna extension, from 0 to 0.350 meters

    # The signals are recorded with a different frequency, namely L_trolley in Hz and GDRC1 in kHz
    # Strech L_trolley to match the size of GDRC1
    L_interp = scipy.interpolate.interp1d(np.arange(L_trolley.size), L_trolley)
    L_stretch = L_interp(np.linspace(0, L_trolley.size - 1, GDRC1.size))
    # TODO: Check if the time ranges are in agreement

    ICRHfreq = getSetting(settings, 'ICRHfreq')

    # Obtain the complex propagation constant gamma on transmission line 2
    rmin_9 = 0.0495  # minor radius for 9’’ transmission line
    rmaj_9 = 0.114  # major radius for 9’’ transmission line
    sigma = 3.816e7  # conductivity for Aluminum at 20 °C, Siemens/m
    omega = 2 * np.pi * ICRHfreq
    mu_0 = 4e-7 * np.pi  # permeability of free space, H/m
    epsilon_0 = 8.854e-12  # permittivity of free space, F/m
    eta = np.sqrt(mu_0 / epsilon_0)  # impedance of free space, Ohm
    Rs = np.sqrt(omega * mu_0 / (2.0 * sigma))  # Surface resistance of conductors, Ohm*m
    alpha = 0.5 * (Rs * (1. / rmin_9 + 1. / rmaj_9) / (eta * np.log(rmaj_9 / rmin_9)))  # log is natural logarithm
    beta = omega / scipy.constants.speed_of_light
    gamma = complex(alpha, beta)

    # To obtain the complex reflection coefficient at antenna port 2
    L2_min = 95.293  # m
    L_DRC1_to_T = 5.684  # m
    # To obtain the loading resistance RL
    Z0L = 50.  # characteristic impedance of TL

    val = np.zeros(GDRC1.shape)
    for i in range(len(GDRC1)):
        # Obtain the complex reflection coefficient at antenna port 2
        L = L2_min + L_stretch[i] - L_DRC1_to_T
        Ginp = GDRC1[i] * np.exp(2. * gamma * L)

        # Obtain the loading resistance RL
        VSWR = (1. + abs(Ginp)) / (1. - abs(Ginp))  # voltage standing wave ratio
        val[i] = Z0L / VSWR  # loading resistance, Ohm

    ok = True if len(val) > 0 else False

    return dim, val, ok


############################


def NBIpowerOP1(signallist, inputsignals, settings={}):
    pass


#
#     # flag to mark if NBI fires into vessel
#     val_fired = inputsignals[signallist[0]].values
#     if not any(np.asarray(val_fired) > 0):
#         print('nothing in NBI > 0')
#         val = []
#         dim = np.array([])
#     else:
#         timeU7 = inputsignals[signallist[1]].dimension
#         timeU8 = inputsignals[signallist[2]].dimension
#         timeI7 = inputsignals[signallist[3]].dimension
#         timeI8 = inputsignals[signallist[4]].dimension
#         U7 = inputsignals[signallist[1]].values
#         U8 = inputsignals[signallist[2]].values
#         I7 = inputsignals[signallist[3]].values
#         I8 = inputsignals[signallist[4]].values
#
#         # interpolation time trace
#         dim = np.linspace(timeU7[0], timeU7[-1], 5000)  # (-0.2, timeU7[-1], 5000)
#
#         # power calculation as given by NBI group
#         val = 0.35 * (np.interp(dim, timeU7, U7, left=0, right=0) / 10 * 73 *
#                     np.interp(dim, timeI7, I7, left=0, right=0) / 10 * 0.25 +
#                     np.interp(dim, timeU8, U8, left=0, right=0) / 10 * 73 *
#                     np.interp(dim, timeI8, I8, left=0, right=0) / 10 * 0.25)
#
#     ok = True if len(dim) > 0 else False
#
#     return dim, val, ok
#
#
def NBIpowerOP2(signallist, inputsignals, settings={}):
    # 'inputsigs': ["Injector/NI20/Calorimeter/Position", "Injector/NI21/Calorimeter/Position", "Injector/NI20/Source3/P3", "Injector/NI20/Source4/P4", "Injector/NI20/Source7/P7", "Injector/NI20/Source8/P8"],

    # Calorimeter position indicates if beam fires into vessel
    interp_signallist = []
    interp_inputsignals = {}
    ok = False
    # NI20
    CalPosNI20 = inputsignals[signallist[0]].values
    if any(np.asarray(CalPosNI20) > 50.0):
        interp_signallist.append("Injector/NI20/Source3/P3")
        interp_signallist.append("Injector/NI20/Source4/P4")
        interp_inputsignals["Injector/NI20/Source3/P3"] = inputsignals["Injector/NI20/Source3/P3"]
        interp_inputsignals["Injector/NI20/Source4/P4"] = inputsignals["Injector/NI20/Source4/P4"]

    # NI21
    CalPosNI21 = inputsignals[signallist[1]].values
    if any(np.asarray(CalPosNI21) > 50.0):
        interp_signallist.append("Injector/NI21/Source7/P7")
        interp_signallist.append("Injector/NI21/Source8/P8")
        interp_inputsignals["Injector/NI21/Source7/P7"] = inputsignals["Injector/NI21/Source7/P7"]
        interp_inputsignals["Injector/NI21/Source8/P8"] = inputsignals["Injector/NI21/Source8/P8"]

    if len(interp_signallist) > 0:
        dim, val, ok = sum_interpolations(interp_signallist, interp_inputsignals, settings)

    if not ok:
        # OP2 fallback if NBI total power hasn't been calculated yet
        log.warning(('NBI total power unavailable, getting alternative streams'))
        interp_signallist = [txt.replace('P', 'Fired') for txt in interp_signallist]
        interp_inputsignals = {}
        for interp_signal in interp_signallist:
            interp_inputsignals[interp_signal] = inputsignals[interp_signal]
        # settings['factor'] = 1e6
        # settings['threshold'] = 1e4
        if len(interp_signallist) > 0:
            dim, val, ok = sum_interpolations(interp_signallist, interp_inputsignals, settings)
        else:
            dim = val = ok = False

    return dim, val, ok


def DivertorSumOfVoltages(signallist, inputsignals, settings={}):
    # You could call sum_interpolations at once as derivating function. With this implementation, you can add additional functionality if desired

    # signals from
    # 'Module 1': ['H10', 'H11'],
    # 'Module 2': ['H20', 'H21'],
    # 'Module 3': ['H30 valve1', 'H30 valve2', 'H30 valve3', 'H30 valve4', 'H30 valve5', 'H31'],
    # 'Module 4': ['H40', 'H41'],
    # 'Module 5': ['H50 valve1', 'H50 valve2', 'H50 valve3', 'H50 valve4', 'H50 valve5',
    #              'H51 valve1', 'H51 valve2', 'H51 valve3', 'H51 valve4', 'H51 valve5']

    dim, val, ok = sum_interpolations(signallist, inputsignals, settings)

    return dim, val, ok

def ECRHSumOfGyrotronPowers(signallist, inputsignals, settings={}):
    # You could call sum_interpolations at once as derivating function. With this implementation, you can add additional functionality if desired

    # signallist is ['Gyrotron A1', 'Gyrotron B1', 'Gyrotron C1', 'Gyrotron D1', 'Gyrotron E1', 'Gyrotron F1',
    #                'Gyrotron A5', 'Gyrotron B5', 'Gyrotron C5', 'Gyrotron D5', 'Gyrotron E5', 'Gyrotron F5']

    # TODO: Determine which gyrotons to consider
    # string with active ECRH channels
    # ECRH_active_url_OP1 = 'http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_PARLOG/V1/parms/ActiveGyrotrons/'
    # ECRH_active_url_OP2_V2 = 'http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_PARLOG/V2/parms/ActiveGyrotrons'
    # ECRH_active_url_OP2_V1' = 'http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_PARLOG/V1/parms/ActiveGyrotrons'
    # data = query_archive(ECRH_active_url)
    # print(data)

    dim, val, ok = sum_interpolations(signallist, inputsignals, settings)

    return dim, val, ok


def ECRHSumOfSetpoints(signallist, inputsignals, settings={}):
    # You could call sum_interpolations at once as derivating function. With this implementation, you can add additional functionality if desired

    # signallist is ['Gyrotron A1', 'Gyrotron B1', 'Gyrotron C1', 'Gyrotron D1', 'Gyrotron E1', 'Gyrotron F1',
    #                'Gyrotron A5', 'Gyrotron B5', 'Gyrotron C5', 'Gyrotron D5', 'Gyrotron E5', 'Gyrotron F5']

    # TODO: Adrian uses a different interpolation approach here, making a linspace of 5000 samples between t1 and t4. Is this necessary?
    # Get the duration from trigger 1 to trigger 4
    # shotlen = (get_t4(ival) - get_t1(ival)) / 1e9

    dim, val, ok = sum_interpolations(signallist, inputsignals, settings)

    # mask = ~np.isnan(val)
    # dim = dim[mask]
    # val = val[mask]

    return dim, val, ok


def ECE_core(signallist, inputsignals, settings={}):
    # signallist ['ECE channel 13', 'ECE channel 18']

    XPid = getSetting(settings, 'XPid')
    magneticSettings = MagneticConfig.getMagneticConfig(XPid)
    currents = getSetting(magneticSettings, 'currents')
    # TODO: Adrian always uses channel 13 for OP1.
    # TODO: do signals from OP1 always give B0<2.55 such that channel 13 is used?
    # TODO: or should I first check if XPid is in OP1 or OP2? Does vmec.get_B0 take long?
    if vmec.get_B0(currents) > 2.55:
        signal = 'ECE channels/ECE/channels/channel 18'
    else:
        signal = 'ECE channels/ECE/channels/channel 13 (~core)'
    dim = inputsignals[signal].dimensions
    val = inputsignals[signal].values
    ok = inputsignals[signal].ok

    return dim, val, ok

def LineAverage(signallist, dims, vals, oks, settings={}):
    """ The values are divided by the plasma length (case specific
    """
    dim = inputsignals[signallist[0]].dimensions
    LineIntegratedDensity = inputsignals[signallist[0]].values
    plasmalength = 1.33  # m

    val = LineIntegratedDensity / plasmalength

    ok = True if len(dim) > 0 else False

    return dim, val, ok


def Same(signallist, inputsignals, settings={}):
    """ Returns the input values as they are. Handy when signal is defined on multiple places on the GUI under different names.
    """
    return inputsignals[signallist[0]].dimensions, inputsignals[signallist[0]].values, inputsignals[signallist[0]].ok


def getSetting(settings, key):
    """ Get the value corresponding to key from the settings dictionary. Apply necessary checks

        Parameters
        ----------
        settings : dict
            Names and files of the settings
        key : string
            name of the setting

        Examples
        --------
        >>> getSetting({'ICRHfreq': 375, 'coiltype' : 'CC'}, 'ICRHfreq')
        375
        """
    if key in settings.keys():
        return settings[key]
    else:
        log.warning('{} not provided in settings for calibration. Aborting.'.format(key))
        raise Exception


def sum_interpolations(signallist, inputsignals, settings={}):
    factor = getSetting(settings, 'factor')
    threshold = getSetting(settings, 'threshold')

    mysignallist = signallist.copy()
    myinputsignals = inputsignals.copy()
    # Iterate over a copy of the list because we are gonna remove items from the list
    for signal in mysignallist:
        if not myinputsignals[signal].ok:
            mysignallist.remove(signal)
            myinputsignals.pop(signal)
            log.warning("The signal {} was not ok and is not used for sum_interpolations".format(signal))

    try:
        # build interpolation time array
        dimensions_list = [signal.dimensions for signal in myinputsignals.values()]
        dim = np.unique(np.concatenate(dimensions_list))
        # initialize output array
        val = np.zeros_like(dim, dtype=np.float64)
        # interpolate and sum
        for signal in mysignallist:
            # Interpolate the signal to the common time base using np.interp
            interpolated_values = np.interp(dim, myinputsignals[signal].dimensions, myinputsignals[signal].values, left=0, right=0) * factor
            # Add the interpolated signal to the sum
            val += interpolated_values
        if threshold is not None:
            if np.all(val < threshold):
                dim = val = np.asarray([])

        ok = True if len(val) > 0 else False

    except Exception as e:
        log.error('exception interpolating for signal: {}'.format(e))
        val = dim = np.asarray([])
        ok = False

    return dim, val, ok

#TODO: revisit this code
def CompOverTotal(signallist, inputsignals, settings={}):

    if not inputsignals[signallist[0]].ok:
        return np.empty(0), [], False

    maxSig = signallist[0]
    for i in range(len(signallist)):
        if inputsignals[signallist[i]].ok:
            if len(inputsignals[signallist[i]].dim) > len(inputsignals[maxSig].dim):
                maxSig = signallist[i]

    for i in range(len(signallist)):
       if not inputsignals[signallist[i]].ok:
            # Replace empty dims with dims[maxSig] and vals with NaN
            inputsignals[signallist[i]].dimensions = inputsignals[maxSig].dimensions
            inputsignals[signallist[i]].values = np.full(len(inputsignals[maxSig].dimensions), 0)

    interp = {}
    for i in range(len(signallist)):
        if not np.all(np.diff(inputsignals[signallist[i]].dimensions) > 0):
            log.info("The dimension is non-increasing, interpolation results may be meaningless.")

        # interp[sigName] = scipy.interpolate.interp1d(sigdims[sigName], sigvals[sigName])
        interp[signallist[i]] = np.interp(inputsignals[maxSig].dimensions, inputsignals[signallist[i]].dimensions, inputsignals[signallist[i]].values, left=np.nan, right=np.nan)  # numpy.interp(x, xp, fp, left=None, right=None, period=None)

    dim = inputsignals[maxSig].dimensions
    RadPower = interp[signallist[0]]
    ICRHPower = interp[signallist[1]]
    ECRHPower = interp[signallist[2]]
    # NBIPower = interp[signallist[3]]

    val = np.zeros(RadPower.shape)
    for i in range(len(RadPower)):
        if math.isnan(ICRHPower[i] + ECRHPower[i]) or (ICRHPower[i] + ECRHPower[i]) < 0.1:
            val[i] = 0
        else:
            val[i] = RadPower[i] / (ICRHPower[i] + ECRHPower[i])  # + NBIPower

    ok = True if len(dim) > 0 else False

    return dim, val, ok

def gpipuffs(signallist, inputsignals, settings={}):

    if inputsignals[signallist[0]].ok:
        dim = inputsignals[signallist[0]][::3].dimensions
        val = inputsignals[signallist[0]][::3].values

        dim = dim / 1e9
        # Calculate the sampling frequency
        fs = 1 / (dim[1] - dim[0])

        # Design a 4th order Butterworth low-pass filter with a cutoff frequency of 20 Hz
        [b, a] = butter(4, 20, fs=fs)  # arguments are N, Wn, fs

        # Apply the filter to the differential pressure data using zero-phase filtering
        filtered_diffp = filtfilt(b, a, val)

        # Calculate the flow rate by taking the gradient of the filtered data and applying a median filter
        flow = medfilt(0.802 * np.gradient(filtered_diffp, dim), 5)
        # Convert the time step to milliseconds
        dt = round((dim[1] - dim[0]) * 1e3)

        # Identify indices where the flow rate exceeds 10, excluding the first 20 points
        idx_flow = np.where(np.diff(flow[20:] > 10))[0][::2] + 20
        # Initialize a list to store valid puff times
        idx_puff = []
        for i in idx_flow:
            # check both criteria for each index found
            crit1 = np.mean(flow[i - 200 * dt:i - 50 * dt]) <= 0.5
            crit2 = np.min(flow[i - 100 * dt:i + 100 * dt]) >= -15

            # If both criteria are met, add the index to the puff times list
            if crit1 and crit2:
                idx_puff.append(i)
    else:
        log.debug("couldn't get GPI puff times")
        return [], [], False

    dimpuff = dim[idx_puff] * 1e9
    valpuff = val[idx_puff]
    ok = True if len(dim) > 0 else False

    return dimpuff, valpuff, ok

def MPMplunges(signallist, inputsignals, settings={}):
    """Manipulator plunge times

    Returns:
        plungtimes (str): plunge times and reasons
        ok (bool): data successfully retrieved
    """
    if inputsignals[signallist[0]].ok:

        dim = inputsignals[signallist[0]][::3].dimensions
        val = inputsignals[signallist[0]][::3].values

        # dim = dim / 1e9
        # Calculate the sampling frequency
        fs = 1e9 / (dim[1] - dim[0])

        # Design a 4th order Butterworth low-pass filter with a cutoff frequency of 20 Hz
        [b, a] = butter(1, 100, fs=fs)  # arguments are N, Wn, fs

        mpmpos = filtfilt(b, a, val)
        # mpm considered "in" where position > 130
        v = mpmpos > 130
        # find indices of transitions through target position
        indices = np.where(np.diff(v))[0]
        # time inside target position should be larger than 50ms
        sel = np.where(dim[indices[1::2]] - dim[indices[:-1:2]] > 0.05e9)[0]
        # get time index of innermost position in each interval
        tind = [indices[2 * i] + np.argmax(mpmpos[indices[2 * i]:indices[2 * i + 1]]) for i in sel]
        plungetimes = dim[tind]
        val = np.ones_like(plungetimes)
    else:
        log.debug("couldn't get MPM plunge times")
        return [], [], False

    ok = True if len(dim) > 0 else False

    return plungetimes, val, ok



# FROM ADRIAN (used to read parlog from ArchiveDB
# def query_archive(url, params=None):
#     """lowest level function to get data from archive.
#     does no version checking or anything fancy.
#
#     Args:
#         url (str): full URL to resource
#         params (dict): http request parameters
#
#     Returns:
#         data (str): json response from server
#     """
#     try:
#         # context manager ensures connection is properly closed on exception
#         with requests.Session() as s:
#             response = s.get(url, params=params,
#                              headers={'Accept': 'application/json'})
#     except requests.exceptions.ConnectionError:
#         raise ConnectionError('Archive connection issue') from None
#     if not response.ok:
#         raise FileNotFoundError(
#             f"""Request failed with status code {response.status_code},
#             label: {response.json().get('label')},
#             message: {response.json().get('message')},
#             url: {response.url}.""")
#     data = response.json()
#     if type(data) == dict:
#         data['url'] = response.url
#     return data

def ICRHpower(signallist, inputsignals, settings={}):
    gen1 = signallist[0]
    gen2 = signallist[1]

    if inputsignals[gen1].ok:
        if max(inputsignals[gen1].values) > 0.001:
            return inputsignals[gen1].dimensions, inputsignals[gen1].values, inputsignals[gen1].ok
    if inputsignals[gen2].ok:
        return inputsignals[gen2].dimensions, inputsignals[gen2].values, inputsignals[gen2].ok

    return [], [], False

def getSetting(settings, key):
    """ Get the value corresponding to key from the settings dictionary. Apply necessary checks

        Parameters
        ----------
        settings : dict
            Names and files of the settings
        key : string
            name of the setting

        Examples
        --------
        >>> getSetting({'ICRHfreq': 375, 'coiltype' : 'CC'}, 'ICRHfreq')
        375
        """
    if key in settings.keys():
        return settings[key]
    else:
        log.warning('{} not provided in settings for calibration. Aborting.'.format(key))
        raise Exception
