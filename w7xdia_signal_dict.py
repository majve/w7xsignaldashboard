# from w7xdia import zeff
# from w7xdia import xics
try:
    from w7xdia import zeff
    UseZeff = True
except ModuleNotFoundError:
    print("Could not import w7xdia.zeff. Signals from this source will not be found.")
    UseZeff = False

try:
    from w7xdia import xics
    UseXics = True
except ModuleNotFoundError:
    print("Could not import w7xdia.xics. Signals from this source will not be found.")
    UseXics = False

w7xdia_signal_dict = {}

if UseZeff:
    w7xdia_signal_dict['W7X/Environment/Zeff/H over H+He'] = {
        'w7xdiafunction': zeff.get_h_to_hhe_ratio,
        'type': 'timetrace',
        'description': "",
        'valconv': 100,
        'unit': "ratio [%]",
        'legend': "H/(H+He)",
        'calibrationfunc': zeff.compute_corrected_h_to_hhe_ratio
    }

if UseXics:
    # Temperature
    # Already in physics_signals
    # w7xdia_signal_dict['W7X/Environment/Line integrated temperature/Ion temperature (only with w7xdia.xics)'] = {
    #     'w7xdiafunction': xics.get_line_integrated_Ti,
    #     'type': 'timetrace',
    #     'description': "",
    #     'valconv': 1,
    #     'unit': "T [keV]",
    #     'legend': "$\\int T_i dl$"
    # }
    # w7xdia_signal_dict['W7X/Environment/Line integrated temperature/Electron temperature (only with w7xdia.xics)'] = {
    #     'w7xdiafunction': xics.get_line_integrated_Te,
    #     'type': 'timetrace',
    #     'description': "",
    #     'valconv': 1,
    #     'unit': "T [keV]",
    #     'legend': "$\\int T_e dl$"
    # }
    w7xdia_signal_dict['W7X/Environment/Central temperature/Ion temperature'] = {
        'w7xdiafunction': xics.get_central_Ti,
        'type': 'timetrace',
        'description': "",
        'valconv': 1,
        'unit': "T [keV]",
        'legend': "central $T_i$"
    }
    w7xdia_signal_dict['W7X/Environment/Central temperature/Electron temperature'] = {
        'w7xdiafunction': xics.get_central_Te,
        'type': 'timetrace',
        'description': "",
        'valconv': 1,
        'unit': "T [keV]",
        'legend': "central $T_e$"
    }


