import w7xarchive
import logging
from Selection import Selection

log = logging.getLogger("Outputsettings")

class OutputSettings:
    """ This class can be used to obtain all the settings that are specified on the GUI
    """

    def __init__(self, selection):
        self.my_selection = selection
        self.resolution = 100
        self.timetype = 2
        self.timewindow = 2
        self.tWindowFrom = 0
        self.tWindowTo = 20
        self.tWindowFromTrig = 1
        self.tWindowToTrig = 5
        self.Scale = 1
        self.annotate = 0
        self.threshold = 5
        self.cstwindow = 1
        self.integral = False
        self.show = True
        self.savePDF = False
        self.savePNG = False
        self.saveMP4 = False
        self.uploadcomponentlog = False
        self.saveData = False
        self.align = 1
        self.alignStart = 0.001

    def serialize(self):
        """Convert OutputSettings and the associated Selection object into a JSON-serializable dictionary."""
        return {
            'my_selection': self.my_selection.serialize(),
            'resolution': self.resolution,
            'timetype': self.timetype,
            'timewindow': self.timewindow,
            'tWindowFrom': self.tWindowFrom,
            'tWindowTo': self.tWindowTo,
            'tWindowFromTrig': self.tWindowFromTrig,
            'tWindowToTrig': self.tWindowToTrig,
            'Scale': self.Scale,
            'annotate': self.annotate,
            'threshold': self.threshold,
            'cstwindow': self.cstwindow,
            'integral': self.integral,
            'show': self.show,
            'savePDF': self.savePDF,
            'savePNG': self.savePNG,
            'saveMP4': self.saveMP4,
            'uploadcomponentlog': self.uploadcomponentlog,
            'saveData': self.saveData,
            'align': self.align,
            'alignStart': self.alignStart
        }

    @staticmethod
    def deserialize(data):
        """Reconstruct OutputSettings from a dictionary."""
        selection = Selection.deserialize(data.get('my_selection'))  # Deserialize the Selection object
        settings = OutputSettings(selection)
        settings.resolution = data.get('resolution')
        settings.timetype = data.get('timetype')
        settings.timewindow = data.get('timewindow')
        settings.tWindowFrom = data.get('tWindowFrom')
        settings.tWindowTo = data.get('tWindowTo')
        settings.tWindowFromTrig = data.get('tWindowFromTrig')
        settings.tWindowToTrig = data.get('tWindowToTrig')
        settings.Scale = data.get('Scale')
        settings.annotate = data.get('annotate')
        settings.threshold = data.get('threshold')
        settings.cstwindow = data.get('cstwindow')
        settings.integral = data.get('integral')
        settings.show = data.get('show')
        settings.savePDF = data.get('savePDF')
        settings.savePNG = data.get('savePNG')
        settings.saveMP4 = data.get('saveMP4')
        settings.uploadcomponentlog = data.get('uploadcomponentlog')
        settings.saveData = data.get('saveData')
        settings.align = data.get('align')
        settings.alignStart = data.get('alignStart')
        return settings

    def getResolution(self):
        """ Get the resolution for the signals that will be read and plotted
        """
        return self.resolution

    def getTimeType(self):
        """ Get the type of the time that is displayed on the x-axis, namely
        Absolute time, that will be shown in by a part of YYYY-MM-DD hh:mm:ss
        or Relative time, that will be shown in second relative to trel
         """
        type = ''
        if self.timetype == 1:
            type = 'Abs'
        if self.timetype == 2:
            type = 'Rel'
        return type

    def getTimeWindow(self):
        """ Get the timewindow in which the signals will be plotted, in seconds since epoch.
        - If no programID is specified, the whole time range is considered, as entered in the from and upto boxes in the Selection frame.
        - If the programID is specified
          - and automatic is selected, the timewindow from t0 to t6 is considered
          - and fixed is selected, the start and end time, relative to t1, are set as entered in the boxes on the GUI
          - and fixed triggers is selected, the timewindow is between the triggers that are entered in the boxes on the GUI
          in all of these cases, a time deltat in s can be added to the front and back of the timewindow
        """

        if self.my_selection.getCurrentProgramID() == "None":
            tmin, tmax = self.my_selection.getTimeRange()
            tmin = w7xarchive.to_timestamp(tmin) / 1e9
            tmax = w7xarchive.to_timestamp(tmax) / 1e9
        elif self.timewindow == 1:  # automatic
            tmin = self.my_selection.getProgramTriggerTs(0) / 1e9
            tmax = self.my_selection.getProgramTriggerTs(6) / 1e9
        elif self.timewindow == 2:  # fixed times from ... to ...
            tmin = self.my_selection.getProgramTriggerTs(1) / 1e9 + self.tWindowFrom
            tmax = self.my_selection.getProgramTriggerTs(1) / 1e9 + self.tWindowTo
        else:  # fixed triggers, from trigger ... upto trigger ...
            tmin = self.my_selection.getProgramTriggerTs(self.tWindowFromTrig) / 1e9
            tmax = self.my_selection.getProgramTriggerTs(self.tWindowToTrig) / 1e9

        log.debug('Time window: {} to {} '.format(tmin, tmax))

        return tmin, tmax

    def getYScale(self):
        """ Get the scale for the Y-axis, being linear or logarithmic
        """
        choice = self.Scale
        if choice == 1:
            scale = "lin"
        else:
            scale = "log"

        log.debug('Y scale is set to {} '.format(scale))

        return scale

    def getConstantRegions(self):
        """ Get the two parameters that determine how to search for constant regions, namely
        1) the threshold in percent, depicting how constant the value has to remain relative to the whole value range
        2) the timewindow during which the value has to remain constant
        """
        if self.annotate == 1:
            return float(self.threshold) / 100, float(self.cstwindow)
        else:
            return 0, 0

    def getIntegral(self):
        """ Get the boolean whether the integral of the timetraces has to be determined and printed
        """
        return self.integral

    def getShowplot(self):
        """ Get the boolean whether the plot has to be shown
        """
        return self.show

    def getSavePDF(self):
        """ Get the boolean whether the figures have to be saved as PDF
        """
        return self.savePDF

    def getSavePNG(self):
        """ Get the boolean whether the figures have to be saved as PNG
        """
        return self.savePNG

    def getSaveMP4(self):
        """ Get the boolean whether the videos have to be saved as MP4
        """
        return self.saveMP4

    def getUploadComponentlog(self):
        """ Get the boolean whether the figures have to be uploaded to the logbook as component log
        """
        return self.uploadcomponentlog

    def getUploadcompeventlog(self):
        """ Get the boolean whether the figures have to be uploaded to the logbook as component event log
        """
        return False
        # return self.GUI.uploadcompeventlog_var.get()

    def getSaveData(self):
        """ Get the boolean whether the data has to be saved to a hf5 file
        """
        return self.saveData

    def getAlignmentXPs(self):
        """ In case the signals of multiple XPs are shown in one plot, get the alignment of the signals, being either aligned on:
        - The time window around trel
        - The start of the signals, as soon as the value goes over threshold
        - The maximal value of the signals
        """
        if self.align == 1:
            align = 'Window'
        if self.align == 2:
            align = 'Start'
        if self.align == 3:
            align = 'Max'
        return align

    def getAlignmentStartXPs(self):
        """ See previous getter. Get the threshold value that indicates where the signals start
        """
        return self.alignStart

    def getOutputSettings(self):
        """Get all output settings as a dict
        """
        Outputsettings = {}
        Outputsettings["XPid"] = self.my_selection.getCurrentProgramID()
        mindim, maxdim = self.getTimeWindow()  # in s since epoch
        Outputsettings["mindim"] = mindim
        Outputsettings["maxdim"] = maxdim
        Outputsettings["res"] = self.getResolution()
        Outputsettings["timeType"] = self.getTimeType()  # Abs or Rel

        Outputsettings["yScale"] = self.getYScale()
        Outputsettings["cstRegions"] = self.getConstantRegions()
        Outputsettings["integral"] = self.getIntegral()

        Outputsettings["showOutput"] = self.getShowplot()
        Outputsettings["savePNG"] = self.getSavePNG()
        Outputsettings["savePDF"] = self.getSavePDF()
        Outputsettings["saveMP4"] = self.getSaveMP4()
        Outputsettings["uploadComponentlog"] = self.getUploadComponentlog()
        Outputsettings["uploadcompeventlog"] = self.getUploadcompeventlog()
        Outputsettings["saveData"] = self.getSaveData()

        return Outputsettings


