# Maja: I copy pasted this from Adrian Stechow's Logbookoverview code
"""
Provides a convenient interface to the VMEC web services.
Imported from PCIanalysis, commit 88208f1e9302d96ea201a537d22d631314710561
Author: Adrian von Stechow (astechow@ipp.mpg.de)
"""
try:
    from osa import Client
except Exception:
    pass
import numpy as np
import requests


def query_rest(baseurl, target, params=None):
    fullurl = baseurl + target
    response = requests.get(fullurl, params, headers={'Accept': 'application/json'})
    if not response.ok:
        raise Exception('Request failed (url: {:s}).'.format(response.url))
    data = response.json()
    if type(data) == dict:
        data['url'] = response.url
    return data


def query_vmec_rest(target, params=None):
    baseurl = 'http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1'
    return query_rest(baseurl, target, params)


def query_confencode_rest(target, params=None):
    baseurl = 'http://esb.ipp-hgw.mpg.de:8280/services/encode_w7x_config'
    return query_rest(baseurl, target, params)


def get_threeletter(currents):
    """Gets the three letter magnetic configuration code.

    Args:
        currents (list): list of coil currents

    Returns:
        threelettercode (str): string of format "EIM+252"
    """
    c = currents
    target = f'?i1={c[0]}&i2={c[1]}&i3={c[2]}&i4={c[3]}&i5={c[4]}&ia={c[5]}&ib={c[6]}'
    return query_confencode_rest(target).get('3-letter-code')


def get_vmecidstring(currents):
    """Gets the vmec id string (which doesn't necessarily correspond to an existing VMEC run)

    Args:
        currents (list): list of coil currents

    Returns:
        vmecidstring (str): string of format "1000_1000_1000_1000_+0390_+0390"
    """
    c = currents
    target = f'?i1={c[0]}&i2={c[1]}&i3={c[2]}&i4={c[3]}&i5={c[4]}&ia={c[5]}&ib={c[6]}'
    return query_confencode_rest(target).get('vmec-id-string')


def get_confid(code):
    """gets the w7x reference VMEC run ID for a given 3-letter-code"""
    # TODO: get closest VMEC run from coil current values
    code = code[0:3]
    confdict = {
        'EIM': 172,
        'EJM': 172,
        'KJM': 163,
        'KKM': 140,
        'FTM': 177
    }
    return confdict.get(code)


def get_fs_vmec(phi=0, cid=172):
    phi = phi * 180 / np.pi
    target = f'/w7x_ref_{cid}/fluxsurfaces.json?phi={phi}'
    return query_vmec_rest(target)


def get_lcfs_vmec(phi=0, cid=172):
    phi = phi * 180 / np.pi
    target = '/w7x_ref_{cid}/lcfs.json?phi={phi}'
    return query_vmec_rest(target)['lcfs'][0]


def get_reff_vmec(x, y, z, cid=172):
    """get reff values from VMEC run with given configuration ID

    Args:
        x, y, z (float OR list of float): cartesian positions, can be multiple
        cid (int): W7X VMEC reference configuration ID

    Returns:
        reff (list of float): list of reff values for input coordinates
    """
    try:  # check if numpy array or list
        iter(x)
    except TypeError:  # is float or int
        x = str(x)
        y = str(y)
        z = str(z)
    else:  # is iterable
        x = ','.join(map(str, x))
        y = ','.join(map(str, y))
        z = ','.join(map(str, z))
    target = f'/w7x_ref_{cid}/reff.json?x={x}&y={y}&z={z}'
    return query_vmec_rest(target)['reff']


def get_reff_flt(x, y, z, currents):
    # W7-X sc coils, as-built + DW+CD+e/m, KJM001 <2.5 T>, source: ckb 1_28_7: 1852 - 1921
    # W7-X sc coils, as-built + DW+CD+e/m, KKM001 <2.33 T>, source: ckb 1_28_8: 2062 - 2131
    # W7-X sc coils, as-built + DW+CD+e/m, EIM <2.5 T>, "standard" stiffness, source: ckb 1_28_7: 1362 - 1431
    # W7-X sc coils, as-built + DW+CD+e/m, EIM <2.5 T>, modified stiffness, source: ckb 1_28_7: 1432 - 1501
    flt = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')
    # definition of points
    p = flt.types.Points3D()
    p.x1 = x
    p.x2 = y
    p.x3 = z
    print('calling field line tracer for x, y, z = {}, {}, {}'.format(x, y, z), flush=True)
    # definition of magnetic config
    coils = range(1362, 1432)
    npc = [c * 108 for c in currents[0:5]] * 10  # nonplanar coils
    pc = [c * 36 for c in currents[5:7]] * 10  # planar coils
    currents = npc + pc
    config = flt.types.MagneticConfig()
    config.inverseField = 0
    config.coilsIds = coils
    config.coilsIdsCurrents = currents
    # task definition
    task = flt.types.Task()
    task.step = 0.2
    task.characteristics = flt.types.MagneticCharacteristics()
    task.characteristics.axisSettings = flt.types.AxisSettings()
    # actual tracer
    res = flt.service.trace(p, config, task, None, None)
    reff = [res.characteristics[i].reff for i in range(len(res.characteristics))]
    return reff


def get_reff(pos, currents, cid):
    reff = get_reff_vmec(pos[0], pos[1], pos[2])
    nans = [i for i, v in enumerate(reff) if v is None]
    if nans:
        reff_flt = get_reff_flt(pos[0, nans], pos[1, nans], pos[2, nans], currents)
        for n, i in enumerate(nans):
            reff[i] = reff_flt[n]
    return reff


def get_B(x, y, z, cid):
    try:  # check if numpy array or list
        iter(x)
    except TypeError:  # is float or int
        x = str(x)
        y = str(y)
        z = str(z)
    else:  # is iterable
        x = ','.join(map(str, x))
        y = ','.join(map(str, y))
        z = ','.join(map(str, z))
    target = f'/w7x_ref_{cid}/magneticfield.json?x={x}&y={y}&z={z}'
    return query_vmec_rest(target)['magneticField']


def get_B_flt(x, y, z, currents):
    """get B-field from field line tracer

    Args:
        x/y/z (float or list of floats): torus hall coordinates
        currents (list of float): NPC/PC currents

    Returns:
        B (list of list of floats): [Bx]/[By]/[Bz] in Tesla
    """
    flt = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')
    p = flt.types.Points3D()
    p.x1 = x
    p.x2 = y
    p.x3 = z
    print('calling magneticField from field line tracer for x, y, z = {}, {}, {}'.format(x, y, z), flush=True)
    # definition of magnetic config
    coils = range(1362, 1432)
    npc = [c * 108 for c in currents[0:5]] * 10  # nonplanar coils
    pc = [c * 36 for c in currents[5:7]] * 10  # planar coils
    currents = npc + pc
    config = flt.types.MagneticConfig()
    config.inverseField = 0
    config.coilsIds = coils
    config.coilsIdsCurrents = currents
    # actual function call
    res = flt.service.magneticField(p, config)
    B = [res.field.x1, res.field.x2, res.field.x3]
    return B


def get_B0(currents):
    """gets on-axis B-field for given currents from VMEC

    Args:
        currents (list of float): NPC/PC currents

    Returns:
        B_ax(phi=0) in T (float)
    """
    c = currents
    target = f'?i1={c[0]}&i2={c[1]}&i3={c[2]}&i4={c[3]}&i5={c[4]}&ia={c[5]}&ib={c[6]}'
    return float(query_confencode_rest(target).get('Bax(phi=0)/T'))


def get_magnetic_axis(phi, cid):
    """gets real space coordinates of magnetic axis

    Args:
        phi (float): angle in rad
        cid (str): VMEC reference ID
    """
    phi = phi * 180 / np.pi
    target = f'/w7x_ref_{cid}/magneticaxis.json?phi={phi}'
    return query_vmec_rest(target)['magneticAxis']


def get_iota_profile(cid):
    target = f'/w7x_ref_{cid}/iota.json'
    return query_vmec_rest(target)['iotaProfile']


def get_reff_profile(cid):
    target = f'/w7x_ref_{cid}/reff.json'
    return query_vmec_rest(target)['reff']


def get_minor_radius(cid):
    reff_profile = get_reff_profile(cid)
    return reff_profile[-1]
