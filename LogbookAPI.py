import requests
import getpass
import logging
import base64
from io import BytesIO

log = logging.getLogger("LogbookAPI")

# List of W7-X components with KKS id: https://w7x-logbook.ipp-hgw.mpg.de/KKS-List-TLvD.json
# List of W7-X experiment days: https://w7x-logbook.ipp-hgw.mpg.de/api/experiment_days


base_url = "https://w7x-logbook.ipp-hgw.mpg.de/api/"
# APIkey is also possible. See logbookAPI from Adrian Stechow

"""
There are different kind of LOGS at W7-X.  We distinguish:

 - Experiment program (XP) logs                 => XPlog
 - Component logs during XP                     => componentlog
 - Component stand-alone-program logs           => SAPlog
 - Component event logs                         => componenteventlog

 (- Operator log                                => OPlog)
 (- Operator event log                          => OPeventlog)

Each log can be created, changed, read, and deleted.
The LOG-OBJECTS are dictionaries with specific attributes (see logbookObjects).
Special attributes are TAGS, which are themselves dictionaries with specific attributes. 

COMMENTS can be added to the different logs.
Each comment can be created, changed, read, and deleted.
"""

"""""""""""""""""""""""""""""
Experiment Program (XP) logs
"""""""""""""""""""""""""""""


# The XP logs contain the sequence and results of W7-X experiments.
# These logs are automatically created by Xedit and XControl.
# The logs are located at https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP...
# See for an example      https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP_20221201.51


def create_program_log():
    print('create_program_log() is not public. Xedit and XControl automatically create logs')


def change_program_log(XPid):
    pass
    # url = base_url + "/log/XP_" + XPid
    # send {"doc": XPlog}


def read_program_log(XPid):
    # Adrian's getprograminfo(self, program=None):
    """
    Get program log.

    Args:
        program (str): W7-X program number (e.g. '20170921.1')

    Returns:
        json (dict):
    """
    url = base_url + '/log/XP_' + XPid
    try:
        json = getjson(url)  # receive {"_source": XPlog}
        XPlog = json['_source']
    except Exception:
        log.error('could not get program log for experiment program {}, probably nonexistent'.format(XPid))
        XPlog = None
    return XPlog


def delete_program_log():
    print('delete_program_log() is not public.')


"""""""""""""""""""""""""""
Component logs during XP
"""""""""""""""""""""""""""


# For an experiment program (XP), in addition to the XP logs (see above), also component logs can be written.
# (components being machine subcomponents, heating systems, fueling systems or diagnostics)
# The logs are visible on
# 1) dedicated areas on the program logbook    https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=...#...
#    See for an example                        https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP_20221201.51#QRN
# 2) a dedicated area on the component logbook https://w7x-logbook.ipp-hgw.mpg.de/components?id=...#componentlogs
#    See for an example                        https://w7x-logbook.ipp-hgw.mpg.de/components?id=QRN#componentlogs


def post_component_log(COMPid, XPid, componentlog, mode="append"):
    # Adrian's postcomponentlog(self, logtext=None, tags=None):
    """
    Post a component log.

    If a component log already exists for the XPid, overwrite it or append to it.
    If no component log exists for the XPid, create a new one.

    Args:
        componentlog
        mode (string): append or overwrite

    Returns:
        None
    """

    # TODO: Check if the componentlog dict is in the correct form
    # checkcomponentlog()

    oldcomponentlog = read_component_log(COMPid, XPid)

    if oldcomponentlog:
        if mode == "overwrite":
            change_component_log(COMPid, XPid, componentlog)
        elif mode == "append":
            appendto_component_log(COMPid, XPid, componentlog)
        else:
            log.error('Mode not recognised')
    else:
        create_component_log(COMPid, componentlog)


def create_component_log(COMPid, componentlog):
    # Confusion on the logbook API webpage: the url for create, change, read, delete are always different (w.r.t. kks, COMPid, XPid)
    message = {"doc": componentlog}
    url = base_url + "/log/" + COMPid
    log.info('Creating new component log for component {} in experiment program log {}: \n {}'.format(COMPid, componentlog['ref_id'], message))
    postjson(url, message)


def change_component_log(COMPid, XPid, componentlog):
    # send {"doc": componentlog} (only description and/or tags)

    url = base_url + "/log/" + COMPid + "/XP_" + XPid
    message = {'doc': {'description': componentlog['description'],
                       'tags': componentlog['tags']}}
    log.info('Editing existing component log for component {} in experiment program log {}: \n {}'.format(COMPid, XPid, message))
    postjson(url, message)


def appendto_component_log(COMPid, XPid, componentlog):
    oldcomponentlog = read_component_log(COMPid, XPid)
    url = base_url + "/log/" + COMPid + "/XP_" + XPid
    message = {'doc': {'description': oldcomponentlog['description'] + componentlog['description'],
                       'tags': oldcomponentlog['tags'] + componentlog['tags']}}
    log.info('Appending to existing component log for component {} in experiment program log {}: \n {}'.format(COMPid, XPid, message))
    postjson(url, message)


def read_component_log(COMPid, XPid):
    # Adrian's getcomponentlog(self, program=None):

    """
    Gets component log JSON response.

    Args:
        COMPid
        XPid

    Returns:
        json (dict): component log
    """

    url = base_url + "/log/" + COMPid + "/XP_" + XPid
    try:
        json = getjson(url)  # receive {"_source": componentlog}
        componentlog = json['_source']
    except Exception:
        log.error('could not get component log for component {} in experiment program {}, probably nonexistent'.format(COMPid, XPid))
        componentlog = None
    return componentlog


# Can only be done by original author (or component team). Moves content (incl. history) to recycling bin.
def delete_component_log(COMPid, XPid):
    url = base_url + "/log/" + COMPid + "/XP_" + XPid
    log.info('Deleting component log for component {} in experiment program log {}'.format(COMPid, XPid))
    deletejson(url)


# Adrian also has the function
# getcomponenttags()


"""""""""""""""""""""""""""""""""""
Component stand-alone-program logs
"""""""""""""""""""""""""""""""""""


# These logs are automatically created by XControl for autonomous component programs.
# The logs are visible alongside the component event logs (see below)
# 1) At the location      https://w7x-logbook.ipp-hgw.mpg.de/components?id=...#otherlogs
# See for an example      https://w7x-logbook.ipp-hgw.mpg.de/components?id=QMJ#otherlogs
# 2) At the location      https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=SAPid
# See for an example      https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=QMJ_20221212.3

# SAPid is of the form QRN_20220926


def create_standaloneprogram_log():
    print('create_standaloneprogram_log() is not public. Xedit and XControl automatically create logs')


def change_standaloneprogram_log(COMPid, SAPid, SAPlog):
    # send {"doc": XPlog} (only description and/or tags)

    url = base_url + "/log/" + COMPid + "/" + SAPid
    message = {'doc': {'description': SAPlog['description'],
                       'tags': SAPlog['tags']}}
    log.info('Editing existing component stand-alone-program log for component {} in SAP log {}: \n {}'.format(COMPid, SAPid, message))
    postjson(url, message)


def read_standaloneprogram_log(SAPid):
    # receive {"_source": XPlog}

    """
    Get component stand-alone-program log JSON response.

    Args:
        SAPid

    Returns:
        json (dict): SAP log
    """
    url = base_url + "/log/" + SAPid

    try:
        json = getjson(url)  # receive {"_source": componentlog}
        SAPlog = json['_source']
    except Exception:
        log.error('could not get component stand-alone-program log in stand alone program {}, probably nonexistent'.format(SAPid))
        SAPlog = None
    return SAPlog


def delete_standaloneprogram_log():
    print('delete_standaloneprogram_log() is not public.')


"""""""""""""""""""""
Component event logs
"""""""""""""""""""""


# For logs of different types, such as tests, calibration, failure, ...
# The component event logs are visible alongside the component stand-alone-program logs (see above)
# 1) at the location    https://w7x-logbook.ipp-hgw.mpg.de/components?id=...#otherlogs
# See for an example    https://w7x-logbook.ipp-hgw.mpg.de/components?id=QRN#otherlogs
# 2) at the location    https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=EVENTid
# See for an example    https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=QMJ_20180919-event1

# JSON file also visible at https://w7x-logbook.ipp-hgw.mpg.de/log/COMPid/SAPid
# See for an example        https://w7x-logbook.ipp-hgw.mpg.de/log/QRN/QRN_20220926-event1

# EVENTid is of te form QRN_20220926-event1


def create_componentevent_log(COMPid, component_event_log):
    # send {"doc": component_event_log}

    # check COMPid and component_event_log
    message = {"doc": component_event_log}
    url = base_url + "/log/" + COMPid
    log.info('Creating new component event log for component {} of type {}: \n {}'.format(COMPid, component_event_log['type'], message))
    postjson(url, message)  # Do web protocols recognise if it is a component_event_log or a component_log based on the json structure?


def change_componentevent_log(COMPid, EVENTid, component_event_log):
    # send {"doc": component_event_log} (only description and/or tags)

    # check EVENTid and component_event_log
    url = base_url + "/log/" + COMPid + "/" + EVENTid
    message = {'doc': {'description': component_event_log['description'],
                       'tags': component_event_log['tags']}}
    log.info('Editing existing component event log for component {} in event {}: \n {}'.format(COMPid, EVENTid, message))
    postjson(url, message)


def read_componentevent_log(EVENTid):
    # receive {"_source": component_event_log}
    """
    Get component event log JSON response.

    Args:
        EVENTid

    Returns:
        json (dict): SAP log
    """
    url = base_url + "/log/" + EVENTid

    try:
        json = getjson(url)  # receive {"_source": component_event_log}
        component_event_log = json['_source']
    except Exception:
        log.error('could not get component event log {}, probably nonexistent'.format(EVENTid))
        component_event_log = None
    return component_event_log


def delete_componentevent_log(COMPid, EVENTid):
    url = base_url + "/log/" + COMPid + "/" + EVENTid
    log.info('Deleting component event {} for component {}'.format(EVENTid, COMPid))
    deletejson(url)


"""""""""
Comments
"""""""""


# W7-X team members can add comments to different logs, namely: XPlogs, componentlogs, and component stand-alone-program (SAP) logs

# Comments to XPlogs
# are located at            https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP...#comments
# See for an example        https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP_20221201.51#comments

# Comments to componentlogs
# 1) are located at         https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP...#components
# See for an example        https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP_20221201.51#QRN
# 2) are located at         https://w7x-logbook.ipp-hgw.mpg.de/component?id=...#otherlogs
# See for an example        https://w7x-logbook.ipp-hgw.mpg.de/component?id=QRN#otherlogs

# Comments to component SAP logs
# 1) are located at         https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=...#comments
# See for an example        https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=QMJ_20221212.3#comments
# 2) are located at         https://w7x-logbook.ipp-hgw.mpg.de/component?id=...#otherlogs
# See for an example        https://w7x-logbook.ipp-hgw.mpg.de/components?id=QMJ#otherlogs


def create_comment(LOGid, comment, user=None, pw=None):
    # Adrian's def postcomponentcomment(self, commenthtml):
    # Adrian's def postprogramcomment(self, commenthtml):
    """
    Post a comment to a log.

    Hint: Use imagetohtml() to embed images in comments.

    Args:
        LOGid (str): id of the log to which the comment will be added.
                The LOGid can take the form of XPid, COMPid, EVENTid or SAPid

    Returns:
        None
    """

    url = base_url + "comment/" + LOGid
    log.info('Writing new comment for log {}'.format(LOGid))
    postjson(url, comment, auth='param', user=user, pw=pw)


def change_comment(LOGid, COMMENTid, comment):
    # Adrian's def editcomponentcomment(self, commenthtml, commentid):
    # Adrian's def editprogramcomment(self, commenthtml, commentid):
    """
    Edits an existing component comment.

    Args:
        LOGid (str):
        COMMENTid (str): the comment id (Note: NOT the ref_id!), retrieve with getcomponentcomments()
    """

    url = base_url + "/comment/" + LOGid + "/" + COMMENTid
    message = {"doc": comment}
    log.info('Editing existing comment {} for log {}'.format(COMMENTid, LOGid))
    postjson(url, message)


def read_comment(LOGid, COMMENTid):
    # receive {"_source": comment}
    """
    Get comment with ID, corresponding to log with LOGid

    Args:
        LOGid

    Returns:
    """

    url = base_url + "/comment/" + LOGid + "/" + COMMENTid
    try:
        json = getjson(url)  # receive {"_source": component_event_log}
        comment = json['_source']
    except Exception:
        log.error('could not get comment {}, probably nonexistent'.format(COMMENTid))
        comment = None
    return comment


def delete_comment(LOGid, COMMENTid):
    # No version control available!
    url = base_url + "/comment/" + LOGid + "/" + COMMENTid
    log.info('Deleting comment {} for log {}'.format(COMMENTid, LOGid))
    deletejson(url)


# The W7-X Logbook provides a dedicated area for the logbook of the Engineer in Charge (German: Technischer Leiter vom Dienst, TLvD).
# Writing of Operator events and logs is restricted to members of TLvD team and is at the moment not part of the public API.

# Operator page as JSON: https://w7x-logbook.ipp-hgw.mpg.de/deviceoperation.json
# Operator page history as JSON: https://w7x-logbook.ipp-hgw.mpg.de/api/log/history/deviceoperation/OPDO


"""""""""""""""
Operator logs
"""""""""""""""

#
# def create_operatorlog():
#     print('create_operatorlog() is not public.')
#
#
# def change_operatorlog():
#     print('create_operatorlog() is not public.')
#
#
# def read_operatorlog(OPid):
# for example AWaIK9gW4dlO3rnfsDfU
#
#     url = base_url + '/log/OPDO/' + OPid
#     try:
#         json = getjson(url)  # receive {"_source": operator_event_log}
#         OPlog = json['_source']
#     except Exception:
#         log.error('could not get operator event log {}, probably nonexistent'.format(OPid))
#         OPlog = None
#     return OPlog
#
#
# def delete_operatorlog():
#     print('create_operatorlog() is not public.')


"""""""""""""""""""""
Operator event logs
"""""""""""""""""""""

#
# def create_operator_eventlog():
#     print('create_operator_eventlog() is not public.')
#
#
# def change_operator_eventlog():
#     print('create_operator_eventlog() is not public.')
#
#
# def read_operator_eventlog(EVENTid):
# for example OPDO_20180801-event1
#
#     url = base_url + '/log/OPDO/' + OPid
#     try:
#         json = getjson(url)  # receive {"_source": operator_event_log}
#         OPlog = json['_source']
#     except Exception:
#         log.error('could not get operator event log {}, probably nonexistent'.format(OPid))
#         OPlog = None
#     return OPlog
#
#
# def delete_operator_eventlog():
#     print('create_operator_eventlog() is not public.')


"""""""""""""""""
Functions on Tags
"""""""""""""""""

# Other functions by Adrian
# SEPARATE, as users can add tags to experiment logs, component logs, and stand-alone programs.
# gettagvalue
# _parsegastag
# appendprogramtag()
# getprogramtags()
# getprogramdict() is specific for tags on gas, Heating/ECRH and the Magnetic field/Main field


"""""""""""""""
Search requests
"""""""""""""""


# All resources within the W7-X logbook can be found via the internal search index.
# Search requests can be done using full-text search over the complete content or based on single attributes.


def search_logbook(url, query, size=100, sort=None, searchfrom=None, fields=None):
    # query (string or numeric): search for values in an attribute, or in a specific log or comment attribute [See dict keys in LogbookObjects.py]
    # for example: query='Thomson', query='id:QTB', query='tags.value:ok'
    # use quotes "..." to search for exact phrases
    # wildcard searches via ? (single character) or * (multiple character):
    # combine criterias with AND, OR, ( )
    # exclude results with !, NOT or -
    # escape with backslash and/or URL encoding (whitespace: %20, +: %2b)
    # range queries via brackets:
    request = 'q=' + query

    # size (numeric): number of response items (default: 10, max: 10000)
    if size:
        if size < 10000:
            request += '&size={}'.format(size)

    # sort (string): sort the results based on numeric attribute
    # for example: sort='timestamp:desc'
    if sort:
        request += '&sort={}'.format(sort)

    # from (numeric): start page index of response items
    # for example: searchfrom=100
    if searchfrom:
        request += '&from={}'.format(searchfrom)

    if fields:
        request += '&fields={}'.format(fields)
    log.debug(request)
    try:
        json = getjson(url + request)  # {"hits": {"total": 5, "hits": [{"_source": {}}] }}
    except Exception:
        log.error('could not get search result for {}'.format(request))
        json = None

    return json


def search_comment(query, specify_url='', size=100, sort=None, searchfrom=None, fields=None):
    # Adrian's getcomponentcomments(self, program=None):
    # Adrian's getprogramcomments(self, program=None):

    # for example: specify_url = /w7xlogbook/{COMPid}_comments, specify_url = /w7xlogbook/XP_comments
    url = base_url + specify_url + '/_search?'  # The search url is different for logs

    return search_logbook(url, query, size=size, sort=sort, searchfrom=searchfrom, fields=fields)


# Comments exist for different logs, namely: XPlogs, componentlogs, and component stand-alone-program (SAP) logs
# Below are search functions to search in specific comments


def search_componentlog_comments(COMPid, XPid=None, time_from=None, time_upto='*'):
    # Adrian's componentcomments(self, program=None):

    """
    Get all comments to component logs during an XP, with their comment IDs.

    Args:
        XPid (str, optional): W7-X program number (e.g. '20170921.1')

    Returns:
        dict with {'comment_id':'comment'}
    """
    specify_url = '/w7xlogbook/' + COMPid + '_comments'
    sort = 'timestamp'
    if XPid:
        query = 'ref_id:XP_' + XPid
    else:
        query = 'ref_id:XP_*'
    if time_from:
        query += ' AND from:[{} TO {}]'.format(time_from, time_upto)

    json = search_comment(query, specify_url=specify_url, sort=sort)  # {"hits": {"total": 5, "hits": [{"_source": {}}] }}

    return json
    # nHits = json['hits']['total']
    # commentdict = {}
    # for i in range(nHits):
    #     key = json['hits']['hits'][i]['_id']
    #     commentdict[key] = json['hits']['hits'][i]['_source']['content']
    # return commentdict


def search_programlog_comments(XPid=None, time_from=None, time_upto='*'):
    # Adrian's getprogramcomments(self, program=None)
    """
    Get all comments to program logs, with their comment IDs.

    Args:
        XPid (str, optional): W7-X program number (e.g. '20170921.1')

    Returns:
        dict with {'comment_id':'comment'}
    """
    specify_url = '/w7xlogbook/XP_comments'  # only difference
    sort = 'timestamp'
    if XPid:
        query = 'ref_id:XP_' + XPid
    else:
        query = 'ref_id:XP_*'
    if time_from:
        query += ' AND from:[{} TO {}]'.format(time_from, time_upto)

    json = search_comment(query, specify_url=specify_url, sort=sort)  # {"hits": {"total": 5, "hits": [{"_source": {}}] }}
    return json

    # nHits = json['hits']['total']
    # commentdict = {}
    # for i in range(nHits):
    #     key = json['hits']['hits'][i]['_id']
    #     commentdict[key] = json['hits']['hits'][i]['_source']['content']
    # return commentdict


def search_log(query, specify_url='', size=100, sort=None, searchfrom=None, fields=None):
    url = base_url + 'search.json?'  # The search url is different for comments (see above)
    return search_logbook(url, query, size=size, sort=sort, searchfrom=searchfrom, fields=fields)


def search_programlogs(query="", time_from=None, time_upto='*', XPid='*'):
    # Adrian's getallprograms(self, filt='')
    # and def searchprograms(self, expression, limit=10000)
    """
    Get all program logs

    Args:
        time_from
        time_upto
        query
    Returns:
        list of program log dicts
    """
    sort = 'from:asc'
    if query != "":
        query += ' AND id:XP_' + XPid
    else:
        query += 'id:XP_' + XPid
    query += ' AND NOT id:CDX*'  # Did not find another way to exclude programs with symbols before XP, i.e. to only select programs that start with XP. There's only issues with XDC so excluded those
    if time_from:
        query += ' AND from:[{} TO {}]'.format(time_from, time_upto)  # Does 'from' as attribute work?

    json = search_log(query, sort=sort)  # {..., "hits": {... "total": 5, "hits": [{..., "_source": {}}] }}

    # return json['hits']['hits']
    return json


def search_SAPlogs(COMPid, time_from=None, time_upto='*'):
    """
    Get all stand-alone-program logs

    Args:
        time_from
        time_upto
    Returns:
        list of SAP log dicts
    """

    sort = 'from:asc'
    # query = '_type:{}_logs%20AND%20!type:experiment'.format(COMPid)
    query = 'id:{}_* AND !id:*event*'.format(COMPid)  # SAPid is of the form QRN_20220926
    if time_from:
        query += ' AND from:[{} TO {}]'.format(time_from, time_upto)

    json = search_log(query, sort=sort)  # {"hits": {"total": 5, "hits": [{"_source": {}}] }}

    # return json['hits']['hits']
    return json


def search_componentlogs(COMPid, XPid=None, time_from=None, time_upto='*'):
    """
    Get all component logs during XP

    Args:
        time_from
        time_upto
    Returns:
        list of component log dicts
    """
    sort = 'from:asc'
    query = '_type:{}_logs AND type:experiment'.format(COMPid)
    if XPid:
        query += ' AND ref_id:XP_{}'.format(XPid)
    if time_from:
        query += ' AND from:[{} TO {}]'.format(time_from, time_upto)  # Not time_from as in json

    json = search_log(query, sort=sort)  # {"hits": {"total": 5, "hits": [{"_source": {}}] }}

    # return json['hits']['hits']
    return json


def search_eventlogs(COMPid, time_from=None, time_upto='*'):
    """
    Get all stand-alone-program logs

    Args:
        time_from
        time_upto
    Returns:
        list of SAP log dicts
    """
    sort = 'from:asc'
    query = 'id:{}_* AND id:*event*'.format(COMPid)  # QRN_20220926-event1
    if time_from:
        query += ' AND from:[{} TO {}]'.format(time_from, time_upto)  # Not time_from as in json

    json = search_log(query, sort=sort)  # {"hits": {"total": 5, "hits": [{"_source": {}}] }}

    # return json['hits']['hits']
    return json


"""""""""
History
"""""""""


# Read log history
def read_loghistory(page, specification, version=None, size=100, readfrom=None):
    # page:          components, deviceoperation, XP
    # specification: QME,        OPDO,            XP_20180718.22
    # version (integer): return only a specific version
    # size (integer): number of values in results array
    # from (integer): start index of results array

    url = base_url + '/log/history/' + page + '/' + specification
    json = getjson(url)  # {"hits": {"total": 5, "hits": {["_source": {}}] }}

    log.debug('Reading history of log {}/{}'.format(page, specification))
    log.debug('number of versions:{}'.format(json['hits']['total']))
    log.debug('Latest version: {}'.format(json['hits']['hits'][0]['_source']['version']))

    return json


"""""""""""
Components
"""""""""""


def read_component_list():
    url = base_url + 'components.json'
    try:
        complist = getjson(url)  # receive dict as in LogbookObjects.Componentcategories
    except Exception:
        log.error('could not get componentlist.')
        complist = None
    return complist


def read_component_page(COMPid):
    url = base_url + '/components/' + COMPid
    try:
        page = getjson(url)  # receive dict as shown at https://w7x-logbook.ipp-hgw.mpg.de/api.html#componentpages
    except Exception:
        log.error('could not get page of component {}, probably nonexistent'.format(COMPid))
        page = None
    return page


"""""""""
Programs
"""""""""

# def setprogram(self, program)
# def getprograminfo(self, program=None)


"""""""""
Helpers
"""""""""


def imagetohtml(image):
    """
    Loads an image file and converts to base64 for use in a comment.

      Args:
          image

      Returns:
          str: full html for drop-in use, including <img> tags
      """

    # Is it possible to convert the image right away?
    # encoded = base64.b64encode(image)

    # or does the image have to be stored temporarily like this?
    try:
        tmpfile = BytesIO()
        image.savefig(tmpfile, format='png')
        encoded = base64.b64encode(tmpfile.getvalue())  # .decode('utf-8')
        html = '<img src="data:image/png;base64,' + encoded.decode('ascii') + '">'
    except Exception:
        log.error("could not convert image to html.")

    return html


def getjson(url):
    """
    Get a JSON response from a URL.

    Args:
        url (str): full URL to retrieve.

    Returns:
        dict: JSON response as dict.

    Raises:
        Exception: if status code != 200.
    """
    log.debug('JSON GET, URL: {}'.format(url))
    r = requests.get(url, verify=True)
    if r.status_code == 200:
        return r.json()
    elif r.status_code == 404:
        # this is only debug because 404 can be a "valid" response
        log.debug('JSON GET 404 for URL: {}'.format(url))
    else:
        log.error('JSON GET error for URL: {}'.format(url))
        raise Exception('JSON request: received status code {}'.format(r.status_code))


def postjson(url, json, auth="input", user=None, pw=None):
    """POSTs a JSON dict using HTTP authentication or token (depending on self.auth).

    Args:
        url (str): full URL to post to.
        json (dict): JSON dict to POST
        auth: 'input': interactively request password via getpass and store in plaintext
              [NOT IMPLEMENTED ATM]  'token': use API token-based authentication
              Defaults to 'input'. If None, no authentication will be performed.
              DEPRECATION NOTE: will default to None in the future, set explicitly!
    Returns:
        bool: True if successful

    Raises:
        Exception: if status code != 200.
    """

    # Use your IPP account and basic authentication (replace 'user'/'password' with your credentials):
    log.debug('Posting JSON to {}'.format(url))

    if auth == 'input':
        url = url.replace('/api', '')
        user = getpass.getuser()
        pw = getpass.getpass('password for {}:'.format(user))
        # print(url, json, user, pw)
        r = requests.post(url, json=json, auth=(user, pw), verify=True)
    # elif auth == 'token':
    #     r = requests.post(url, json=json, headers={'Authorization': 'Bearer ' + password}, verify=True)
    elif auth == 'param':
        url = url.replace('/api', '')
        # print(url, json, user, pw)
        r = requests.post(url, json=json, auth=(user, pw), verify=True)
    else:
        log.error('cannot post json, no authentication mechanism defined')
        raise Exception('JSON POST request cancelled')

    if r.status_code in [200, 201]:
        return True
    elif r.status_code == 401:
        log.error('Authorization denied for JSON PUT at URL: {}'.format(url))
        raise Exception('JSON POST request: not authrorized')
    else:
        log.error('JSON PUT error for URL: {}'.format(url))
        raise Exception('JSON POST request: received status code {}'.format(r.status_code))


def deletejson(url):
    pass
    # Issue warning and request confirmation
    print('Authenticate to DELETE the json at {}.\n Username:'.format(url))
    user = getpass.getuser()
    pw = getpass.getpass('Password for {}'.format(user))
    res = requests.delete(url, auth=(user, pw))
