import Output as Output
import Selection as Selection
import archive_signal_dict as archive_signal_dict
import derived_signal_dict as derived_signal_dict
import tkinter as tk
import logging

log = logging.getLogger("CorrelationsGUI")


class CorrelationsGUI:
    """Class definition of the Correlations GUI, where the user can correlate two signals, being all samples or only extrema, and also fit the result.
    The Correlations GUI is connected to a GUI, from which it uses the other settings, such as the time window.

    Example
    -------
    """

    def __init__(self, GUI, signals, XPid):
        """ The initializer generates the GUI and calls its mainloop (see bottom of this function), awaiting user actions.
        """
        self.GUI = GUI

        # Get the signalnames of time traces, to make a drop down menu for each variable to select the corresponding signal
        timetraces = archive_signal_dict.getTimetraces() + derived_signal_dict.getTimetraces()

        # Create a tk window for the GUI
        self.win = tk.Toplevel()
        self.win.wm_title("Correlations monitor")

        # CORRELATIONS FRAME
        frm = tk.Frame(self.win, borderwidth=15, bg="alice blue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        correlations_lbl = tk.Label(frm, text="Correlation", bg="alice blue", font='helvetica 15 bold')

        # GRID CORRELATIONS FRAME
        frm.grid(column=0, row=0, rowspan=1, sticky="nsew")
        correlations_lbl.grid(column=0, row=0, sticky="nsew")

        # SIGNAL FRAME
        signal_frm = tk.Frame(frm, borderwidth=15, bg="alice blue")
        signal_lbl = tk.Label(signal_frm, text="Signals", bg="alice blue")

        # Create means to select x signal
        x_lbl = tk.Label(signal_frm, text="x = ", bg="alice blue")
        # drop-down-menu listing all the optional signals
        self.signal_x = tk.ttk.Combobox(signal_frm, width=50, state='readonly', values=timetraces)
        # Shows signal as default value
        if len(signals) > 0:
            if signals[0] in timetraces:
                select = timetraces.index(signals[0])
                self.signal_x.current(select)
            else:
                log.error("Signal is not found in dictionaries. Ignoring this signal")

        # Create means to select y signal
        y_lbl = tk.Label(signal_frm, text="y = ", bg="alice blue")
        # drop-down-menu listing all the optional signals
        self.signal_y = tk.ttk.Combobox(signal_frm, width=50, state='readonly', values=timetraces)
        # Shows signal as default value
        if len(signals) > 1:
            if signals[1] in timetraces:
                select = timetraces.index(signals[1])
                self.signal_y.current(select)
            else:
                log.error("Signal is not found in dictionaries. Ignoring this signal")

        # GRID SIGNAL FRAME
        signal_frm.grid(column=0, row=1, sticky="nsew")
        signal_lbl.grid(column=0, row=0, sticky="nw", pady=(5, 0))
        x_lbl.grid(column=0, row=1, columnspan=1, rowspan=1, sticky="w", padx=5)
        self.signal_x.grid(column=1, row=1, sticky="w")
        y_lbl.grid(column=0, row=2, columnspan=1, rowspan=1, sticky="w", padx=5)
        self.signal_y.grid(column=1, row=2, sticky="w")

        # DATA FRAME
        data_frm = tk.Frame(frm, borderwidth=15, bg="alice blue")
        data_lbl = tk.Label(data_frm, text="Datapoints", bg="alice blue")
        self.data_var = tk.IntVar(None, 1)
        all_chk = tk.Radiobutton(data_frm, text="All samples", variable=self.data_var, value=1, bg="alice blue")
        xmaxima_chk = tk.Radiobutton(data_frm, text="Timestamp of maximal x value per shot", variable=self.data_var, value=2, bg="alice blue")
        ymaxima_chk = tk.Radiobutton(data_frm, text="Timestamp of maximal y value per shot", variable=self.data_var, value=3, bg="alice blue")
        maxima_chk = tk.Radiobutton(data_frm, text="maximal x value and maximal y value per shot", variable=self.data_var, value=4, bg="alice blue")

        # GRID DATA FRAME
        data_frm.grid(column=0, row=2, sticky="nsew")
        data_lbl.grid(column=0, row=0, sticky="nw", pady=(5, 0))
        all_chk.grid(column=0, row=1, sticky="nw", padx=5, pady=(0, 1))
        xmaxima_chk.grid(column=0, row=2, sticky="nw", padx=5, pady=(0, 1))
        ymaxima_chk.grid(column=0, row=3, sticky="nw", padx=5, pady=(0, 1))
        maxima_chk.grid(column=0, row=4, sticky="nw", padx=5, pady=(0, 1))

        # CORRELATION COEFFICIENT FRAME
        coeff_frm = tk.Frame(frm, borderwidth=15, bg="alice blue")
        coeff_lbl = tk.Label(coeff_frm, text="Quantify relation", bg="alice blue")
        self.pearsoncoeff_var = tk.IntVar(None, 0)
        pearsoncoeff_chk = tk.Checkbutton(coeff_frm, text="Pearson's correlation coefficient", variable=self.pearsoncoeff_var, bg="alice blue")
        self.spearmancoeff_var = tk.IntVar(None, 0)
        spearmancoeff_chk = tk.Checkbutton(coeff_frm, text="Spearman's correlation coefficient (non linear)", variable=self.spearmancoeff_var, bg="alice blue")
        self.covarcoeff_var = tk.IntVar(None, 0)
        covarcoeff_chk = tk.Checkbutton(coeff_frm, text="Covariance", variable=self.covarcoeff_var, bg="alice blue")
        # self.chisqcoeff_var = tk.IntVar(None, 0)
        # chisqcoeff_chk = tk.Checkbutton(coeff_frm, text="Chi-squared", variable=self.chisqcoeff_var, bg="alice blue")

        # GRID CORRELATION COEFFICIENT FRAME
        coeff_frm.grid(column=0, row=3, sticky="nsew")
        coeff_lbl.grid(column=0, row=0, sticky="nw", pady=(5, 0))
        pearsoncoeff_chk.grid(column=0, row=1, sticky="nw", padx=5, pady=(0, 1))
        spearmancoeff_chk.grid(column=0, row=2, sticky="nw", padx=5, pady=(0, 1))
        covarcoeff_chk.grid(column=0, row=3, sticky="nw", padx=5, pady=(0, 1))
        # chisqcoeff_chk.grid(column=0, row=1, sticky="nw", padx=5, pady=(0, 1))

        # FIT FRAME
        fit_frm = tk.Frame(frm, borderwidth=15, bg="alice blue")
        fit_lbl = tk.Label(fit_frm, text="Fit", bg="alice blue")
        self.fit_var = tk.IntVar(None, 0)
        nofit_chk = tk.Radiobutton(fit_frm, text="None", variable=self.fit_var, value=0, bg="alice blue")
        linfit_chk = tk.Radiobutton(fit_frm, text="Linear", variable=self.fit_var, value=1, bg="alice blue")
        logfit_chk = tk.Radiobutton(fit_frm, text="Logarithmic", variable=self.fit_var, value=2, bg="alice blue")
        expfit_chk = tk.Radiobutton(fit_frm, text="Exponential", variable=self.fit_var, value=3, bg="alice blue")
        polyfit_chk = tk.Radiobutton(fit_frm, text="Polynomial of order ", variable=self.fit_var, value=4, bg="alice blue")
        self.degree = tk.IntVar(None, 2)
        degree_entr = tk.Entry(fit_frm, textvariable=self.degree, width=5)

        # GRID FIT FRAME
        fit_frm.grid(column=0, row=4, sticky="nsew")
        fit_lbl.grid(column=0, row=0, sticky="nw", pady=(5, 0))
        nofit_chk.grid(column=0, row=1, sticky="nw", padx=5, pady=(0, 1))
        linfit_chk.grid(column=0, row=2, sticky="nw", padx=5, pady=(0, 1))
        logfit_chk.grid(column=0, row=3, sticky="nw", padx=5, pady=(0, 1))
        expfit_chk.grid(column=0, row=4, sticky="nw", padx=5, pady=(0, 1))
        polyfit_chk.grid(column=0, row=5, sticky="nw", padx=5, pady=(0, 1))
        degree_entr.grid(column=1, row=5, sticky="nw", pady=(0, 1))

        # SELECTION FRAME
        sel_frm = tk.Frame(frm, borderwidth=15, bg="alice blue")
        sel_lbl = tk.Label(sel_frm, text="Selection", bg="alice blue")
        GUI_lbl = tk.Label(sel_frm, text="Using output settings from the monitor", bg="alice blue")
        XPs_lbl = tk.Label(sel_frm, text="correlate signals, for XP's (csv, - for range):", bg="alice blue")
        self.XPs_var = tk.StringVar(None, "XP_...")
        if XPid != "None": self.XPs_var.set(XPid)
        XPs_entr = tk.Entry(sel_frm, textvariable=self.XPs_var, width=33)
        XPs_btn = tk.Button(sel_frm, text="Go!", command=self.correlateXPs, bg="DarkSeaGreen4")

        # GRID SELECTION FRAME
        sel_frm.grid(column=0, row=5, sticky="nsew")
        sel_lbl.grid(column=0, row=0, sticky="nw")
        GUI_lbl.grid(column=0, row=1, sticky="w", pady=(5, 0), padx=5)
        XPs_lbl.grid(column=0, row=2, sticky="w", padx=5)
        XPs_btn.grid(column=2, row=2, sticky="e", padx=(70, 0))
        XPs_entr.grid(column=0, row=3, sticky="nw", pady=(0, 1), padx=5)

    def correlateXPs(self):
        """ Generate the output where two signals are correlated, for the given shots. The user can select which samples of each shot have to be used (all, maximum x, maximum y, maxima x and y)
        and which fit to apply to the correlation.
        """
        # Get the selected signals and the XPs for which they have to be plotted
        signals = {}
        signals["x"] = [self.signal_x.get()]
        signals["y"] = [self.signal_y.get()]

        if None in signals:
            log.warning("Select x and y signal to correlate")
        else:
            XPstring = self.XPs_var.get()
            XPs = Selection.getXPs(XPstring)

            if len(XPs) == 0:
                log.error('No suitable XPs were found.')
                return

            # Set the timing information for the current XP
            mindims = []
            maxdims = []
            for j, XP in enumerate(XPs):
                try:
                    self.GUI.Selection.updateCurrentProgram("XP", XP)
                except Exception as e:
                    continue
                mindim, maxdim = self.GUI.OutputSettings.getTimeWindow()  # in s since epoch
                mindims.append(mindim)
                maxdims.append(maxdim)
            self.GUI.Selection.updateCurrentProgram("XP", XPs[0])

            # Get data setting and coefficient setting from correlations GUI
            datasettings = {1: "all", 2: "xmaximum", 3: "ymaximum", 4: "xymaxima"}
            datasetting = datasettings[self.data_var.get()]
            coeffsetting = []
            if self.pearsoncoeff_var.get(): coeffsetting.append("pearson")
            if self.spearmancoeff_var.get(): coeffsetting.append("spearman")
            if self.covarcoeff_var.get():  coeffsetting.append("covariance")
            # if self.chisqcoeff_var.get()}: coeffsetting.append("chisq")

            #Get fit settings from correlations GUI
            fitsettings = {0: None, 1: ["lin"], 2: ["log"], 3: ["exp"], 4: ["poly", self.degree.get()]}
            fitsetting = fitsettings[self.fit_var.get()]

            # Correlations
            set = self.GUI.OutputSettings.getOutputSettings()
            specSet = self.GUI.ICRHsettings.getICRHsettings()
            output = Output.Output("Correlation", signals, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                   showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
            output.produceCorrelationOutput(XPs, mindims, maxdims, datasetting, coeffsetting, fitsetting)

            # Update cache information on GUI
            self.GUI.update_cache_info()
