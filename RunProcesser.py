import Processer
import w7xarchive
import MagneticConfig as MagneticConfig
from ICRHsettings import ICRHsettings
# EXAMPLE of processing all signals for an experimental program (XP)

programnumber = "20230223.75"
time_from, time_upto = w7xarchive.get_program_from_to(programnumber)
# time_upto = time_from+100

my_ICRHsettings = ICRHsettings()
settings = my_ICRHsettings.getICRHsettings(time_from)
magneticSettings = MagneticConfig.getMagneticConfig("XP_" + programnumber)
settings.update(magneticSettings)

Processer.ProcessAllICRHsignals(time_from, time_upto, settings)


# EXAMPLE of processing one signal for a given time period

# time_from = "2022-08-10 10:01:01"
# time_upto = "2022-08-10 10:01:03"
# ICRHfreq = 375  # e-1 MHz
# # signal = 'Antenna/RF/Antenna current/Right strap (CE303)'
# # Processer.processSignal(signal, time_from, time_upto, ICRHfreq, forceProcessing=True)
# Processer.calibrateAllICRHsignals(time_from, time_upto, ICRHfreq, forceProcessing=True)
# # Processer.derivateAllICRHsignals(time_from, time_upto, ICRHfreq, forceProcessing=True)

