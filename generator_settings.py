
GenSettings = {
    '0':{
        'start': '2022-07-01 08:00:00',
        'stop': '2022-07-01 08:00:01',
        'data_dict': {
            "ICRHfreq Gen1 value": 37.5,
            "ICRHfreq Gen1 unit": "MHz",
            "ICRHfreq Gen2 value": 25,
            "ICRHfreq Gen2 unit": "MHz",
            "Operational Gen1": True,
            "Operational Gen2": False,
            "Transmission line scenario": 6 # Single strap
        }
    },
    '1': {
        'start': '2024-09-18 08:00:04',
        'stop': '2024-09-18 08:00:05',
        'data_dict': {
            "ICRHfreq Gen1 value": 37.5,
            "ICRHfreq Gen1 unit": "MHz",
            "ICRHfreq Gen2 value": 25,
            "ICRHfreq Gen2 unit": "MHz",
            "Operational Gen1": False,
            "Operational Gen2": True,
            "Transmission line scenario": 12  # Dipole
        }
    },
    '2': {
        'start': '2024-09-24 08:00:04',
        'stop': '2024-09-24 08:00:05',
        'data_dict': {
            "ICRHfreq Gen1 value": 37.5,
            "ICRHfreq Gen1 unit": "MHz",
            "ICRHfreq Gen2 value": 25,
            "ICRHfreq Gen2 unit": "MHz",
            "Operational Gen1": True,
            "Operational Gen2": False,
            "Transmission line scenario": 12  # Dipole
        }
    },
    '3': {
        'start': '2024-09-25 08:00:04',
        'stop': '2024-09-25 08:00:05',
        'data_dict': {
            "ICRHfreq Gen1 value": 37.5,
            "ICRHfreq Gen1 unit": "MHz",
            "ICRHfreq Gen2 value": 25,
            "ICRHfreq Gen2 unit": "MHz",
            "Operational Gen1": False,
            "Operational Gen2": True,
            "Transmission line scenario": 12  # Dipole
        }
    },
    '4': {
        'start': '2024-09-26 15:45:04',
        'stop': '2024-09-26 15:45:05',
        'data_dict': {
            "ICRHfreq Gen1 value": 37.5,
            "ICRHfreq Gen1 unit": "MHz",
            "ICRHfreq Gen2 value": 25,
            "ICRHfreq Gen2 unit": "MHz",
            "Operational Gen1": True,
            "Operational Gen2": False,
            "Transmission line scenario": 12  # Dipole
        }
    },
    '5': {
        'start': '2024-10-08 07:00:04',
        'stop': '2024-10-08 07:00:05',
        'data_dict': {
            "ICRHfreq Gen1 value": 37.5,
            "ICRHfreq Gen1 unit": "MHz",
            "ICRHfreq Gen2 value": 25,
            "ICRHfreq Gen2 unit": "MHz",
            "Operational Gen1": False,
            "Operational Gen2": True,
            "Transmission line scenario": 12  # Dipole
        }
    }
}
