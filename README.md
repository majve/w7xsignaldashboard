This is the code behind the dashboard that is running on 
http://sv-coda-wsvc-32:5000/.


If you want to test changes, please modify in Dashboard.py the bottom two lines.
Namely 
app.run_server(debug=True)
has to be active and
# app.run_servr(host='0.0.0.0', port=5000, debug=False)
has to be commented out with the #.

Run the application with
python3 Dashboard.py
