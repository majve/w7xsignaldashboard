import Reader
import logging
import writeICRHparlog

log = logging.getLogger("ICRHsettings")

class ICRHsettings:
    """This class contains all settings that are specific to the ICRH system
    """

    def __init__(self):
        self.ICRHfreq = 0
        self.phi0 = 0

    # Add serialization method
    def serialize(self):
        """Convert ICRHsettings object into a JSON-serializable dictionary."""
        return {
            "ICRHfreq": self.ICRHfreq,
            "phi0": self.phi0
        }

    # Add deserialization method
    @staticmethod
    def deserialize(data):
        """Recreate ICRHsettings object from a dictionary."""
        icrhsettings = ICRHsettings()
        icrhsettings.ICRHfreq = data.get('ICRHfreq')
        icrhsettings.phi0 = data.get('phi0')
        return icrhsettings

    def getICRHparameters(self, filter_stop=None, count=0):

        try:
            param_signal = 'Generator/Generators/RF/ICRH'
            param_time_from, param_time_upto = Reader.getLastTimeInterval(param_signal, filter_stop)
            signal = Reader.get_signal(param_signal, param_time_from, param_time_upto)
        except Exception as e:
            if count == 0:
                print('exception, write parlog')
                writeICRHparlog.writeICRHParlog()
                count = 1
                self.getICRHparameters(filter_stop=filter_stop, count=1)
            else:
                return [], [], False

        return signal.values  # OperationGen1, ICRHfreq1, OperationGen2, ICRHfreq2

    def getICRHfrequency(self, filter_stop=None):
        """Get the ICRH frequency from the archiveDB
        """
        OperationGen1, ICRHfreq1, OperationGen2, ICRHfreq2 = self.getICRHparameters(filter_stop=filter_stop)

        if OperationGen1 and OperationGen2:
            raise ValueError("Simultaneous operation of Generator 1 and 2 is not implemented yet.")
        elif OperationGen1:
            self.ICRHfreq = ICRHfreq1
        elif OperationGen2:
            self.ICRHfreq = ICRHfreq2

        return self.ICRHfreq

    def getICRHphi0(self):
        """The origin of the angle phi (i.e. 0 degrees) is between module 1 and 5
        The x axis runs along phi = 0 degrees. The xyz-axes are right handed.
        """
        self.phi0 = 152.973249828  # hardcoded toroidal angle of interest in degree = 2.6422868769717205 + 0.0276 radian (Ivan)
        return self.phi0

    def getICRHsettings(self, dim):
        """Get all ICRH settings as a dict
        """
        ICRHsettings = {}
        ICRHsettings["ICRHfreq"] = self.getICRHfrequency(dim)
        ICRHsettings["phi0"] = self.getICRHphi0()

        return ICRHsettings