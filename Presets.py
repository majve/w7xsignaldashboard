from dash import dcc, html
import dash_bootstrap_components as dbc

class Subsystem:
    def __init__(self, name, signals):
        self.name = name
        self.signals = signals

class System:
    def __init__(self, name, subsystems):
        self.name = name
        self.subsystems = [Subsystem(name, signals) for name, signals in subsystems.items()]

class Group:
    def __init__(self, name, systems):
        self.name = name
        self.systems = [System(name, subsystems) for name, subsystems in systems.items()]

class Tab:
    def __init__(self, name, groups):
        self.name = name
        self.groups = [Group(name, systems) for name, systems in groups.items()]


# Initialize the tabs, groups, systems, and subsystems
ICRH_groups = {
    "Generator": {
        "Generator 1": {
            "Endstage power": ["Approx net power", "Forward power", "Reflected power"],
            "Endstage voltages": ["Anode", "Grid 2", "Grid 1"],
            "Driver stage power": ["Forward power", "Reflected power"],
            "Driver stage voltages": ["Grid 2", "Grid 1"],
            "Cooling water temperatures": [
                "Inlet temperature", "Outlet anode endstage (CT100)", "Outlet gate endstage (CT110)",
                "Outlet cathode endstage (CT120)", "Outlet anode driver (CT130)"
            ],
            "Cooling water flow": [
                "Endstage anode (CF100)", "Endstage gate (CF110)", "Endstage cathode (CF120)", "Driver stage anode (CF130)"
            ]
        },
        "Generator 2": {
            "Endstage power": ["Approx net power", "Forward power", "Reflected power"],
            "Endstage voltages": ["Anode", "Grid 2", "Grid 1"],
            "Driver stage power": ["Forward power", "Reflected power"],
            "Driver stage voltages": ["Anode", "Grid 2", "Grid 1"],
            "Cooling water temperatures": [
                "Inlet temperature", "Outlet anode endstage (CT100)", "Outlet gate endstage (CT110)",
                "Outlet cathode endstage (CT120)", "Outlet anode driver stage (CT130)"
            ],
            "Cooling water flow": [
                "Endstage anode (CF100)", "Endstage gate (CF110)", "Endstage cathode (CF120)", "Driver stage anode (CF130)"
            ]
        }
    },
    "Antenna": {
        "Temperature": {
            "Strap temperature": ["strap 1 high (CT345)", "strap 1 center (CT346)", "strap 2 high (CT446)", "strap 2 center (CT445)"],
            "Box temperature": [
                "box top (CT344)", "box left top (CT443)", "box left center (CT442)", "box left bottom (CT441)", "box bottom (CT444)",
                "box right bottom (CT341)", "box right center (CT342)", "box right top (CT343)"
            ],
            "Thermocouples image": ["Thermocouples.png"]
        },
        "RF": {
            "Antenna current": ["Right strap (CE303)", "Left strap (CE403)", "Phase between straps"],
            "Capacitor voltages": ["Capacitor 1 (CE301)", "Capacitor 2 (CE401)"],
            "Capacitor currents": ["Capacitor 1 (CE302)", "Capacitor 2 (CE402)"],
            "Capacitor power": ["Capacitor 1", "Capacitor 2"]
        },
        "Positions": {
            "Antenna": ["Antenna head"],
            "Profiles": ["Poincare plot", "LCFS", "Antenna"],
            "Matching system": ["line stretcher 1", "line stretcher 2", "stub 1", "stub 2", "Capacitor 1", "Capacitor 2"]
        }
    },
    "Calorimetry": {
        "Calorimeter": {
            "Heat exchange": ["Strap 1", "Edge 1", "Front 1", "Strap 2", "Edge 2", "Front 2", "Capacitor 1", "Capacitor 2"],
            "Water flow": [
                "Strap 1 (CF304)", "Edge 1 (CF305)", "Front 1 (CF306)", "Strap 2 (CF404)", "Edge 2 (CF405)", "Front 2 (CF406)",
                "Capacitor 1 (CF301)", "Capacitor 2 (CF401)"
            ],
            "Water temperature": [
                "Outlet Strap 1 (CT304)", "Outlet Edge 1 (CT305)", "Outlet Front 1 (CT306)", "Outlet Strap 2 (CT404)", "Outlet Edge 2 (CT405)",
                "Outlet Front 2 (CT406)", "Outlet Capacitor 1 (CT301)", "Outlet Capacitor 2 (CT401)", "Inlet antenna head (CT501)",
                "Inlet Capacitors (CT502)"
            ]
        },
        "Directional Couplers": {
            "DRC1 Power": ["Forward power", "Reflected power", "Net power"],
            "DRC1 Reflection coefficient": ["GMSR"],
            "DRC3 Power": ["Forward power", "Reflected power", "Net power"],
            "DRC3 Reflection coefficient": ["GMSR"]
        },
        "Strap 2": {
            "Load resistance": ["Reflection coefficient port 2"]
        }
    }
}

ECRH_groups = {
    "Gyrotrons": {
        "Power": {
            "Total": ["ECRH power", "Sum of gyrotron powers", "Sum of gyrotron setpoints"],
            "Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
            "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]
        },
        "z offset": {
            "Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
            "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]
        },
        "phi offset": {
            "Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
            "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]
        },
        "setpoint": {
            "Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
            "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]
        }
    },
    "ECEchannels": {
        "ECE": {
            "core": ["core channel"],  #  (18 if B0 above 2.55, else 13)"],
            "channels": [f"channel {i:02d}" for i in range(1, 12)] + ["channel 12 (~core)", "channel 13 (~core)"]
                        + [f"channel {i:02d}" for i in range(14, 24)] + ["channel 24 (~LFS half radius)"]
                        + [f"channel {i:02d}" for i in range(25, 33)]
        }
    }
}

NBI_groups = {
    "Injector": {
        "NI20": {
            "Calorimeter": ["Position"],
            "Source3": ["P3", "Fired3"],
            "Source4": ["P4", "Fired4"]
        },
        "NI21": {
            "Calorimeter": ["Position"],
            "Source7": ["P7", "Fired7"],
            "Source8": ["P8", "Fired8"]
        }
    }
}
Physics_groups = {
    "W7X": {
        "Power": {
            "Plasma heating": ["ECRH power", "ICRH power", "NBI power"],
            "Gas": ["Main gas", "Main gas type Infobox", "Divertor gas", "Divertor gas type Infobox"],
            "Stored energy": ["Wdia"],
            "Bolometry": ["Total radiated power", "Inboard", "Outboard", "Core radiation", "GPI events", "MPM events", "LBO events"],
            "Power deposition": ["Radiated over total power"]
        },
        "Environment": {
            "Pressure": ["Plasma vessel pressure"],
            "Density": ["Line integrated density", "Line average density", "Line integrated density fluctuations (Det 1)"],
            "Zeff": ["H over H+He"],
            "Central temperature": ["Ion temperature", "Electron temperature"],
            "Line integrated temperature": ["Ion temperature", "Electron temperature"]
        }
    }
}

Video_groups ={
    "Video": {
        "In vessel": {
            "Edicam": ["AEQ10", "AEQ11", "AEQ20", "AEQ30", "AEQ31", "AEQ40", "AEQ41", "AEQ50", "AEQ51"],
            "Edicams image": ["Edicams.png"]
        }
    }
}


Diagnostics_groups = {
    "E3" : {
        "Faraday Cup": {
            "Raw voltage": ["sensor 1", "sensor 2", "sensor 3", "sensor 4", "sensor 5"],
            "Source current": ["sensor 1", "sensor 2", "sensor 3", "sensor 4", "sensor 5"],
            "Distance": ["Head", "Plate"]
            # In addition on logbookoverviewplot
            # Magnetic configuration
            # Thermocouple Temperature
            # draw interlock labels
            # draw qhf/mpm plunges
        }
    },
    "E5" : {
        "MPM": {
            "Position": ["FZJ-COMB3", "MPM events"]
        }
    }
}

Gas_groups = {
    "Main": {
        "Valves": {
            "Active valves": ["Total flow rate", "Gas type Infobox"],
            "Valve 1": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 2": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 3": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 4": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 5": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 6": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 7": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 8": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 9": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"],
            "Valve 10": ["Flow rate", "Gas type Infobox", "Valve voltage", "Density controller setpoint"]
       }
    },
    "Midplane": {
        "Valves": {
            "Module 2": ["A21 Piezo V", "A21 Infobox", "A21 pressure low"]  # , "A21 pressure high"]
        },
        "GPI" : {
            "Gas Puff Imaging": ["Differential pressure", "GPI events"]
        }
    },
    "Divertor": {
        "Valves": {
            "All valves": ["Total voltage", "Gas type Infobox"],
            "Module 1": [
                "H10 Piezo V", "H11 Piezo V", "H10 Infobox", "H11 Infobox",
                "H10 box P high", "H10 box P low", "H11 box P high", "H11 box P low"
            ],
            "Module 2": [
                "H20 Piezo V", "H21 Piezo V", "H20 Infobox", "H21 Infobox",
                "H20 box P high", "H20 box P low", "H21 box P high", "H21 box P low"
            ],
            "Module 3": [
                "H30 valve1 Piezo V", "H30 valve2 Piezo V", "H30 valve3 Piezo V",
                "H30 valve4 Piezo V", "H30 valve5 Piezo V", "H31 Piezo V",
                "H30 Infobox", "H31 Infobox", "H30 box P high", "H31 box P high",
                "H30 box P low", "H31 box P low"
            ],
            "Module 4": [
                "H40 Piezo V", "H41 Piezo V", "H40 Infobox", "H41 Infobox",
                "H40 box P high", "H40 box P low", "H41 box P high", "H41 box P low"
            ],
            "Module 5": [
                "H50 valve1 Piezo V", "H50 valve2 Piezo V", "H50 valve3 Piezo V",
                "H50 valve4 Piezo V", "H50 valve5 Piezo V", "H51 valve1 Piezo V",
                "H51 valve2 Piezo V", "H51 valve3 Piezo V", "H51 valve4 Piezo V",
                "H51 valve5 Piezo V", "H50 Infobox", "H51 Infobox",
                "H50 box P high", "H51 box P high", "H50 box P low", "H51 box P low"
            ]
        },
        "PID feedback": {
            "Module 1": [
                "H10 reference", "H10 P-contr.", "H10 I-contr.", "H10 D-contr.",
                "H11 reference", "H11 P-contr.", "H11 I-contr.", "H11 D-contr."
            ],
            "Module 2": [
                "H20 reference", "H20 P-contr.", "H20 I-contr.", "H20 D-contr.",
                "H21 reference", "H21 P-contr.", "H21 I-contr.", "H21 D-contr."
            ],
            "Module 3": [
                "H30 valve1 reference", "H30 valve1 P-contr.", "H30 valve1 I-contr.", "H30 valve1 D-contr.",
                "H30 valve2 reference", "H30 valve2 P-contr.", "H30 valve2 I-contr.", "H30 valve2 D-contr.",
                "H30 valve3 reference", "H30 valve3 P-contr.", "H30 valve3 I-contr.", "H30 valve3 D-contr.",
                "H30 valve4 reference", "H30 valve4 P-contr.", "H30 valve4 I-contr.", "H30 valve4 D-contr.",
                "H30 valve5 reference", "H30 valve5 P-contr.", "H30 valve5 I-contr.", "H30 valve5 D-contr.",
                "H31 reference", "H31 P-contr.", "H31 I-contr.", "H31 D-contr."
            ],
            "Module 4": [
                "H40 reference", "H40 P-contr.", "H40 I-contr.", "H40 D-contr.",
                "H41 reference", "H41 P-contr.", "H41 I-contr.", "H41 D-contr."
            ],
            "Module 5": [
                "H50 valve1 reference", "H50 valve1 P-contr.", "H50 valve1 I-contr.", "H50 valve1 D-contr.",
                "H50 valve2 reference", "H50 valve2 P-contr.", "H50 valve2 I-contr.", "H50 valve2 D-contr.",
                "H50 valve3 reference", "H50 valve3 P-contr.", "H50 valve3 I-contr.", "H50 valve3 D-contr.",
                "H50 valve4 reference", "H50 valve4 P-contr.", "H50 valve4 I-contr.", "H50 valve4 D-contr.",
                "H50 valve5 reference", "H50 valve5 P-contr.", "H50 valve5 I-contr.", "H50 valve5 D-contr.",
                "H51 valve1 reference", "H51 valve1 P-contr.", "H51 valve1 I-contr.", "H51 valve1 D-contr.",
                "H51 valve2 reference", "H51 valve2 P-contr.", "H51 valve2 I-contr.", "H51 valve2 D-contr.",
                "H51 valve3 reference", "H51 valve3 P-contr.", "H51 valve3 I-contr.", "H51 valve3 D-contr.",
                "H51 valve4 reference", "H51 valve4 P-contr.", "H51 valve4 I-contr.", "H51 valve4 D-contr.",
                "H51 valve5 reference", "H51 valve5 P-contr.", "H51 valve5 I-contr.", "H51 valve5 D-contr."
            ]
        }
    },
    "HeBeam": {
        "Voltages": {
            "HM30": ["Valve 1", "Valve 2", "Valve 3", "Valve 4", "Valve 5"],
            "HM51": ["Valve 1", "Valve 2", "Valve 3", "Valve 4", "Valve 5"]
        },
        "Feedback": {
            "Density setpoint": ["Setpoint 0", "Setpoint 1", "Setpoint 2", "Setpoint 3", "Setpoint 4", "Setpoint 5"]
        }
    }
}


tabs = {
    "ICRH": Tab("ICRH", ICRH_groups),
    "ECRH": Tab("ECRH", ECRH_groups),
    "NBI": Tab("NBI", NBI_groups),
    "Physics": Tab("Physics", Physics_groups),
    "Diagnostics": Tab("Diagnostics", Diagnostics_groups),
    "Gas": Tab("Gas", Gas_groups),
    "Video": Tab("Video", Video_groups)
}