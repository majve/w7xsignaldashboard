import Reader as Reader
import Plotter as Plotter
import Saver as Saver
import LogbookObjects as LogbookObjects
import LogbookAPI as LogbookAPI
import Video as Video
from Signal import Signal

import os
import numpy as np
import matplotlib.pyplot as plt
import datetime
import time
import logging
import pprint
import tkinter as tk
import w7xarchive

log = logging.getLogger("Output")

pp = pprint.PrettyPrinter(indent=2)  # just for looks


class Output:
    """ The Output class is the layer on top of the Reader, Plotter, Saver and Writer.
        The initializer can receive all possible input parameters and stores them as member variables.
        After initialization, the user should call one of three kinds of output function, namely: produceOutput, produceOutputForXPs and produceAnalysisoutput.
        The output functions will use the member functions _handlevideos, _readOutput, _analyzeOutput, _plotOutput, _saveDate, and _handleFigures
    """

    def __init__(self, outputname, plot_signals, XPid=None, res=100, timeType="Rel", mindim=None, maxdim=None, yScale="lin", cstRegions=None, integral=None,
                 showOutput=True, savePNG=False, savePDF=False, saveMP4=False, uploadComponentlog=False, uploadCompeventlog=False, saveData=False, specificsettings={}):
        """ Initializer of the output class, that receives and stores all the (optional) input parameters that can be used when reading, plotting, saving and writing the signals.
            If necessary, instantiate plotter object and prepare the figure with the appropriate title and number of subplots

            Parameters
            ----------
            outputname : string
                name that will be used in the filename and in the title on the figure
            plot_signals : dict
                dict where the key is the name of the subplot and the value is a list of signalnames that will be shown on the subplot.
                The signalnames correspond to any name in archive_signal_dict, derived_signal_dict, w7x_profile_dict or w7xdia_signal dict
                [the following not anymore:] or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'
            XPid : string
                Following the format "XP_20230330.64" or "None". If "None", then mindim and maxdim have to be provided
            res : int
                Maximal number of time intervals that will be read in the given time range
            timeType : string
                indicates whether the time is displayed in "Abs" values or "Rel" to trel
            mindim : float
                timestamp of the minimal time that is shown on the plot
            maxdim : float
                timestamp of the maximal time that is shown on the plot
            yScale : string
                indicates whether the y axis is "lin" or "log"
            cstRegions : tuple of floats
                Depict the tolerance to find regions of constant values. The first float depicts the percentage in which the signal has to
                remain constant within the rolling time window. The second float depicts the length in s of the rolling time window
            integral : Boolean
                Whether the integral of the signals has to be determined and printed in the terminal
            showOutput : Boolean
                Whether the output has to be shown on the screen
            savePNG : Boolean
                Whether the figures has to be saved as a png
            savePDF : Boolean
                Whether the figure has to be saved as a pdf
            saveMP4 : Boolean
                Whether the video has to be saved as an mp4
            uploadComponentlog : Boolean
                Whether the figure has to be uploaded to the logbook, as a component log
            uploadCompeventlog : Boolean
                Whether the figure has to be uploaded to the logbook, as a component event log

            Examples
            --------
            >>> Output("Generator 1", {'Endstage power': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']}, mindim=1680186447514002176, maxdim=1680186452909001984)
            """

        # Store input parameters as member variables
        self.plot_signals = {k: v for k, v in plot_signals.items() if v != []}  # Remove empty values
        self.XPid = XPid
        self.res = res
        self.timeType = timeType
        self.mindim = mindim
        self.maxdim = maxdim
        self.yScale = yScale
        self.cstRegions = cstRegions
        self.integral = integral
        self.showOutput = showOutput
        self.savePNG = savePNG
        self.savePDF = savePDF
        self.saveMP4 = saveMP4
        self.uploadComponentlog = uploadComponentlog
        self.uploadCompeventlog = uploadCompeventlog
        self.saveData = saveData

        self.unique_fig_name = ""
        # Safety checks:
        # Check if XPid or mindim, maxdim is provided
        if XPid == "None" and mindim is None:
            log.error("XPid or mindim,maxdim must be provided")
            return

        self.plot_type = {}
        self.plot_infos = {}
        for plotname, signals in self.plot_signals.items():
            types = Reader.getsignalTypesOfList(signals)
            # Collect 'info' type signals and other, 'dimval' signals separately
            info_signals = []
            dimval_signals = []
            for signal in signals:
                if types[signal] == 'info':
                    info_signals.append(signal)
                else:
                    dimval_signals.append(signal)

            if info_signals:
                self.plot_infos[plotname] = info_signals
                self.plot_signals[plotname] = dimval_signals

            # Check if all remaining types are identical (ignore event types)
            remaining_types = {signal: type for signal, type in types.items() if type not in ['info', 'event']}
            if len(set(remaining_types.values())) == 0:
                raise Exception('The plot cannot contain only events and/or infoboxes')
                # return
            elif len(set(remaining_types.values())) == 1:
                self.plot_type[plotname] = remaining_types[dimval_signals[0]]
            else:
                log.error('All types of one plot have to be identical')
                # return

#         log.info('Time of trigger t0 is {}'.format(t0))
#         log.info('Time of trigger t1 is {}'.format(t1))
#         log.info('Maximally {} time intervals will be plotted'.format(res))
#         log.info('Graphs will be plotted from {} (UTC {}) up to {} (UTC {})'.format(mindim*1e9, datetime.datetime.utcfromtimestamp(mindim).strftime('%d/%m/%Y  %H:%M:%S'), maxdim*1e9, datetime.datetime.utcfromtimestamp(maxdim).strftime('%d/%m/%Y  %H:%M:%S')))
#         log.debug('Y scale is set to {} '.format(yScale))
#         if showOutput: log.debug('The figures and videos will be shown')
#         if savePNG: log.debug('Figures will be saved as png at plots/{}.png'.format(filename))
#         if savePDF: log.debug('Figures will be saved as pdf at pdfs/{}.pdf'.format(filename))
#         if saveMP4: log.debug('Videos will be saved as mp4 at videos/{}xxx.mp4'.format(filename))
#         if saveData: log.debug('The time trace data will be stored at data/{}'.format(filename))

        #  Determine t1 and filename
        if self.XPid != "None":
            self.t1 = w7xarchive.get_program_triggerts(self.XPid.split("_")[1], 1)
            log.info('Time of trigger t1 is {}'.format(self.t1))
            self.filename = str(datetime.date.today()) + "_" + outputname + "_" + XPid
        else:
            self.t1 = self.mindim*1e9
            self.filename = str(datetime.date.today()) + "_" + outputname + "_timerange"

        # Set specific settings
        self.settings = {}
        self.settings.update(specificsettings)
        if 'ICRHfreq' in self.settings:
            log.info('The selected ICRH frequency is {:.1f} MHz'.format(self.settings['ICRHfreq']/10))
        self.settings['trel'] = self.t1  # TODO: this setting is needed to pass trel for w7xdia because of stupid reason (see Reader.py)
        self.settings['XPid'] = self.XPid  # This setting is needed to request PARLOGs or magneticSetting in Calibrating.py, Derivating.py or Profiles.py
        self.settings['mindim'] = self.mindim
        self.settings['maxdim'] = self.maxdim

        # If necessary, instantiate plotter object and prepare the figure with the appropriate title and number of subplots
        if self.showOutput or self.savePDF or self.savePNG:
            if any(sigtype in self.plot_type.values() for sigtype in ['timetrace', 'profile', 'image']):

                # Instantiate Plotter object
                self.plotter = Plotter.Plotter()

                # Determine title for figure
                if self.XPid == "None":
                    mindimstr = datetime.datetime.utcfromtimestamp(self.mindim).strftime('%d/%m/%Y  %H:%M:%S')
                    maxdimstr = datetime.datetime.utcfromtimestamp(self.maxdim).strftime('%d/%m/%Y  %H:%M:%S')
                    # title = '{} | {} - {}'.format(outputname, mindimstr, maxdimstr)
                    title = '{} - {}'.format(mindimstr, maxdimstr)
                else:
                    t0 = w7xarchive.get_program_triggerts(self.XPid.split("_")[1], 0)
                    log.info('Time of trigger t0 is {}'.format(t0))
                    t0str = datetime.datetime.utcfromtimestamp(t0 / 1e9).strftime('%H:%M:%S', )
                    title = '{} | {} | UTC: {}'.format(self.XPid, outputname, t0str)
                    # title = '{} | {} | UTC: {} | T0: {}'.format(self.XPid, outputname, t0str, t0)
                    # title = '{} | UTC: {} | T0: {}'.format(self.XPid, t0str, t0)

                # Determine the number of subplots that will go on the figure
                # It is intended that the images are counted double in nSubplots because they will span 2 subplots
                nImg = len(list(signaltype for signaltype in self.plot_type.values() if 'image' in signaltype or 'profile' in signaltype))
                nVideo = len(list(signaltype for signaltype in self.plot_type.values() if 'video' in signaltype))
                self.nSubplots = len(self.plot_signals) + nImg - nVideo  # TODO: If there are more than 6 subplots, make new figure.

                # prepare figure
                # Create a unique figure name with the username
                username = os.getenv('USER') or os.getenv('USERNAME', 'unknown_user')
                self.unique_fig_name = f'fig_{username}_{time.time()}'
                self.plotter.prepfig(self.nSubplots, title, fig_name=self.unique_fig_name)

                figok = True
                self.rowI = 1

    def produceOutput(self):
        """ Default way of producing the output. Videos will be handled. Other signals will be read, plotted and saved, following the request specified via the initializer.

            Returns
            -------
                figure: figure
                The figure that was created with all the subplots and graphs

            Examples
            --------
            >>> myOutput.produceOutput()
            """
        # Work plot per plot (for presets this means subsystem per subsystem)
        # This has the advantage that when one plot fails, the previous ones are already done.
        self.plotnames = list(self.plot_signals.keys())  # Necessary to work this way, to access the next plot later on
        for i, plotname in enumerate(self.plotnames):

            if 'video' in self.plot_type[plotname]:
                ani, fps = self._handlevideos(plotname)
                return ani, fps

            self._readOutput(plotname)
            self._integrateOutput()
            self._plotOutput(i, plotname)
            self._saveData(plotname)

        self._handleFigures()
        return plt.figure(self.unique_fig_name)

    def produceOutputForXPs(self, XPs, mindims=None, maxdims=None, align='Window', threshold=0.1):
        """ The signals will be shown for multiple XPs. For every signal, a different subplot will be used. In case the signals are aligned on the startvalue or on the
        maximal value, the signals will be shown on different figures, to avoid confusion in the time axis.

            Parameters
            ----------
            XP : string
                string of XPids, separated by a comma
            mindims : list of float
                list of respective timestamps of the minimal time that is shown on the plot
                If none, mindim is set to t0 for each XP
            maxdims : list of float
                list of respective timestamps of the maximal time that is shown on the plot
                If none, maxdim is set to t6 for each XP
            align : string
                indicating how the signals from different XPs will be aligned. For 'Start', the signals will be aligned on the first value that goes over threshold
                For 'Window', the relative time windows of the different XPs will be aligned. For 'Max', the signals will be aligned on their maximal value.
            threshold : float
                In case the alignment is set to 'Start', the threshold indicates the value that will be used for alignment.

            Returns
            -------
                figure: figure
                The figure that was created with all the subplots and graphs

            Note
            ----
            In Output.__init__(), the input parameter plot_signals has to contain only one signal per plot. Plot_signals is a dict
            where the key is the name of the subplot and the value is a list of signalnames that will be shown on the subplot. In the case of produceOutputForXPs, the list
            of signalnames should contain only one signalname. Otherwise an error message is shown and the call interrupted.

             Examples
             --------
             >>> myOutput.produceOutputForXPs("XP_20230328.27,XP_20230328.28,XP_20230328.32", 'Start', 0.3)
             """

        # Get current figure and change title
        f = plt.figure(self.unique_fig_name)
        f.suptitle('Comparison of experimental programs')

        # For each plot
        self.plotnames = list(self.plot_signals.keys())  # Necessary to work this way, to access the next plot later on
        for i, plotname in enumerate(self.plotnames):

            if 'video' in self.plot_type[plotname]:
                raise ValueError("Videos can not be requested for multiple XPs")

            # Safety check if on every plot is only one signal
            signals = self.plot_signals[plotname]
            if len(signals) > 1:
                log.error("Only one signal per subplot is possible.")
                return

            legendtitle = self._readOutputForXPs(plotname, XPs, timeType="Rel", mindims=mindims, maxdims=maxdims, align=align, threshold=threshold)
            # For the current plot, plot the signals and save the data
            self.t1 = 0
            self._integrateOutput() # TODO: useless?
            self._plotOutput(i, plotname)
            if self.showOutput or self.savePDF or self.savePNG:
                self.plotter.addLegendTitle(legendtitle)
            self._saveData(plotname)

        # Handle the figures
        self._handleFigures()
        return plt.figure(self.unique_fig_name)

    def produceAnalysisoutput(self, sigNames, functions):
        """ The signals are given names, such that they can be conveniently used in functions. The resulting function values are shown on one plot.

            Parameters
            ----------
            sigNames : list of strings
                Names that are given to the signals that were already passed to the initializer. In the Function objects, the signals are designated with this name.
            functions : list of Functions
                The Function class is defined in the file AnalysisGUI.py

            Note
            ----
            In Output.__init__(), the input parameter plot_signals has to contain only one plot. Plot_signals is a dict
            where the key is the name of the subplot and the value is a list of signalnames that will be shown on the subplot.
            In the case of produceAnalysisoutput, there should be only one key. Otherwise an error message is shown and the call interrupted.

             Examples
             --------
             >>> myOutput = Output.Output("Analysis", {'Analysis': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']})
                 myOutput.produceAnalysisoutput(["a", "b"], [Function1, Function2])
             """

        # TODO: the function objects are connected to the analysis GUI. Disentangle them.

        # Safety check if plot_signals contains only one key
        if len(self.plot_signals.keys()) > 1:
            log.error("To produce analysis output, pass to the Output initializer a signal dict with only one key.")
            return

        # Safety check if the signal names can correspond to the signallist
        if len(list(self.plot_signals.values())[0]) != len(sigNames):
            log.error("The signal names must correspond to the signals that were passed to the Output initializer. However, they do not have the same length.")
            return

        # In this case, there is only one plotname
        self.plotnames = list(self.plot_signals.keys())
        for i, plotname in enumerate(self.plotnames):
            self._readOutput(plotname)
            self._analyzeOutput(plotname, sigNames, functions)
            self._integrateOutput()
            self._plotOutput(i, plotname)
            self._saveData(plotname)

        self._handleFigures()

    # Note: when fitting is made a general feature, then fittype can be passed to the initializer
    def produceCorrelationOutput(self, XPs=None, mindims=None, maxdims=None, data="all", coeff=[], fittype=None):
        """ The two signals will be correlated. The signals will be read, plotted and saved, following the request specified via the initializer.

            Parameters
            ----------
            XP : string
                string of XPids, separated by a comma
            mindims : list of float
                list of respective timestamps of the minimal time that is shown on the plot
                If none, mindim is set to t0 for each XP
            maxdims : list of float
                list of respective timestamps of the maximal time that is shown on the plot
                If none, maxdim is set to t6 for each XP
            data:
            coeff:
            fittype:


            Returns
            -------
                figure: figure
                The figure that was created with all the subplots and graphs

            Examples
            --------
            >>> myOutput.produceCorrelationOutput()
            """

        # prepare figure again, to get bigger canvas with different title
        plt.close()
        username = os.getenv('USER') or os.getenv('USERNAME', 'unknown_user')
        self.unique_fig_name = f'fig_{username}_{time.time()}'
        self.plotter.prepfig(3, "Correlation", fig_name=self.unique_fig_name)

        # Safety check if plot_signals contains only one key
        if len(self.plot_signals.keys()) != 1:
            log.error("To produce correlation output, pass to the Output initializer a signal dict with only one key.")

        # In this case, there are two "plotnames" that each contain one signal. The signal will end up on one plot
        self.plotnames = list(self.plot_signals.keys())
        for i, plotname in enumerate(self.plotnames):
            legendtitle = self._readOutputForXPs(plotname, XPs, timeType="Abs", mindims=mindims, maxdims=maxdims, align='Window')
            if i==0:
                x_graphs = self.graphs
                x_label = legendtitle
            elif i==1:
                y_graphs = self.graphs
                y_label = legendtitle

        self._correlateOutput(x_graphs, y_graphs, x_label, y_label, data, coeff, fittype)
        self._saveData(plotname)

        self._handleFigures()
        return plt.figure(self.unique_fig_name)

    def _handlevideos(self, plotname):
        """For the signals in plot_signals that correspond to the key plotname, read the video signals from the ArchiveDB, make the video and save it if requested

            Parameters
            ----------
            plotname : string
                key of the plot_signals dict that was passed to the initializer, that has as values the list of videos

            Examples
            --------
            >>> myOutput = Output.Output("Video tracks", {'Edicam': ['W7-X/Video tracks/Edicam/AEQ10', 'W7-X/Video tracks/Edicam/AEQ11']})
                myOutput._handlevideos('Edicam')
            """

        # Work video per video. Only the first video will be processed and returned
        for signal in self.plot_signals[plotname]:
            chunk_filenames = []
            chunk_duration_ns = 3e9  # 3 seconds in nanoseconds
            current_start_time = int(self.mindim * 1e9)
            end_time = int(self.maxdim * 1e9)
            chunk_index = 0

            # Loop through the data in chunks of 3 seconds
            # Use multithreading to speed up the process. Make sure the video's are appended in the correct order
            while current_start_time < end_time:
                current_end_time = int(min(current_start_time + chunk_duration_ns, end_time))
                data = Reader.get_signal(signal, current_start_time, current_end_time, self.res, settings=self.settings)
                fig, chunk_filename, fps, plotok = Video.make_video(data, self.timeType, chunk_counter=chunk_index, trel=self.t1, scale=self.yScale, title="", cmap="Greys_r", axes=False, show=self.showOutput)
                chunk_filenames.append(chunk_filename)
                data = None  # to clear up memory
                current_start_time = current_end_time
                chunk_index += 1

            # only with old implementation, where ani was returned in stead of chunk filenames
            # if self.saveMP4:
            #     self.filename += "_" + signal.split('/')[-1]
            #     Saver.saveVideo(ani, self.filename, savepath='./videos/', fps=fps)
            # # rectime('download and store video done')

            return chunk_filenames, fps

    def _readOutput(self, plotname):
        """For the signals in plot_signals that correspond to the key plotname, read the signals from the ArchiveDB

            Parameters
            ----------
            plotname : string
                key of the plot_signals dict that was passed to the initializer, that has as values the list of signals

             Examples
             --------
             >>> myOutput = Output.Output("Generator 1", {'Endstage power': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']},)
                 myOutput._readOutput('Endstage power')
             """
        if 'image' not in self.plot_type[plotname]:
            signalnames = self.plot_signals[plotname]
            # Get the data for the graphs of all the signals that will go in plot
            rectime('download signals start')
            self.graphs = Reader.get_signalsOfList(signalnames, int(self.mindim*1e9), int(self.maxdim*1e9), self.res, settings=self.settings)
            rectime('download signals end')
            # TODO: maybe better to set self.plotname as a member, for the memberfunctions to access, instead of passing the plotname?

            self.infoboxes = None
            if plotname in self.plot_infos:
                infos = self.plot_infos[plotname]
                self.infoboxes = Reader.get_texts(infos, int(self.mindim*1e9), int(self.maxdim*1e9), settings=self.settings)

    # When reading multiple XPs, the structure of the dicts is different to when reading just one XP.
    # one XP: the key is the signalname.
    # multiple XPs: the key is the XPid. legends contains the XPid. The legend (one string per plot) is passed as returnvalue legendtitle
    # It would be better if reading one XP is a special case of reading many XPs.
    def _readOutputForXPs(self, plotname, XPs, timeType="Rel", mindims=None, maxdims=None, align='Window', threshold=0.1, interpolate=False):

        # Define a dictionary to hold the updated Signal objects
        new_graphs = {}
        legendtitle = ""
        # For each XP
        for j, XP in enumerate(XPs):
            # Set the timing information for the current XP
            # If the user specified mindims and maxdims, those are used (convenient for special GUI settings). Otherwise, the signal from t0 upto t6 is used.
            if mindims:
                self.mindim = mindims[j]
            else:
                self.mindim = w7xarchive.get_program_triggerts(XP.split("_")[1], 0) / 1e9
            if maxdims:
                self.maxdim = maxdims[j]
            else:
                self.maxdim = w7xarchive.get_program_triggerts(XP.split("_")[1], 6) / 1e9

            self.t1 = w7xarchive.get_program_triggerts(XP.split("_")[1], 1)
            self.settings['trel'] = self.t1  # Used when reading signals from w7xdia...

            # Read the signals for the plot for the current XP
            self._readOutput(plotname)  # self.graphs is filled

            # For each signal (should be only one signal, but for correlation, this will be two signals)
            for signalname, signal in self.graphs.items():
                if signal.ok:
                    if timeType == "Rel":
                        # Determine trel
                        if align == 'Start':
                            # trel is at the first value that goes over threshold
                            index = np.argmax(signal.values > threshold)
                            trel = signal.dimensions[index]
                        elif align == 'Window':
                            # trel is t1
                            trel = self.t1
                        elif align == 'Max':
                            # trel is at the maximal value
                            index = np.argmax(signal.values)
                            trel = signal.dimensions[index]

                        # Set the times relative to trel now, such that plotter plots them in the same time window
                        dimrel = [t - trel for t in signal.dimensions]
                        signal.dimensions = np.array(dimrel)

                    # Create a new Signal object for the updated values
                    new_graphs[XP] = Signal(
                        signalname=XP,
                        dimensions=signal.dimensions,
                        values=signal.values,
                        unit=signal.unit,
                        legend=XP,  # The legend now holds the XP instead of the signalname
                        ok=signal.ok,
                        signal_type=signal.signal_type
                    )
                    legendtitle = signal.legend  # !Note that the signalname is passed via the returnvalue legendtitle

            # Update self.graphs to the new structure with updated Signal objects
            self.graphs = new_graphs

        if timeType == "Rel":
            # Set the mindim and maxdim conveniently for the plotter
            if align == 'Start':
                self.mindim = -0.2
                # Find the latest time for which the value of any signal was over 0.01
                self.maxdim = max(
                    time for signal in self.graphs.values()
                    for time, value in zip(signal.dimensions, signal.values)
                    if value > 0.001
                )
                # self.maxdim = (max(max(dims) for dims in alldims.values()))  # -= self.t1/1e9
                self.maxdim *= 1.2 / 1e9
            elif align == 'Window':
                self.mindim -= self.t1 / 1e9
                self.maxdim -= self.t1 / 1e9
            elif align == 'Max':
                # Find the first time for which the value of any signal was over 0.01
                self.mindim = min(
                    time for signal in self.graphs.values()
                    for time, value in zip(signal.dimensions, signal.values)
                    if value > 0.001
                )
                self.mindim *= 1.2 / 1e9
                # Find the latest time for which the value of any signal was over 0.01
                self.maxdim = max(
                    time for signal in self.graphs.values()
                    for time, value in zip(signal.dimensions, signal.values)
                    if value > 0.001
                )
                self.maxdim *= 1.2 / 1e9

        return legendtitle

    def _integrateOutput(self):
        """For the signals in plot_signals that correspond to the key plotname, integrate the signals from the ArchiveDB (after they were read)

              Examples
              --------
              >>> myOutput = Output.Output("Generator 1", {'Endstage power': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']},)
                  myOutput._readOutput('Endstage power')
                  myOutput._integrateOutput()
              """

        if self.integral:
            # The member variable self.graphs is defined in _readOutput()
            # Iterate over each signal in the updated self.graphs structure
            for key, signal in self.graphs.items():
                integral = 0
                for i in range(1, len(signal.dimensions)):
                    # Uncomment the following line to only regard the signal above zero (to avoid noise)
                    # if signal.values[i] >= 0 and signal.values[i - 1] >= 0:
                    # Determine the integral using the Trapezoid rule
                    dt = signal.dimensions[i] - signal.dimensions[i - 1]
                    avg_value = (signal.values[i] + signal.values[i - 1]) / 2
                    integral += dt * avg_value

                # Print the result in the terminal
                # TODO: show the integral in a dedicated window or on the plot
                # Note: Time conversions are already applied in plotter
                print('The integral of {} over [{:.2f},{:.2f}] s is {:.3f} {}*s'.format(
                    signal.legend,
                    signal.dimensions[0] - self.t1 / 1e9,
                    signal.dimensions[-1] - self.t1 / 1e9,
                    integral,
                    signal.unit.split('[')[1].split(']')[0]
                ))

    def _analyzeOutput(self, plotname, sigNames, functions):
        """For the signals in plot_signals that correspond to the key plotname, assign them the names of sigNames and evaluate the functions that use these signals (after they were read)

            Parameters
            ----------
            plotname : string
                key of the plot_signals dict that was passed to the initializer, that has as values the list of signals
            sigNames : list of strings
                Names that are given to the signals that were already passed to the initializer. In the Function objects, the signals are designated with this name.
            functions : list of Functions
                The Function class is defined in the file AnalysisGUI.py

            Examples
            --------
            >>> myOutput = Output.Output("Generator 1", {'Endstage power': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']},)
                myOutput._readOutput('Endstage power')
                myOutput._analyzeOutput('Endstage power')
        """

        # TODO: tricky that signames and signals is passed separately to the class.
        # Put analysis algorithm in separate script Analyzer.py

        signals = self.plot_signals[plotname]

        # Initialize maxSig for later
        maxSig = sigNames[0]

        # For each signal (iterating sigNames or signals should be the same)
        for i in range(len(sigNames)):
            signal_name = signals[i]
            sigName = sigNames[i]

            # Retrieve the current signal from the graphs
            signal = self.graphs.pop(signal_name, None)
            if signal is None:
                log.error(f"Signal {signal_name} not found in self.graphs.")
                return

            # Safety check if only time traces are used
            if signal.signal_type != 'timetrace':
                log.error(f'For analysis, only timetrace signals are accepted. Got {signal.signal_type} instead')
                return

            # Assign the renamed signal back to self.graphs with the new name
            self.graphs[sigName] = signal

            # Determine the signal with the maximal number of timestamps, to use these as timestamps to evaluate the interpolation
            if len(signal.dimensions) > len(self.graphs[maxSig].dimensions):
                maxSig = sigName

        # Safety check if all signals are ok
        if not all(signal.ok for signal in self.graphs.values()):
            log.error('One of the signals that should be used in an analysis function is not ok. Aborting.')
            return

        # Interpolate all the signals, such that they have as time stamps the one from maxSig
        interp = {}
        max_dims = self.graphs[maxSig].dimensions

        for sigName in sigNames:
            signal = self.graphs[sigName]

            # Safety check if the dimensions are increasing
            if not np.all(np.diff(signal.dimensions) > 0):
                log.info("The dimension is non-increasing, interpolation results may be meaningless.")

            # Perform interpolation
            interp[sigName] = np.interp(
                max_dims, signal.dimensions, signal.values, left=np.NaN, right=np.NaN
            )

        # Initialize dictionaries to store the function values
        new_graphs = {}

        # For each function
        for function_obj in functions:
            # If the checkbox of the function was not selected, continue (GUI dependent!)
            if not function_obj.check_var.get():
                continue

            funcName = function_obj.funcName_var.get()
            function = function_obj.function_var.get()

            try:
                # Evaluate the function. Put results in dicts for plotter
                func_vals = eval(function, interp)

                # If no signal was used in the evaluation, the result is a constant integer,
                # so turn it into ndarray of same length as the dimension
                if isinstance(func_vals, int):
                    func_vals = np.repeat(func_vals, len(max_dims))
                    log.info(f'Increased length of {funcName} from 1 to {len(func_vals)}, to match dimension of length {len(max_dims)}')

                # Create a new Signal object for the function result
                new_signal = Signal(
                    signalname=funcName,
                    dimensions=max_dims,
                    values=func_vals,
                    unit=f"[{function_obj.funcUnit_var.get()}]",
                    legend=funcName,
                    ok=True,
                    signal_type='timetrace'
                )
                new_graphs[funcName] = new_signal

            except Exception as e:
                log.error(f'{e}: exception evaluating function: {funcName}')
                new_signal = Signal(
                    signalname=funcName,
                    dimensions=max_dims,
                    values=np.full_like(max_dims, np.nan),
                    unit=f"[{function_obj.funcUnit_var.get()}]",
                    legend=funcName,
                    ok=False,
                    signal_type='timetrace'
                )
                new_graphs[funcName] = new_signal

        # Update self.graphs to include the new functions
        self.graphs.update(new_graphs)

    def _plotOutput(self, i, plotname):  # PLOTTING
        """For the signals in plot_signals that correspond to the key plotname, plot the signals (after they were read) in the current subplot of the figure that was
            prepared by the initializer

            Parameters
            ----------
            i: int
                index of the plotname in the memmmmberlist plotnames
            plotname : string
                key of the plot_signals dict that was passed to the initializer, that has as values the list of signals

            Examples
            --------
            >>> myOutput = Output.Output("Generator 1", {'Endstage power': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']},)
                myOutput._readOutput('Endstage power')
                myOutput._plotOutput('Endstage power')
            """

        # Only proceed if the plot has to be shown or saved
        if True:  # self.showOutput or self.savePDF or self.savePNG:
            signals = self.plot_signals[plotname]
            rectime('plot start')

            # If this subplot will hold an image
            if 'image' in self.plot_type[plotname]:
                # Add the image that spans 2 subplots

                imagefile = "images/" + signals[0].split('/')[-1]
                filename = os.path.join(os.path.dirname(__file__), imagefile)
                plotok = self.plotter.plotIm(filename, self.rowI)
                self.rowI += 2

            # If this subplot will hold a profile
            elif 'profile' in self.plot_type[plotname]:
                # Add the 2D profiles to the subplot that will span 2 subplots
                plotok = self.plotter.plot2D(self.graphs, self.rowI, show=self.showOutput)
                self.rowI += 2

            # If this subplot will hold a timetrace
            elif 'timetrace' in self.plot_type[plotname]:
                # By default, the X axis is not drawn for the subplot
                drawXaxis = False
                # If this is the last plot, draw the X axis (probably time)
                if plotname == self.plotnames[-1]:
                    drawXaxis = True
                # if the next plot is an image or Profile, draw the X axis
                elif any(sigtype in self.plot_type[self.plotnames[i+1]] for sigtype in ['image', 'profile']):
                    drawXaxis = True

                # Make the plot with all the graphs and add it as subplot to the figure
                # TODO: make an option to change the xlabel (e.g. to tstart, tmaxval, tmin)
                plotok = self.plotter.plotTimeTrace(self.graphs, self.mindim, self.maxdim, self.yScale, self.timeType, self.rowI, drawXaxis, show=self.showOutput, trel=self.t1, cstRegions=self.cstRegions)
                self.plotter.addtextboxes(self.infoboxes)

                self.rowI += 1
            else:
                log.error('Type not recognised by plotOutput')

            rectime('plot end')

            # TODO: figok is not returned or used at the moment
            if not plotok:
                figok = False

    def _correlateOutput(self, x_graphs, y_graphs, x_label, y_label, datasetting, coeffsetting, fitsetting):
        """For the two signals in plot_signals that correspond to the key plotname, read the two signals and use their values
           to make a 2D plot in the one and only subplot of the figure that was prepared by the initializer

            Parameters
            ----------
            x_graphs
            y_graphs
            x_label
            y_label
            datasetting
            coeffsetting
            fitsetting

            Examples
            --------
            >>> myOutput = Output.Output("Correlation", {'Correlation': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']})
                myOutput._readOutput('Correlation')
                myOutput._correlateOutput('Correlation')
            """

        # Only proceed if the plot has to be shown or saved
        if self.showOutput or self.savePDF or self.savePNG:
            rectime('plot start')
            # Add the correlation to the subplot that will span 3 subplots
            plotok = self.plotter.plotCorrelation(x_graphs, y_graphs, x_label, y_label, self.rowI, scale=self.yScale, data=datasetting, coeff=coeffsetting, fit=fitsetting, show=self.showOutput)
        else:
            log.error("You can only use time traces for correlation")

        rectime('plot end')

        # TODO: figok is not returned or used at the moment
        if not plotok:
            figok = False

    def _saveData(self, plotname):
        """For the signals in plot_signals that correspond to the key plotname, save the signals (after they were read)

            Parameters
            ----------
            plotname : string
                key of the plot_signals dict that was passed to the initializer, that has as values the list of signals

            Examples
            --------
            >>> myOutput = Output.Output("Generator 1", {'Endstage power': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']},)
                myOutput._readOutput('Endstage power')
                myOutput._saveData('Endstage power')
            """

        if self.saveData:
            rectime('save data start')
            # Pass the timetraces to the saver, which exports them to HDF5 file
            if 'timetrace' in self.plot_type[plotname]:
                Saver.saveData(self.graphs, self.filename, savepath='./data/')
            rectime('save data end')


#TODO: The functions below are only used for the tkinter GUI. Not for the dashboard app. Move these functions to GUI.py or elsewhere
    def _handleFigures(self):
        """Handle the figures, i.e. if requested, save them as a png or pdf, or upload them to the logbook

            Examples
            --------
            >>> myOutput = Output.Output("Generator 1", {'Endstage power': ['Generator/Generator 1/Endstage power/Forward power', 'Generator/Generator 1/Endstage power/Reflected power']},)
                myOutput._readOutput('Endstage power')
                myOutput._plotOutput('Endstage power')
                myOutput._handleFigures()
            """

        # Save figure as PNG
        if self.savePNG:
            # if figok:
            rectime('store as PNG start')
            Saver.saveFigAsPNG(self.filename, savepath='./plots/')
            rectime('store as PNG end')

        # Save figure as PDF
        if self.savePDF:
            # if figok:
            rectime('store as PDF start')
            Saver.saveFigsAsPDF(self.filename, savepath='./pdfs/')
            rectime('store as PDF end')

        # Upload the figure as comment to a component log
        if self.uploadComponentlog:
            # At the moment, the comment is just uploaded to the component
            # The more correct way would be to create a component log for the component, and upload the comment to the log.
            # In order to do this, when creating the component log, we must know the log ID it received.
            # then we can upload the comment to this log ID.

            rectime('logbook upload start')
            plt.pause(1)  # this is the only way I found to show the plot before asking to upload to the logbook

            # Create component log (skipped at the moment)
            # description = 'ICRH operation'
            # tag = LogbookObjects.Tag('ICRH frequency', self.settings['ICRHfreq']/10, 'MHz', 'ICRH frequency', 'Heating', 'CC')
            # TODO: ask user to update fields (name, description, etc.)
            # mycomplog = LogbookObjects.componentlog(self.XPid, description, tag.Tag, int(self.mindim*1e9), int(self.maxdim*1e9))
            # LogbookAPI.post_component_log('CC', XPid, mycomplog.log, mode="append")
            # 216   CC	"ICRH (Heizsysteme)"

            self.comment_window()

        if self.uploadCompeventlog:  # upload comment to component event log
            # Not implemented at the moment
            pass

            # xcloudtransfer()
            # Not implemented at the moment

    def comment_window(self):

        self.win = tk.Toplevel()
        self.win.wm_title("Component comment")

        frm = tk.Frame(self.win, borderwidth=15, bg="alice blue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        lbl = tk.Label(frm, text="Component comment", bg="alice blue", font='helvetica 15 bold')
        frm.grid(row=0, column=0, rowspan=1, sticky="nsew")
        lbl.grid(row=0, column=0, sticky="w", padx=5)

        username_lbl = tk.Label(frm, text="username: ", bg="alice blue")
        self.username_var = tk.StringVar("")
        username_entr = tk.Entry(frm, textvariable=self.username_var, width=20)
        username_lbl.grid(row=1, column=0, sticky="w", padx=5)
        username_entr.grid(row=1, column=1, sticky="w", padx=5, pady=8)

        comment_lbl = tk.Label(frm, text="comment (optional): ", bg="alice blue")
        self.comment_var = tk.StringVar("")
        comment_entr = tk.Entry(frm, textvariable=self.comment_var, width=20)
        comment_lbl.grid(row=2, column=0, sticky="w", padx=5)
        comment_entr.grid(row=2, column=1, sticky="w", padx=5, pady=8)

        compid_lbl = tk.Label(frm, text="comp ID: ", bg="alice blue")
        self.compid_var = tk.StringVar("")
        compid_entr = tk.Entry(frm, textvariable=self.compid_var, width=20)
        compid_lbl.grid(row=3, column=0, sticky="w", padx=5)
        compid_entr.grid(row=3, column=1, sticky="w", padx=5, pady=8)

        btn = tk.Button(frm, text="Set", bg="LightSteelBlue", command=self.set_comment_info)
        btn.grid(row=4, column=1, sticky="e", padx=5, pady=8)

    def set_comment_info(self):
        self.username = self.username_var.get()
        self.comment = self.comment_var.get()
        self.compid = self.compid_var.get()

        self.win.destroy()
        self.create_comment()

    def create_comment(self):
        # Get the current figure
        image = plt.figure(self.unique_fig_name)
        # Or potentially, use an image that is stored on your computer
        # image = open(imagepath, "rb")
        # Convert the image to html
        imagehtml = LogbookAPI.imagetohtml(image)

        content = imagehtml + self.comment
        # Adrian just omits the LOGid in the comment
        mycomment = LogbookObjects.comment(self.username, content, self.XPid, int(self.mindim*1e9), int(self.maxdim*1e9))

        # Adrian checks if comment exists by:
        # - getting all comments from programcomments or programcomponentcomments
        # [I can do this via LogbookAPI.search_componentlog_comments(COMPid, XPid=None)]
        # - checking if self-created string "id:astech_overviewplot_full_" is present
        # Then editprogramcomment or postprogramcomment

        tk.messagebox.showinfo("Note", "After closing this window, enter password on commandline")
        # Upload the comment to the logbook
        LogbookAPI.create_comment(self.compid, mycomment.comment)
        rectime('logbook upload end')

def rectime(item):
    log.info('{} at {}'.format(item, datetime.datetime.utcfromtimestamp(time.time()).strftime('%H:%M:%S')))
