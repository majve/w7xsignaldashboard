from archive_signal_dict import archive_signal_dict
from derived_signal_dict import derived_signal_dict
import w7xarchive
import numpy as np
import logging
import json
import matplotlib.pyplot as plt

log = logging.getLogger("Writer")

def write(signalList, dims, vals, units, oks):
    """ Write signals from archive_signal_dict or derived_signal_dict to the ArchiveDB, i.e. calibrated or derived signals.
        (1) Collect signals into streams. Within a stream, the timestamps of the signals must match.
            The stream is determined by the url. Everything befpre _DATASTREAM is the name of the stream.
            Everything behind _DATASTREAM/Vx is ordered in one hdf5 file. Therefore the signals in one stream must hame the same imestamps
        (2) For every stream, put all signals in one numpy array and write the array to the archivedb, together with its dimension and channel descriptions.

        Parameters
        ----------
        signalList : list of signal names,
            where the names correspond to any name in archive_signal_dict, derived_signal_dict, w7x_profile_dict or w7xdia_signal dict
            or where the name is the signal address in ArchiveDB as URL 'Database/View/Project/StreamGroup /Stream/Channel#/Channel Name'
        dims (usually timestamps) : numpy array of dtype _np.int64
        vals : numpy array of dtype
        units : string that contains unit in [ ]
        oks : boolean

        Examples
        --------
        >>> write(
        ['Generator/Generator 1/Endstage power/Forward power','Generator/Generator 1/Endstage power/Reflected power']
        {'Generator/Generator 1/Endstage power/Forward power': array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984]),
         'Generator/Generator 1/Endstage power/Reflected power': array([1680186447514002176, 1680186447515002368, 1680186447516002304, ..., 1680186452907002368, 1680186452908002560, 1680186452909001984])},
        {'Generator/Generator 1/Endstage power/Forward power': array([3.31494596e-01, 3.21567363e-01, 3.26694578e-01, ..., 5.88288144e-08, 4.69324613e-08, 3.60661929e-08]),
         'Generator/Generator 1/Endstage power/Reflected power': array([5.74903380e-01, 6.26232477e-01, 6.62313770e-01, ..., 2.48760446e-05, 2.46257701e-05, 2.94657759e-05])},
        {'Generator/Generator 1/Endstage power/Forward power': 'P [MW]',
         'Generator/Generator 1/Endstage power/Reflected power': 'P [kW]'},
        {'Generator/Generator 1/Endstage power/Forward power': True,
         'Generator/Generator 1/Endstage power/Reflected power': True}
         )
        """

    # Initialize dicts
    stream_chanNr_chdescr = {}  # {stream: {channelnumber: description}}
    stream_sig_chanNr = {}  # {stream: {signal: channelnumber}}
    stream = None

    # (1) Cluster signals into streams. The timestamps of the signals must match.

    # For every signal (i.e. every channel)
    for i in range(len(signalList)):

        signal = signalList[i]
        if not oks[signal]:
            continue

        if signal in archive_signal_dict:
            parts = archive_signal_dict[signal]['url'].split('/')  # e.g. Sandbox/raw/W7XAnalysis/Maja_tests/CCDAQ.1_calibrated_DATASTREAM/V1/0/Gen1_Endstage_forward_power
            description = archive_signal_dict[signal]['description']
        elif signal in derived_signal_dict:
            parts = derived_signal_dict[signal]['url'].split('/')
            description = derived_signal_dict[signal]['description']
        # Take first 5 parts of url. As tuple, the delimiter / is only placed in between elements, not at front and back
        stream = "/".join(tuple(parts[0:5]))  # e.g. Sandbox/raw/W7XAnalysis/Maja_tests/CCDAQ.1_calibrated_DATASTREAM
        stream = stream.replace('_DATASTREAM', '')  # e.g. /Sandbox/raw/W7XAnalysis/Maja_tests/CCDAQ.1_calibrated

        # Store the channel number in the dedicated dict
        if stream not in stream_sig_chanNr.keys():
            stream_sig_chanNr[stream] = {}
        stream_sig_chanNr[stream][signal] = int(parts[6])
        channelStr = "[" + parts[6] + "]"

        # Store the channel descriptor in the dedicated dict. The channel descriptor describes the channel in a predefined format
        if stream not in stream_chanNr_chdescr.keys():
            stream_chanNr_chdescr[stream] = {}

        unitstr = units[signal]
        unit = unitstr[unitstr.find('[') + 1: unitstr.find(']')]
        stream_chanNr_chdescr[stream][channelStr] = {"name": parts[-1],
                                                     "description": description,
                                                     "physicalQuantity": {"type": unit, "from": np.nanmin(vals[signal]), "upto": np.nanmax(vals[signal])}
                                                     }
        #TODO: it is not possible to only write channel with channel number 4. The code below, initializes
        # streams for 0, 1, 2, 3, 4. If here a channeldescription would be given for all streams. You could write for channel 4 and the rest would be filled with 0.


    # (2) For every stream, collect all signals in one numpy array and write the array to the archivedb, together with its dimension and channel descriptions.
    # For every stream
    for stream, sig_chanNr in stream_sig_chanNr.items():
        firstStreamSig = list(sig_chanNr.keys())[0]
        num_channels = max(sig_chanNr.values()) + 1

        # If there is only one signal, initialize adb_vals as 1D; otherwise, use 2D.
        if num_channels == 1:
            # Only one signal, use 1D array
            if oks[firstStreamSig]:
                adb_vals = vals[firstStreamSig].copy()  # Directly assign the 1D values
        else:
            # Multiple signals, use 2D array with channels and time
            adb_vals = np.zeros((num_channels, len(vals[firstStreamSig])))
            # Populate adb_vals with signal data
            for signal in sig_chanNr.keys():
                if oks[signal]:
                    adb_vals[sig_chanNr[signal]] = vals[signal]

        try:
            chdescr_env = {"chanDescs": stream_chanNr_chdescr[stream]}
            # print(json.dumps(chdescr_env))
            # Use the w7xarchive functionality to write to the stream (url)
            # (i) the dict {signal: channelnumber}
            # (ii) the dimensions (of the first signal but which are (MUST BE) the same for all signals of the stream
            # (iii) the 2D array, containing for every channel its values as one row

            # plt.plot(dims[firstStreamSig].tolist(), adb_vals.T)
            # plt.show()
            w7xarchive.write_to_adb(stream, chdescr_env, dims[firstStreamSig], adb_vals, debug=False)
        except Exception as e:
            log.error('{}: exception writing signals to: {}'.format(e, stream))
            return

        log.info('Wrote signals to {} from {} upto {}'.format(stream, dims[firstStreamSig][0], dims[firstStreamSig][-1]))

