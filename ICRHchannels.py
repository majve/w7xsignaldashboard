import w7xarchive
import Output as Output
import tkinter as tk
import datetime
import pprint
pp = pprint.PrettyPrinter(indent=2)  # just for looks


class ICRHchannels:

    def __init__(self, GUI):
        self.GUI = GUI

    def getChannels(self):
        """Get the channels for the ICRH streams in the selected program or time selection"""

        # Clear the channels that are currently in the tree
        for i in self.GUI.channel_tree.get_children():
            self.GUI.channel_tree.delete(i)

        # Get the program ID. If it is None (as will be the case often for ICRH), use the time selection instead
        currentProgramID = self.GUI.Selection.getCurrentProgramID()
        if currentProgramID == None:
            time_from, time_upto = self.GUI.Selection.getTimeRange()
        else:
            time_from, time_upto = w7xarchive.get_program_from_to(currentProgramID.split("_")[1])
            time_from = datetime.datetime.utcfromtimestamp(time_from/1e9).strftime('%Y-%m-%d %H:%M:%S')
            time_upto = datetime.datetime.utcfromtimestamp(time_upto/1e9).strftime('%Y-%m-%d %H:%M:%S')

        try:
            # The ICRH stream names are hardcoded. Get the available channels for the streams during the time selection.
            CCDAQ1_stream = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/ControlStation.2179/CCDAQ.1_DATASTREAM/"
            CCDAQ1_chans = w7xarchive.get_available_channels(CCDAQ1_stream, time_from, time_upto)
            # print("CCDAQ1")
            # pp.pprint(self.CCDAQ1_chans)

            CCDAQ2_stream = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/ControlStation.2179/CCDAQ.2_DATASTREAM/"
            CCDAQ2_chans = w7xarchive.get_available_channels(CCDAQ2_stream, time_from, time_upto)
            # print("CCDAQ2")
            # pp.pprint(self.CCDAQ2_chans[1])

            CCDAQ3_stream = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/ControlStation.2179/CCDAQ.3_DATASTREAM/"
            CCDAQ3_chans = w7xarchive.get_available_channels(CCDAQ3_stream, time_from, time_upto)
            # print("CCDAQ3")
            # pp.pprint(self.CCDAQ3_chans[1])

            PLC1_stream = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/ControlStation.2144/PLC.1_DATASTREAM/"
            PLC1_chans = w7xarchive.get_available_channels(PLC1_stream, time_from, time_upto)
            # print("PLC1")
            # pp.pprint(self.PLC1_chans[1])

            PLC2_stream = "http://archive-webapi.ipp-hgw.mpg.de/ArchiveDB/raw/W7X/ControlStation.2144/PLC.2_DATASTREAM/"
            PLC2_chans = w7xarchive.get_available_channels(PLC2_stream, time_from, time_upto)
            # print("PLC2")
            # pp.pprint(self.PLC2_chans[1])
        except Exception as e:
            self.GUI.channel_tree.insert('', tk.END, text='None', iid="0", open=False)

        else:
            self.GUI.channel_tree.insert('', tk.END, text='CCDAQ.1', iid="0", open=False)
            self.GUI.channel_tree.insert('', tk.END, text='CCDAQ.2', iid="1", open=False)
            self.GUI.channel_tree.insert('', tk.END, text='CCDAQ.3', iid="2", open=False)
            self.GUI.channel_tree.insert('', tk.END, text='PLC.1', iid="3", open=False)
            self.GUI.channel_tree.insert('', tk.END, text='PLC.2', iid="4", open=False)

            id = 5
            for i in range(len(CCDAQ1_chans[1])):
                self.GUI.channel_tree.insert('', tk.END, text=str(CCDAQ1_chans[0][i]) + "/" + str(CCDAQ1_chans[1][i]), iid=str(id), open=False)
                self.GUI.channel_tree.move(str(id), "0", i)
                id += 1

            for i in range(len(CCDAQ2_chans[1])):
                self.GUI.channel_tree.insert('', tk.END, text=str(CCDAQ2_chans[0][i]) + "/" + str(CCDAQ2_chans[1][i]), iid=str(id), open=False)
                self.GUI.channel_tree.move(str(id), "1", i)
                id += 1

            for i in range(len(CCDAQ3_chans[1])):
                self.GUI.channel_tree.insert('', tk.END, text=str(CCDAQ3_chans[0][i]) + "/" + str(CCDAQ3_chans[1][i]), iid=str(id), open=False)
                self.GUI.channel_tree.move(str(id), "2", i)
                id += 1

            for i in range(len(PLC1_chans[1])):
                self.GUI.channel_tree.insert('', tk.END, text=str(PLC1_chans[0][i]) + "/" + str(PLC1_chans[1][i]), iid=str(id), open=False)
                self.GUI.channel_tree.move(str(id), "3", i)
                id += 1

            for i in range(len(PLC2_chans[1])):
                self.GUI.channel_tree.insert('', tk.END, text=str(PLC2_chans[0][i]) + "/" + str(PLC2_chans[1][i]), iid=str(id), open=False)
                self.GUI.channel_tree.move(str(id), "4", i)
                id += 1

        self.GUI.update()

    def plotChannels(self):

        plot_graphs = {"ICRH channels": []}
        selected_indices = self.GUI.channel_tree.selection()
        if selected_indices == ():
            pass
        else:
            plot_graphs = {}
            plot_graphs["ICRH channels"] = []
            for i in selected_indices:
                chanName = self.GUI.channel_tree.item(i)["text"]
                streamid = self.GUI.channel_tree.parent(i)
                streamName = self.GUI.channel_tree.item(streamid)["text"]

                signal_name = ""
                if "PLC" in streamName:
                    signal_name = "ArchiveDB/raw/W7X/ControlStation.2144/" + streamName + "_DATASTREAM/" + chanName
                elif "CCDAQ" in streamName:
                    signal_name = "ArchiveDB/raw/W7X/ControlStation.2179/" + streamName + "_DATASTREAM/" + chanName
                plot_graphs["ICRH channels"].append(signal_name)

            Output.Output(self.GUI, "ICRH", plot_graphs)
