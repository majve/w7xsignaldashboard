# Signal URLs to add to make the ICRH monitor a more general tool

# this will make it a more general tool for
# a) scripting:  using the backend of the tool for physics analysis
# b) signal observation: using the GUI to monitor fast data streams




# ECRH power
archive_signal_dict = {
# ADDED TO ECRH_signals
# 'ECRH_A1': '/ArchiveDB/codac/W7X/CoDaStationDesc.108/'
#            'DataModuleDesc.240_DATASTREAM/0/Rf_A1/scaled/',
# 'ECRH_B1': '/ArchiveDB/codac/W7X/CoDaStationDesc.108/'
#            'DataModuleDesc.240_DATASTREAM/8/Rf_B1/scaled/',
# 'ECRH_C1': '/ArchiveDB/codac/W7X/CoDaStationDesc.104/'
#            'DataModuleDesc.236_DATASTREAM/0/Rf_C1/scaled/',
# 'ECRH_D1': '/ArchiveDB/codac/W7X/CoDaStationDesc.104/'
#            'DataModuleDesc.236_DATASTREAM/8/Rf_D1/scaled/',
# 'ECRH_E1': '/ArchiveDB/codac/W7X/CoDaStationDesc.94/'
#            'DataModuleDesc.209_DATASTREAM/0/Rf_E1/scaled/',
# 'ECRH_F1': '/ArchiveDB/codac/W7X/CoDaStationDesc.94/'
#            'DataModuleDesc.209_DATASTREAM/8/Rf_F1/scaled/',
# 'ECRH_A5': '/ArchiveDB/codac/W7X/CoDaStationDesc.106/'
#            'DataModuleDesc.237_DATASTREAM/0/Rf_A5/scaled/',
# 'ECRH_B5': '/ArchiveDB/codac/W7X/CoDaStationDesc.106/'
#            'DataModuleDesc.237_DATASTREAM/8/Rf_B5/scaled/',
# 'ECRH_C5': '/ArchiveDB/codac/W7X/CoDaStationDesc.101/'
#            'DataModuleDesc.229_DATASTREAM/0/Rf_C5/scaled/',
# 'ECRH_D5': '/ArchiveDB/codac/W7X/CoDaStationDesc.101/'
#            'DataModuleDesc.229_DATASTREAM/8/Rf_D5/scaled/',
# 'ECRH_E5': '/ArchiveDB/codac/W7X/CoDaStationDesc.17/'
#            'DataModuleDesc.24_DATASTREAM/0/Rf_E5/scaled/',
# 'ECRH_F5': '/ArchiveDB/codac/W7X/CoDaStationDesc.17/'
#            'DataModuleDesc.179_DATASTREAM/4/Rf_F5/scaled/',
#
# # total ECRH power
# 'ECRH_tot': '/ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_DATASTREAM/'
#             'V1/0/Ptot_ECRH/',
# 'ECRH_tot_old': '/raw/W7X/CoDaStationDesc.18774/'
#                 'FeedBackProcessDesc.18770_DATASTREAM/0/ECRH%20Total%20Power/',
#
# # string with active ECRH channels
# 'ECRH_active': '/ArchiveDB/codac/W7X/CBG_ECRH/TotalPower_PARLOG/V1/parms/ActiveGyrotrons/',

# randomly chosen sniffer channel
'sniffer': '/ArchiveDB/codac/W7X/CoDaStationDesc.111/'
           'DataModuleDesc.250_DATASTREAM/15/L1_ECA63/scaled/',


# diamagnetic energy, processed by Minerva
# old, direct link
'Wdia': '/Test/raw/Minerva1/Minerva.Magnetics15.Wdia/'
         'Wdia_compensated_QXD31CE001x_DATASTREAM/V1/0/'
         'Wdia_compensated_for_diamagnetic_loop%20QXD31CE001x/',
# new, fancy alias
'Wdia': '/Test/views/Minerva/DiamagneticEnergy/DiamagneticLoop.TriangularPlane/'
        'Wdia_compensated/signal/',

# NBI voltages and currents
'NBI_U7': '/ArchiveDB/raw/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/4/HGV_3 Monitor U/',
'NBI_I7': '/ArchiveDB/raw/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/5/HGV_3 Monitor I/',
'NBI_U8': '/ArchiveDB/raw/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/6/HGV_4 Monitor U/',
'NBI_I8': '/ArchiveDB/raw/W7X/CoDaStationDesc.31/DataModuleDesc.24119_DATASTREAM/7/HGV_4 Monitor I/',

# flag to mark if NBI fires into vessel
'NBI_w7X': '/ArchiveDB/raw/W7X/CoDaStationDesc.30/DataModuleDesc.25219_DATASTREAM/210/PlzKKW7XPlasma Value/',

# Rogowski coil toroidal current
# old, direct link
'Irog': '/Test/raw/Minerva1/Minerva.Magnetics15.Iplasma/'
         'Iplasma_QXR11CE001x_DATASTREAM/V1/0/Iplasma_for_continous_Rogowski_QXR11CE001x/',
# new, fancy alias
'Irog': '/Test/views/Minerva/PlasmaCurrent/RogowskiCoil.Continuous/Iplasma/signal/',

# interferometry raw signal, uncalibrated, with phase wraps
'density': '/ArchiveDB/codac/W7X/CoDaStationDesc.110/'
           'DataModuleDesc.246_DATASTREAM/11/L5_ECA59/',

# interferometry slow signal with considerable time jitter but no phase wrap and low noise
'density_slow': '/ArchiveDB/codac/W7X/CoDaStationDesc.16339/'
                'DataModuleDesc.16341_DATASTREAM/0/Line%20integrated%20density/',

# old thomson central channel (OP1.2a only)
'ne_center': '/Test/raw/W7X/QTB_Central/volume_2_DATASTREAM/V1/1/ne_map/',
'Te_center': '/Test/raw/W7X/QTB_Central/volume_2_DATASTREAM/V1/0/Te_map/',

# total radiative power from bolometer
'Prad_OP12a': '/Test/raw/W7XAnalysis/BoloTest6/PradVBC_DATASTREAM/',
'Prad': '/Test/raw/W7XAnalysis/BoloData/PradHBC_DATASTREAM/0/Prad_HBC/',
'Prad_HBC': '/Test/raw/W7XAnalysis/BoloTest6/PradHBC_DATASTREAM/0/Prad_HBC/',
'Prad_fast': '/Test/raw/W7X/QSB_Bolometry/Bolo_HBCmPrad_DATASTREAM/',

# selected channels from bolometer
'bolo_core_OP12a': '/Test/raw/W7X/QSB_Bolometry/BoloSignal_DATASTREAM/48/E_Chan_49/',
'bolo_core': '/Test/raw/W7XAnalysis/BoloData/lin_VBCr_DATASTREAM/15/gch16/',
'bolo_core_fast': '/Test/raw/W7X/QSB_Bolometry/BoloPowerRaw_DATASTREAM/79/E_Chan_80/',
'bolo_outeredge_OP12a': '/Test/raw/W7X/QSB_Bolometry/BoloSignal_DATASTREAM/66/E_Chan_67/',
'bolo_outeredge': '/Test/raw/W7XAnalysis/BoloData/lin_VBCr_DATASTREAM/1/gch2/',
'bolo_outeredge_fast': '/Test/raw/W7X/QSB_Bolometry/BoloPowerRaw_DATASTREAM/65/E_Chan_66/',
'bolo_inneredge_OP12a': '/Test/raw/W7X/QSB_Bolometry/BoloSignal_DATASTREAM/56/E_Chan_57/',
'bolo_inneredge': '/Test/raw/W7XAnalysis/BoloData/lin_VBCl_DATASTREAM/22/gch23/',
'bolo_inneredge_fast': '/Test/raw/W7X/QSB_Bolometry/BoloPowerRaw_DATASTREAM/62/E_Chan_63/',

# mirnov coils
'QXM1B1_rand': '/ArchiveDB/raw/W7X/CoDaStationDesc.23686/DataModuleDesc.23696_DATASTREAM/QXM10CE010x/',
'QXM2B1_rand': '/ArchiveDB/raw/W7X/CoDaStationDesc.23701/DataModuleDesc.23710_DATASTREAM/7/QXM31CE040x/',

# H/He line ratio from edge spectroscopy
'HHeratio': '/ArchiveDB/raw/W7XAnalysis/QSK06_PassiveSpectroscopy/FastSpec_USB_HR4000_HHeRatio_DATASTREAM/V1/0/HHeRatio/',
'HHeratio_OP12a': '/Test/raw/W7XAnalysis/QSK06-PassiveSpectroscopy/FastSpec_USB_HR4000_HHeRatio_DATASTREAM/V1/0/HHeRatio/',

# coils
'NPC1': '/ArchiveDB/raw/W7X/CoDaStationDesc.84/DataModuleDesc.21643_DATASTREAM/0/current_npc1 (AAE10)/',
'NPC2': '/ArchiveDB/raw/W7X/CoDaStationDesc.84/DataModuleDesc.21643_DATASTREAM/1/current_npc2 (AAE29)/',
'NPC3': '/ArchiveDB/raw/W7X/CoDaStationDesc.84/DataModuleDesc.21643_DATASTREAM/2/current_npc3 (AAE38)/',
'NPC4': '/ArchiveDB/raw/W7X/CoDaStationDesc.84/DataModuleDesc.21643_DATASTREAM/3/current_npc4 (AAE47)/',
'NPC5': '/ArchiveDB/raw/W7X/CoDaStationDesc.84/DataModuleDesc.21643_DATASTREAM/4/current_npc5 (AAE56)/',
'PCA': '/ArchiveDB/raw/W7X/CoDaStationDesc.84/DataModuleDesc.21643_DATASTREAM/5/current_pca (AAE14)/',

# this coil is confused and changed labels mid-campaign
'PCB': '/ArchiveDB/raw/W7X/CoDaStationDesc.84/DataModuleDesc.21643_DATASTREAM/6/',
'TC1': '/ArchiveDB/views/KKS/AAQ_TrimCoils/Measured_currents/AAQ11_c1/',
'TC2': '/ArchiveDB/views/KKS/AAQ_TrimCoils/Measured_currents/AAQ22_c2/',
'TC3': '/ArchiveDB/views/KKS/AAQ_TrimCoils/Measured_currents/AAQ31_c3/',
'TC4': '/ArchiveDB/views/KKS/AAQ_TrimCoils/Measured_currents/AAQ41_c4/',
'TC5': '/ArchiveDB/views/KKS/AAQ_TrimCoils/Measured_currents/AAQ51_c5/',

# interlocks
'interlock_QXD': '/ArchiveDB/raw/W7X/CoDaStationDesc.21003/DataModuleDesc.21005_DATASTREAM/28/cFIS_Intlk_QXD_akt/',
'interlock_QMJ_ECRH': '/ArchiveDB/raw/W7X/CoDaStationDesc.21003/DataModuleDesc.21005_DATASTREAM/29/cFIS_Intlk_QMJ_ECRH_akt/',
'interlock_QMJ_NBI': '/ArchiveDB/raw/W7X/CoDaStationDesc.21003/DataModuleDesc.21005_DATASTREAM/30/cFIS_Intlk_QMJ_NBI_akt/',
'interlock_ECRH': '/ArchiveDB/raw/W7X/CoDaStationDesc.21003/DataModuleDesc.21005_DATASTREAM/34/cFIS_Intlk_Streustr_akt/',

# VMEC best match reference ID and scaling factor for B
'VMEC_id': '/ArchiveDB/raw/W7XAnalysis/Equilibrium/RefEq_PARLOG/V1/parms/equilibriumID/',
'VMEC_Bscale': '/ArchiveDB/raw/W7XAnalysis/Equilibrium/RefEq_PARLOG/V1/parms/scalingFactorB0/',
'VMEC_boz100_id': '/ArchiveDB/raw/W7XAnalysis/Equilibrium/BozEquilibrium_100ms_PARLOG/V1/parms/equilibriumID/',
'VMEC_boz100_Bscale': '/ArchiveDB/raw/W7XAnalysis/Equilibrium/BozEquilibrium_100ms_PARLOG/V1/parms/scalingFactorB0/',
'VMEC_boz200_id': '/ArchiveDB/raw/W7XAnalysis/Equilibrium/BozEquilibrium_200ms_PARLOG/V1/parms/equilibriumID/',
'VMEC_boz200_Bscale': '/ArchiveDB/raw/W7XAnalysis/Equilibrium/BozEquilibrium_200ms_PARLOG/V1/parms/scalingFactorB0/',

# pellet event trigger
'event_trigger': '/ArchiveDB/raw/W7X/CoDaStationDesc.19944/TimeModuleDesc.28059_DATASTREAM/0/Pellet Events/',
}

# gyrotrons
_gyros = ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'A5', 'B5', 'C5', 'D5', 'E5', 'F5']
for idx, g in enumerate(_gyros):
    # z offset of launchers
    key = 'ECRH_{}_zoff'.format(g)
    archive_signal_dict[key] = '/ArchiveDB/codac/W7X/CoDaStationDesc.69/' \
                               'DataModuleDesc.138_DATASTREAM/{}/Z_off_{}/scaled/'.format(34 + idx * 9, g)

    # phi offset of launchers
    key = 'ECRH_{}_phioff'.format(g)
    archive_signal_dict[key] = '/ArchiveDB/codac/W7X/CoDaStationDesc.69/' \
                               'DataModuleDesc.138_DATASTREAM/{}/Phi_tor_{}/scaled/'.format(33 + idx * 9, g)

    # ECRH power setpoints
    key = 'ECRH_setpoint_{}'.format(g)
    archive_signal_dict[key] = '/Test/raw/Data4SoftwareTest/CBG_ECRH.Test/' \
                               '{}_MORE_DATASTREAM/V1/5/{}Pset/'.format(g, g)

# ECE channels
for i in range(1, 33):
    # "fast" channels (available soon after shot)
    key = 'ECE_{:02d}'.format(i)
    archive_signal_dict[key] = '/ArchiveDB/views/KKS/QME_ECE/standard_reduced/' \
                               '{:02d}/'.format(i - 1)

    # Minerva-processed channels (available minutes to hours after shot)
    key = 'ECE_{:02d}_minerva'.format(i)
    archive_signal_dict[key] = '/ArchiveDB/raw/Minerva/' \
                               'Minerva.ECE.DownsampledRadiationTemperatureTimetraces/' \
                               'signal_DATASTREAM/V1/{:02d}/QME-ch{:02d}/'.format(i - 1, i)


# Thomson channels
_tschans = list(range(1, 17)) + list(range(1004, 1007)) + list(range(1016, 1019)) + \
    [1025, 1026, 1035, 1036, 1041, 1042, 1045, 1046] + \
    [1053, 1054, 1057, 1058, 1063, 1064, 1069, 1070, 1073, 1074, 1075, 1076]
baseurl = '/Test/raw/W7X/QTB_Profile'
for idx, c in enumerate(_tschans):
    # electron density
    key = 'ne_ts_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_DATASTREAM/V1/1/ne_map/'.format(c)

    # electron density upper error
    key = 'ne_ts_hi_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_DATASTREAM/V1/9/ne_high97.5/'.format(c)

    # electron density lower error
    key = 'ne_ts_lo_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_DATASTREAM/V1/8/ne_low97.5/'.format(c)

    # electron temperature
    key = 'Te_ts_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_DATASTREAM/V1/0/Te_map/'.format(c)

    # electron temperature upper error
    key = 'Te_ts_hi_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_DATASTREAM/V1/7/Te_high97.5/'.format(c)

    # electron temperature lower error
    key = 'Te_ts_lo_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_DATASTREAM/V1/6/Te_low97.5/'.format(c)

    # scattering volume coordinates
    key = 'thomson_pos_x_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_PARLOG/V1/parms/position/x_m/'.format(c)

    key = 'thomson_pos_y_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_PARLOG/V1/parms/position/y_m/'.format(c)

    key = 'thomson_pos_z_{}'.format(c)
    archive_signal_dict[key] = baseurl + '/volume_{}_PARLOG/V1/parms/position/z_m/'.format(c)

# gas valves
_vorder = [343, 349, 344, 350, 345, 351, 348, 346, 352, 347, 353]
baseurl = '/ArchiveDB/raw/W7X/CoDaStationDesc.10/'
for i in range(0, 11):
    # valve voltages
    key = 'ValveV{}'.format(i)
    archive_signal_dict[key] = baseurl + 'DataModuleDesc.10_DATASTREAM/{}/'.format(i)

    # density controller setpoints
    key = 'ValveSP{}'.format(i)
    archive_signal_dict[key] = baseurl + 'DataModuleDesc.12/{}/'.format(i)

baseurl = '/ArchiveDB/codac/W7X/CoDaStationDesc.10/'
for i in range(0, 11):
    # valve flows
    key = 'ValveF{}'.format(i)
    archive_signal_dict[key] = baseurl + 'DataModuleDesc.10_DATASTREAM/{}/'.format(i + 22)

    # gas type in gas line
    key = 'ValveGas{}'.format(i)
    archive_signal_dict[key] = baseurl + 'DataModuleDesc.11_DATASTREAM/{}/'.format(_vorder[i])

# He beam valves
baseurl = '/ArchiveDB/raw/W7X/QSQ_Hebeam/Piezo_valve_voltage_'
for i in range(1, 6):
    # voltages in HM30
    key = 'HeBeam30V{}'.format(i)
    archive_signal_dict[key] = baseurl + 'AEH30_DATASTREAM/V1/{}/AEH30_{}/'.format(i - 1, i)

    # voltages in HM51
    key = 'HeBeam51V{}'.format(i)
    archive_signal_dict[key] = baseurl + 'AEH51_DATASTREAM/V1/{}/AEH51_{}/'.format(i - 1, i)

# Mirnov coils in half module 41 (next to 50)
baseurl = '/ArchiveDB/raw/W7X/CoDaStationDesc.23701/DataModuleDesc.23713_DATASTREAM/'
for i,nr in enumerate([70, 80, 90, 100, 110, 260, 270, 280, 290, \
    300, 120, 130, 370, 310, 320, 140, 150, 160, 170, 180, 190, 200, \
    210, 220, 230, 240, 250, 330, 340, 350, 360]):
    key = 'QXM41CE{:03d}'.format(nr)
    archive_signal_dict[key] = baseurl + '{}/{}x/'.format(i + 1, key)

# Mirnov coils in half module 11
baseurl = '/ArchiveDB/raw/W7X/CoDaStationDesc.23686/DataModuleDesc.23696_DATASTREAM/'
for i,nr in [(26, 120)]:
    key = 'QXM11CE{:03d}'.format(nr)
    archive_signal_dict[key] = baseurl + '{}/{}x/'.format(i + 1, key)





