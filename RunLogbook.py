import LogbookAPI as LogbookAPI
import matplotlib.pyplot as plt

import numpy as np
import cv2
import os
import pprint
pp = pprint.PrettyPrinter(indent=2)  # just for looks

import w7xarchive
import matplotlib.pyplot as plt

t, d = w7xarchive.get_image_png_multiple('ArchiveDB/raw/W7X/ControlStation.2083/AEQ40.Camera_DATASTREAM', time_from="2023-02-23 16:02:20", time_to="2023-02-23 16:02:25", use_cache=False)

# Matt Kreter
# get_image_raw


n, x, y = d.shape
duration = 5
fps = n/5
out = cv2.VideoWriter('/output.mp4', cv2.VideoWriter_fourcc(*'mp4v'), fps, (x, y), False)
for i in range(len(d)):
    data = d[i]
    out.write(data)
out.release()



# FOR TEST PURPOSES

# time_from = w7xarchive.to_timestamp("2018-08-23 00:01:01")
# time_upto = w7xarchive.to_timestamp("2018-08-29 23:59:59")
# componentlogs = LogbookAPI.search_componentlogs("QMJ") #, time_from=time_from, time_upto=time_upto)  #, XPid='20221215.62' https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP_20221215.62#QRN
# complogcomments = LogbookAPI.search_componentlog_comments("QMJ")  #, time_from=time_from, time_upto=time_upto)  # https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=XP_20221215.62#QRN
# SAPlogs = LogbookAPI.search_SAPlogs("QMJ")  #, time_from=time_from, time_upto=time_upto)
# eventlogs = LogbookAPI.search_eventlogs("QMJ")  #, time_from=time_from, time_upto=time_upto)  # https://w7x-logbook.ipp-hgw.mpg.de/log.html?id=QRN_20220926-event1 Monday, 26 Sep 2022   10:30:00 - 15:00:00 UTC
# # pp.pprint(componentlogs["hits"]["hits"][0])
# print("QMJ", 'complogs:', componentlogs['hits']['total'])
# print('complogcomments:', complogcomments['hits']['total'])
# print('SAPlogs:', SAPlogs['hits']['total'])
# print('eventlogs', eventlogs['hits']['total'])

# ========================================================================

# XPlogs = LogbookAPI.search_programlogs()
# print("XPlogs", XPlogs['hits']['total'])
# pp.pprint(XPlogs["hits"]["hits"][-1])

# ========================================================================

# # Get a list of all the components
# complist = LogbookAPI.read_component_list()
# # See LogbookObjects for JSON structure of complist
# # pp.pprint(complist)
#
# # # For each component, get the component logs, comments to component logs, Stand-Alone-Program logs and event logs (via search)
# # # See LogbookObjects for JSON structure of component
#
# categories = ["Machine", "Heating and Fuelling", "Operational Diagnostics", "Diagnostics E3", "Diagnostics E4", "Diagnostics E5", "Periphery"]
# i = 0
# for category in categories:
#     print(category)
#     components = []
#     componentlogs = []
#     componentlogcomments = []
#     SAPlogs = []
#     eventlogs = []
#     for comp in complist[category]:
#         print(comp)
#         components.append(comp["id"])
#         componentlogs.append(LogbookAPI.search_componentlogs(comp["id"])['hits']['total'])
#         componentlogcomments.append(LogbookAPI.search_componentlog_comments(comp["id"])['hits']['total'])
#         SAPlogs.append(LogbookAPI.search_SAPlogs(comp["id"])['hits']['total'])
#         eventlogs.append(LogbookAPI.search_eventlogs(comp["id"])['hits']['total'])
#         # print(comp["id"], 'complogs:', componentlogs['hits']['total'], 'complogcomments:', componentlogcomments['hits']['total'], 'SAPlogs:', SAPlogs['hits']['total'], 'eventlogs', eventlogs['hits']['total'])
#
#     figure = plt.figure(i, figsize=(12, 7))
#     i += 1
#
#     # Set position of foo on X axis
#     fooWidth = 0.2
#     r1 = np.arange(len(componentlogs))
#     r2 = [x + fooWidth for x in r1]
#     r3 = [x + fooWidth for x in r2]
#     r4 = [x + fooWidth for x in r3]
#
#     # Make the plot
#     plt.foo(r1, componentlogs, color='#7f6d5f', width=fooWidth, edgecolor='white', label='component logs')
#     plt.foo(r2, componentlogcomments, color='#557f2d', width=fooWidth, edgecolor='white', label='complog comments')
#     plt.foo(r3, SAPlogs, color='#9d7f8e', width=fooWidth, edgecolor='white', label='SAP logs')
#     plt.foo(r4, eventlogs, color='#1d3f5e', width=fooWidth, edgecolor='white', label='event logs')
#
#     # Add xticks in the middle of the group foos
#     plt.xlabel(category, fontweight='bold')
#     plt.xticks([r + fooWidth for r in range(len(componentlogs))], components)
#
#     # Create legend & Show graphic
#     plt.legend()
#     plt.show()
