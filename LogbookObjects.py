# Overview of the dictionaries that are used as JSON by the logbook.
# In the first place, this serves as look-up-table to know what you will receive or have to send in LogbookAPI.py
# Some objects are used indeed.

# IDEA: When posting a json, remove all key-value pairs for which the value is None.
# or ask Michael Grahl how the None-values are handled.

# IDEA: When posting a json to the logbook, check if the dict has the correct keys.

class XPlog:  # Also used as component stand-alone-program log
    def __init__(self, name, description, session_comment, tags):
        self.log = {  # 'id': id,                                 # string	        ID of the program.	                                                    Created by Xcontrol
                        'name': name,                             # string	        Name of the program.
                        'description': description,               # string	        Description of the program.
                        'session_comment': session_comment,       # string	        Session information.
                        'tags': tags,                             # array of tags	Program tags. [see Tag class]
                        # 'component_status': component_status,   # array 	        Status information of components with segment control.                  Created by Xcontrol
                        # 'execution_status': execution_status,   # int 	        Execution status of the segment control program.	                    Created by Xcontrol
                        # 'scenarios': scenarios,                 # array        	Listing of scenarios of program, incl. tags, exec. and comp. status.	Created by Xcontrol
                        # 'from': time_from,                      # long	        Start time in W7-X time	                                                Created by Xcontrol
                        # 'upto': time_upto,                      # long	        End time in W7-X time.	                                                Created by Xcontrol
                        # 'from_str': from_str,                   # string	        Start time in W7-X time as string.	                                    Created by Logbook
                        # 'upto_str': upto_str,                   # string	        End time in W7-X time as string.	                                    Created by Logbook
                        # 'version': version                      # version	        A version object.	[See Version class]                                 Created by Logbook
                        }


class componentlog:
    def __init__(self, ref_id, description, tags, time_from, time_upto=None, type=None):
        self.log = {'ref_id': ref_id,            # string	        ID of the reference XP.
                    'description': description,  # string	        Log content.
                    'tags': tags,                # array of tags	component tags. [See Tag class]
                    'from': time_from,           # long	            start time of the log in W7-X time.
                    'upto': time_upto,           # long	            end time of the log in W7-X time.       optional
                    'type': type,                # string	        Log type.	                            optional (not used at the moment)
                    # 'id': id,                  # string	        internal ID of the log.	                Created by Logbook
                    # 'version': version         # version	        A version object. [See Version class]   Created by Logbook
                    }


class component_event_log:
    def __init__(self, name, description, session_comment, tags, mytype, time_from, time_upto, from_str, upto_str, id, version):
        self.log = {'name':  name,                       # string	        Event name.	            optional
                    'description': description,          # string	        Log content.
                    'session_comment': session_comment,  # string	        Session information.	Shown as Session info on the website.
                    'tags': tags,                        # array of tags	Event tags.
                    'type': mytype,                      # string	        Event type.             can be: Event, Overview, Test, Commissioning, Calibration, Downtime, Failure, Other
                    'from': time_from,                   # long	            start time.
                    'upto': time_upto,                   # long	            end time.	            optional
                    'from_str': from_str,                # string       	start time.	            Created by Logbook
                    'upto_str': upto_str,                # string	        end time.	            optional / Created by Logbook
                    'id': id,                            # string	        event log id.	        Created by Logbook
                    'version': version                   # version	        the version object	    Created by Logbook
                    }


class comment:
    def __init__(self, user, content, ref_id, time_from, time_upto=None):
        self.comment = {'user': user,                   # string 	The user id of a W7-X team member
                        'content': content,             # string	The content of the comment.
                        'ref_id': ref_id, 	            # string	The ID of the corresponding log.
                        'from': time_from,              # long	    Start of a reference period in W7-X time.
                        'upto': time_upto,              # long	    End of a reference period in W7-X time.	            optional
                        # 'timestamp': timestamp,       # long	    Creation time of the comment in W7-X time.	        Created by Logbook
                        # 'changed_time': changed_time  # long	    Last change time of the comment in W7-X time.   	Created by Logbook
                        }

class Tag:
    def __init__(self, name, value, unit, description, category, component):
        self.Tag = {'name': name,
                    'value': value,
                    'unit': unit,
                    'description': description,
                    'category': category,  # Experiment, Heating, Fuelling, Magnetic field, Diagnostics
                    'component': component
                    }


class Version:
    def __init__(self, user, creation_date, version_number):
        self.Version = {'user': user,
                        'creation_date': creation_date,
                        'version_number': version_number
                        }


# class operator_log:
#     def __init__(self, tlvd, phase, description, time_from, time_upto):
#         self.log = {'tlvd': tlvd,	                # string	TLvD of the shift.	                Created by Logbook
#                     'phase': phase,	            # string	W7-X operation phase.
#                     'description': description,	# string	Description part of the log.
#                     'from': time_from,	        # long	    Start time in W7-X time
#                     'upto': time_upto,	        # long	    End time in W7-X time.
#                     # 'from_str': from_str,	    # string	Start time in W7-X time as string.	Created by Logbook
#                     # 'upto_str': upto_str,	    # string	End time in W7-X time as string.	Created by Logbook
#                     # 'version': version	        # version	The version object.	                Created by Logbook
#                     }

# class operator_event_log:
#     def __init__(self, name, description, measures, kks, component_name, phase, bsk, time_from, time_upto):
#         self.log = {'type': type,                       # string 	Operator event type.	                    Planned or Incident
#                     'description': description,         # string	Description of the event.
#                     'counter_measures': measures,       # string	counter measures	                        optional
#                     'kks': kks,                         # string	KKS value of the component	                Must be from kks_list
#                     'component_name': component_name,   # string	W7-X component name.	                    Must be from kks_list
#                     'phase': phase,                     # string	W7-X operation phase.
#                     'bsk': bsk,                         # int	    Number of the Betriebsstörungskarte.	    optional
#                     'from': time_from,                  # long	    Start time in W7-X time
#                     'upto': time_upto,                  # long	    End time in W7-X time.
#                     # 'from_str': from_str,             # string	Start time in W7-X time as string.	        Created by Logbook
#                     # 'upto_str': upto_str,             # string	End time in W7-X time as string.	        Created by Logbook
#                     # 'version': version,               # version	The version object.	                        Created by Logbook
#                     # 'id': id                          # string	Logbook identifier of the Operator event.	Created by Logbook
#                     }

class ComponentCategories:
    def __init__(self, Machine_list, HeFu_list, OP_list, E3_list,E4_list, E5_list, Periphery_list):
        self.categories = {"Machine": Machine_list,             # List of component dicts (see below)
                           "Heating and Fuelling": HeFu_list,
                           "Operational Diagnostics": OP_list,
                           "Diagnostics E3": E3_list,
                           "Diagnostics E4": E4_list,
                           "Diagnostics E5": E5_list,
                           "Periphery": Periphery_list
                           }


class Component:
    def __init__(self, id, name, ro, type, category, status, status_comment):
        self.compdict = {"id": id,                          # string    component id (kks)
                         "name": name,                      # string    component name
                         "ro": ro,                          # string    responsible officer
                         "type": type,                      # string    component department (OP,E3,E4,E5,TD, or DIR)
                         "category": category,              # string    Component category (Machine, Heating and Fueling, etc.)
                         "status": status,                  # integer   Component status	0 = offline, 1 = ready, 2 = diag. tests, 3 = Test operation
                         "status_comment": status_comment   # integer   Status comment
                         }
