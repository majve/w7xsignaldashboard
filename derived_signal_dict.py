import Derivating as Derivating

derived_signal_dict = {
    'Antenna/RF/Antenna current/Phase between straps': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaRF_DATASTREAM/V1/0/AntennaPhaseBetweenStraps',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Antenna/RF/Antenna phase/Left strap",
                      "Antenna/RF/Antenna phase/Right strap"],
        'unit': "phase [$°$]",
        'legend': "$Phi_{between\\ straps}$",
        'derivationfunc': Derivating.voltage_to_phase_from_table
    },
    'Antenna/RF/Capacitor power/Capacitor 1': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaRF_DATASTREAM/V1/1/CapacitorPower1',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Antenna/RF/Capacitor voltages/Capacitor 1 (CE301)", "Antenna/RF/Capacitor currents/Capacitor 1 (CE302)"],
        'unit': "power [a.u.]",
        'legend': "$P_{cap1}$",
        'derivationfunc': Derivating.power_from_current_and_voltage
    },
    'Antenna/RF/Capacitor power/Capacitor 2': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaRF_DATASTREAM/V1/2/CapacitorPower2',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Antenna/RF/Capacitor voltages/Capacitor 2 (CE401)", "Antenna/RF/Capacitor currents/Capacitor 2 (CE402)"],
        'unit': "power [a.u.]",
        'legend': "$P_{cap2}$",
        'derivationfunc': Derivating.power_from_current_and_voltage
    },
    # DRC1
    'Calorimetry/Directional Couplers/DRC1 Power/Net power': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC1_DATASTREAM/V1/0/DRC1_NetPower',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC1 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC1 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC1 Voltages/Reflected voltage mix"],
        'unit': "P [kW]",
        'legend': "$P_{net}$ DRC1",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'pwr_net', 'DRC': 1, 'CMfile': 'Calibrationfiles/CM_DRC1L2_INSITU_AL_WFILTERS_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC1 Power/Forward power': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC1_DATASTREAM/V1/1/DRC1_ForwardPower',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC1 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC1 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC1 Voltages/Reflected voltage mix"],
        'unit': "P [kW]",
        'legend': "$P_{forw}$ DRC1",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'pwr_fwd', 'DRC': 1, 'CMfile': 'Calibrationfiles/CM_DRC1L2_INSITU_AL_WFILTERS_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC1 Power/Reflected power': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC1_DATASTREAM/V1/2/DRC1_ReflectedPower',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC1 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC1 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC1 Voltages/Reflected voltage mix"],
        'unit': "P [kW]",
        'legend': "$P_{refl}$ DRC1",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'pwr_rfl', 'DRC': 1, 'CMfile': 'Calibrationfiles/CM_DRC1L2_INSITU_AL_WFILTERS_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC1 Reflection coefficient/GMSR': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC1_DATASTREAM/V1/3/DRC1_GMSR',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC1 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC1 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC1 Voltages/Reflected voltage mix"],
        'unit': "$\\Gamma$ [None]",
        'legend': "GSMR DRC1",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'gam_amp', 'DRC': 1, 'CMfile': 'Calibrationfiles/CM_DRC1L2_INSITU_AL_WFILTERS_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC1 Voltages/Phase difference': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC1_DATASTREAM/V1/4/DRC1_PhaseDifference',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC1 Voltages/Phase 1", "Calorimetry/Directional Couplers/DRC1 Voltages/Phase 2"],
        'unit': "phase [$°$]",
        'legend': "$Phi$ DRC1",
        'derivationfunc': Derivating.voltage_to_phase_from_table
    },
    # Strap 2
    'Calorimetry/Strap 2/Load resistance/Reflection coefficient port 2': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/CalStrap2_DATASTREAM/V1/0/Strap2_reflection',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC1 Reflection coefficient/GMSR", "Antenna/Positions/Antenna/Antenna head"],
        'unit': "R [$\\Omega$]",
        'legend': "load resistance port 2 ",
        'derivationfunc': Derivating.calc_Rload,
        # 'derivationsetting': {}
    },
    # DRC3
    'Calorimetry/Directional Couplers/DRC3 Power/Net power': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC3_DATASTREAM/V1/0/DRC3_NetPower',
        'type': 'timetrace',
        'description': "Net power",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC3 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC3 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC3 Voltages/Reflected voltage mix"],
        'unit': "P [kW]",
        'legend': "$P_{net}$ DRC3",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'pwr_net', 'DRC': 3, 'CMfile': 'Calibrationfiles/CM_DRC3_INMU1_WFILTERS_NEW_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC3 Power/Forward power': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC3_DATASTREAM/V1/1/DRC3_ForwardPower',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC3 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC3 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC3 Voltages/Reflected voltage mix"],
        'unit': "P [kW]",
        'legend': "$P_{forw}$ DRC3",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'pwr_fwd', 'DRC': 3, 'CMfile': 'Calibrationfiles/CM_DRC3_INMU1_WFILTERS_NEW_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC3 Power/Reflected power': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC3_DATASTREAM/V1/2/DRC3_ReflectedPower',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC3 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC3 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC3 Voltages/Reflected voltage mix"],
        'unit': "P [kW]",
        'legend': "$P_{refl}$ DRC3",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'pwr_rfl', 'DRC': 3, 'CMfile': 'Calibrationfiles/CM_DRC3_INMU1_WFILTERS_NEW_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC3 Reflection coefficient/GMSR': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC3_DATASTREAM/V1/3/DRC3_GMSR',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC3 Voltages/Phase difference", "Calorimetry/Directional Couplers/DRC3 Voltages/Forward voltage mix", "Calorimetry/Directional Couplers/DRC3 Voltages/Reflected voltage mix"],
        'unit': "$\\Gamma$ [None]",
        'legend': "GSMR  DRC3",
        'derivationfunc': Derivating.calc_Gmsr_single,
        'derivationsetting': {'signal': 'gam_amp', 'DRC': 3, 'CMfile': 'Calibrationfiles/CM_DRC3_INMU1_WFILTERS_NEW_20_40.s2p'}
    },
    'Calorimetry/Directional Couplers/DRC3 Voltages/Phase difference': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/DRC3_DATASTREAM/V1/4/DRC3_PhaseDifference',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Directional Couplers/DRC3 Voltages/Phase 1", "Calorimetry/Directional Couplers/DRC3 Voltages/Phase 2"],
        'unit': "phase [$°$]",
        'legend': "$Phi$ DRC3",
        'derivationfunc': Derivating.voltage_to_phase_from_table
    },
    'Calorimetry/Calorimeter/Heat exchange/Strap 1': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaHeatExchange_DATASTREAM/V1/0/Heatexchange_strap1',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Strap 1 (CF304)", "Calorimetry/Calorimeter/Water temperature/Inlet antenna head (CT501)", "Calorimetry/Calorimeter/Water temperature/Outlet Strap 1 (CT304)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange strap 1",
        'derivationfunc': Derivating.heat_exchange
    },
    'Calorimetry/Calorimeter/Heat exchange/Edge 1': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaHeatExchange_DATASTREAM/V1/1/Heatexchange_edge1',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Edge 1 (CF305)", "Calorimetry/Calorimeter/Water temperature/Inlet antenna head (CT501)", "Calorimetry/Calorimeter/Water temperature/Outlet Edge 1 (CT305)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange edge 1",
        'derivationfunc': Derivating.heat_exchange
    },
    'Calorimetry/Calorimeter/Heat exchange/Front 1': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaHeatExchange_DATASTREAM/V1/2/Heatexchange_front1',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Front 1 (CF306)", "Calorimetry/Calorimeter/Water temperature/Inlet antenna head (CT501)", "Calorimetry/Calorimeter/Water temperature/Outlet Front 1 (CT306)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange front 1",
        'derivationfunc': Derivating.heat_exchange
    },
    'Calorimetry/Calorimeter/Heat exchange/Strap 2': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaHeatExchange_DATASTREAM/V1/3/Heatexchange_strap2',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Strap 2 (CF404)", "Calorimetry/Calorimeter/Water temperature/Inlet antenna head (CT501)", "Calorimetry/Calorimeter/Water temperature/Outlet Strap 2 (CT404)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange strap 2",
        'derivationfunc': Derivating.heat_exchange
    },
    'Calorimetry/Calorimeter/Heat exchange/Edge 2': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaHeatExchange_DATASTREAM/V1/4/Heatexchange_edge2',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Edge 2 (CF405)", "Calorimetry/Calorimeter/Water temperature/Inlet antenna head (CT501)", "Calorimetry/Calorimeter/Water temperature/Outlet Edge 2 (CT405)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange edge 2",
        'derivationfunc': Derivating.heat_exchange
    },
    'Calorimetry/Calorimeter/Heat exchange/Front 2': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/AntennaHeatExchange_DATASTREAM/V1/5/Heatexchange_front2',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Front 2 (CF406)", "Calorimetry/Calorimeter/Water temperature/Inlet antenna head (CT501)", "Calorimetry/Calorimeter/Water temperature/Outlet Front 2 (CT406)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange front 2",
        'derivationfunc': Derivating.heat_exchange
    },
    'Calorimetry/Calorimeter/Heat exchange/Capacitor 1': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/CapacitorHeatExchange_DATASTREAM/V1/6/Heatexchange_capacitor1',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Capacitor 1 (CF301)", "Calorimetry/Calorimeter/Water temperature/Inlet Capacitors (CT502)", "Calorimetry/Calorimeter/Water temperature/Outlet Capacitor 1 (CT301)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange capacitor 1",
        'derivationfunc': Derivating.heat_exchange
    },
    'Calorimetry/Calorimeter/Heat exchange/Capacitor 2': {
        'url': 'Sandbox/raw/W7XAnalysis/CC_ICRH/CapacitorHeatExchange_DATASTREAM/V1/7/Heatexchange_capacitor2',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Calorimetry/Calorimeter/Water flow/Capacitor 2 (CF401)", "Calorimetry/Calorimeter/Water temperature/Inlet Capacitors (CT502)", "Calorimetry/Calorimeter/Water temperature/Outlet Capacitor 2 (CT401)"],
        'unit': "$\\dot{Q}$ [J/s]",
        'legend': "Heat exchange capacitor 2",
        'derivationfunc': Derivating.heat_exchange
    },
#####
# Total ECRH power as sum of gyrotron powers
# This works for OP1 and OP2! To the inputsigs, automatically the correct _OP1 or _OP2 will be added
    'Gyrotrons/Power/Total/Sum of gyrotron powers': {
        'url': None,  # see 'Gyrotrons/Power/Total/ECRH power' + '_OP1' or '_OP2' for corresponding url
        'type': 'timetrace',
        'description': "",
        'inputsigs': [f'Gyrotrons/Power/Series {g[1]}/Gyrotron {g}' for g in ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'A5', 'B5', 'C5', 'D5', 'E5', 'F5']],
        'unit': "P [MW]",
        'legend': "$P_{sum\\ gyrotrons}$",
        'derivationfunc': Derivating.ECRHSumOfGyrotronPowers,
        'derivationsetting': {'factor': 1, 'threshold': None}
    },
# Total ECRH power setpoint as sum of gyrotron power setpoints
# This works for OP1 and OP2! To the inputsigs, automatically the correct _OP1 or _OP2 will be added
    'Gyrotrons/Power/Total/Sum of gyrotron setpoints': {
        'url': None,
        'type': 'timetrace',
        'description': "",
        'inputsigs': [f'Gyrotrons/setpoint/Series {g[1]}/Gyrotron {g}' for g in ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'A5', 'B5', 'C5', 'D5', 'E5', 'F5']],
        'unit': "P [MW]",
        'legend': "$P_{ECRH}\\ setpoint$",
        'derivationfunc': Derivating.ECRHSumOfSetpoints,
        'derivationsetting': {'factor': 1, 'threshold': None}
    },
# ECE core channel (radiative T_e) data source.
    'ECE channels/ECE/core/core channel': {  # (18 if B0 above 2.55, else 13)': {
        'url': None,
        'type': 'timetrace',
        'description': "",
        'inputsigs': ['ECE channels/ECE/channels/channel 13 (~core)', 'ECE channels/ECE/channels/channel 18'],
        'unit': "$T_e$ [keV]",
        'legend': "$T_{e,ECE}$ core",
        'derivationfunc': Derivating.ECE_core
    },
#####
# Total NBI power as on overview logbook plot
    'W7X/Power/Plasma heating/NBI power': {
        'url': 'Sandbox/raw/W7XAnalysis/NBI/NBI_power_DATASTREAM/V1/0/NBI_power',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["Injector/NI20/Calorimeter/Position", "Injector/NI21/Calorimeter/Position", "Injector/NI20/Source3/P3", "Injector/NI20/Source4/P4", "Injector/NI21/Source7/P7", "Injector/NI21/Source8/P8",
                      "Injector/NI20/Source3/Fired3", "Injector/NI20/Source4/Fired4", "Injector/NI21/Source7/Fired7", "Injector/NI21/Source8/Fired8"],
        'unit': "P [MW]",
        'legend': "$P_{NBI}$",
        'derivationfunc': Derivating.NBIpowerOP2,
        'derivationsetting': {'factor': 1, 'threshold': None}
    },
#####
    'W7X/Power/Plasma heating/NBI power OP 1': {
        'url': 'Sandbox/raw/W7XAnalysis/Maja_tests/NBI_power_DATASTREAM/V1/0/NBI_power',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["NBI_w7X", "NBI_U7", "NBI_U8", "NBI_I7", "NBI_I8"],
        'unit': "P [MW]",
        'legend': "$P_{NBI}$",
        'derivationfunc': Derivating.NBIpowerOP1
    },
    'W7X/Power/Bolometry/GPI events': {
        'url': 'None',
        'type': 'event',
        'description': "",
        'inputsigs': ["GPI/Gas Puff Imaging/gas puffs/GPI events"],
        'unit': "[a.u.]",
        'legend': "GPI",
        'derivationfunc': Derivating.Same
    },
    'W7X/Power/Bolometry/MPM events': {
        'url': 'None',
        'type': 'event',
        'description': "",
        'inputsigs': ["E5/MPM/Position/MPM events"],
        'unit': "[a.u.]",
        'legend': "MPM",
        'derivationfunc': Derivating.Same
    },
    'W7X/Environment/Density/Line average density': {
        'url': 'Sandbox/raw/W7XAnalysis/Maja_tests/Line_average_density_DATASTREAM/V1/0/Line_average_density',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ["W7X/Environment/Density/Line integrated density"],
        'unit': "$n_e$ [$m^{-3}$]",
        'legend': "$n_{e,\\ line\\ avg.}$",
        'derivationfunc': Derivating.LineAverage
    },
    'W7X/Power/Plasma heating/ICRH power_OP2.1': {
        'url': 'None',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ['Generator/Generator 1/Endstage power/Forward power'],
        'unit': "P [MW]",
        'legend': "$P_{ICRH}$",
        'derivationfunc': Derivating.Same
    },
    'W7X/Power/Plasma heating/ICRH power_OP2.2': {
        'url': 'None',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ['Generator/Generator 1/Endstage power/Approx net power', 'Generator/Generator 2/Endstage power/Approx net power'],
        'unit': "P [MW]",
        'legend': "$P_{ICRH}$",
        'derivationfunc': Derivating.ICRHpower
    },
    'W7X/Power/Plasma heating/ECRH power': {
        'url': 'None',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ['Gyrotrons/Power/Total/ECRH power'],  # TODO: you can add as input signal sum of gyrotrons, and if the first fails, use the second one
        'unit': "P [MW]",
        'legend': "$P_{ECRH}$",
        'derivationfunc': Derivating.Same
    },
    'W7X/Power/Power deposition/Radiated over total power': {
        'url': 'None',
        'type': 'timetrace',
        'description': "",
        'inputsigs': ['W7X/Power/Bolometry/Total radiated power', 'W7X/Power/Plasma heating/ICRH power', 'W7X/Power/Plasma heating/ECRH power'],  # 'W7X/Power/Plasma heating/NBI power'],
        'unit': "fraction []",
        'legend': "$P_{rad}/P_{tot}$",
        'derivationfunc': Derivating.CompOverTotal
    },
    'Main/Valves/Active valves/Total flow rate': {
        'url': 'None',
        'type': 'timetrace',
        'description': "",
        'inputsigs': [f'Main/Valves/Valve {i}/Valve voltage' for i in range(0, 11)] +
                     [f'Main/Valves/Valve {i}/Flow rate' for i in range(0, 11)],
        'unit': "$n_e$ []",
        'legend': "$\Sigma$ main flow rates",
        'derivationfunc': Derivating.AllMainGas
    },
    'W7X/Power/Gas/Main gas': {
        'url': 'None',
        'type': 'timetrace',
        'description': "",
        'inputsigs': [f'Main/Valves/Valve {i}/Valve voltage' for i in range(0, 11)] +
                     [f'Main/Valves/Valve {i}/Flow rate' for i in range(0, 11)],
        'unit': "$n_e$ []",
        'legend': "$\Sigma$ main flow rates",
        'derivationfunc': Derivating.AllMainGas
    },
    'Midplane/GPI/Gas Puff Imaging/GPI events': {
        'url': 'None',
        'type': 'event',
        'description': "",
        'inputsigs': ['GPI/Gas Puff Imaging/gas puffs/Differential pressure'],
        'unit': "[a.u.]",
        'legend': "GPI",
        'derivationfunc': Derivating.gpipuffs
    },
    'E5/MPM/Position/MPM events': {
        'url': 'None',
        'type': 'event',
        'description': "",
        'inputsigs': ['E5/MPM/Position/FZJ-COMB3'],
        'unit': "[a.u.]",
        'legend': "MPM",
        'derivationfunc': Derivating.MPMplunges
    }
}


# To get the sum of all the divertor voltages
modules = {
    'Module 1': ['H10', 'H11'],
    'Module 2': ['H20', 'H21'],
    'Module 3': ['H30 valve1', 'H30 valve2', 'H30 valve3', 'H30 valve4', 'H30 valve5', 'H31'],
    'Module 4': ['H40', 'H41'],
    'Module 5': ['H50 valve1', 'H50 valve2', 'H50 valve3', 'H50 valve4', 'H50 valve5',
                 'H51 valve1', 'H51 valve2', 'H51 valve3', 'H51 valve4', 'H51 valve5']
}
derived_signal_dict['Divertor/Valves/All valves/Total voltage'] = {
    'url': None,
    'type': 'timetrace',
    'description': "",
    'inputsigs': [f'Divertor/Valves/{module}/{signal} Piezo V'
                  for module, signals in modules.items()
                  for signal in signals],
    'legend': "$\Sigma$ divertor valve V",
    'unit': "valve voltages [V]",
    'derivationfunc': Derivating.DivertorSumOfVoltages,
    'derivationsetting': {'factor': 1, 'threshold': None}
}

# Todo: The same as above. Avoid duplicate
derived_signal_dict['W7X/Power/Gas/Divertor gas'] = {
    'url': None,
    'type': 'timetrace',
    'description': "",
    'inputsigs': [f'Divertor/Valves/{module}/{signal} Piezo V'
                  for module, signals in modules.items()
                  for signal in signals],
    'legend': "$\Sigma$ divertor valve V",
    'unit': "valve voltages [V]",
    'derivationfunc': Derivating.DivertorSumOfVoltages,
    'derivationsetting': {'factor': 1, 'threshold': None}
}

def getTimetraces():
    """ Get the signals from the dicts that are of the type timetrace
    """

    # Create a tuple that hold all signals o the type timetrace
    timetracetuple = ()
    for signal, specs in derived_signal_dict.items():
        if specs['type'] == 'timetrace':
            timetracetuple = timetracetuple + (signal,)
    return timetracetuple