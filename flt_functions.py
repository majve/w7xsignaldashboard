# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 13:47:04 2020

@author: caki

This is a collection of functions working with the Field Line Tracing 
Webservice (http://webservices.ipp-hgw.mpg.de/docs/fieldlinetracer.html)

The general style for input parameters is

Start_Pos:  Array of cartesian positions on which FLT is to be performed. 
            Nx3 array with (x,y,z) coordinates in m.
            
coils:      List of coils from CoilsDB (see example below)
currents:   List of coil currents in A (see example below)

configID:   ID for preset magnetic configurations. 
            If configID is specified as integer, coils and currents are ignored
            configID is ignored if it is not specified as integer

            --> specify either coils/currents or configID


inverse:    0 for normal field, 1 for inverse field
symmetry:   symmetry = 5 exploits 5-fold symmetry of W7-X for more efficient calculation
            Set to symmetry = 1 for asymmetric configurations (e.g. as-built coils, control / trim coils)

I_plasma:   Toroidal plasma current in A (default = 0)
            Currents from the W7-X Archive (Rogowski coil) have to be multiplied 
            by -1 in order to be correctly implemented due to the way the axis 
            files (see below) are designed.
            
            [As an example: in forward magnetic field setup (see definition below), 
            a positive plasma current in the Rogowski coil is defined such that 
            it increases iota (i.e. current flows along the field). In this case,
            a I_plasma in the field line tracer has to be negative.

axis:       configuration specifier for plasma axis, onto which the plasma current is applied
            available: 'standard', 'lowiota', 'highiota'

------------------------------------
Example for setting coils + currents:
------------------------------------

coils_ideal = np.arange(160,230)  # ideal Kisslinger coils
I_NPC = -13045 # A ,  to get a forward magnetic field (see below)
I_PC = 499 # A (estimate for iota correction)
currents = [I_NPC*108, I_NPC*108, I_NPC*108, I_NPC*108, I_NPC*108]*10 # nonplanar coil currents (5 coil types, 108 windings)
currents.extend([I_PC*36, I_PC*36]*10) # Planar coil currents (2 coil types, 36 windings)


--------------------------------------
A note on the magnetic field direction:
--------------------------------------
The positive/normal/forward magnetic field direction is defined such that the 
field is ccw when viewed from top, i.e. By is positive at the 0° bean plane
([x,y,z] = [5.9,0,0]m),  see e.g. 1-EB-R0001.

Using the preset configurations (0,1,2,...) directly provides the positive 
magnetic field.

When using the ideal superconducting coil sets (e.g. 0-69, 160-229), the NPC 
and PC currents from the Archive have to be multiplied by -1 in order to get  
the correct field direction corresponding to an experiment (since the filament 
direction of these coils is reversed)
"""

from osa import Client
import numpy as np
import matplotlib.pyplot as plt


def get_pc(Start_Pos, coils=[], currents=[], phi=200.8, configID='foo',  N=1000, symmetry=5, I_plasma=0, axis='standard', stepsize=1e-3):
    """ Calculates Poincare plot

    Parameters:
    -----------
    phi: single value or a list
        Angle for calculation of Poincare plot in degrees.
    N: int
        Number of Poincare points per start point
    """

    tracer = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')

    pos = tracer.types.Points3D()

    pos.x1 = Start_Pos[:, 0]
    pos.x2 = Start_Pos[:, 1]
    pos.x3 = Start_Pos[:, 2]

    config = tracer.types.MagneticConfig()
    config.inverseField = 0

    if isinstance(configID, int):  # use pre-defined configuration
        config.configIds = configID
    else:   # set up configuration with coils and currents
        config.coilsIds = coils
        config.coilsIdsCurrents = currents

    # apply plasma current if specified
    if I_plasma != 0:

        filament = tracer.types.PolygonFilament() # plasma axis
        filament.vertices = tracer.types.Points3D()

        if axis == 'standard':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_standard_case.dat')
        elif axis == 'lowiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_low_iota.dat')
        elif axis == 'highiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_high_iota.dat')

        else:
            print('Please choose valid plasma axis file.'+
                  '\nIn future versions, calculation of the magnetic axis for arbitrary configurations using the webservice is foreseen.')
            return

        filament.vertices.x1 = plasma_axis[::20,0] # only use every 20th point for efficiency
        filament.vertices.x2 = plasma_axis[::20,1]
        filament.vertices.x3 = plasma_axis[::20,2]

        config.coils = [filament]
        config.coilsCurrents = [I_plasma]

    machine = tracer.types.Machine(1)
    machine.grid.numX, machine.grid.numY, machine.grid.numZ = 500, 500, 100
    machine.grid.ZMin, machine.grid.ZMax = -1.5, 1.5
    machine.grid.YMin, machine.grid.YMax = -7, 7
    machine.grid.XMin, machine.grid.XMax = -7, 7
    # machine.assemblyIds = [9] # 2: ideal divertor, 9: ideal vessel
    machine.meshedModelsIds = [164]  # 164: rectangular torus

    my_grid = tracer.types.CylindricalGrid()
    my_grid.RMin = 4.05
    my_grid.RMax = 6.75
    my_grid.ZMin = -1.35
    my_grid.ZMax = 1.35
    my_grid.numR = 181
    my_grid.numZ = 181
    my_grid.numPhi = 481

    g = tracer.types.Grid()
    g.cylindrical = my_grid
    g.fieldSymmetry = symmetry
    config.grid = g

    poincare = tracer.types.PoincareInPhiPlane()
    poincare.numPoints = N

    if  type(phi) == list:
        poincare.phi0 = [item*np.pi/180 for item in phi]
    else:
        poincare.phi0 = phi*np.pi/180

    task = tracer.types.Task()
    task.step = stepsize
    task.poincare = poincare

    res = tracer.service.trace(pos, config, task, machine, None)

    print('Poincare calculation done')

    return res


def get_cl(Start_Pos, coils, currents, configID='foo', inverse=0, symmetry=5, climit=1000, assemblies=[2, 9], I_plasma=0, axis='standard'):
    """ Calculates connection lengths
    
    Parameters:
    ----------
    climit: int
        maximum tracing distance (m)
    assemblies: single value or list
        assembly identifiers from ComponentsDB
    """

    tracer = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')

    pos = tracer.types.Points3D()

    pos.x1 = Start_Pos[:, 0]
    pos.x2 = Start_Pos[:, 1]
    pos.x3 = Start_Pos[:, 2]

    task = tracer.types.Task()
    task.step = 0.01

    con = tracer.types.ConnectionLength()
    con.limit = climit
    con.returnLoads = False
    task.connection = con

    config = tracer.types.MagneticConfig()
    config.inverseField = inverse

    if isinstance(configID, int):
        config.configIds = configID
    else:
        config.coilsIds = coils
        config.coilsIdsCurrents = currents

    # apply plasma current if specified
    if I_plasma != 0:

        filament = tracer.types.PolygonFilament()  # plasma axis
        filament.vertices = tracer.types.Points3D()

        if axis == 'standard':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_standard_case.dat')
        elif axis == 'lowiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_low_iota.dat')
        elif axis == 'highiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_high_iota.dat')

        else:
            print('Please choose valid plasma axis file.'+
                  '\nIn future versions, calculation of the magnetic axis for arbitrary configurations using the webservice is foreseen.')
            return

        filament.vertices.x1 = plasma_axis[::20, 0]  # only use every 20th point for efficiency
        filament.vertices.x2 = plasma_axis[::20, 1]
        filament.vertices.x3 = plasma_axis[::20, 2]

        config.coils = [filament]
        config.coilsCurrents = [I_plasma]

    machine = tracer.types.Machine(1)
    machine.grid.numX, machine.grid.numY, machine.grid.numZ = 500, 500, 100
    machine.grid.ZMin, machine.grid.ZMax = -1.5, 1.5
    machine.grid.YMin, machine.grid.YMax = -7, 7
    machine.grid.XMin, machine.grid.XMax = -7, 7
    machine.assemblyIds = assemblies  # 2: ideal divertor, 9: ideal vessel
    machine.meshedModelsIds = [164]  # 164: rectangular torus

    my_grid = tracer.types.CylindricalGrid()
    my_grid.RMin = 4.05
    my_grid.RMax = 6.75
    my_grid.ZMin = -1.35
    my_grid.ZMax = 1.35
    my_grid.numR = 181
    my_grid.numZ = 181
    my_grid.numPhi = 481

    g = tracer.types.Grid()
    g.cylindrical = my_grid
    g.fieldSymmetry = symmetry
    config.grid = g

    res_cl = tracer.service.trace(pos, config, task, machine)

    print('connection length calculation done')

    return res_cl


def get_fieldline(Start_Pos, coils=[], currents=[], configID='foo', inverse=0, symmetry=1, steps=1000, stepsize=0.01, phi=False, I_plasma=0, assemblies=[9], axis='standard'):
    """ Performs field line tracing.
    If phi is unspecified, the tracing length is determined by "steps"
    If phi is specified, the field line is traced until it reaches the relative angle phi (w.r.t the start phi)
    
    Parameters:
    -----------
    steps: int
        number of steps to trace
    stepsize: float
        stepsize in m (default: 0.01)
    phi: float
        toroidal angle in degree at to span by the tracing (default: False)
    """

    tracer = Client('http://esb:8280/services/FieldLineProxy?wsdl')

    pos = tracer.types.Points3D()
    pos.x1 = Start_Pos[:, 0]
    pos.x2 = Start_Pos[:, 1]
    pos.x3 = Start_Pos[:, 2]

    task = tracer.types.Task()
    task.step = stepsize

    if not phi: # if no phi is given, trace the given number of steps
        lineTask = tracer.types.LineTracing()
        lineTask.numSteps = steps
        task.lines = lineTask

    else: # trace until reach angle phi (input in deg)
        task.linesPhi = tracer.types.LinePhiSpan()
        task.linesPhi.phi = phi*np.pi/180

    config = tracer.types.MagneticConfig()
    config.inverseField = inverse

    if isinstance( configID, int ):
        config.configIds = configID
    else:
        config.coilsIds = coils
        config.coilsIdsCurrents = currents

    # apply plasma current if specified
    if I_plasma != 0:

        filament = tracer.types.PolygonFilament() # plasma axis
        filament.vertices = tracer.types.Points3D()

        if axis == 'standard':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_standard_case.dat')
        elif axis == 'lowiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_low_iota.dat')
        elif axis == 'highiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_high_iota.dat')

        else:
            print('Please choose valid plasma axis file.'+
                  '\nIn future versions, calculation of the magnetic axis for arbitrary configurations using the webservice is foreseen.')
            return

        filament.vertices.x1 = plasma_axis[::20, 0] # only use every 20th point for efficiency
        filament.vertices.x2 = plasma_axis[::20, 1]
        filament.vertices.x3 = plasma_axis[::20, 2]

        config.coils = [filament]
        config.coilsCurrents = [I_plasma]

    machine = tracer.types.Machine(1)
    machine.grid.numX, machine.grid.numY, machine.grid.numZ = 500, 500, 100
    machine.grid.ZMin, machine.grid.ZMax = -1.5, 1.5
    machine.grid.YMin, machine.grid.YMax = -7, 7
    machine.grid.XMin, machine.grid.XMax = -7, 7
    machine.assemblyIds = assemblies # 2: ideal divertor, 9: ideal vessel
    machine.meshedModelsIds = [164]  # 164: rectangular torus

    my_grid = tracer.types.CylindricalGrid()
    my_grid.RMin = 4.05
    my_grid.RMax = 6.75
    my_grid.ZMin = -1.35
    my_grid.ZMax = 1.35
    my_grid.numR = 181
    my_grid.numZ = 181
    my_grid.numPhi = 481

    g = tracer.types.Grid()
    g.cylindrical = my_grid
    g.fieldSymmetry = symmetry
    config.grid = g

    res = tracer.service.trace(pos, config, task, machine, None)

    return res


def get_iotaprofile(Start_Pos, coils, currents, configID='foo', symmetry=5, I_plasma=0, axis='standard'):

    tracer = Client('http://esb.ipp-hgw.mpg.de:8280/services/FieldLineProxy?wsdl')

    pos = tracer.types.Points3D()

    pos.x1 = Start_Pos[:, 0]
    pos.x2 = Start_Pos[:, 1]
    pos.x3 = Start_Pos[:, 2]

    task = tracer.types.Task()
    task.step = 1e-2

    config = tracer.types.MagneticConfig()

    if isinstance(configID, int ):
        config.configIds = configID
    else:

        config.coilsIds = coils
        config.coilsIdsCurrents = currents

        # apply plasma current if specified
    if I_plasma != 0:

        filament = tracer.types.PolygonFilament() # plasma axis
        filament.vertices = tracer.types.Points3D()

        if axis == 'standard':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_standard_case.dat')
        elif axis == 'lowiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_low_iota.dat')
        elif axis == 'highiota':
            plasma_axis = np.loadtxt('C:\\Users\\caki\\Desktop\\python\\\Field_line_tracer\\flt_git\\files\\axis_high_iota.dat')

        else:
            print('Please choose valid plasma axis file.'+
                  '\nIn future versions, calculation of the magnetic axis for arbitrary configurations using the webservice is foreseen.')
            return

        filament.vertices.x1 = plasma_axis[::20, 0]  # only use every 20th point for efficiency
        filament.vertices.x2 = plasma_axis[::20, 1]
        filament.vertices.x3 = plasma_axis[::20, 2]

        config.coils = [filament]
        config.coilsCurrents = [I_plasma]

    task.characteristics = tracer.types.MagneticCharacteristics()
    task.characteristics.axisSettings = tracer.types.AxisSettings()

    res = tracer.service.trace(pos, config, task, None, None)

    # extract reff and iota profiles
    N = len(res.characteristics)
    reff = np.zeros(N)
    iota = np.zeros(N)
    for c in range(N):
        reff[c] = res.characteristics[c].reff
        iota[c] = res.characteristics[c].iota

    print('iota profile done')
    return reff,iota


def get_lcfs(coils, currents, configID='foo', plotflag=False, side='LFS'):
    """ Obtain LCFS position by calculation of connection length (CL) profile
        CL profile is calculated in the mid plane of the bean cross section.
        "side" is either 'LFS' or 'HFS' and indicates whether high field side or low field side is to be used
    """

    if side == 'LFS':
        x0_lc = np.arange(6.15, 6.25+1e-4, 1e-3)  # works for configurations tested so far.
        # x0_lc = np.arange(6.15, 6.25 + 1e-4, 1e-2)  # lower resolution MAJA
    elif side == 'HFS':
        x0_lc = np.arange(5.75, 5.65-1e-4, -1e-3)  # works for configurations tested so far.
        # x0_lc = np.arange(5.75, 5.65 - 1e-4, -1e-2)  # lower resolution MAJA
    else:
        print('side must be LFS or HFS')
        return

    y0_lc = np.zeros(x0_lc.size)
    z0_lc = np.zeros(x0_lc.size)
    Start_Pos_Lc = np.vstack([x0_lc, y0_lc, z0_lc]).T

    cl = get_cl(Start_Pos_Lc, coils, currents, configID=configID, inverse=0, symmetry=5, climit=5000)
    L, _, _, _ = convert_cl_results(cl)

    Lmax = L[0]  # maximum connection length

    id_lcfs = np.where(L < Lmax)[0][0]  # first entry in array which is smaller than Lmax (i.e. actually the first result outside the LCFS)
    x0_lcfs = x0_lc[id_lcfs-1]  # correct by 1 to get the actual LCFS

    if plotflag:
        plt.figure()
        plt.plot(x0_lc, L)
        plt.plot([x0_lcfs, x0_lcfs], [0, Lmax])
        plt.title('LCFS at %.3fm'%x0_lcfs)

    return x0_lcfs  # returns x position of LCFS in mid plane bean cross section (y = z = 0)


def get_mesh_at_phi(componentID, phi, assembly=True):
    """ Gets mesh information of physical components at a given toroidal angle
        componentID - ID (string or int) of ASSEMBLY from http://webservices.ipp-hgw.mpg.de/docs/componentsDbRest.html

    Parameters:
    -----------
    phi : float
        toroidal angle in degree
    assembly : Boolean
        True (default) : componentID refers to Assembly
        False: componentID refers to MeshedModel
    """
    mesh_srv = Client("http://esb.ipp-hgw.mpg.de:8280/services/MeshSrv?wsdl")

    mset = mesh_srv.types.SurfaceMeshSet()

    if assembly:
        ref = mesh_srv.types.DataReference()
        ref.dataId = componentID
        mset.references = [ref]
    else:
        mwrap = mesh_srv.types.SurfaceMeshWrap()
        mwrap.reference = mesh_srv.types.DataReference()
        mwrap.reference.dataId = componentID
        mset.meshes = [mwrap, ]

    result = mesh_srv.service.intersectMeshPhiPlane(np.deg2rad(phi), mset)

    return result


def convert_cl_results(res_cl):
    # extract results of connection length calculation and return as numpy arrays
    N = len(res_cl.connection)
    L = np.zeros(N)
    Part = np.zeros(N)
    Element = np.zeros(N)
    End_pos = np.zeros((N, 3))

    for c in range(N):
        L[c] = res_cl.connection[c].length
        Part[c] = res_cl.connection[c].part
        Element[c] = res_cl.connection[c].element
        End_pos[c, 0] = res_cl.connection[c].x
        End_pos[c, 1] = res_cl.connection[c].y
        End_pos[c, 2] = res_cl.connection[c].z

    return L, Part, Element, End_pos


def convert_pc_results(res_pc):
    # extract results of Poincare calculation and return as numpy arrays

    R_poincare = np.array([])
    Z_poincare = np.array([])

    for c in range(0, len(res_pc.surfs)):
        t1 = np.asarray(res_pc.surfs[c].points.x1) # extract x coordinates as numpy array
        t2 = np.asarray(res_pc.surfs[c].points.x2) # extract y coordinates as numpy array
        t3 = np.asarray(res_pc.surfs[c].points.x3) # extract z coordinates as numpy array

        if t1.size > 1: # only if there is any data in there
            R_poincare = np.concatenate([R_poincare, np.sqrt(t1**2 + t2**2)])
            Z_poincare = np.concatenate([Z_poincare, t3])

    return R_poincare, Z_poincare


def convert_fieldline_results(res_flt):
    # extract results of field line tracing  and return as numpy arrays

    x = np.array([])
    y = np.array([])
    z = np.array([])

    for c in range(len(res_flt.lines)):
        x = np.concatenate([x, np.asarray(res_flt.lines[c].vertices.x1)])
        y = np.concatenate([y, np.asarray(res_flt.lines[c].vertices.x2)])
        z = np.concatenate([z, np.asarray(res_flt.lines[c].vertices.x3)])

    return x, y, z
