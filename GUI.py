import ICRHsettings as ICRHsettings
import OutputSettings as OutputSettings
import Selection as Selection
import ICRHchannels as ICRHchannels
import Preset as Preset
import Output as Output
import AnalysisGUI as AnalysisGUI
import CorrelationsGUI as CorrelationsGUI

import w7xarchive
import tkinter as tk
from tkinter import ttk

import os
import subprocess
import pprint
import logging

log = logging.getLogger("GUI")
logging.basicConfig(level=logging.DEBUG)
pp = pprint.PrettyPrinter(indent=2)  # just for looks

# noinspection PyDictCreation
class GUI(tk.Tk):
    """Class definition of the GUI. The class inherits from the parent class tk (see online tkinter).
    """
    def __init__(self):
        """ The initializer generates the GUI and calls its mainloop (see bottom of this function), awaiting user actions.
        """

        # Initialization of the parent class (tk) and setting some of its properties
        super().__init__()
        super().title("w7xsignalviewer")
        super().option_add('*Dialog.msg.font', 'Helvetica 12')

        # Creation of objects that are defined in other scripts and that are added as members to the GUI
        self.Selection = Selection.Selection(self)
        self.ICRHsettings = ICRHsettings.ICRHsettings(self)
        self.ICRHchannels = ICRHchannels.ICRHchannels(self)
        self.OutputSettings = OutputSettings.OutputSettings(self)

        # Create a canvas widget with vertical and horizontal scrollfoos
        # Use the GUI size or the size of the screen
        screenwidth = min(1710, self.winfo_screenwidth()-100)
        screenheight = min(1030, self.winfo_screenheight()-100)
        canvas = tk.Canvas(self, borderwidth=0, width=screenwidth, height=screenheight)
        canv_vscrollfoo = tk.Scrollfoo(self, orient="vertical", command=canvas.yview)
        canv_hscrollfoo = tk.Scrollfoo(self, orient="horizontal", command=canvas.xview)
        canvas.configure(yscrollcommand=canv_vscrollfoo.set, xscrollcommand=canv_hscrollfoo.set)
        # Create a frame inside the canvas to contain the frames
        frame_container = tk.Frame(canvas)
        canvas.create_window((0, 0), window=frame_container, anchor="nw")

        # Below, all the frames, buttons, labels etc. for the GUI are described
        # The buttons are connected to functions that can be found throughout the scripts
        # The values in the entry boxes and of the radio buttons are collected by Outputsettings.py and passed on to the designated functions

        # SELECTION FRAME
        selection_frm = tk.Frame(frame_container, borderwidth=15, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        selection_lbl = tk.Label(selection_frm, text="Selection", bg="LightsteelBlue", font='helvetica 15 bold')

        # GRID SELECTION
        selection_frm.grid(column=0, row=0, rowspan=1, sticky="nsew")
        selection_lbl.grid(column=0, row=0, sticky="nsew")

        # FROM UPTO FRAME
        fromupto_frm = tk.Frame(selection_frm, borderwidth=15, bg="LightsteelBlue")
        # Use os path to make sure script works stand alone and as PyPI package
        calendarpath = os.path.join(os.path.dirname(__file__), 'images/calendar.png')
        cal_icon = tk.PhotoImage(file=calendarpath)

        timerange_lbl = tk.Label(fromupto_frm, text="Specify time range", bg="LightsteelBlue")
        from_lbl = tk.Label(fromupto_frm, text="From", bg="LightsteelBlue")
        self.from_date_var = tk.StringVar()
        self.from_time_var = tk.StringVar()
        from_cal_btn = tk.Button(fromupto_frm, image=cal_icon, height=25, width=25, command=self.Selection.openFromCalendar)
        from_date_entr = tk.Entry(fromupto_frm, textvariable=self.from_date_var, width=15, fg='grey')
        self.from_date_var.set("YYYY-MM-DD")  # ("2023-03-30")  # ("2022-08-10") # ("2022-06-01")  # ("2016-03-10")  # ("2018-10-09")  #
        from_time_entr = tk.Entry(fromupto_frm, textvariable=self.from_time_var, width=15, fg='grey')
        self.from_time_var.set("00:00:00")  # ("HH:MM:SS.SSSS")  # ("10:01:01")  # ("00:00:00.0")  # ("13:06:00.0")  #("08:35:00")  # #

        upto_lbl = tk.Label(fromupto_frm, text="Upto", bg="LightsteelBlue")
        self.upto_date_var = tk.StringVar()
        self.upto_time_var = tk.StringVar()
        upto_cal_btn = tk.Button(fromupto_frm, image=cal_icon, height=25, width=25, command=self.Selection.openUptoCalendar)
        upto_date_entr = tk.Entry(fromupto_frm, textvariable=self.upto_date_var, width=15, fg='grey')
        self.upto_date_var.set("YYYY-MM-DD")  # ("2023-03-30")  # ("2022-08-10")  #  ("2022-06-01")  # ("2016-03-10")  #("2018-10-09")   #
        upto_time_entr = tk.Entry(fromupto_frm, textvariable=self.upto_time_var, width=15, fg='grey')
        self.upto_time_var.set("23:59:59")  # ("HH:MM:SS.SSSS")  # ("10:01:03")  # ("HH:MM:SS.SSSS")  #("13:06:11.0")  #("08:40:00")  #

        filter_btn = tk.Button(fromupto_frm, text="Add filter", bg="LightsteelBlue3", command=self.Selection.add_filters)

        # GRID FROM UPTO
        fromupto_frm.grid(column=0, row=1, sticky="nsew")
        timerange_lbl.grid(column=0, row=0, columnspan=2, sticky="w", padx=5)
        from_lbl.grid(column=0, row=1, sticky="w", padx=5)
        from_cal_btn.grid(column=0, row=2, sticky="e", padx=5)
        from_date_entr.grid(column=1, row=2, sticky="w", padx=5)
        from_time_entr.grid(column=1, row=3, sticky="w", padx=5)

        upto_lbl.grid(column=2, row=1, sticky="w", padx=15)
        upto_cal_btn.grid(column=2, row=2, sticky="e", padx=5)
        upto_date_entr.grid(column=3, row=2, sticky="w", padx=5)
        upto_time_entr.grid(column=3, row=3, sticky="w", padx=5)

        filter_btn.grid(column=0, row=4, columnspan=2, sticky="w", padx=5, pady=(8, 0))

        # PROGRAM FRAME
        program_frm = tk.Frame(selection_frm, borderwidth=15, bg="LightsteelBlue")
        program_lbl = tk.Label(program_frm, text="List experimental programs (XP) in time range", bg="LightsteelBlue")
        program_btn = tk.Button(program_frm, text="Go!", command=self.Selection.getPrograms, bg="DarkSeaGreen4")

        self.XPs_var = tk.StringVar()
        self.Program_list = tk.Listbox(program_frm, listvariable=self.XPs_var, width=45, height=12, selectmode='extended')
        self.Program_list.bind('<Double-1>', self.Selection.setXPprogram)
        Vscrollfoo = tk.Scrollfoo(program_frm, orient='vertical', command=self.Program_list.yview)
        self.Program_list['yscrollcommand'] = Vscrollfoo.set
        Hscrollfoo = tk.Scrollfoo(program_frm, orient='horizontal', command=self.Program_list.xview)
        self.Program_list['xscrollcommand'] = Hscrollfoo.set

        # GRID PROGRAM
        program_frm.grid(column=0, row=2, sticky="nsew")
        program_lbl.grid(column=0, row=0, columnspan=2, sticky="w", padx=5)
        program_btn.grid(column=2, row=0, sticky="e", padx=10, pady=(0, 8))
        self.Program_list.grid(column=0, row=1, columnspan=3, sticky='nwes', padx=5)
        Vscrollfoo.grid(column=2, row=1, sticky='ens')
        Hscrollfoo.grid(column=0, row=1, columnspan=3, sticky='wes', padx=5)

        # ICRH event logs FRAME
        # ICRH not integrated in SAP (XControl and XEdit), so event logs are used
        ICRHevent_frm = tk.Frame(selection_frm, borderwidth=15, bg="LightsteelBlue")
        ICRHevent_lbl = tk.Label(ICRHevent_frm, text="List ICRH events in time range", bg="LightsteelBlue")
        ICRHevent_btn = tk.Button(ICRHevent_frm, text="/ Go!", command=self.Selection.getICRHevents, bg="DarkSeaGreen4")

        self.ICRHevent_var = tk.StringVar()
        self.ICRHevent_list = tk.Listbox(ICRHevent_frm, listvariable=self.ICRHevent_var, width=45, height=6, selectmode='extended')
        self.ICRHevent_list.bind('<Double-1>', self.Selection.setevent)
        ICRHVscrollfoo = tk.Scrollfoo(ICRHevent_frm, orient='vertical', command=self.ICRHevent_list.yview)
        self.ICRHevent_list['yscrollcommand'] = ICRHVscrollfoo.set
        ICRHHscrollfoo = tk.Scrollfoo(ICRHevent_frm, orient='horizontal', command=self.ICRHevent_list.xview)
        self.ICRHevent_list['xscrollcommand'] = ICRHHscrollfoo.set
        # Scenarios_btn = tk.Button(ICRHevent_frm, text="Show scenarios", command=self.getICRHeventScenarios, bg="LightsteelBlue")
        # Triggers_btn = tk.Button(ICRHevent_frm, text="Show triggers", command=self.getICRHeventTriggers, bg="LightsteelBlue")

        # GRID ICRH event logs
        ICRHevent_frm.grid(column=0, row=3, sticky="nsew")
        ICRHevent_lbl.grid(column=0, row=0, columnspan=2, sticky="w", padx=5)
        ICRHevent_btn.grid(column=2, row=0, sticky="e", padx=10, pady=(0, 8))
        self.ICRHevent_list.grid(column=0, row=1, columnspan=3, sticky='nwes', padx=5)
        ICRHVscrollfoo.grid(column=2, row=1, sticky='ens')
        ICRHHscrollfoo.grid(column=0, row=1, columnspan=3, sticky='wes', padx=5)
        # Scenarios_btn.grid(column=0, row=2, sticky='w', padx=5, pady=8)
        # Triggers_btn.grid(column=1, row=2, sticky='w', padx=5, pady=8)

        # CURRENT PROGRAM FRAME
        # selected_frm = tk.Frame(frame_container, borderwidth=15, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        CurrentProgram_frm = tk.Frame(selection_frm, borderwidth=15, bg="LightsteelBlue")
        self.Currentprogram_lbl = tk.Label(CurrentProgram_frm, text="The selected program is: None (using from-upto)", bg="LightsteelBlue", fg="midnight blue", font='helvetica 12 bold')
        scenarios_btn = tk.Button(CurrentProgram_frm, text="Show scenarios", command=self.Selection.getProgramScenario, bg="LightsteelBlue3", width=12)
        triggers_btn = tk.Button(CurrentProgram_frm, text="Show triggers ", command=self.Selection.getProgramTriggers, bg="LightsteelBlue3", width=12)

        # GRID CURRENT PROGRAM
        # selected_frm.grid(column=0, row=1, rowspan=1, sticky="nsew")
        CurrentProgram_frm.grid(column=0, row=3, sticky="nsew", )
        self.Currentprogram_lbl.grid(column=0, row=0, columnspan=3, sticky="w", padx=5)
        scenarios_btn.grid(column=0, row=1, sticky='w', padx=5, pady=8)
        triggers_btn.grid(column=0, row=2, sticky='w', padx=5, pady=0)

        # CACHING FRAME
        clearcache_frm = tk.Frame(frame_container, borderwidth=30, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        clearcache_frm.grid(column=0, row=1, sticky="nsew")

        caching_lbl = tk.Label(clearcache_frm, text="Cache", bg="LightsteelBlue", font='helvetica 15 bold', width=40)
        cachetxt_lbl = tk.Label(clearcache_frm, text="The cache currently contains", bg="LightsteelBlue")
        self.cache_lbl = tk.Label(clearcache_frm, text="", bg="LightsteelBlue")
        clearcache_btn = tk.Button(clearcache_frm, text="Clear cache", command=self.clear_cache, bg="LightPink3", width=12)

        # GRID CACHING
        caching_lbl.grid(column=0, row=0, columnspan=2, sticky="nsew")
        cachetxt_lbl.grid(column=0, row=1, sticky="sw", padx=5, pady=8)
        self.cache_lbl.grid(column=1, row=1, sticky="sw", padx=0, pady=8)
        clearcache_btn.grid(column=0, row=2, sticky="w", padx=5, pady=0)
        self.update_cache_info()

        # ICRH SETTINGS FRAME
        ICRHsettings_frm = tk.Frame(frame_container, borderwidth=15, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        ICRHsettings_lbl = tk.Label(ICRHsettings_frm, text="ICRH settings", bg="LightsteelBlue", font='helvetica 15 bold')

        # GRID ICRH SETTINGS
        ICRHsettings_frm.grid(column=0, row=2, sticky="nsew")
        ICRHsettings_lbl.grid(column=0, row=0, sticky="nsew")

        # ICRH FREQUENCY FRAME
        ICRHfreq_frm = tk.Frame(ICRHsettings_frm, borderwidth=15, bg="LightsteelBlue")
        ICRHfreq_lbl = tk.Label(ICRHfreq_frm, text="Specify ICRH frequency (as scenario is not saved in archiveDB)", bg="LightsteelBlue")
        self.ICRHfreq_var = tk.IntVar(None, 375)
        ICRHfreq250_chk = tk.Radiobutton(ICRHfreq_frm, text="25 MHz", variable=self.ICRHfreq_var, value=250, bg="LightsteelBlue")
        ICRHfreq375_chk = tk.Radiobutton(ICRHfreq_frm, text="37.5 MHz", variable=self.ICRHfreq_var, value=375, bg="LightsteelBlue")
        ICRHfreq380_chk = tk.Radiobutton(ICRHfreq_frm, text="38 MHz", variable=self.ICRHfreq_var, value=380, bg="LightsteelBlue")

        # GRID ICRH FREQUENCY
        ICRHfreq_frm.grid(column=0, row=1, sticky="nsew")
        ICRHfreq_lbl.grid(column=0, row=0, sticky="nsew", padx=5)
        ICRHfreq250_chk.grid(column=0, row=1, sticky="w", padx=5)
        ICRHfreq375_chk.grid(column=0, row=2, sticky="w", padx=5)
        ICRHfreq380_chk.grid(column=0, row=3, sticky="w", padx=5)

        ########################################

        # OUTPUT SETTINGS FRAME
        outputset_frm = tk.Frame(frame_container, borderwidth=15, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        outputset_lbl = tk.Label(outputset_frm, text="Output settings", bg="LightsteelBlue",
                                 font='helvetica 15 bold')

        # GRID OUTPUT SETTINGS
        outputset_frm.grid(column=1, row=0, sticky="nsew")
        outputset_lbl.grid(column=0, row=0, sticky="nsew")

        # TIME WINDOW FRAME
        timewindow_frm = tk.Frame(outputset_frm, borderwidth=15, bg="LightsteelBlue")
        timetype_lbl = tk.Label(timewindow_frm, text="Set time", bg="LightsteelBlue")
        self.timetype_var = tk.IntVar(None, 2)
        timetypeAbs_chk = tk.Radiobutton(timewindow_frm, text="absolute", variable=self.timetype_var, value=1, bg="LightsteelBlue")
        timetypeRel_chk = tk.Radiobutton(timewindow_frm, text="relative to t1, in time window:", variable=self.timetype_var, value=2, bg="LightsteelBlue")

        self.timewindow_var = tk.IntVar(None, 1)
        # TODO: Add info about what automatic means, when hovering over word or via info-button
        timewindowAuto_chk = tk.Radiobutton(timewindow_frm, text="automatic", variable=self.timewindow_var, value=1, bg="LightsteelBlue")
        timewindowFix_chk = tk.Radiobutton(timewindow_frm, text="fixed times, from ", variable=self.timewindow_var, value=2, bg="LightsteelBlue")
        self.tWindowFrom_var = tk.DoubleVar()
        timewindowFixFrom_entr = tk.Entry(timewindow_frm, textvariable=self.tWindowFrom_var, width=5)
        timewindowFixTo_lbl = tk.Label(timewindow_frm, text=" s upto ", bg="LightsteelBlue")
        self.tWindowTo_var = tk.DoubleVar()
        timewindowFixTo_entr = tk.Entry(timewindow_frm, textvariable=self.tWindowTo_var, width=5)
        timewindowFixs_lbl = tk.Label(timewindow_frm, text=" s", bg="LightsteelBlue")

        timewindowFixTrig_chk = tk.Radiobutton(timewindow_frm, text="fixed triggers, from trigger ", variable=self.timewindow_var, value=3, bg="LightsteelBlue")
        self.tWindowFromTrig_var = tk.IntVar()
        timewindowFixFromTrig_entr = tk.Entry(timewindow_frm, textvariable=self.tWindowFromTrig_var, width=5)
        timewindowFixToTrig_lbl = tk.Label(timewindow_frm, text=" upto trigger ", bg="LightsteelBlue")
        self.tWindowToTrig_var = tk.IntVar()
        timewindowFixToTrig_entr = tk.Entry(timewindow_frm, textvariable=self.tWindowToTrig_var, width=5)

        timewindowAdd_lbl = tk.Label(timewindow_frm, text="with addition of ", bg="LightsteelBlue")
        self.deltat = tk.DoubleVar(None, 0.0)
        timewindowAdd_entr = tk.Entry(timewindow_frm, textvariable=self.deltat, width=5)
        timewindowAdds_lbl = tk.Label(timewindow_frm, text=" s before and after ", bg="LightsteelBlue")

        # GRID TIME WINDOW
        timewindow_frm.grid(column=0, row=1, sticky="nsew")
        timetype_lbl.grid(column=0, row=0, columnspan=4, rowspan=1, sticky="w")
        timetypeAbs_chk.grid(column=0, row=1, columnspan=2, rowspan=1, sticky="w")
        timetypeRel_chk.grid(column=0, row=2, columnspan=4, rowspan=1, sticky="w")
        timewindowAuto_chk.grid(column=1, row=5, columnspan=1, rowspan=1, sticky="w")
        timewindowFix_chk.grid(column=1, row=6, columnspan=2, rowspan=1, sticky="w")
        timewindowFixFrom_entr.grid(column=3, row=6, columnspan=1, rowspan=1, sticky="e")
        timewindowFixTo_lbl.grid(column=4, row=6, columnspan=1, rowspan=1, sticky="w")
        timewindowFixTo_entr.grid(column=5, row=6, columnspan=1, rowspan=1, sticky="e")
        timewindowFixs_lbl.grid(column=6, row=6, columnspan=1, rowspan=1, sticky="w")

        timewindowFixTrig_chk.grid(column=1, row=7, columnspan=3, rowspan=1, sticky="w")
        timewindowFixFromTrig_entr.grid(column=4, row=7, columnspan=1, rowspan=1, sticky="e")
        timewindowFixToTrig_lbl.grid(column=5, row=7, columnspan=2, rowspan=1, sticky="w")
        timewindowFixToTrig_entr.grid(column=7, row=7, columnspan=1, rowspan=1, sticky="w")

        timewindowAdd_lbl.grid(column=1, row=8, columnspan=2, rowspan=1, sticky="w")
        timewindowAdd_entr.grid(column=2, row=8, columnspan=1, rowspan=1, sticky="e")
        timewindowAdds_lbl.grid(column=3, row=8, columnspan=3, rowspan=1, sticky="w")

        # SCALE FRAME
        Scale_frm = tk.Frame(outputset_frm, borderwidth=15, bg="LightsteelBlue")
        Scale_lbl = tk.Label(Scale_frm, text="Scale y axis", bg="LightsteelBlue")
        self.varScale = tk.IntVar(None, 1)
        ScaleLin_chk = tk.Radiobutton(Scale_frm, text="linear", variable=self.varScale, value=1, bg="LightsteelBlue")
        ScaleLog_chk = tk.Radiobutton(Scale_frm, text="logarithmic", variable=self.varScale, value=2, bg="LightsteelBlue")

        # GRID SCALE
        Scale_frm.grid(column=0, row=2, sticky="nsew")
        Scale_lbl.grid(column=0, row=0, columnspan=1, rowspan=1, sticky="w")
        ScaleLin_chk.grid(column=0, row=1, columnspan=1, rowspan=1, sticky="w")
        ScaleLog_chk.grid(column=0, row=2, columnspan=1, rowspan=1, sticky="w")

        # RESOLUTION FRAME
        resolution_frm = tk.Frame(outputset_frm, borderwidth=15, bg="LightsteelBlue")
        resolution_lbl = tk.Label(resolution_frm, text="Resolution", bg="LightsteelBlue")
        resolutionDraw_lbl = tk.Label(resolution_frm, text="Plot ", bg="LightsteelBlue")
        self.resolution = tk.StringVar(None, "100")
        resolution_entr = tk.Entry(resolution_frm, textvariable=self.resolution, width=5)
        resolutionIntrvls_lbl = tk.Label(resolution_frm, text=" data intervals [enter 0 for full resolution]", bg="LightsteelBlue")

        # GRID RESOLUTION
        resolution_frm.grid(column=0, row=3, sticky="nsew")
        resolution_lbl.grid(column=0, row=0, columnspan=2, sticky="w")
        resolutionDraw_lbl.grid(column=0, row=1, sticky="w")
        resolution_entr.grid(column=1, row=1, columnspan=1, rowspan=1, sticky="w")
        resolutionIntrvls_lbl.grid(column=2, row=1, columnspan=1, rowspan=1, sticky="w")

        # Annotate FRAME
        annotate_frm = tk.Frame(outputset_frm, borderwidth=15, bg="LightsteelBlue")
        self.annotate_var = tk.IntVar(None, 0)
        annotate_cst = tk.Checkbutton(annotate_frm, variable=self.annotate_var, text="Annotate plateaus within", bg="LightsteelBlue")
        self.threshold_var = tk.StringVar(None, "5")
        annotate_entr = tk.Entry(annotate_frm, textvariable=self.threshold_var, width=5)
        annotateIntrvls_lbl = tk.Label(annotate_frm, text=" % over at least", bg="LightsteelBlue")
        self.cstwindow_var = tk.StringVar(None, "1")
        cstwindow_entr = tk.Entry(annotate_frm, textvariable=self.cstwindow_var, width=5)
        cstunit_lbl = tk.Label(annotate_frm, text="s", bg="LightsteelBlue")

        self.integral_var = tk.IntVar(None, 0)
        annotate_int = tk.Checkbutton(annotate_frm, variable=self.integral_var, text="Annotate integral of signal", bg="LightsteelBlue")

        # GRID annotate
        annotate_frm.grid(column=0, row=4, sticky="nsew")
        annotate_cst.grid(column=0, row=0, sticky="w")
        annotate_entr.grid(column=1, row=0, columnspan=1, rowspan=1, sticky="w")
        annotateIntrvls_lbl.grid(column=2, row=0, columnspan=1, rowspan=1, sticky="w")
        cstwindow_entr.grid(column=3, row=0, columnspan=1, rowspan=1, sticky="w")
        cstunit_lbl.grid(column=4, row=0, columnspan=1, rowspan=1, sticky="w")
        annotate_int.grid(column=0, row=1, sticky="w")

        # # VIDEO IMAGES
        # photo_frm = tk.Frame(outputset_frm, borderwidth=15, bg="LightsteelBlue")
        # video_lbl = tk.Label(photo_frm, text="Add photo's from video AEQ", bg="LightsteelBlue")
        # self.video = tk.StringVar(None, "41")
        # video_entr = tk.Entry(photo_frm, textvariable=self.video, width=5)
        # phototimes_lbl = tk.Label(photo_frm, text="for t-t1:", bg="LightsteelBlue")
        # self.photo0 = tk.StringVar(None, "0")
        # photo0_entr = tk.Entry(photo_frm, textvariable=self.photo0, width=5)
        # self.photo1 = tk.StringVar(None, "1")
        # photo1_entr = tk.Entry(photo_frm, textvariable=self.photo1, width=5)
        # self.photo2 = tk.StringVar(None, "2")
        # photo2_entr = tk.Entry(photo_frm, textvariable=self.photo2, width=5)
        # self.photo3 = tk.StringVar(None, "3")
        # photo3_entr = tk.Entry(photo_frm, textvariable=self.photo3, width=5)
        # photounit_lbl = tk.Label(photo_frm, text=" s", bg="LightsteelBlue")
        #
        # # GRID VIDEO IMAGES
        # photo_frm.grid(column=0, row=4, sticky="nsew")
        # video_lbl.grid(column=0, row=0, columnspan=4, sticky="w", padx=1)
        # video_entr.grid(column=4, row=0, columnspan=1, sticky="w", padx=1, pady=2)
        # phototimes_lbl.grid(column=0, row=1, sticky="w", padx=1)
        # photo0_entr.grid(column=1, row=1, columnspan=1, rowspan=1, sticky="w", padx=1)
        # photo1_entr.grid(column=2, row=1, columnspan=1, rowspan=1, sticky="w", padx=1)
        # photo2_entr.grid(column=3, row=1, columnspan=1, rowspan=1, sticky="w", padx=1)
        # photo3_entr.grid(column=4, row=1, columnspan=1, rowspan=1, sticky="w", padx=1)
        # photounit_lbl.grid(column=5, row=1, columnspan=1, rowspan=1, sticky="w", padx=1)

        # OUTPUT HANDLING FRAME
        # TODO: provide option to choose to upload once figures are shown or saved.
        output_frm = tk.Frame(outputset_frm, borderwidth=15, bg="LightsteelBlue")
        output_lbl = tk.Label(output_frm, text="Output handling", bg="LightsteelBlue")
        self.show = tk.IntVar(None, 1)
        show_chk = tk.Checkbutton(output_frm, variable=self.show, text="show output (figures and videos)", bg="LightsteelBlue")
        self.savePDF_var = tk.IntVar()
        savePDF_chk = tk.Checkbutton(output_frm, variable=self.savePDF_var, text="save figures as PDF", bg="LightsteelBlue")
        self.savePNG_var = tk.IntVar()
        savePNG_chk = tk.Checkbutton(output_frm, variable=self.savePNG_var, text="save figures as PNG", bg="LightsteelBlue")
        self.saveMP4_var = tk.IntVar()
        saveMP4_chk = tk.Checkbutton(output_frm, variable=self.saveMP4_var, text="save videos as MP4", bg="LightsteelBlue")
        self.uploadcomponentlog_var = tk.IntVar()
        uploadcomponentlog_chk = tk.Checkbutton(output_frm, variable=self.uploadcomponentlog_var, text="upload plots as comment to program log (XP or SAP)", bg="LightsteelBlue")
        # self.uploadcompeventlog_var = tk.IntVar()
        # uploadcompeventlog_chk = tk.Checkbutton(output_frm, variable=self.uploadcompeventlog_var, text="/ upload plots with component event log", bg="LightsteelBlue")
        self.saveData_var = tk.IntVar()
        saveData_chk = tk.Checkbutton(output_frm, variable=self.saveData_var, text="store time trace data", bg="LightsteelBlue")

        # GRID OUTPUT HANDLING
        output_frm.grid(column=0, row=5, sticky="nsew")
        output_lbl.grid(column=0, row=0, columnspan=1, sticky="w", padx=5)
        show_chk.grid(column=0, row=1, columnspan=1, sticky="w", padx=5)
        savePDF_chk.grid(column=0, row=2, columnspan=1, sticky="w", padx=5)
        savePNG_chk.grid(column=0, row=3, columnspan=1, sticky="w", padx=5)
        saveMP4_chk.grid(column=0, row=4, columnspan=1, sticky="w", padx=5)
        uploadcomponentlog_chk.grid(column=0, row=5, columnspan=1, sticky="w", padx=5)
        # uploadcompeventlog_chk.grid(column=0, row=6, columnspan=1, sticky="w", padx=5)
        saveData_chk.grid(column=0, row=6, columnspan=1, sticky="w", padx=5)

        # # PLOT CHANNEL FRAME
        # anyChannel_frm = tk.Frame(frame_container, borderwidth=15, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=1)
        # anyChannel_lbl = tk.Label(anyChannel_frm, text="Plot any ICRH channel", bg="LightsteelBlue", font='helvetica 15 bold')
        #
        # # GRID PLOT CHANNEL
        # anyChannel_frm.grid(column=1, row=2, sticky="nsew")
        # anyChannel_lbl.grid(column=0, row=0, sticky="nsew")
        #
        # # CHANNELS
        # channel_frm = tk.Frame(anyChannel_frm, borderwidth=15, bg="LightsteelBlue")
        # channel_lbl = tk.Label(channel_frm, text="List ICRH channels in program or time range", bg="LightsteelBlue")
        # channel_btn = tk.Button(channel_frm, text="Go!", command=self.ICRHchannels.getChannels, bg="DarkSeaGreen4")
        # self.channel_tree = ttk.Treeview(channel_frm, height=6)
        # channel_Vscrollfoo = tk.Scrollfoo(channel_frm, orient='vertical', command=self.channel_tree.yview)
        # self.channel_tree['yscrollcommand'] = channel_Vscrollfoo.set
        # # tree.bind('<<TreeviewSelect>>', self.item_selected)  #if clicked, add it to list to plot
        # plot_lbl = tk.Label(channel_frm, text="Plot selected channels", bg="LightsteelBlue")
        # plot_btn = tk.Button(channel_frm, text="Go!", command=self.ICRHchannels.plotChannels, bg="DarkSeaGreen4")
        #
        # # GRID CHANNELS
        # channel_frm.grid(column=0, row=1, sticky="nsew")
        # channel_lbl.grid(column=0, row=0, columnspan=2, sticky="w", padx=5)
        # channel_btn.grid(column=2, row=0, sticky="w", padx=10, pady=5)
        # self.channel_tree.grid(column=0, row=1, columnspan=3, sticky='nwes', padx=5)
        # channel_Vscrollfoo.grid(column=2, row=1, sticky='ens')
        # plot_lbl.grid(column=0, row=2, columnspan=2, sticky="w", padx=5)
        # plot_btn.grid(column=2, row=2, sticky="w", padx=10, pady=8)

        # PRESET FRAME
        self.preset_frm = tk.Frame(frame_container, borderwidth=15, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=2)
        preset_lbl = tk.Label(self.preset_frm, text="Plot presets", bg="LightsteelBlue", font='helvetica 15 bold')

        # Styling for the tabs
        style = ttk.Style()
        style.configure('TNotebook', background='LightsteelBlue')
        style.configure('TFrame', background='LightsteelBlue')
        style.configure('TNotebook.Tab', font=('Helvetica', 12, 'bold'))
        style.map('TNotebook.Tab',
                  background=[('selected', 'LightsteelBlue'), ('!selected', 'LightsteelBlue3')],
                  foreground=[('selected', 'black'), ('!selected', 'black')])

        tabControl = ttk.Notebook(self.preset_frm, style='TNotebook')
        tabframeICRH = ttk.Frame(tabControl)
        tabframeECRH = ttk.Frame(tabControl)
        tabframePhysics = ttk.Frame(tabControl)
        tabControl.add(tabframeICRH, text='ICRH')
        tabControl.add(tabframeECRH, text='ECRH')
        tabControl.add(tabframePhysics, text='Physics')

        # See below for the rest of the preset contents

        # GRID PRESET
        self.preset_frm.grid(column=2, row=0, rowspan=3, sticky="nsew")
        preset_lbl.grid(column=0, row=0, sticky="nsew")
        tabControl.grid(column=0, row=1, sticky="nsew")

        # ANALYSIS FRAME
        self.analysis_frm = tk.Frame(frame_container, borderwidth=15, bg="LightsteelBlue", highlightbackground="LightsteelBlue3", highlightthickness=2)
        analysis_lbl = tk.Label(self.analysis_frm, text="Analysis", bg="LightsteelBlue", font='helvetica 15 bold')  #, width=59)

        self.analysis_frm.grid(column=1, row=1, columnspan=1, rowspan=2, sticky="nsew")
        analysis_lbl.grid(column=0, row=0, columnspan=3, sticky="nsew")

        plotall_lbl = tk.Label(self.analysis_frm, text="Plot selected signals in 1 plot", bg="LightsteelBlue")
        plotall_btn = tk.Button(self.analysis_frm, text="Go!", command=self.outputPresetPlots, bg="DarkSeaGreen4")
        analyze_lbl = tk.Label(self.analysis_frm, text="Open selected signals in analysis GUI", bg="LightsteelBlue")
        analyze_btn = tk.Button(self.analysis_frm, text="Go!", command=self.AnalyzeGUI, bg="DarkSeaGreen4")
        correlate_lbl = tk.Label(self.analysis_frm, text="Open selected signals in Correlation GUI", bg="LightsteelBlue")
        correlate_btn = tk.Button(self.analysis_frm, text="Go!", command=self.CorrelationsGUI, bg="DarkSeaGreen4")
        plotXP_lbl = tk.Label(self.analysis_frm, text="Plot selected signals for XP's (csv, - for range):", bg="LightsteelBlue")
        self.plotXP_var = tk.StringVar(None, "XP_...")
        plotXP_entr = tk.Entry(self.analysis_frm, textvariable=self.plotXP_var, width=33)
        plotXP_btn = tk.Button(self.analysis_frm, text="Go!", command=self.plotXPs, bg="DarkSeaGreen4")

        align_lbl = tk.Label(self.analysis_frm, text="where the signals are aligned based on", bg="LightsteelBlue")
        self.align_var = tk.IntVar(None, 1)
        alignTime_chk = tk.Radiobutton(self.analysis_frm, text="the time window", variable=self.align_var, value=1, bg="LightsteelBlue")
        alignStart_chk = tk.Radiobutton(self.analysis_frm, text="the signal start, going over threshold", variable=self.align_var, value=2, bg="LightsteelBlue")
        self.alignStart_var = tk.DoubleVar(None, 0.001)
        alignStart_entr = tk.Entry(self.analysis_frm, textvariable=self.alignStart_var, width=5)
        alignMax_chk = tk.Radiobutton(self.analysis_frm, text="the signal maximum", variable=self.align_var, value=3, bg="LightsteelBlue")

        # GRID ANALYSIS
        plotall_lbl.grid(column=0, row=1, sticky="w", pady=(10, 5))
        plotall_btn.grid(column=2, row=1, sticky="e", pady=(10, 5), padx=(50, 0))
        analyze_lbl.grid(column=0, row=2, sticky="w", pady=5)
        analyze_btn.grid(column=2, row=2, sticky="e", pady=5, padx=(50, 0))
        correlate_lbl.grid(column=0, row=3, sticky="w", pady=5)
        correlate_btn.grid(column=2, row=3, sticky="e", pady=5, padx=(50, 0))
        plotXP_lbl.grid(column=0, row=4, sticky="w", pady=(5, 0))
        plotXP_btn.grid(column=2, row=4, sticky="e", pady=(5, 0), padx=(50, 0))
        plotXP_entr.grid(column=0, row=5, sticky="nw", pady=(0, 1))
        align_lbl.grid(column=0, row=6, sticky="nw", pady=(0, 1))
        alignTime_chk.grid(column=0, row=7, sticky="nw", pady=(0, 1))
        alignStart_chk.grid(column=0, row=8, sticky="nw", pady=(0, 1))
        alignStart_entr.grid(column=1, row=8, sticky="nw", pady=(0, 1), padx=5)
        alignMax_chk.grid(column=0, row=9, sticky="nw", pady=(0, 1))

        ########################################
        #TODO: Turn this into a function and make another file with dictionaries to hold the presets in a more convenient way for the user
        # Dictionary to hold the different tabs
        self.tabs = {}
        self.tabs["ICRH"] = Preset.Tab(tabframeICRH)

        # GENERATOR
        self.tabs["ICRH"].addGroup(self, "Generator")
        self.tabs["ICRH"].addSystemToGroup("Generator", "Generator 1")
        genOneSubsystems = {"Endstage power": ["Forward power", "Reflected power"],
                            "Endstage voltages": ["Anode", "Grid 2", "Grid 1"],
                            "Driver stage power": ["Forward power", "Reflected power"],
                            "Driver stage voltages": ["Grid 2", "Grid 1"],
                            "Cooling water temperatures": ["Inlet temperature (???)", "Outlet anode endstage (CT100)", "Outlet gate endstage (CT110)",
                                                           "Outlet cathode endstage (CT120)", "Outlet anode driver (CT130)"],
                            "Cooling water flow": ["Endstage anode (CF100)", "Endstage gate (CF110)", "Endstage cathode (CF120)", "Driver stage anode (CF130)"]}
        self.tabs["ICRH"].setSubsystemsToSystem("Generator", "Generator 1", genOneSubsystems)

        self.tabs["ICRH"].addSystemToGroup("Generator", "Generator 2")
        genTwoSubsystems = {"Endstage power": ["Forward power", "Reflected power"],
                            "Endstage voltages": ["Anode", "Grid 2", "Grid 1"],
                            "Driver stage power": ["Forward power", "Reflected power"],
                            "Driver stage voltages": ["Anode", "Grid 2", "Grid 1"],
                            "Cooling water temperatures": ["Inlet temperature (???)", "Outlet anode endstage (CT100)", "Outlet gate endstage (CT110)",
                                                           "Outlet cathode endstage (CT120)", "Outlet anode driver stage (CT130)"],
                            "Cooling water flow": ["Endstage anode (CF100)", "Endstage gate (CF110)", "Endstage cathode (CF120)", "Driver stage anode (CF130)"]}
        self.tabs["ICRH"].setSubsystemsToSystem("Generator", "Generator 2", genTwoSubsystems)

        # ANTENNA
        self.tabs["ICRH"].addGroup(self, "Antenna")
        self.tabs["ICRH"].addSystemToGroup("Antenna", "Temperature")
        environmentSubsystems = {"Strap temperature": ["strap 1 high (CT345)", "strap 1 center (CT346)", "strap 2 high (CT446)", "strap 2 center (CT445)"],  # "strap 1", "edge 1", "front 1", "strap 2","edge 2", "front 2",
                                 "Box temperature": ["box top (CT344)", "box left top (CT443)", "box left center (CT442)", "box left bottom (CT441)", "box bottom (CT444)", "box right bottom (CT341)", "box right center (CT342)", "box right top (CT343)"],
                                 "Thermocouples image": ["Thermocouples.png"]}
        self.tabs["ICRH"].setSubsystemsToSystem("Antenna", "Temperature", environmentSubsystems)

        self.tabs["ICRH"].addSystemToGroup("Antenna", "RF")
        RFSubsystems = {"Antenna current": ["Right strap (CE303)", "Left strap (CE403)", "Phase between straps"],
                        # "Antenna voltage": ["/ Right strap", "/ Left strap"],
                        # "System power": ["to do: Generator net power", "/ Antenna radiated power", "/ Estimated radiated power"],
                        # "Antenna resistance": ["/ Resistance"],
                        "Capacitor voltages": ["Capacitor 1 (CE301)", "Capacitor 2 (CE401)"],
                        "Capacitor currents": ["Capacitor 1 (CE302)", "Capacitor 2 (CE402)"],
                        "Capacitor power": ["Capacitor 1", "Capacitor 2"]}
        self.tabs["ICRH"].setSubsystemsToSystem("Antenna", "RF", RFSubsystems)

        self.tabs["ICRH"].addSystemToGroup("Antenna", "Positions")
        positionSubsystems = {"Antenna": ["Antenna head"],
                              "Profiles": ["Poincare plot", "LCFS", "Antenna"],
                              "Matching system": ["line stretcher 1", "line stretcher 2", "stub 1", "stub 2", "Capacitor 1", "Capacitor 2"]}
        self.tabs["ICRH"].setSubsystemsToSystem("Antenna", "Positions", positionSubsystems)

        # CALORIMETRY
        self.tabs["ICRH"].addGroup(self, "Calorimetry")

        self.tabs["ICRH"].addSystemToGroup("Calorimetry", "Calorimeter")
        calorimeterCalSubsystems = {"Heat exchange": ["Strap 1", "Edge 1", "Front 1", "Strap 2", "Edge 2", "Front 2", "Capacitor 1", "Capacitor 2"],
                                    "Water flow": ["Strap 1 (CF304)", "Edge 1 (CF305)", "Front 1 (CF306)", "Strap 2 (CF404)", "Edge 2 (CF405)", "Front 2 (CF406)", "Capacitor 1 (CF301)", "Capacitor 2 (CF401)"],
                                    "Water temperature": ["Outlet Strap 1 (CT304)", "Outlet Edge 1 (CT305)", "Outlet Front 1 (CT306)", "Outlet Strap 2 (CT404)", "Outlet Edge 2 (CT405)", "Outlet Front 2 (CT406)", "Outlet Capacitor 1 (CT301)", "Outlet Capacitor 2 (CT401)", "Inlet antenna head (CT501)", "Inlet Capacitors (CT502)"]}

        self.tabs["ICRH"].setSubsystemsToSystem("Calorimetry", "Calorimeter", calorimeterCalSubsystems)

        self.tabs["ICRH"].addSystemToGroup("Calorimetry", "Directional Couplers")
        DRCSubsystems = {"DRC1 Power": ["Forward power", "Reflected power", "Net power"],
                         "DRC1 Reflection coefficient": ["GMSR"],
                         "DRC3 Power": ["Forward power", "Reflected power", "Net power"],
                         "DRC3 Reflection coefficient": ["GMSR"]}
        self.tabs["ICRH"].setSubsystemsToSystem("Calorimetry", "Directional Couplers", DRCSubsystems)

        self.tabs["ICRH"].addSystemToGroup("Calorimetry", "Strap 2")
        strapSubsystems = {"Load resistance": ["Reflection coefficient port 2"]}
        self.tabs["ICRH"].setSubsystemsToSystem("Calorimetry", "Strap 2", strapSubsystems)

        ########################################

        self.tabs["ECRH"] = Preset.Tab(tabframeECRH)

        # GYROTRONS
        self.tabs["ECRH"].addGroup(self, "Gyrotrons")

        self.tabs["ECRH"].addSystemToGroup("Gyrotrons", "Power")
        gyrotronpowerSubsystems = {"Total": ["ECRH power", "Sum of gyrotron powers", "Sum of gyrotron setpoints"],
                                 "Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
                                 "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]}
        self.tabs["ECRH"].setSubsystemsToSystem("Gyrotrons", "Power", gyrotronpowerSubsystems)

        self.tabs["ECRH"].addSystemToGroup("Gyrotrons", "z offset")
        gyrotronzoffsetSubsystems = {"Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
                                 "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]}
        self.tabs["ECRH"].setSubsystemsToSystem("Gyrotrons", "z offset", gyrotronzoffsetSubsystems)

        self.tabs["ECRH"].addSystemToGroup("Gyrotrons", "phi offset")
        gyrotronphioffsetSubsystems = {"Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
                                 "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]}
        self.tabs["ECRH"].setSubsystemsToSystem("Gyrotrons", "phi offset", gyrotronphioffsetSubsystems)

        self.tabs["ECRH"].addSystemToGroup("Gyrotrons", "setpoint")
        gyrotronsetpointSubsystems = {"Series 1": ["Gyrotron A1", "Gyrotron B1", "Gyrotron C1", "Gyrotron D1", "Gyrotron E1", "Gyrotron F1"],
                                 "Series 5": ["Gyrotron A5", "Gyrotron B5", "Gyrotron C5", "Gyrotron D5", "Gyrotron E5", "Gyrotron F5"]}
        self.tabs["ECRH"].setSubsystemsToSystem("Gyrotrons", "setpoint", gyrotronsetpointSubsystems)

        # ECE channels
        self.tabs["ECRH"].addGroup(self, "ECE channels")

        self.tabs["ECRH"].addSystemToGroup("ECE channels", "ECE")
        # ECEchannelSubsystems = {"channels": [f"channel {i:02d}" for i in range(1,33)]}
        ECEchannelSubsystems = {"core": ["core channel (18 if B0 above 2.55, else 13)"],
                                "channels": [f"channel {i:02d}" for i in range(1, 12)] + ["channel 12 (~core)", "channel 13 (~core)"]
                                            + [f"channel {i:02d}" for i in range(14, 24)] + ["channel 24 (~LFS half radius)"]
                                            + [f"channel {i:02d}" for i in range(25, 33)]}
        self.tabs["ECRH"].setSubsystemsToSystem("ECE channels", "ECE", ECEchannelSubsystems)

        ########################################

        self.tabs["Physics"] = Preset.Tab(tabframePhysics)

        self.tabs["Physics"].addGroup(self, "W7X")

        self.tabs["Physics"].addSystemToGroup("W7X", "Power")
        powerSubsystems = {"Plasma heating": ["ECRH power", "ICRH power", "NBI power"],
                             "Stored energy": ["Wdia"],
                             "Bolometry": ["Total radiated power"],
                             "Power depositition": ["Radiated over total power"]
                             }
        self.tabs["Physics"].setSubsystemsToSystem("W7X", "Power", powerSubsystems)

        self.tabs["Physics"].addSystemToGroup("W7X", "Environment")
        environmentSubsystems = {"Pressure": ["Plasma vessel pressure"],
                                 "Density": ["Line integrated density", "Line average density", "Line integrated density fluctuations (Detector 1)"],  # "Line integrated density fluctuations (Detector 2)" , "/ Thomson", "/ Gas flow"],
                                 "Zeff": ["H over H+He (only with w7xdia.zeff)"],
                                 "Central temperature": ["Ion temperature (only with w7xdia.xics)", "Electron temperature (only with w7xdia.xics)"],
                                 "Line integrated temperature": ["Ion temperature", "Electron temperature"]
                                 }
        self.tabs["Physics"].setSubsystemsToSystem("W7X", "Environment", environmentSubsystems)

        self.tabs["Physics"].addSystemToGroup("W7X", "Diagnostics")
        diagnosticSubsystems = {"Multi-Purpose-Manipulator (MPM)": ["Position"]}
        self.tabs["Physics"].setSubsystemsToSystem("W7X", "Diagnostics", diagnosticSubsystems)

        self.tabs["Physics"].addGroup(self, "Video")

        self.tabs["Physics"].addSystemToGroup("Video", "In vessel")
        videoSubsystems = {"Edicam": ["AEQ10", "AEQ11", "AEQ20", "AEQ30", "AEQ31", "AEQ40", "AEQ41", "AEQ50", "AEQ51"]}
        self.tabs["Physics"].setSubsystemsToSystem("Video", "In vessel", videoSubsystems)


        ###################################################

        # Update the canvas's scroll region after adding frames and content
        frame_container.update_idletasks()
        canvas.configure(scrollregion=canvas.bbox("all"))

        # Grid the canvas and scrollfoos
        canvas.grid(row=0, column=0, sticky="nsew")
        canv_vscrollfoo.grid(row=0, column=1, sticky="ns")
        canv_hscrollfoo.grid(row=1, column=0, sticky="ew")

        # Make sure that the scrollfoo remains visible when resizing the canvas
        def on_canvas_configure(event):
            canvas.configure(scrollregion=canvas.bbox("all"))
        canvas.bind("<Configure>", on_canvas_configure)

        # Configure row and column weights for resizing
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.mainloop()

    # Functions regarding special treatment of the signals, i.e. showing all signals in one plot, superimposing signals for different Xps, and opening the signals in an analysis GUI
    def getselected(self):
        """ Collect all the signals that are selected on the GUI in a list and return the list.
        """
        signals = []

        # Group (pdf): Generator
        # Sytems (figures): Generator 1, Generator 2
        # Subsystems (plots): [Endstage power, Cooling water, ...]
        # Signals (graphs): Forward power, Reflected power, ...
        for tabName, tab in self.tabs.items():
            for groupName, group in tab.groups.items():
                for systemName, system in group.systems_dict.items():
                    for subsystem in system.subsystems_lst:  # list of subsystem objects
                        for signal, var in subsystem.vars.items():
                            if var.get() == 1:
                                if ".png" not in signal:
                                    signals.append(groupName + "/" + systemName + "/" + subsystem.name + "/" + signal)
        return signals

    def outputPresetPlots(self):
        """ Generate the output where all the signals will be in one plot
        """
        # All signal-graphs have to be on the same plot
        signals = self.getselected()
        # Receive signals as a list and put it in a dict {"custom": list}
        if not len(signals) == 0:
            signaldict = {"Custom": signals}

            set = self.OutputSettings.getOutputSettings()
            specSet = self.ICRHsettings.getICRHsettings()
            output = Output.Output("Custom", signaldict, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], mindim=set["mindim"], maxdim=set["maxdim"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                   showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
            output.produceOutput()
            self.update_cache_info()

    def AnalyzeGUI(self):
        """ Open the dedicated GUI for analysis
        """
        signals = self.getselected()
        Analyse = AnalysisGUI.AnalysisGUI(self, signals)

    def CorrelationsGUI(self):
        """ Open the dedicated GUI for analysis
        """
        signals = self.getselected()
        XPid = self.Selection.getCurrentProgramID()
        Correlate = CorrelationsGUI.CorrelationsGUI(self, signals, XPid)

    def plotXPs(self):
        """ Generate the output where the signals are plotted for a list of signals
        """
        # Get the selected signals and the XPs for which they have to be plotted
        signals = self.getselected()
        XPstring = self.plotXP_var.get()
        XPs = Selection.getXPs(XPstring)

        if len(XPs) == 0:
            log.error('No suitable XPs were found.')
            return

        # Set the timing information for the current XP
        mindims = []
        maxdims = []
        for j, XP in enumerate(XPs):
            try:
                self.Selection.updateCurrentProgram("XP", XP)
            except Exception as e:
                log.error('{}: exception setting program: {}'.format(e, XP))
                continue
            mindim, maxdim = self.OutputSettings.getTimeWindow()  # in s since epoch
            mindims.append(mindim)
            maxdims.append(maxdim)
        self.Selection.updateCurrentProgram("XP", XPs[0])

        # The user defined if the signals have to be aligned on the relative time window, on the signal start or on the signal maximum
        if not len(signals) == 0:
            align = self.OutputSettings.getAlignmentXPs()
            # if aligned on the window, the signals are plotted in one figure on different plots. They share a time axis
            if align == 'Window':
                signaldict = {signal: [signal] for signal in signals}
                set = self.OutputSettings.getOutputSettings()
                specSet = self.ICRHsettings.getICRHsettings()
                output = Output.Output("XP comparison", signaldict, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                       showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
                output.produceOutputForXPs(XPs, mindims, maxdims, align)

            # If aligned on max or start of signal, the different signals cannot share a time axis. Use separate figures to avoid confusion
            else:
                for signal in signals:
                    signaldict = {signal: [signal]}
                    set = self.OutputSettings.getOutputSettings()
                    specSet = self.ICRHsettings.getICRHsettings()
                    thresh = self.OutputSettings.getAlignmentStartXPs() # only necessary for alignment on signal start
                    output = Output.Output("XP comparison", signaldict, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                           showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
                    output.produceOutputForXPs(XPs, mindims, maxdims, align, threshold=thresh)

        self.update_cache_info()


    # Functions regarding the cache, i.e. updating cache information and clearing the cache
    def update_cache_info(self):
        """ Update the information that is provided on the cache
        """
        cache_path = w7xarchive.get_cache_path()
        try:
            cache_size = subprocess.check_output(['du', '-sh', cache_path]).split()[0].decode('utf-8')  # disk usage in human readable format (e.g. '2,1GB')
            cache_str = "(" + cache_size + ")"
            self.cache_lbl.config(text=cache_str)
        except Exception as e:
            cache_str = "(cache not found)"
            self.cache_lbl.config(text=cache_str)
        self.update()

    def clear_cache(self):
        """ Call the w7xarchive function that handles clearing he cache
        """
        w7xarchive.clear_cache()
        self.update_cache_info()

