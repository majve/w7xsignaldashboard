import dash_bootstrap_components as dbc
from dash import html, dcc
from menu_utils import get_menus_from_tabs

def analysis_layout():
    """Initial layout for the Analysis tab, starting with an 'Add Plot' button."""
    return html.Div([
        html.Button('Add Plot', id='add-plot-btn', n_clicks=0, style={'background-color': '#006c66', 'color': 'white', 'margin-bottom': '20px'}),
        html.Div(id='plots-section', children=[]),  # This will hold dynamically added plots
        html.Div([
            html.Button('Plot', id='plot-analysis', n_clicks=0, style={'background-color': '#006c66', 'color': 'white'})
        ], style={'text-align': 'right'})  # Align the button to the right
    ])

def plot_layout(plot_id):
    """Generates layout for a plot with 'Add Signal' and 'Add Function' buttons."""
    return dbc.Card([
        dbc.CardHeader(f"Plot {plot_id}"),
        dbc.CardBody([
            html.Button('Add Signal', id={'type': 'add-signal-btn', 'spec': f'plot{plot_id}'}, n_clicks=0, style={'background-color': '#006c66', 'color': 'white', 'margin-right': '10px', 'margin-bottom': '10px'}),
            # html.Button('Add Function', id={'type': 'add-function-btn', 'spec': f'plot{plot_id}'}, n_clicks=0, style={'background-color': '#006c66', 'color': 'white', 'margin-bottom': '10px'}),
            html.Div(id={'type': 'signals-section', 'spec': f'plot{plot_id}'}, children=[]),
            html.Div(id={'type': 'functions-section', 'spec': f'plot{plot_id}'}, children=[]),
        ])
    ], style={'margin-bottom': '20px'})


def signal_layout(tabs, plot_id, signal_id):
    """Creates the layout for each variable (checkbox, name entry, signal dropdown, unit label) with alphabetic names."""
    variable_name = get_variable_name(signal_id)
    return dbc.Row([
        dbc.Col(dbc.Checkbox(id={'type': 'variable-checkbox', 'spec': f'plot{plot_id}_sig{signal_id}'}, value=True), width=1),
        dbc.Col(get_menus_from_tabs(tabs, {'type': 'dropdown-menu', 'spec': f'plot{plot_id}_sig{signal_id}'}), width=8),
        dbc.Col(html.Div(id={'type': 'signal-unit', 'spec': f'plot{plot_id}_sig{signal_id}'}, children="a.u."), width=2)
    ], style={'margin-bottom': '5px'})


# Function to create the layout for functions associated with a plot
def function_layout(plot_id, function_id):
    """Creates the layout for each function with appropriate margins."""
    function_name = get_function_name(function_id-1)  # Get the reverse alphabetical function name
    return dbc.Row([
        dbc.Col(dbc.Checkbox(id={'type': 'function-checkbox', 'spec': f'plot{plot_id}_func{function_id}'}, value=True), width=1),
        dbc.Col(dcc.Input(id={'type': 'function-name', 'spec': f'plot{plot_id}_func{function_id}'}, value=function_name, style={'width': '40px'}), width=1),
        dbc.Col(html.Label("=", style={'margin-left': '10px'}), width=1),
        dbc.Col(dcc.Input(id={'type': 'function-expression', 'spec': f'plot{plot_id}_func{function_id}'}, placeholder="Enter function", style={'width': '190px', 'margin-right': '10px'}), width=6),
        dbc.Col(dcc.Input(id={'type': 'function-unit', 'spec': f'plot{plot_id}_func{function_id}'}, value="a.u.", style={'width': '40px'}), width=1),
        dbc.Col(html.Button('var.', id={'type': 'info-btn', 'spec': f'plot{plot_id}_func{function_id}'}, n_clicks=0, style={'background-color': '#006c66', 'color': 'white', 'margin-left': '15px'}), width=1),
        variable_popup(plot_id, function_id)
    ], style={'margin-bottom': '10px'})

def variable_popup(plot_id, function_id):
    """Pop-up for defining variables for each function."""
    return dbc.Modal([
        dbc.ModalHeader(f"Define variables for the function"),
        dbc.ModalBody([
            html.Div(id={'type': 'variables-section', 'spec': f'plot{plot_id}_func{function_id}'}),
            html.Button('Add Variable', id={'type': 'add-variable-btn','spec': f'plot{plot_id}_func{function_id}'}, n_clicks=0, style={'background-color': '#006c66', 'color': 'white', 'margin-top': '10px'}),
        ]),
    ], id={'type': 'modal', 'spec': f'plot{plot_id}_func{function_id}'}, is_open=False)

def variable_layout(tabs, plot_id, function_id, variable_id):
    """Creates the layout for each variable (checkbox, name entry, signal dropdown, unit label) with alphabetic names."""
    variable_name = get_variable_name(variable_id-1)
    return dbc.Row([
        dbc.Col(dcc.Input(id={'type': 'variable-name', 'spec': f'plot{plot_id}_func{function_id}_var{variable_id}'}, value=variable_name, style={'width': '40px'}), width=1),
        dbc.Col(html.Label("=", style={"margin-left": "10px"}), width=1),
        dbc.Col(get_menus_from_tabs(tabs, {'type': 'dropdown-menu', 'spec': f'plot{plot_id}_func{function_id}_var{variable_id}'}), width=7),
        dbc.Col(html.Div(id={'type': 'signal-unit', 'spec': f'plot{plot_id}_func{function_id}_var{variable_id}'}, children="a.u."), width=2)
    ], style={'margin-bottom': '5px'})

def get_variable_name(index):
    """Returns the alphabetic variable name (a, b, c, ..., z, aa, bb, etc.)."""
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    name = ""
    while index >= 0:
        name = alphabet[index % 26] + name
        index = index // 26 - 1
    return name

def get_function_name(index):
    """Returns the reverse alphabetic function name (Z, Y, X, ..., A, ZZ, YY, etc.)."""
    alphabet = "ZYXWVUTSRQPONMLKJIHGFEDCBA"
    name = ""
    while index >= 0:
        name = alphabet[index % 26] + name
        index = index // 26 - 1
    return name