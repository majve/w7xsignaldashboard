import dash
from dash import dcc, html
from dash.dependencies import Input, Output, State, ALL, MATCH

import dash_bootstrap_components as dbc
from dash.exceptions import PreventUpdate

import io
import tempfile
import base64
import os
import shutil
import subprocess
from matplotlib.animation import FFMpegWriter
import matplotlib.pyplot as plt
plt.switch_backend('Agg')  # Use Agg backend for non-GUI environments

import logging
from datetime import date, datetime
import w7xarchive

from Selection import Selection, getXPs
from OutputSettings import OutputSettings
from ICRHsettings import ICRHsettings
import Correlations as Correlations
import Analysis as Analysis
import Output as MyOutput
import Presets as Preset
import LogbookObjects as LogbookObjects
import LogbookAPI as LogbookAPI
import archive_signal_dict
import derived_signal_dict
import re

# Set up logging
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)

# Initialize the Dash app
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP], suppress_callback_exceptions=True)

tabs = Preset.tabs

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    dcc.Store(id='card-content-store', data=0),  # 0 for Presets, 1 for Correlation, 2 for Analysis
    dcc.Store(id='program-list-store'),
    dcc.Store(id='figure-plotted-store', data=False),
    dcc.Store(id='selection-store', data=None),
    dcc.Store(id='icrh-settings-store', data=None),
    dcc.Store(id='output-settings-store', data=None),
    dcc.Download(id="download-plot"),
    dbc.Row([
        # Settings Column
        dbc.Col([
            dbc.Card([
                dbc.CardBody([
                    html.H4("Settings", style={'text-align': 'center'}),
                    # dbc.Col(html.H4(id="card-title", style={'text-align': 'center'}), width=10),
                    html.Div([
                        html.H5("Selection"),
                        # html.Label("Specify time range"),
                        html.Div([
                            html.Label("From", style={'marginBottom': '10px', 'marginRight': '3px'}),
                            dcc.Input(id='from-date', type='text', value=date.today().strftime('%Y-%m-%d'),
                                      style={'width': '36%', 'marginLeft': '10px', 'marginRight': '10px'}),
                            dcc.Input(id='from-time', type='text', value='00:00:00',
                                      style={'width': '36%', 'marginRight': '10px'}),
                            html.Button(
                                children=[
                                    html.Img(src='/assets/calendar.png', style={'height': '20px'})
                                ],
                                id='from-cal-btn',
                                n_clicks=0,
                                style={'align-items': 'center', 'background-color': '#006c66'}
                            ),
                        ]),
                        html.Div([
                            html.Label("Up to"),
                            dcc.Input(id='upto-date', type='text', value=date.today().strftime('%Y-%m-%d'),
                                      style={'width': '36%', 'marginLeft': '10px', 'marginRight': '10px'}),
                            dcc.Input(id='upto-time', type='text', value='23:59:59',
                                      style={'width': '36%', 'marginRight': '10px'}),
                            html.Button(
                                children=[
                                    html.Img(src='/assets/calendar.png', style={'height': '20px'})
                                ],
                                id='upto-cal-btn',
                                n_clicks=0,
                                style={'align-items': 'center', 'background-color': '#006c66'}
                            ),
                        ]),
                        dcc.Dropdown(id='program-list', options=[], multi=True,
                                     placeholder='Select XP(s) in time range',
                                     style={'width': '375px', 'marginTop': '10px'}),
                        dcc.Interval(
                            id='interval-component',
                            interval=1000,  # Check every 1 second
                            n_intervals=0
                        ),
                        dcc.Input(id='manual-xp-input', type='text', placeholder='Or enter XP(s) manually as csv, range via -', style={'width': '375px', 'marginTop': '25px'}),
                        html.Button('Last Shot', id='last-shot-btn', n_clicks=0, style={'marginTop': '10px', 'background-color': '#006c66', 'color': 'white'})

                    ]),
                    html.Hr(),
                    html.Div([
                        html.H5("Output Settings"),
                        html.Div([
                            html.Label("Set time-type", style={'marginTop': '10px'}),
                            dcc.RadioItems(
                                id='time-type',
                                options=[
                                    {'label': ' Absolute', 'value': 1},
                                    {'label': ' Relative to t1, in time window', 'value': 2}
                                ],
                                value=2,
                                labelStyle={'display': 'block', 'margin-left': '10px'}
                            ),
                            html.Label("Set time-range", style={'marginTop': '10px'}),
                            dcc.RadioItems(
                                id='time-range',
                                options=[
                                    {'label': ' Automatic', 'value': 1},
                                    {'label': ' Fixed times', 'value': 2},
                                    {'label': ' Fixed triggers', 'value': 3}
                                ],
                                value=2,
                                labelStyle={'display': 'block', 'margin-left': '10px'}
                            ),
                            html.Div([
                                html.Div([
                                    html.Label("From time", style={'display': 'inline-block', 'margin-left': '50px'}),
                                    dcc.Input(id='timewindow-fix-from', type='number', value=0.0,
                                              style={'width': '60px', 'marginLeft': '10px'}),
                                    html.Label("s upto", style={'margin-left': '10px'}),
                                    dcc.Input(id='timewindow-fix-to', type='number', value=20.0,
                                              style={'width': '60px', 'marginLeft': '10px'}),
                                    html.Label("s", style={'margin-left': '10px'})
                                ], style={'display': 'flex', 'alignItems': 'center', 'marginBottom': '10px'}),
                            ], id='fixed-time-inputs', style={'display': 'none'}),

                            html.Div([
                                html.Div([
                                    html.Label("From trigger",
                                               style={'display': 'inline-block', 'margin-left': '50px'}),
                                    dcc.Input(id='timewindow-fix-from-trig', type='number', min=0, max=5, value=1,
                                              style={'width': '60px', 'marginLeft': '10px'}),
                                    html.Label("upto trigger", style={'margin-left': '10px'}),
                                    dcc.Input(id='timewindow-fix-to-trig', type='number', min=0, max=5, value=5,
                                              style={'width': '60px', 'marginLeft': '10px'}),
                                ], style={'display': 'flex', 'alignItems': 'center', 'marginBottom': '10px'}),
                            ], id='fixed-trigger-inputs', style={'display': 'none'}),
                        ], style={'margin-bottom': '20px'}),
                        html.Div([
                            html.Label("Scale y axis"),
                            dcc.RadioItems(
                                id='scale-type',
                                options=[
                                    {'label': ' Linear', 'value': 1},
                                    {'label': ' Logarithmic', 'value': 2}
                                ],
                                value=1,
                                labelStyle={'display': 'block', 'margin-left': '10px'}
                            ),
                        ], style={'margin-bottom': '20px'}),
                        html.Div([
                            html.Label("Resolution"),
                            html.Div([
                                html.Label("Plot", style={'margin-left': '10px'}),
                                dcc.Input(id='resolution', type='number', value='100', style={'margin-left': '10px', 'margin-right': '10px', 'width': '20%'}),
                                html.Label(" intervals [0 for full resolution]")
                            ]),
                        ], style={'margin-bottom': '20px'}),
                        html.Div([
                            html.Label('In case of multiple XPs, align signals based on'),
                            dcc.RadioItems(
                                options=[
                                    {'label': ' The time window', 'value': 1},
                                    {'label': ' The signal start', 'value': 2},
                                    {'label': ' The signal maximum', 'value': 3},
                                ],
                                value=1,
                                id='align_XPs',
                                labelStyle={'display': 'block', 'margin-left': '10px'}
                            ),
                            html.Div([
                                html.Div([
                                    html.Label("where signal goes over", style={'display': 'inline-block', 'margin-left': '50px'}),
                                    dcc.Input(id='threshold_input', type='number', value=0.001,
                                              style={'width': '80px', 'marginLeft': '10px'}),
                                ], style={'display': 'flex', 'alignItems': 'center', 'marginBottom': '10px'}),
                            ], id='threshold_input_div', style={'display': 'none'})
                        ], id='align_XPs_div', style={'display': 'none'})
                    ]),
                    # Cache Management
                    html.Hr(),
                    html.Div([
                        html.H5("Cache Management"),
                        html.Div(id='cache-info', children=[
                            html.P([
                                "The cache currently contains: ",
                                html.Span(id='cache-size')
                            ])
                        ]),
                        html.Button('Clear Cache', id='clear-cache-btn', n_clicks=0, style={'background-color': '#006c66', 'color': 'white'})
                    ], style={'marginTop': '10px'}),
                ])
            ], style={'marginTop': '10px', 'marginLeft': '10px'})
        ], width=3),

        # Presets Column
        dbc.Col([
            dbc.Card([
                dbc.CardBody([
                    # html.H4("Presets"),
                    # html.Div([
                    #     dcc.Tabs(id="presets-tabs", value='ICRH', children=[
                    #         dcc.Tab(label=tab_name, value=tab_name, style={'background-color': 'white', 'font-size': '20px'}, selected_style={'font-weight': 'bold', 'font-size': '20px', 'background-color': 'white', 'border': '2px solid #006c66'}, className='tab')
                    #         for tab_name in tabs.keys()
                    #     ])
                    # ], className='tab-container'),
                    # html.Div(id='tabs-content')
                    dbc.Row([
                        dbc.Col(html.Button('<', id='left-arrow', n_clicks=0, style={'background-color': '#006c66', 'color': 'white'}), width=1),
                        dbc.Col(html.H4(id="card-title", style={'text-align': 'center'}), width=10),
                        dbc.Col(html.Button('>', id='right-arrow', n_clicks=0, style={'background-color': '#006c66', 'color': 'white'}), width=1)
                    ]),
                    html.Div(id='card-content'),
                ])
            ], style={'marginTop': '10px'})
        ], width=3),

        # Output Column
        dbc.Col([
            dbc.Card([
                dbc.CardBody([
                    html.H4("Output", style={'text-align': 'center'}),
                    # html.Div(id='output-plot'),
                    html.Div(id='output-plot', children=[
                        dcc.Loading(
                            id="loading-output",
                            type="circle",  # or "default", "cube", "dot", etc.
                            children=[
                                dcc.Store(id='fig-cache-store'),  # Wrap fig-cache-store with dcc.Loading
                                html.Div(id="plot-container", children=[
                                    html.P("Instructions:"),
                                    html.P([
                                        "In Selection, enter dates and select one or multiple experimental programs (XPs)",
                                        html.Br(),
                                        "OR enter XP(s) in the form XP_yyyymmdd.xx.",
                                        html.Br(),
                                        "ICRH experiments were performed on 20230223, 20230328 and 20230330.",
                                        html.Br(),
                                        "A good example shot is XP_20230330.64",
                                        html.Br(),
                                        "Optionally, adjust Output Settings.",
                                        html.Br(),
                                        "Select presets and press a plot-button. The plot will appear here.",
                                        html.Br(),
                                        "OR, with the button next to presets, navigate to correlations or analysis"

                                    ]),
                                ])
                            ]
                        ),
                    ]),
                    html.Hr(),
                    html.Div(id='buttons-container', children=[
                        html.Button(
                            children=[
                                html.Img(src='/assets/download-png.png', style={'height': '50px'})
                            ],
                            id='save-png-btn',
                            n_clicks=0,
                            style={'align-items': 'center', 'margin': '10px', 'background-color': '#006c66'},
                            title='Download figure as png'
                        ),
                        html.Button(
                            children=[
                                html.Img(src='/assets/download-hdf.png', style={'height': '50px'})
                            ],
                            id='store-timetrace-btn',
                            n_clicks=0,
                            style={'align-items': 'center', 'margin': '10px', 'background-color': '#006c66'},
                            title='Download data as hierarchical data format'

                        ),
                        html.Button(
                            children=[
                                html.Img(src='/assets/upload-logbook.png', style={'height': '50px'})
                            ],
                            id='upload-logbook-btn',
                            n_clicks=0,
                            style={'align-items': 'center', 'margin': '10px', 'background-color': '#006c66'},
                            title='Upload figure to W7-X logbook'
                        ),
                    ], style={'display': 'none'}),
                    html.Button(
                        children=[
                            html.Img(src='/assets/download-mp4.png', style={'height': '50px'})
                        ],
                        id='save-mp4-btn',
                        n_clicks=0,
                        style={'align-items': 'center', 'margin': '10px', 'background-color': '#006c66', 'display': 'none'},
                        title='Download video as MP4'
                    ),
                    # Modal for user input
                    dbc.Modal([
                        dbc.ModalHeader("Component Comment"),
                        dbc.ModalBody([
                            dbc.Row([
                                dbc.Col(dbc.Label("Username:"), width=4),
                                dbc.Col(dbc.Input(id='username-input', type='text', value='', autoFocus=True), width=8),
                            ], style={'margin-bottom': '15px'}),
                            dbc.Row([
                                dbc.Col(dbc.Label("Password:"), width=4),
                                dbc.Col(dbc.Input(id='password-input', type='password', value=''), width=6),
                                dbc.Col(dbc.Button(
                                    children=[html.Img(src='/assets/show.png', style={'height': '20px'})], id='toggle-password-btn', n_clicks=0,
                                    style={'align-items': 'center', 'background-color': '#c3d3d2', 'border': '1px solid lightgrey', 'margin-left': '4px'}), width=2),
                            ], style={'margin-bottom': '15px'}),
                            dbc.Row([
                                dbc.Col(dbc.Label("Comment (optional):"), width=4),
                                dbc.Col(dbc.Input(id='comment-input', type='text'), width=8),
                            ], style={'margin-bottom': '0px'}),
                            dbc.Row([
                                dbc.Col(dbc.Label("Comp ID:"), width=4),
                                dbc.Col(dbc.Input(id='compid-input', type='text'), width=8),
                            ], style={'margin-bottom': '15px'}),
                        ]),
                        dbc.ModalFooter([
                            dbc.Button("Upload", id='submit-comment-btn', className="ml-auto",
                                       style={'background-color': '#006c66', 'border': '1px solid lightgrey'})
                        ]),
                    ], id="comment-modal", is_open=False)
                ])
            ], style={'marginTop': '10px', 'marginRight': '10px'})
        ], width=6)
    ]),

    # Modal for calendar
    dbc.Modal([
        dbc.ModalHeader("Select From Date"),
        dbc.ModalBody(dcc.DatePickerSingle(
            id='date-picker',
            min_date_allowed=date(1995, 8, 5),
            max_date_allowed=date(2100, 9, 19),
            initial_visible_month=date.today(),
            date=date.today(),
            display_format='DD/MM/YYYY'
        )),
        dbc.ModalFooter(
            dbc.Button("Set Date", id="set-date-btn", className="ml-auto")
        ),
    ], id="modal", is_open=False),

    # Modal for upto calendar
    dbc.Modal([
        dbc.ModalHeader("Select Upto Date"),
        dbc.ModalBody(dcc.DatePickerSingle(
            id='upto-date-picker',
            min_date_allowed=date(1995, 8, 5),
            max_date_allowed=date(2100, 9, 19),
            initial_visible_month=date.today(),
            date=date.today(),
            display_format='DD/MM/YYYY'
        )),
        dbc.ModalFooter(
            dbc.Button("Set Date", id="set-upto-date-btn", className="ml-auto")
        ),
    ], id="upto-modal", is_open=False)
])


# Callback to open the modals
@app.callback(
    [Output("modal", "is_open"),
     Output("upto-modal", "is_open")],
    [Input("from-cal-btn", "n_clicks"),
     Input("set-date-btn", "n_clicks"),
     Input("upto-cal-btn", "n_clicks"),
     Input("set-upto-date-btn", "n_clicks")],
    [State("modal", "is_open"),
     State("upto-modal", "is_open")]
)
def toggle_modal_calendar(n1, n2, n3, n4, is_open1, is_open2):
    ctx = dash.callback_context

    if not ctx.triggered:
        return is_open1, is_open2
    else:
        button_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if button_id == "from-cal-btn" or button_id == "set-date-btn":
        return not is_open1, is_open2
    elif button_id == "upto-cal-btn" or button_id == "set-upto-date-btn":
        return is_open1, not is_open2

    return is_open1, is_open2

# Callback to update the from-date input
@app.callback(
    Output('from-date', 'value'),
    Input('set-date-btn', 'n_clicks'),
    State('date-picker', 'date')
)
def set_from_date(n_clicks, selected_date):
    if n_clicks and selected_date:
        date_obj = datetime.strptime(selected_date, '%Y-%m-%d')
        formatted_date_store = date_obj.strftime('%Y-%m-%d')
        return formatted_date_store
    return dash.no_update

# Callback to update the upto-date input
@app.callback(
    Output('upto-date', 'value'),
    Input('set-upto-date-btn', 'n_clicks'),
    State('upto-date-picker', 'date')
)
def set_upto_date(n_clicks, selected_date):
    if n_clicks and selected_date:
        date_obj = datetime.strptime(selected_date, '%Y-%m-%d')
        formatted_date_store = date_obj.strftime('%Y-%m-%d')
        return formatted_date_store
    return dash.no_update

# Callback to update the program list
@app.callback(
    Output('program-list', 'options'),
    [Input('interval-component', 'n_intervals')],
    [State('from-date', 'value'),
     State('from-time', 'value'),
     State('upto-date', 'value'),
     State('upto-time', 'value'),
     State('selection-store', 'data'),
     State('program-list', 'options'),],
    prevent_initial_call=True
)
def update_program_list(n_intervals, from_date, from_time, upto_date, upto_time, selection_data, current_options):
    if selection_data is None:
        return dash.no_update

    try:
        if from_date and from_time and upto_date and upto_time:
            datetime.strptime(from_date, '%Y-%m-%d')
            datetime.strptime(from_time, '%H:%M:%S')
            datetime.strptime(upto_date, '%Y-%m-%d')
            datetime.strptime(upto_time, '%H:%M:%S')

            my_selection = Selection.deserialize(selection_data)
            programs = my_selection.getPrograms()
            programs = programs[::-1]
            current_programs = [program['label'] for program in current_options]
            if programs == current_programs:
                return dash.no_update
            else:
                return [{'label': program, 'value': program} for program in programs]
    except ValueError as e:
        return [{'label': 'Check dates and times', 'value': 'error'}]

    return dash.no_update


@app.callback(
    Output('manual-xp-input', 'value', allow_duplicate=True),
    Input('last-shot-btn', 'n_clicks'),
    prevent_initial_call=True
)
def set_last_shot(n_clicks):
    if n_clicks > 0:
        prog = "None"
        try:
            lastt0 = w7xarchive.get_last_trigger_timestamp()
            json = LogbookAPI.search_programlogs("", int(lastt0-10e9), int(lastt0+10e9))
            XPs = json['hits']['hits']
        except Exception as e:
            log.error('{}: exception getting program list from {} upto {}'.format(e, int(lastt0-10e9), int(lastt0+10e9)))
        else:
            if XPs:
                prog = XPs[-1]["_source"]["id"]
        return prog
    return dash.no_update

@app.callback(
    Output('align_XPs_div', 'style'),
    [Input('program-list-store', 'data'),
     Input('card-title', 'children')],
    prevent_initial_call=True
)
def display_align_XPs_div(program_list, card_title):
    if program_list is not None and len(program_list) > 1 and card_title == "Presets":
        return {'display': 'block'}
    else:
        return {'display': 'none'}


@app.callback(
    Output('threshold_input_div', 'style'),
    [Input('align_XPs', 'value')]
)
def display_threshold_input(selected_value):
    if selected_value == 2:
        return {'display': 'block'}
    else:
        return {'display': 'none'}

@app.callback(
    [Output('fixed-time-inputs', 'style'),
     Output('fixed-trigger-inputs', 'style')],
     Input('time-range', 'value')
)
def toggle_relative_options(option):
    if option == 2:  # fixed times
        fixed_time_style = {'display': 'flex'}
        fixed_trigger_style = {'display': 'none'}
    elif option == 3:  # fixed triggers
        fixed_time_style = {'display': 'none'}
        fixed_trigger_style = {'display': 'flex'}
    else:
        fixed_time_style = {'display': 'none'}
        fixed_trigger_style = {'display': 'none'}

    return fixed_time_style, fixed_trigger_style

# Callback to update the card content based on arrow clicks
@app.callback(
    [Output('card-title', 'children'),
     Output('card-content', 'children'),
     Output('card-content-store', 'data')],
    [Input('left-arrow', 'n_clicks'),
     Input('right-arrow', 'n_clicks')],
    [State('card-content-store', 'data')]
)
def update_card_content(left_clicks, right_clicks, current_card):
    # Calculate the new card index
    new_card = current_card
    if left_clicks > 0:
        new_card = (current_card - 1) % 3
    elif right_clicks > 0:
        new_card = (current_card + 1) % 3

    # Determine the title and content based on the new card index
    if new_card == 0:
        title = "Presets"
        content = html.Div([
            # Presets content here...
            dcc.Tabs(id="presets-tabs", value='ICRH', children=[
                dcc.Tab(label=tab_name, value=tab_name, style={'background-color': 'white', 'font-size': '20px'},
                        selected_style={'font-weight': 'bold', 'font-size': '20px', 'background-color': 'white',
                                        'border': '2px solid #006c66'}, className='tab')
                for tab_name in tabs.keys()
            ]),
            html.Div(id='tabs-content')
        ])
    elif new_card == 1:
        title = "Correlation"
        content = Correlations.correlation_layout(tabs)

    elif new_card == 2:
        title = "Analysis"
        content = Analysis.analysis_layout()

    return title, content, new_card

@app.callback(
    Output('tabs-content', 'children'),
    Input('presets-tabs', 'value')
)
def render_content(tab):
    if tab in tabs:
        return render_presets_content(tabs[tab])
    return html.Div()

def create_inner_content_callback(tab_name):
    @app.callback(
        Output(f'{tab_name}-inner-tabs-content', 'children'),
        Input(f'{tab_name}-inner-tabs', 'value')
    )
    def render_inner_content(tab_value):
        tab = tabs[tab_name]
        group_name = tab_value.split('-')[1]
        group = next((grp for grp in tab.groups if grp.name.replace(' ', '-') == group_name), None)
        if group:
            return dbc.Row([
                dbc.Col([render_system_tab(tab, group, system)], width=6) for system in group.systems
            ])
        return html.Div()

    return render_inner_content


@app.callback(
    Output({'type': 'system-tab-content', 'index': ALL}, 'children'),
    Input({'type': 'system-tab', 'index': ALL}, 'value')
)
def render_system_content(system):
    tab_name, group_name, system_name = system[0].split('-')
    group = next((group for group in tabs[tab_name].groups if group.name == group_name), None)
    if group:
        system_obj = next((sys for sys in group.systems if sys.name == system_name), None)
        if system_obj:
            return [render_system_tab(system_obj)]
    return html.Div()

def render_presets_content(tab):
    return html.Div([
        dcc.Tabs(
            id=f"{tab.name}-inner-tabs",
            value=f"{tab.name}-{tab.groups[0].name}",
            children=[
                dcc.Tab(
                    label=group.name,
                    value=f"{tab.name}-{group.name}",
                    style={'background-color': 'white', 'marginTop': '5px'},
                    selected_style={'background-color': 'white', 'font-weight': 'bold', 'marginTop': '5px'}
                ) for group in tab.groups
            ],
            className='inner-tab'
        ),
        html.Div(id=f'{tab.name}-inner-tabs-content', style={'marginTop': '20px'})
    ])

def render_inner_content(tab, group_name):
    group = next((group for group in tab.groups if group.name == group_name), None)
    if group:
        return html.Div([
            dcc.Tabs(
                id=f"{tab.name}-{group.name}-tabs",
                value=f"{tab.name}-{group.name}-{group.systems[0].name}",
                children=[
                    dcc.Tab(
                        label=system.name,
                        value=f"{tab.name}-{group.name}-{system.name}",
                        style={'background-color': 'white', 'marginTop': '10px'},
                        selected_style={'background-color': 'white', 'font-weight': 'bold', 'marginTop': '10px'}
                    ) for system in group.systems
                ]
            ),
            html.Div(id=f'{tab.name}-{group.name}-tabs-content', style={'marginTop': '20px'})
        ])
    return html.Div()

def render_system_tab(tab, group, system):
    button_id = f'plot-{tab.name.lower().replace(" ", "-")}-{group.name.lower().replace(" ", "-")}-{system.name.lower().replace(" ", "-")}'

    return html.Div([
        html.H5(system.name, style={'display': 'inline-block'}),
        html.Button('Plot', id=f'plot-{tab.name.lower().replace(" ", "-")}-{group.name.lower().replace(" ", "-")}-{system.name.lower().replace(" ", "-")}', n_clicks=0, style={'color': 'white', 'marginLeft': '10px', 'display': 'inline-block', 'background-color': '#006c66'}),
        html.Div([
            html.Div([
                html.H6(subsystem.name),
                dbc.Checklist(
                    options=[{'label': signal, 'value': signal} for signal in subsystem.signals],
                    value=[],
                    id=f'{tab.name.lower()}-{group.name.lower().replace(" ", "-")}-{system.name.lower().replace(" ", "-")}-{subsystem.name.lower().replace(" ", "-")}'
                )
            ], style={'marginBottom': '20px'}) for subsystem in system.subsystems
        ])
    ])


@app.callback(
    [Output('selection-store', 'data', allow_duplicate=True),
     Output('output-settings-store', 'data', allow_duplicate=True),
     Output('icrh-settings-store', 'data', allow_duplicate=True)],
    Input('url', 'pathname'),
    State('from-date', 'value'),
    State('from-time', 'value'),
    State('upto-date', 'value'),
    State('upto-time', 'value'),
    prevent_initial_call=True
)
def initialize_selection_and_outputsettings(pathname, from_date, from_time, upto_date, upto_time):
    selection = Selection()
    selection.from_date = from_date
    selection.from_time = from_time
    selection.upto_date = upto_date
    selection.upto_time = upto_time
    serialized_selection = selection.serialize()

    output_settings = OutputSettings(selection)
    serialized_output_settings = output_settings.serialize()
    icrh_settings = ICRHsettings()
    serialized_icrh_settings = icrh_settings.serialize()

    return serialized_selection, serialized_output_settings, serialized_icrh_settings

# Serialization Breaks References: Outputsettings holds a reference to Selection, which you lose when Selection is serialized and deserialized.
# Each deserialization creates a new object. Thus, we need to keep both Selection and OutputSettings in sync
@app.callback(
    Output('output-settings-store', 'data'),
    Input('selection-store', 'data'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def sync_output_settings_with_selection(selection_data, output_settings_data):
    if selection_data is None or output_settings_data is None:
        return dash.no_update

    my_selection = Selection.deserialize(selection_data)
    output_settings = OutputSettings.deserialize(output_settings_data)
    # Update the selection reference inside output settings
    output_settings.my_selection = my_selection

    return output_settings.serialize()

@app.callback(
    Output('selection-store', 'data', allow_duplicate=True),
    Input('from-date', 'value'),
    State('selection-store', 'data'),
    prevent_initial_call=True
)
def store_from_date(value, selection_data):
    my_selection = Selection.deserialize(selection_data)
    my_selection.from_date = value
    return my_selection.serialize()

@app.callback(
    Output('selection-store', 'data', allow_duplicate=True),
    Input('from-time', 'value'),
    State('selection-store', 'data'),
    prevent_initial_call=True
)
def store_from_time(value, selection_data):
    my_selection = Selection.deserialize(selection_data)
    my_selection.from_time = value
    return my_selection.serialize()

@app.callback(
    Output('selection-store', 'data', allow_duplicate=True),
    Input('upto-date', 'value'),
    State('selection-store', 'data'),
    prevent_initial_call=True
)
def store_upto_date(value, selection_data):
    my_selection = Selection.deserialize(selection_data)
    my_selection.upto_date = value
    return my_selection.serialize()

@app.callback(
    Output('selection-store', 'data', allow_duplicate=True),
    Input('upto-time', 'value'),
    State('selection-store', 'data'),
    prevent_initial_call = True
)
def store_upto_time(value, selection_data):
    my_selection = Selection.deserialize(selection_data)
    my_selection.upto_time = value
    return my_selection.serialize()

@app.callback(
    [Output('program-list-store', 'data', allow_duplicate=True),
    Output('manual-xp-input', 'value'),
    Output('selection-store', 'data', allow_duplicate=True)],
    Input('program-list', 'value'),
    State('selection-store', 'data'),
    prevent_initial_call=True
)
def store_program_list(value, selection_data):
    if not value:
        raise PreventUpdate

    # Store first (or only) XP in Selection
    my_selection = Selection.deserialize(selection_data)
    my_selection.updateCurrentProgram("XP", value[0])
    # Store whole XP list in program-list-store
    return value, '', my_selection.serialize()

@app.callback(
    [Output('program-list-store', 'data', allow_duplicate=True),
    Output('program-list', 'value', allow_duplicate=True),
    Output('selection-store', 'data', allow_duplicate=True)],
    Input('manual-xp-input', 'value'),
    State('selection-store', 'data'),
    prevent_initial_call=True
)
def store_program_list_manually(XPstring, selection_data):

    if not XPstring:
        raise PreventUpdate
    # Get the selected signals and the XPs for which they have to be plotted
    XPs = getXPs(XPstring)
    if len(XPs) == 0:
        # log.error('No suitable XPs were found.')
        raise PreventUpdate

    # Store first (or only) XP in Selection
    my_selection = Selection.deserialize(selection_data)
    my_selection.updateCurrentProgram("XP", XPs[0])
    # Store whole XP list in program-list-store
    return XPs, [], my_selection.serialize()


@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('time-type', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def store_time_type(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.timetype = value
    return output_settings.serialize()


@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('time-range', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def store_relative_options(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.timewindow = value
    return output_settings.serialize()

@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('timewindow-fix-from', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def store_timewindow_fix_from(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.tWindowFrom = value
    return output_settings.serialize()

@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('timewindow-fix-to', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def store_timewindow_fix_to(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.tWindowTo = value
    return output_settings.serialize()


@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('timewindow-fix-from-trig', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def store_timewindow_fix_from_trig(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.tWindowFromTrig = value
    return output_settings.serialize()

@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('timewindow-fix-to-trig', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def store_timewindow_fix_to_trig(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.tWindowToTrig = value
    return output_settings.serialize()

@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('resolution', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def update_resolution(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.resolution = value
    return output_settings.serialize()

@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('align_XPs', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def update_alignXPS(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.align = value
    return output_settings.serialize()

@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('threshold_input', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def update_alignthreshold(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.alignStart = value
    return output_settings.serialize()

@app.callback(
    Output('output-settings-store', 'data', allow_duplicate=True),
    Input('scale-type', 'value'),
    State('output-settings-store', 'data'),
    prevent_initial_call=True
)
def update_scale(value, output_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.Scale = value
    return output_settings.serialize()


@app.callback(
    Output('buttons-container', 'style'),
    [Input('figure-plotted-store', 'data')]
)
def toggle_buttons(figure_plotted):
    if figure_plotted and figure_plotted.get("plotted"):
        if figure_plotted.get("kind") == "figure":
            return {'display': 'block'}
    return {'display': 'none'}


@app.callback(
    Output('save-mp4-btn', 'style'),
    [Input('figure-plotted-store', 'data')]
)
def toggle_videobutton(figure_plotted):
    if figure_plotted and figure_plotted.get("plotted"):
        if figure_plotted.get("kind") == "video":
            return {'align-items': 'center', 'margin': '10px', 'background-color': '#006c66', 'display': 'block'}
    return {'align-items': 'center', 'margin': '10px', 'background-color': '#006c66', 'display': 'none'}


@app.callback(
    Output('download-plot', 'data', allow_duplicate=True),
    Input('save-png-btn', 'n_clicks'),
    State('fig-cache-store', 'data'),
    prevent_initial_call=True
)
def download_plot(n_clicks, fig_cache):
    if n_clicks is None:
        raise PreventUpdate

    download_data = convertfig(fig_cache, "png")

    return download_data

@app.callback(
    Output('download-plot', 'data', allow_duplicate=True),
    Input('save-mp4-btn', 'n_clicks'),
    State('fig-cache-store', 'data'),
    prevent_initial_call=True
)
def download_mp4(n_clicks, video_cache):
    if n_clicks is None:
        raise PreventUpdate

    return dict(content=video_cache, filename="animation.mp4", base64=True)

@app.callback(
    Output('plot-container', 'children'),
    Input('fig-cache-store', 'data'),
    State('figure-plotted-store', 'data'),
    prevent_initial_call=True
)
def show_figure(fig_cache, figure_plotted):
    if figure_plotted:
        if figure_plotted.get("kind") == "figure":
            htmloutput = convertfig(fig_cache, "html")
        if figure_plotted.get("kind") == "video":
            htmloutput = convertvideo(fig_cache, "html")
        if figure_plotted.get("kind") == "htmlstring":
            htmloutput = fig_cache

        return htmloutput
    return dash.no_update

def convertfig_base64(fig):
    # Save the figure to a BytesIO object
    buf = io.BytesIO()
    fig.savefig(buf, format="png")
    buf.seek(0)
    # Encode the BytesIO object as a base64 string
    img_base64 = base64.b64encode(buf.getvalue()).decode("ascii")
    return img_base64

def convertfig(img_base64, type, filename="figure.png"):

    if type=="html":
        # Return the image
        return html.Img(src=f'data:image/png;base64,{img_base64}')
    if type=="htmlstring":
        # To pass to logbookAPI for upload
        return '<img src="data:image/png;base64,' + img_base64 + '">'
    elif type=="png":
        return dict(content=img_base64, filename=filename, base64=True)
    else:
        log.info(f'conversion type {type} not supported')

def convertvideo_base64(chunk_filenames, fps):
    print('Converting video chunks to base64')

    # Concatenate the video chunks using ffmpeg
    with tempfile.NamedTemporaryFile(suffix='.mp4', delete=False) as temp_video_file:
        concat_file = 'concat_filelist.txt'
        with open(concat_file, 'w') as f:
            for filename in chunk_filenames:
                f.write(f"file '{filename}'\n")

        # Use ffmpeg to concatenate the video chunks
        os.system(f"ffmpeg -f concat -safe 0 -i {concat_file} -c copy -y {temp_video_file.name}")
        # Read the saved video file
        temp_video_file.seek(0)
        video_data = temp_video_file.read()

    # Clean up chunk files and concat file
    for filename in chunk_filenames:
        os.remove(filename)
    os.remove(concat_file)

    # Encode the video data as Base64
    video_base64 = base64.b64encode(video_data).decode('ascii')
    print('Converting video done')
    return video_base64


def convertvideo(video_base64, type):
    if type == "html":
        return html.Video(src=f"data:video/mp4;base64,{video_base64}", controls=True, style={'width': '80%', 'margin': '0', 'padding': '0'})


def generate_plot(systemName, signals, XPs, selection_data, output_settings_data, icrh_settings_data):

    if selection_data is None or output_settings_data is None or icrh_settings_data is None:
        return dash.no_update

    my_selection = Selection.deserialize(selection_data)
    output_settings = OutputSettings.deserialize(output_settings_data)
    output_settings.my_selection = my_selection
    icrh_settings = ICRHsettings.deserialize(icrh_settings_data)

    if any(signals.values()):
        if XPs is None or len(XPs) <= 1:
            try:
                set = output_settings.getOutputSettings()
                specSet = icrh_settings.getICRHsettings(int(set["mindim"]*1e9))
                output = MyOutput.Output(systemName, signals, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], mindim=set["mindim"], maxdim=set["maxdim"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"], specificsettings=specSet)
                result = output.produceOutput()
                # Check if the result is a figure (plt.gcf())
                if isinstance(result, plt.Figure):
                    output_base64 = convertfig_base64(result)
                    kind = "figure"
                # If result is a video, it will return a tuple (ani, fps)
                elif isinstance(result, tuple) and len(result) == 2:
                    chunk_filenames, fps = result
                    output_base64 = convertvideo_base64(chunk_filenames, fps)
                    kind = "video"

                # w7xarchive.clear_cache()
                # Return the figure HTML and additional required settings
                return output_base64, {"XPid": set["XPid"], "mindim": set["mindim"], "maxdim": set["maxdim"], "kind": kind, "plotted": True}, get_cache_info()

            except Exception as e:
                error_message = html.P(f"Error: {str(e)}", style={"color": "red"})
                return error_message, {"kind": "htmlstring", "plotted": False}, get_cache_info()
        else:
            try:
                ###
                """ Generate the output where the signals are plotted for a list of signals
                """
                # Get the timing information for the XPs
                mindims = []
                maxdims = []
                for j, XP in enumerate(XPs):
                    try:
                        my_selection.updateCurrentProgram("XP", XP)
                        output_settings.my_selection = my_selection
                    except Exception as e:
                        log.error('{}: exception setting program: {}'.format(e, XP))
                        continue
                    mindim, maxdim = output_settings.getTimeWindow()  # in s since epoch
                    mindims.append(mindim)
                    maxdims.append(maxdim)
                my_selection.updateCurrentProgram("XP", XPs[0])
                output_settings.my_selection = my_selection

                # The user defined if the signals have to be aligned on the relative time window, on the signal start or on the signal maximum
                if not len(signals) == 0:
                    align = output_settings.getAlignmentXPs()
                    # if aligned on the window, the signals are plotted in one figure on different plots. They share a time axis
                    if align == 'Window':
                        signaldict = {}
                        for key, values in signals.items():
                            for value in values:
                                signaldict[value] = [value]
                        set = output_settings.getOutputSettings()
                        specSet = icrh_settings.getICRHsettings(int(set["mindim"]*1e9))
                        output = MyOutput.Output("XP comparison", signaldict, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                               showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
                        result = output.produceOutputForXPs(XPs, mindims, maxdims, align)
                        output_base64 = convertfig_base64(result)

                        # w7xarchive.clear_cache()
                        # Return the figure HTML and additional required settings
                        return output_base64, {"XPid": set["XPid"], "mindim": set["mindim"], "maxdim": set["maxdim"], "kind": "figure", "plotted": True}, get_cache_info()

                    # If aligned on max or start of signal, the different signals cannot share a time axis. Use separate figures to avoid confusion
                    else:
                        for signal in signals:
                            # signaldict = {signal: [signal]}
                            signaldict = {}
                            for key, values in signals.items():
                                for value in values:
                                    signaldict[value] = [value]
                            set = output_settings.getOutputSettings()
                            specSet = icrh_settings.getICRHsettings(int(set["mindim"]*1e9))
                            thresh = output_settings.getAlignmentStartXPs()  # only necessary for alignment on signal start
                            output = MyOutput.Output("XP comparison", signaldict, XPid=set["XPid"], res=set["res"], timeType=set["timeType"], yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                                   showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"], saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"], uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"], specificsettings=specSet)
                            result = output.produceOutputForXPs(XPs, mindims, maxdims, align, threshold=thresh)
                            output_base64 = convertfig_base64(result)

                            # Return the figure HTML and additional required settings
                            # w7xarchive.clear_cache()
                            return output_base64, {"XPid": set["XPid"], "mindim": set["mindim"], "maxdim": set["maxdim"], "kind": "figure", "plotted": True}, get_cache_info()

            except Exception as e:
                error_message = html.P(f"Error: {str(e)}", style={"color": "red"})

                return error_message, {"kind": "htmlstring", "plotted": False}, get_cache_info()

    return dash.no_update, dash.no_update, get_cache_info()



def create_plot_callback(tab_name, group_name, system_name, subsystems):
    @app.callback(
        [Output('fig-cache-store', 'data', allow_duplicate=True),
         Output('figure-plotted-store', 'data', allow_duplicate=True),
         Output('cache-size', 'children', allow_duplicate=True)],
        [Input(f'plot-{tab_name.lower()}-{group_name.lower().replace(" ", "-")}-{system_name.lower().replace(" ", "-")}', 'n_clicks')],
        [State(f'{tab_name.lower()}-{group_name.lower().replace(" ", "-")}-{system_name.lower().replace(" ", "-")}-{subsystem.name.lower().replace(" ", "-")}', 'value') for subsystem in subsystems] +
        [State('program-list-store', 'data'),
         State('selection-store', 'data'),
         State('output-settings-store', 'data'),
         State('icrh-settings-store', 'data')],
        prevent_initial_call=True
    )
    def plot_callback(n_clicks, *args):
        if n_clicks is None or n_clicks == 0:
            raise PreventUpdate

        # Retrieve the program list and subsystem values
        icrh_settings_data = args[-1]
        output_settings_data = args[-2]
        selection_data = args[-3]
        program_list = args[-4]
        subsystem_values = args[:-4]

        signals = {}
        system_full_name = f'{tab_name} {group_name} {system_name}'
        # Use regex to find and remove consecutive duplicate words (for "ICRH Generator Generator 1")
        system_full_name = re.sub(r'\b(\w+)\s+\1\b', r'\1', system_full_name)

        for subsystem, values in zip(subsystems, subsystem_values):
            if values:
                signals[subsystem.name] = [f'{group_name}/{system_name}/{subsystem.name}/{signal}' for signal in values]
        # Call the function to generate the plot
        output_base64, figure_data, cache_info = generate_plot(system_full_name, signals, program_list, selection_data, output_settings_data, icrh_settings_data)

        # Return the figure and figure data
        return output_base64, figure_data, cache_info

    return plot_callback


def register_callbacks():
    for tab_name, tab in tabs.items():
        create_inner_content_callback(tab_name)
        for group in tab.groups:
            group_name = group.name
            for system in group.systems:
                system_name = system.name
                create_plot_callback(tab_name, group_name, system_name, system.subsystems)


# Call the function to register all callbacks
register_callbacks()


# Callback to open the modal
@app.callback(
    Output("comment-modal", "is_open"),
    [Input("upload-logbook-btn", "n_clicks"),
     Input("submit-comment-btn", "n_clicks")],
    [State("comment-modal", "is_open")],
)
def toggle_modal_logbookupload(open_clicks, submit_clicks, is_open):
    if open_clicks or submit_clicks:
        return not is_open
    return is_open

# Callback to handle the form submission
@app.callback(
    Input('submit-comment-btn', 'n_clicks'),
    State('username-input', 'value'),
    State('password-input', 'value'),
    State('comment-input', 'value'),
    State('compid-input', 'value'),
    State('figure-plotted-store', 'data'),
    State('fig-cache-store', 'data')
)
def handle_submit(n_clicks, username, password, comment, compid, figure_data, fig_cache):
    if n_clicks:
        upload_data = convertfig(fig_cache, "htmlstring")

        content = upload_data + (comment if comment is not None else "")
        XPid = figure_data["XPid"]
        mindim = figure_data["mindim"]
        maxdim = figure_data["maxdim"]
        # Adrian just omits the LOGid in the comment
        mycomment = LogbookObjects.comment(username, content, XPid, int(mindim * 1e9), int(maxdim * 1e9))

        # Adrian checks if comment exists by:
        # - getting all comments from programcomments or programcomponentcomments
        # [I can do this via LogbookAPI.search_componentlog_comments(COMPid, XPid=None)]
        # - checking if self-created string "id:astech_overviewplot_full_" is present
        # Then editprogramcomment or postprogramcomment

        # Upload the comment to the logbook
        LogbookAPI.create_comment(compid, mycomment.comment, user=username, pw=password)

    return dash.no_update

# Callback to toggle password visibility
@app.callback(
    [Output('password-input', 'type'),
     Output('toggle-password-btn', 'children')],
    Input('toggle-password-btn', 'n_clicks'),
    State('password-input', 'type')
)
def toggle_password_visibility(n_clicks, current_type):
    if n_clicks % 2 == 1:
        return 'text', html.Img(src='/assets/hide.png', style={'height': '20px'})
    else:
        return 'password', html.Img(src='/assets/show.png', style={'height': '20px'})

@app.callback(
    Output({'type': 'dropdown-menu', 'spec': MATCH}, 'label'),
    Input({'type': 'dropdown-item', 'spec': MATCH, 'item': ALL}, 'n_clicks'),
    prevent_initial_call=True
)
def update_menu_label(n_clicks):
    ctx = dash.callback_context
    if not ctx.triggered:
        return 'Select Signal'
    else:
        item_id = ctx.triggered_id['item']
        parts = item_id.split('-')
        # Join parts to form the signal name to display in the label
        transformed_id = '/'.join(parts[1:])
        return transformed_id

# Callback to handle the correlation logic
@app.callback(
    [Output('fig-cache-store', 'data', allow_duplicate=True),
    Output('figure-plotted-store', 'data', allow_duplicate=True),
    Output('cache-size', 'children', allow_duplicate=True)],
    Input('correlate-button', 'n_clicks'),
    State({'type': 'dropdown-menu', 'spec': 'corr_x'}, 'label'),
    State({'type': 'dropdown-menu', 'spec': 'corr_y'}, 'label'),
    State('data-var', 'value'),
    State('coeff-var', 'value'),
    State('fit-var', 'value'),
    State('degree', 'value'),
    State('program-list-store', 'data'),
    State('selection-store', 'data'),
    State('output-settings-store', 'data'),
    State('icrh-settings-store', 'data'),
    prevent_initial_call=True
)
def correlateXPs(n_clicks, signal_x, signal_y, data_var, coeff_var, fit_var, degree, XPs, selection_data, output_settings_data, icrh_settings_data):
    output_settings = OutputSettings.deserialize(output_settings_data)
    my_selection = Selection.deserialize(selection_data)
    icrh_settings = ICRHsettings.deserialize(icrh_settings_data)

    if n_clicks == 0:
        return dash.no_update

    signals = {"x": [signal_x], "y": [signal_y]}
    if not signal_x or not signal_y:
        return "Please select both x and y signals.", {"kind": "htmlstring", "plotted": False}, get_cache_info()

    if len(XPs) == 0:
        return "No suitable XPs were found.", {"kind": "htmlstring", "plotted": False}, get_cache_info()

    # Set the timing information for the current XP
    mindims, maxdims = [], []
    for XP in XPs:
        try:
            my_selection.updateCurrentProgram("XP", XP)
            output_settings.my_selection = my_selection
        except Exception as e:
            log.error(f"exception {e}")
        mindim, maxdim = output_settings.getTimeWindow()
        mindims.append(mindim)
        maxdims.append(maxdim)
    my_selection.updateCurrentProgram("XP", XPs[0])
    output_settings.my_selection = my_selection

    # Get data setting and coefficient setting
    datasettings = {1: "all", 2: "xmaximum", 3: "ymaximum", 4: "xymaxima"}
    datasetting = datasettings[data_var]
    # Get coefficient settings
    coeffsetting = []
    if coeffsetting:
        if 'pearson' in coeff_var:
            coeffsetting.append("pearson")
        if 'spearman' in coeff_var:
            coeffsetting.append("spearman")
        if 'covariance' in coeff_var:
            coeffsetting.append("covariance")
    # Get fit settings
    fitsettings = {0: None, 1: ["lin"], 2: ["log"], 3: ["exp"], 4: ["poly", degree]}
    fitsetting = fitsettings[fit_var]
    # Perform correlation
    set = output_settings.getOutputSettings()
    specSet = icrh_settings.getICRHsettings(int(set["mindim"]*1e9))
    output = MyOutput.Output("Correlation", signals, XPid=set["XPid"], res=set["res"], timeType=set["timeType"],
                                 yScale=set["yScale"], cstRegions=set["cstRegions"], integral=set["integral"],
                                 showOutput=set["showOutput"], savePNG=set["savePNG"], savePDF=set["savePDF"],
                                 saveMP4=set["saveMP4"], uploadComponentlog=set["uploadComponentlog"],
                                 uploadCompeventlog=set["uploadcompeventlog"], saveData=set["saveData"],
                                 specificsettings=specSet)
    result = output.produceCorrelationOutput(XPs, mindims, maxdims, datasetting, coeffsetting, fitsetting)
    output_base64 = convertfig_base64(result)

    # w7xarchive.clear_cache()
    # Return the figure HTML and additional required settings
    return output_base64, {"XPid": set["XPid"], "mindim": set["mindim"], "maxdim": set["maxdim"], "kind": "figure", "plotted": True}, get_cache_info()

def get_cache_info():
    cache_path = w7xarchive.get_cache_path()
    try:
        cache_size = subprocess.check_output(['du', '-sh', cache_path]).split()[0].decode('utf-8')
        return f"{cache_size}"
    except Exception as e:
        return "(cache not found)"

@app.callback(
    Output('cache-size', 'children', allow_duplicate=True),
    Input('clear-cache-btn', 'n_clicks'),
    prevent_initial_call=True
)
def clear_cache(n_clicks):
    if n_clicks > 0:
        w7xarchive.clear_cache()
    return get_cache_info()

@app.callback(
    Output('cache-size', 'children', allow_duplicate=True),
    [Input('url', 'pathname')],
    prevent_initial_call=True
)
def display_initial_cache(pathname):
    return get_cache_info()

@app.callback(
    Output('plots-section', 'children'),
    Input('add-plot-btn', 'n_clicks'),
    State('plots-section', 'children'),
    prevent_initial_call=True
)
def add_plot(n_clicks, existing_plots):
    new_plot = Analysis.plot_layout(n_clicks)  # Create a new plot
    existing_plots.append(new_plot)
    return existing_plots

@app.callback(
    Output({'type': 'signals-section', 'spec': MATCH}, 'children'),
    Input({'type': 'add-signal-btn', 'spec': MATCH}, 'n_clicks'),
    State({'type': 'signals-section', 'spec': MATCH}, 'children'),
    prevent_initial_call=True
)
def add_signal(n_clicks, existing_signals):
    ctx = dash.callback_context
    plot_id = ctx.triggered_id['spec'][4:]  # everything after plot
    new_signal = Analysis.signal_layout(tabs, plot_id, n_clicks)
    existing_signals.append(new_signal)
    return existing_signals

@app.callback(
    Output({'type': 'functions-section', 'spec': MATCH}, 'children'),
    Input({'type': 'add-function-btn', 'spec': MATCH}, 'n_clicks'),
    State({'type': 'functions-section', 'spec': MATCH}, 'children'),
    prevent_initial_call=True
)
def add_function(n_clicks, existing_functions):
    ctx = dash.callback_context
    plot_id = ctx.triggered_id['spec'][4:]  # everything after plot
    new_function = Analysis.function_layout(plot_id, n_clicks)
    existing_functions.append(new_function)
    return existing_functions

@app.callback(
    Output({'type': 'modal', 'spec': MATCH}, 'is_open'),
    Input({'type': 'info-btn', 'spec': MATCH}, 'n_clicks'),
    [State({'type': 'modal', 'spec': MATCH}, 'is_open')],
    prevent_initial_call=True
)
def toggle_modal_variable(open_clicks, is_open):
    return True

@app.callback(
    Output({'type': 'variables-section', 'spec': MATCH}, 'children'),
    Input({'type': 'add-variable-btn', 'spec': MATCH}, 'n_clicks'),
    State({'type': 'variables-section', 'spec': MATCH}, 'children'),
    prevent_initial_call=True
)
def add_variable(n_clicks, existing_variables):
    ctx = dash.callback_context
    spec = ctx.triggered_id['spec']
    match = re.search(r'plot(\d+)_func(\d+)', spec)
    plot_id = match.group(1)  # Extract the number after 'plot'
    function_id = match.group(2)  # Extract the number after 'func'
    new_variable = Analysis.variable_layout(tabs, plot_id, function_id, n_clicks)
    if existing_variables:
        existing_variables.append(new_variable)
    else:
        existing_variables = [new_variable]
    return existing_variables


@app.callback(
    Output({'type': 'signal-unit', 'spec': MATCH}, 'children'),
    Input({'type': 'dropdown-menu', 'spec': MATCH}, 'label'),
    prevent_initial_call=True
)
def update_variable_unit(signal):
    """Updates the unit for the selected signal"""
    ctx = dash.callback_context
    if not ctx.triggered:
        return "a.u."  # Default unit
    else:
        unit = None
        # Extract the selected signal
        for key in archive_signal_dict.archive_signal_dict.keys():
            if signal in key:
                unit = archive_signal_dict.archive_signal_dict[key]['unit']
                break
        if not unit:
            for key in derived_signal_dict.derived_signal_dict.keys():
                if signal in key:
                    unit = derived_signal_dict.derived_signal_dict[signal]['unit']
                    break
        if not unit:
            log.error(f"Signal not found: {signal}. Unit cannot be displayed.")
            return "a.u."

        # Extract the unit from the string (e.g., "P [MW]" -> "MW")
        unit = unit.split("[")[1].split("]")[0]
        return unit


from dash.dependencies import Input, Output, State, MATCH, ALL


@app.callback(
    [Output('fig-cache-store', 'data', allow_duplicate=True),
     Output('figure-plotted-store', 'data', allow_duplicate=True),
     Output('cache-size', 'children', allow_duplicate=True)],
    Input('plot-analysis', 'n_clicks'),
    State({'type': 'dropdown-menu', 'spec': ALL}, 'label'),
    State({'type': 'dropdown-menu', 'spec': ALL}, 'id'),
    [State('program-list-store', 'data'),
     State('selection-store', 'data'),
     State('output-settings-store', 'data'),
     State('icrh-settings-store', 'data')],
    prevent_initial_call=True
)
def generate_analysis_plot(n_clicks, signal_labels, id_list, program_list, selection_data, output_settings_data, icrh_settings_data):

    if not signal_labels or not id_list:
        raise PreventUpdate

    # Create a dictionary to hold signals for each plot
    signal_dict = {}

    # Iterate over all spec strings and corresponding labels to extract plot_id and group signals
    for label, id in zip(signal_labels, id_list):
        # Extract plot_id from the 'spec' string, which has the form 'plot{plot_id}_sig{signal_id}'
        plot_id = id['spec'].split('_')[0].replace('plot', '')

        # Initialize the plot entry if not already present
        if plot_id not in signal_dict:
            signal_dict[plot_id] = []

        # Append the signal label to the corresponding plot
        signal_dict[plot_id].append(label)

    # Prepare signals for the plotting function
    signals = {}
    for plot_id, labels in signal_dict.items():
        signals[plot_id] = [label for label in labels]  # Format signal labels for plotting

    # Call the function to generate the plot
    output_base64, figure_data, cache_info = generate_plot("Analysis", signals, program_list, selection_data, output_settings_data, icrh_settings_data)

    # Return the figure and figure data
    return output_base64, figure_data, cache_info


# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)
    # app.run_server(host='0.0.0.0', port=5000, debug=False)