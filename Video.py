# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 19:50:00 2023

@author: krim

Utility for downloading data from the Edicams and making a video.
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation, cm, colors
from matplotlib.animation import FFMpegWriter

import logging

log = logging.getLogger("Video")

def make_video(signal, timeType, chunk_counter=0, vmin=0, vmax=None, trel=0, scale="lin", title="", label="", cmap="Greys_r", axes=False):
    """
    Creates a video from a sequence of images.

    Parameters
    ----------
    val : ndarray
        Images that form the video. Must have dimensions (time, y, x).
    dim : ndarray
        Times of the images in seconds.
    vmin, vmax : scalar, optional
        Minimum and maximum colorscale limits. Defaults to values that cover
        exactly the full range of images.
    scale : 'lin' or 'log'
        Scale for the colormap.
    title : string, optional
        Title of the video. Defaults to no title.
    label : string, optional
        Label for the colorfoo. Defaults to no label.
    cmap : string or matplotlib Colormap, or 2-tuple of these, optional
        Colormap to use for the plot. If a 2-tuple of colormaps is passed, the
        first is used for the image and the second for the colorfoo. This is a
        hack to avoid colorfoo rendering bugs when the colormap includes
        transparency, in which case the colormap with transparency should be
        passed first and a fully opaque version should be passed second.
        Defaults to 'viridis'.
    axes : bool
        If True, axis labels and tick marks will be shown. Otherwise only a
        frame outlining the images will be shown. Defaults to True.
    show : boolean, optional
        If True the video will be shown on the screen. Defaults to False.


    Returns
    -------
    video_base64 : string
    Base64-encoded string of the generated video.
    """

    # Convert the dim (in ns) to s and if requested, set the dimension relative to the input value.
    if timeType == 'Rel':
        dim = [t/1e9 - trel / 1e9 for t in signal.dimensions]
        tlabel = "t - $t_1$ = "
    else:
        dim = signal.dimensions/1e9
        tlabel = "t = "

    # Determine the frames per second, from the intervals between the consecutive times
    intervals = [t2 - t1 for t1, t2 in zip(dim, dim[1:])]
    # Take the median interval instead of the mean to avoid the outliers
    interval = np.median(intervals)
    maxerror = (max(intervals-interval))/interval
    fps = int(1 / interval)
    log.info('animation with intervals of {} s.  Maximal error {}. Frames per second will be {} '.format(interval, maxerror, fps))

    # Rotate and flip the images to the desired display orientation
    rotations = 1  # Number of counter-clockwise 90 degree rotations to perform
    flip_horizontal = False
    flip_vertical = False

    val = np.rot90(signal.values, k=rotations, axes=(-2, -1))
    if flip_horizontal:
        val = val[..., :, ::-1]
    if flip_vertical:
        val = val[..., ::-1, :]

    # Set the colorscale limits if they weren't specified
    if vmin is None:
        vmin = np.nanmin(val)
    if vmax is None:
        vmax = np.nanmax(val)

    if scale == "lin":
        norm = colors.Normalize(vmin, vmax)
    elif scale == "log":
        norm = colors.LogNorm(vmin, vmax)
    else:
        raise ValueError(f"Unknown scale '{scale}'")

    # Create the figure for the animation
    fig, ax = plt.subplots(figsize=(6, 6 * val.shape[1] / val.shape[2]))
    ax.set_axis_off()

    # Create the animation frame set
    imageset = []
    for i, frame in enumerate(val):
        #     if dim[i] < 0 or dim[i] > 10:
        image = ax.imshow(frame, cmap=cmap, norm=norm, animated=True)
        time_label = ax.text(0.06, 0.06, f"{tlabel}{dim[i]:.2f} s", transform=ax.transAxes, ha="left", color="salmon")
        imageset.append([image, time_label])

    # Create the animation object
    ani = animation.ArtistAnimation(fig, imageset, blit=True, interval=interval * 1000, repeat=False)

    # Save each chunk as a separate video file
    chunk_filename = f'video_chunk_{chunk_counter}.mp4'
    print('chunk filename', chunk_filename)
    writer = FFMpegWriter(fps=fps, codec='libx264', bitrate=1800)
    ani.save(chunk_filename, writer=writer)

    # if show:
    #     log.info("Showing animation. Please close animation to proceed.")
    #     plt.draw()
    #     plt.show()

    # Close the figure to free memory
    plt.close(fig)

    print('make video done')
    return fig, chunk_filename, fps, True